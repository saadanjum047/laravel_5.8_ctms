<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Points extends Model
{
    protected $table = 'patients_points';
    protected $guarded = [];

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }
}
