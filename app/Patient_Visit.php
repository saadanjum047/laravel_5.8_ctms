<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Visit extends Model
{
    protected $table = 'patient_visits';
    protected $guarded = [];

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }

    public function schedule(){
        return  $this->belongsTo('App\Patient_Schedule' , 'schedule_id');
    }
}
