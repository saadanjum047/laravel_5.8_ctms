<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'tip_sponsor';
    protected $primaryKey = 'sponsor_id';
    protected $guarded = [];
    public $timestamps = false;


    public function sponsor_roles(){
        return $this->belongsTo('App\Sponsor_Role' );
    }
}
