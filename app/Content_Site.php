<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content_Site extends Model
{
    protected $table = 'tip_content_site';
    protected $guarded = [];
    protected $primaryKey = 'content_site_id';

    public $timestamps = false;


    public function site(){
        return $this->belongsTo('App\Site' , 'site_id');
    }


    public function revisions(){
        return $this->belongsTo('App\Revision' , 'revision_id');
    }
    
    public function content(){
        return $this->belongsTo('App\Content' , 'content_id');
    }

}
