<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group_Task extends Model
{
    protected $table = 'group_tasks';
    protected $guarded = [];


    public function group(){
        return $this->belongsTo('App\Group');
    }

    public function patients(){
        return $this->belongsToMany('App\Study_Patient' , 'group_task_patient' , 'task_id' , 'patient_id')->withPivot('status' , 'completed_at' , 'repeated');
    }

    public function schedule()
    {
        return $this->belongsTo('App\Study_Visit_Schedule', 'schedule_id');
    }
}
