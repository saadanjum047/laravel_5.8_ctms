<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    //
    protected $table = 'tip_revision';
    protected $guarded = [];
    protected $primaryKey = 'revision_id';

    public $timestamps = false;

    public function content(){
        return $this->belongsTo('App\Content' , 'content_id');
        
    }

    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function site(){
        return $this->hasOne('App\Content_Site' , 'revision_id');
    }

    public function languages(){
        return $this->hasMany('App\Translated_Revision' , 'revision_id');
    }
}
