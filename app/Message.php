<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];

    public function investigator(){
        return $this->belongsTo('App\Tip_User' , 'investigator_id');
    }

    public function sponsor(){
        return $this->belongsTo('App\Tip_User' , 'sponsor_id');
    }

    public function replies(){
        return $this->hasMany('App\Investigator_Message_Reply' , 'message_id');
    }
}
