<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Diary_Answer_Specification extends Model
{
    protected $table = 'study_diary_answer_specifications';
    protected $guarded = [];

    public function answer(){
        return $this->belongsTo('App\Study_Diary_Answers');
    }
}
