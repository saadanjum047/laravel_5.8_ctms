<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pre_Screening extends Model
{
    protected $table = 'pre_screening';
    protected $guarded = [];


    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function questions(){
        return $this->hasMany('App\Pre_Screening_Question' , 'screening_id');
    }

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }

}
