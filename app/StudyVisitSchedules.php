<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyVisitSchedules extends Model
{
    public $table = "study_visit_schedule";
    
    public $fillable = ['visit_name', 'target', 'window_low','window_high'];
}
