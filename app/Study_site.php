<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_site extends Model
{
    protected $table = 'tip_study_site';
    protected $primaryKey = 'study_site_id';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function site(){
        return $this->belongsTo('App\Site' , 'site_id' , 'site_id' );
    }
}
