<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Visit_Schedule extends Model
{
    protected $table = 'study_visit_schedule';
    protected $guarded  = [];

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }

    public function study(){
        return $this->belongsTo('App\Study');
    }

    
    public function task()
    {
        return $this->hasOne('App\Group_Task', 'schedule_id');
    }
    public function patient_groups(){
    return $this->belongsToMany('App\Group' , 'patient_group__study_visit_schedule' , 'schedule_id' ,'group_id'  );
    }
}
