<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor_Permission extends Model
{
    protected $table = 'sponsor_permissions';
    protected $guarded = []; 
    
    public function module(){
        return $this->beloogsTo('App\Modules' , 'module_id');
    }

    public function roles(){
        return $this->belongsToMany('App\Sponsor_Role');
    }
}
