<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translated_Revision extends Model
{
    protected $table = 'tranlated_revisions';
    protected $guarded = [];

    public function revision(){
        return $this->belongsTo('App\Revision' , 'revision_id');
    }
}
