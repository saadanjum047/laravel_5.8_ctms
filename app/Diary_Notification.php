<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diary_Notification extends Model
{
    protected $table = 'tip_diary_notification';
    protected $guarded = [];
    protected $primarykey = 'notification_id';
    public $timestamp = false;
}
