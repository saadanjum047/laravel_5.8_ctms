<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Images extends Model
{
    protected $table = 'patientsimages';
    protected $guarded = [];

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }
}
