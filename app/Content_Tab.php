<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content_Tab extends Model
{
    protected $table = 'tip_content_tab';
    protected $primarykey = 'tab_id';
    protected $guarded = [];
    
    public $timestamp = false;



    public function content(){
        return $this->hasMany('App\Content' , 'content_tab_id');
    }


}
