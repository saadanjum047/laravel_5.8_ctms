<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteUser extends Model
{
    protected $table = 'tip_site_users';
    protected $guarded = [];
    protected $primaryKey = 'site_user_id';
    public $timestamps = false;
    

    public function site(){
        return $this->belongsTo('App\Site' , 'site_id');
    }

    
    public function user(){
        return $this->belongsTo('App\User' , 'user_id');
    }
}
