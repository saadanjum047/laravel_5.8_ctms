<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diary_Schedule extends Model
{
    protected $table = 'diary_schedule';
    protected $guarded = [];

    public function diary(){
        return $this->belongsTo('App\Study_Diary' , 'diary_id');
    }

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }
}
