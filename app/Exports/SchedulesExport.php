<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 6/26/2018
 * Time: 4:14 PM
 */


namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SchedulesExport implements FromView, WithHeadings
{
    use Exportable;

    private $data;
    private $headings;

    public function __construct($data = [], $headings = [])
    {
        $this->data = $data;
        $this->headings = $headings;
    }

    public function view(): view {
        // return view('patient.schedules_export', [
        //     'schedules' => $this->data
        // ]);

        return view('exports.schedules', [
            'schedules' => $this->data
        ]);
    }

    public function headings(): array
    {
        return $this->headings;
    }

}