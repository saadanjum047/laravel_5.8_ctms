<?php
/*
 * Created by Asif.
 * Date: 12/26/2018
 * 
 */


namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DiaryExport implements FromView, WithHeadings
{
    use Exportable;

    private $data;
    private $headings;

    public function __construct($data = [], $headings = [])
    {
        $this->data = $data;
        $this->headings = $headings;
    }

    public function view(): view {
        return view('exports.patients', [
            'data' => $this->data
        ]);
    }

    public function headings(): array
    {
        return $this->headings;
    }

}