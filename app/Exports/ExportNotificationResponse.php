<?php
/**
 * Created by Asif.
 * User: Admin
 * Date: 16/09/2019
 */


namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportNotificationResponse implements FromView, WithHeadings
{
    use Exportable;

    private $data;
    private $headings;

    public function __construct($data = [], $headings = [])
    {
        $this->data = $data;
        $this->headings = $headings;
    }

    public function view(): view {
        return view('notifications.notification_response', [
            'data' => $this->data
        ]);
    }

    public function headings(): array
    {
        return $this->headings;
    }

}