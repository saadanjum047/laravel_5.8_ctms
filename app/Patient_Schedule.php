<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Schedule extends Model
{
    protected $table = 'tip_patient_schedule';
    protected $guarded = [];

    public function study(){
        return $this->belongsTo('App\Study'  );
    }

    public function user(){
        return $this->belongsTo('App\Study_Patient', 'patient_id');
    }

    public function patientVisits(){
        return $this->hasMany('App\Patient_Visit' , 'schedule_id');
    }

    public function change_request(){
        return $this->hasOne('App\Patient_Schedule_Change' , 'visit_id');
    }
}
