<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Patient extends Model
{

    protected $table = 'tip_study_patient';
    protected $guarded = [];
    protected $dates = ['created_at' , 'updated_at' , 'confirmation_date'];


    public function study(){
        return $this->belongsTo('App\Study' , 	'study_id');
    }

    public function site(){
        return $this->belongsTo('App\Site' , 'site_id'  );
    }

    public function user(){
        return $this->belongsTo('App\User' , 'patient_id' );
    }


    public function tasks(){
        return $this->belongsToMany('App\Group_Task' , 'group_task_patient' , 'patient_id' , 'task_id')->withPivot('status' , 'completed_at' , 'repeated');
    }
    

    public function pre_screenings()
    {
        return $this->hasMany('App\Pre_Screening', 'patient_id');
    }

    public function patient_schedule(){
        return $this->hasMany('App\Patient_Schedule' , 'patient_id');
    }

    public function messages(){
        return $this->hasMany('App\Patient_Message' , 'patient_id' );
    }

    public function images(){
        return $this->hasMany('App\Patient_Images'  , 'patient_id');
    }   

    public function points(){
        return $this->hasOne('App\Patient_Points' , 'patient_id');
    }
    // public function activity(){
    //     return $this->hasMany('App\Study_Audit' , 'patient_id');
    // }

    public function visit_history(){
        return $this->hasMany('App\Patient_Visit_History' , 'patient_id' );
    }

    public function activity(){
        return $this->hasMany('App\Patient_Visit' , 'patient_id');
    }

    public function diary_schedule(){
        return $this->hasMany('App\Diary_Schedule' , 'patient_id');
    }

    public function consent(){
        return $this->hasOne('App\Patient_Consent' , 'patient_id');
    }

}
