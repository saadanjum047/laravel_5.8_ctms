<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Visit_History extends Model
{
    protected $table = 'patient_visits_history';

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id' );
    }

    public function creator(){
        return $this->belongsTo('App\Tip_User' , 'created_by');
    }
}
