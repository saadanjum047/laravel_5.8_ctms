<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Message extends Model
{
    protected $table = 'patient_messages';
    protected $guarded = [];

    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function investigator(){
        return $this->belongsTo('App\Tip_User' , 'investigator_id');
    }

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }

    public function replies(){
        return $this->hasMany('App\Patient_Message_Reply' , 'message_id');
    }
}
