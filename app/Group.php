<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $guarded = [];

    public function tasks(){
        return $this->hasMany('App\Group_Task');
    }

    public function schedule(){
        return $this->belongsTo('App\Study_Visit_Schedule', 'schedule_id');
    }
    
    public function schedules(){
        return $this->belongsToMany('App\Study_Visit_Schedule' , 'patient_group__study_visit_schedule' , 'group_id' , 'schedule_id'  );
    }

}
