<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Diary_Question extends Model
{
    protected $table = 'tip_study_diary_questions';
    protected $guarded = [];
    // protected $casts = [
    //     'answers' => 'array',
    // ];
    public $timestamps = false;

    public function diary(){
        return $this->belongsTo('App\Study_Diary'  , 'diary_id');
    }

    public function specifications(){
        return $this->hasMany('App\Study_Question_Specification' , 'question_id' );
    }


    public function ans() {
        return $this->hasMany('App\Study_Diary_Answers' , 'question_id' );
    }

    public function versions(){
        return $this->hasMany('App\Translated_Questions' , 'question_id' );
    }
}
