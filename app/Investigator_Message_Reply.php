<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investigator_Message_Reply extends Model
{
    protected $table = 'investigator_message_replies';
    protected $guarded = [];

    public function message(){
        return $this->belongsTo('App\Message');
    }
    public function sponsor(){
        return $this->belongsTo('App\Tip_User' , 'sponsor_id');
    }
}
