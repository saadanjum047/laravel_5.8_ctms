<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Diary extends Model
{
    protected $table = 'tip_study_diary';
    protected $guarded = [];


    public function study(){
        return $this->belongsTo('App\Study' , 'study_id' );
    }

    public function questions(){
        return $this->hasMany('App\Study_Diary_Question' , 'diary_id');
    }
    public function diary_schedules(){
        return $this->hasMany('App\Diary_Schedule' , 'diary_id');
    }

    public function ans(){
        return $this->hasMany('App\Study_Diary_Answers' , 'diary_id');
    }
}
