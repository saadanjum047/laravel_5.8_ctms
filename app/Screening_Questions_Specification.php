<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screening_Questions_Specification extends Model
{
    protected $table = 'screening_questions_specifications';
    protected $guarded = [];

    public function question(){
        return $this->belongsTo('App\Pre_Screening_Question' , 'question_id');
    }
}
