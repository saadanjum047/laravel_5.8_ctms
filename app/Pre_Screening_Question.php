<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pre_Screening_Question extends Model
{
    protected $table = 'pre_screening_questions';
    protected $guarded = [];


    public function pre_screen(){
        return $this->belongsTo('App\Pre_Screening' , 'screening_id');
    }

    public function ans()
    {
        return $this->hasMany('App\Pre_Screening_Answer', 'question_id');
    }

    public function sponsor(){
        return $this->belongsTo('App\Study_Patient' , 'user_id');
    }

    public function specifications(){
        return $this->hasMany('App\Screening_Questions_Specification' , 'question_id');
    }

}
