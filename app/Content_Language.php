<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content_Language extends Model
{
    protected $table = 'tip_content_languages';
    protected $guarded = [];
}
