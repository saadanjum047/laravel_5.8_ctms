<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pre_Screening_Answer extends Model
{
    protected $table = 'pre_screening_answers';
    protected $guarded = [];

    public function questions(){
        return $this->belongsTo('App\Pre_Screening_Question' , 'question_id');
    }

    public function investigator(){
        return $this->belongsTo('App\Tip_User' , 'investigator_id');
    }

    // public function specifications(){
    //     return $this->hasMany('App\Screening_Answers_Specification' , 'answer_id');
    // }
}
