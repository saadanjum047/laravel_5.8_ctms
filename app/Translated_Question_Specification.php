<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translated_Question_Specification extends Model
{
    protected $table = 'translated_question_specifications';
    protected $guarded = [];

    public function question(){
        return $this->belongsTo('App\Translated_Questions' , 'question_id');
    }
}
