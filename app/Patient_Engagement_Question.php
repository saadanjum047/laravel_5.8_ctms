<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Engagement_Question extends Model
{
    protected $table = 'patient_engagements';
    protected $guarded = [];

    public function ans(){
        return $this->hasMany('App\Patient_Engagement_Answers' , 'question_id');
    }

    public function study(){
        return $this->belongsTo('App\Study');
    }
}
