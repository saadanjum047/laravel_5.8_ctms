<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'tip_site';

    protected $guarded = ['id'];	
    protected $primaryKey = 'site_id';
    public $timestamps = false;

    public function site(){
        return $this->hasOne('App\Study_site' , 'site_id' , 'site_id'  );
    }

    public function site_user(){
        return $this->hasOne('App\SiteUser' , 'site_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function patients(){
        return $this->hasMany('App\Study_Patient' , 'site_id' );
    }
}
