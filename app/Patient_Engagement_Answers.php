<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Engagement_Answers extends Model
{
    protected $table = 'patient_engagements_answers';
    protected $guarded = [];

    public function question(){
        return $this->belongsTo('App\Patient_Engagement_Question' , 'question_id' );
    }

    public function patient(){
        return $this->belongsTo('App\Study_Patient' , 'patient_id');
    }
}
