<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $guarded = [];

    public function users(){
        return $this->belongsTo('App\User' , 'user_id');
    }

    public function answers(){
        return $this->belongsTo('App\Study_Diary_Answers' , 'user_id');
    }
}
