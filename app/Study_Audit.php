<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Audit extends Model
{
    protected $table = 'tip_study_audit';
    protected $guarded = [];

    public function patient(){
        return $this->belongsTo('App\Study' , 'study_id');
    }

}
