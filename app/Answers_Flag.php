<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers_Flag extends Model
{
    protected $table = 'answers_flags';
    protected $guarded  = [];

    public function answer(){
        return $this->belongsTo('App\Study_Diary_Answers' , 'diary_answer_id');
    }
   
}
