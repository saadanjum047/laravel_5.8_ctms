<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_User extends Model
{
    protected $table = 'tip_study_users';
    protected $guarded = [];
    protected $primaryKey = 'user_study_id';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function role_detail(){
        return $this->belongsTo('App\Sponsor_Role' , 'role');
    }
}
