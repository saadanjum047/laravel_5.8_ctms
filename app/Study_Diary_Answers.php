<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Diary_Answers extends Model
{
    protected $table = 'tip_study_diary_answers';
    protected $guarded = [];

    public function question(){
        return $this->belongsTo('App\Study_Diary_Question' , 'question_id');
    }
    public function diary(){
        return $this->belongsTo('App\Study_Diary' , 'diary_id');
    }
    public function study(){
        return $this->belongsTo('App\Study' , 'study_id');
    }
    public function patient(){
        return $this->belongsTo('App\User' , 'patient_id');
    }

    public function specifications(){
        return $this->hasMany('App\Study_Diary_Answer_Specification', 'answer_id');
    }

    public function comment(){
        return $this->hasMany('App\Coordinator_Comment' , 'diary_answer_id');
    }

    public function flag(){
        return $this->hasOne('App\Answers_Flag' , 'diary_answer_id');
    }

    public function votes(){
        return $this->hasMany('App\Vote' , 'answer_id');
    }
}
