<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    public function sponsor(){
        return $this->belongsTo('App\Tip_User' , 'sponsor_id');
    }

    public function users(){
        return $this->belongsToMany('App\User' , 'tags_users' , 'tag_id' , 'user_id');
    }

    public function newsletters(){
        return $this->belongsToMany('App\Newsletter' , 'newsletters_tags' , 'newsletter_id' , 'tag_id');
    }
}
