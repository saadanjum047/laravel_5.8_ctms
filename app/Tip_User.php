<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tip_User extends Model
{
    protected $table = 'tip_users';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User' );
    }

    public function study(){
        return $this->belongsTo('App\Study' , 'study_id');
    }

    public function site(){
        return $this->belongsTo('App\Site' ,'site_id' );
    }

    // public function newsletters(){
    //     return $this->belongsToMany('App\Newsletter')->withPivot('status' , 'read_at');
    // }

    public function tasks(){
        return $this->belongsToMany('App\Site_Task' , 'site_task_investigator' , 'investigator_id' , 'task_id')->withPivot('status' , 'complete_at');
    }

    public function contents(){
        return $this->hasMany('App\Content' , 'created_by');
    }

    public function messages(){
        return $this->hasMany('App\Message' , 'investigator_id' );
    }

    public function patient_visit_history(){
        return $this->hasMany('App\Patient_Visit_History' , 'created_by');
    }

}
