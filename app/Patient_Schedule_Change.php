<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Schedule_Change extends Model
{
    protected $table = 'patient_schedule_change_requests';

    protected $guarded = [];

    public function schedule(){
        return $this->belongsTo('App\Patient_Schedule' , 'visit_id');
    }

}
