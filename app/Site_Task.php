<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site_Task extends Model
{
    protected $table = 'site_tasks';

    protected $guarded = [];


    public function investigators(){
        return $this->belongsToMany('App\Tip_User'  , 'site_task_investigator' , 'task_id' , 'investigator_id' )->withPivot('status' , 'complete_at');
    }

    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function group(){
        return $this->belongsTo('App\Investigator_Task_Group' , 'group_id');
    }

}
