<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor_Role extends Model
{
    protected $guarded = [];



    public function orgs(){
        return $this->hasOne('App\Organization' , 'sponsor_id' , 'organization_id' );
    }

    
 public function permissions(){
        return $this->belongsToMany('App\Sponsor_Permission');
    }

    public function study_user(){
        return $this->hasMany('App\Study_User' , 'role_id');
    }
}
