<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screening_Answers_Specification extends Model
{
    protected $table = 'pre_screening_answer_specifications';

    protected $guarded = [];

    public function ans(){
        return $this->belongsTo('App\Pre_Screening_Answer' , 'answer_id');
    }
}
