<?php

namespace App;

use App\Patient_Consent;
use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $table = 'tip_study';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function creator(){
        return $this->belongsTo('App\User' , 'created_by');
    }

    public function sites(){
        return $this->hasMany('App\Study_site');
    }

    public function diary(){
        return $this->hasMany('App\Study_Diary');
    }

    public function patients(){
        return $this->hasMany('App\Study_Patient');
    }

    public function schedules(){
        return $this->hasMany('App\Study_Visit_Schedule');
    }

    public function newsletters(){
        return $this->hasMany('App\Newsletter');
    }

    public function tip_users(){
        return $this->hasMany('App\Tip_User' );
    }

    public function medicines(){
        return $this->hasMany('App\Medicine' );
    }

    public function tasks(){
        return $this->hasMany('App\Site_Task');
    }


    public function groups(){
        return $this->hasMany('App\Group');
    }

    public function contents(){
        return $this->hasMany('App\Content');
    }

    public function consent()
    {
        return $this->hasMany('App\Content')->where('content_type' , 'consent');
    }

    public function patient_consents(){
        return $this->hasMany('App\Patient_Consent' , 'study_id');
    }

    public function pre_screens(){
        return $this->hasMany('App\Pre_Screening' , 'study_id');
    }

    public function engagement_questions(){
        return $this->hasMany('App\Patient_Engagement_Question' , 'study_id');
    }

   
}
