<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    protected $table = 'tip_workflow';
    protected $primaryKey = 'workflow_id';
    protected $guarded = [];


    public function study(){
        return $this->belongsTo('App\Study' , 'study_id ');
    }

    public function parent(){
        return $this->belongsTo('App\Workflow' , 'parent_id');
    }

    public function children(){
        return $this->belongsToMany(Workflow::class , 'tip_workflow_relations' , 'workflow_id' ,  'parent_id');
    }
}
