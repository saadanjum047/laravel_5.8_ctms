<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translated_Questions extends Model
{
    protected $table = 'translated_questions';
    protected $guarded = [];

    public function base_question(){
        return $this->belongsTo('App\Study_Diary_Question' , 'question_id');
    }

    public function specifications(){
        return $this->hasMany('App\Translated_Question_Specification' , 'question_id');
    }
}
