<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'tip_content';
    protected $primaryKey = 'content_id';
    protected $guarded = [];
    public $timestamps = false;

    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function content_tab(){
        return $this->belongsTo('App\Content_Tab' , 'content_tab_id' , 'tab_id');
    }

    public function revisions(){
        return $this->hasMany('App\Revision' , 'content_id');
    }

    public function site(){
        return $this->hasOne('App\Content_Site' , 'content_id');
    }
}
