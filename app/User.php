<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Routing\Route;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use Notifiable;
    use LogsActivity;

    protected static $logAttributes = ['name', 'action performed'];


	// protected $table="users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id' , 'created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function activityRecord(){
        return $this->hasMany( 'Spatie\Activitylog\Models\Activity' , 'causer_id' );
    }

    
    public function site(){
        return $this->hasOne('App\Site');
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function notifications(){
        return $this->hasMany('App\Notification');
    }

    public function patient(){
        return $this->hasOne('App\Study_Patient' , 'patient_id' );
    }

    public function sponsor_user(){
        return $this->hasOne('App\Tip_User');
    }

    public function study_user(){
        return $this->hasMany('App\Study_User' , 'user_id');
    }

    // public function patient_schedule(){
    //     return $this->hasMany('App\Patient_Schedule' , 'patient_id');
    // }

    public function votes(){
        return $this->hasMany('App\Vote' , 'user_id' );
    }

    public function site_user(){
        return $this->hasMany('App\SiteUser' , 'user_id');
    }

    
    public function tags(){
        return $this->belongsToMany('App\Tag' , 'tags_users' , 'user_id' , 'tag_id');
    }

    public function newsletters(){
        return $this->belongsToMany('App\Newsletter' , 'newsletter_user', 'user_id' , 'newsletter_id' )->withPivot('status' , 'read_at') ;
    }


    
}

