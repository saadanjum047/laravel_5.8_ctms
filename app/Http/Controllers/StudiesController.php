<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;


class StudiesController extends SiteController
{
    public function __construct()
    {
        $this->template = 'study.frame';
    }


public function ViewStudies(){
$data=['user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];
$response = $this->apiRequest('GetSponserStudies',$data);
//dd($response);


return view('studies.index',compact('response'));

}


public function CreateStudy(){
$data=[
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];
$response = $this->apiRequest('CreateSonserStudy',$data);
//dd($response);


return view('studies.create',compact('response'));

}


public function AddStudy(Request $request){
if($request->hasfile('background_img'))
         {

            foreach($request->file('background_img') as $file)
            {
               		$fileName = $file->getClientOriginalName() ;
			$destinationPath ='studies';
			$path_image =  $file->move($destinationPath, $file->getClientOriginalName());  
           }
         }

       $document_path='organization/'.$fileName;
	//dd($document_path);

	$data=[
		'study_id'=>$request->study_id,
		'data'=>$request->except('_token'),
		'background_img'=>$document_path,
		'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name')
		];		
//dd($data);
$response = $this->apiRequest('AddStudy',$data);
//dd($response);
if($response->status="Success"){
return back()->with('success',$response->Message);
}
else{
return back()->with('error',$response->Message);
}


}



public function EditStudy($study_id){
$data=['study_id'=>$study_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];
$response = $this->apiRequest('EditSponserStudy',$data);
//dd($response);


return view('studies.edit',compact('response'));

}


public function UpdateStudy(Request $request){
if($request->hasfile('background_img')!='')
         {

            foreach($request->file('background_img') as $file)
            {
               		$fileName = $file->getClientOriginalName() ;
			$destinationPath ='studies';
			$path_image =  $file->move($destinationPath, $file->getClientOriginalName());  
           }
 		$document_path='organization/'.$fileName;
         }
	else{$document_path='';}
      
	//dd($document_path);

	$data=[
		'study_id'=>$request->study_id,
		'data'=>$request->except('_token'),
		'background_img'=>$document_path,
		'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name')
		];		
//dd($data);
$response = $this->apiRequest('UpdateStudy',$data);
//dd($response);
if($response->status="Success"){
return back()->with('success',$response->Message);
}
else{
return back()->with('error',$response->Message);
}

}




}