<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Study;
use App\Tip_User;
use Carbon\Carbon;
use App\Newsletter;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    public function index($id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            dd('yes');
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 14 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }

        }


        $study = Study::findOrFail($id);
        $newsletters = Newsletter::where('study_id' , $id)->get();
        return view('sponsor_newsletter.index' , compact('newsletters' ,'study'));
    }


    public function show($study , $newsletter){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            dd('yes');
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 14 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }

        }


        $study = Study::findOrFail($study);
        $newsletter = Newsletter::findOrFail( $newsletter);

        // dd($newsletter->users);

        return view('sponsor_newsletter.show' , compact('study' , 'newsletter'));

    }

    public function showDetails($study_id , $news_id){
        $study = Study::findOrFail($study_id);
        $newsletter = Newsletter::findOrFail( $news_id);
        return view('sponsor_newsletter.details' , compact('study' , 'newsletter'));
    }


    public function create($id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            dd('yes');
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $id)->first()->role_detail->permissions->where('name' , 'create')->where('module_id' , 14 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }

        }

        $study = Study::findOrFail($id);
        // $investigators = Tip_User::where('study_id' , $study->id)->get();
        $tags = Tag::where('sponsor_id' , auth()->user()->sponsor_user->id )->get();
        return view('sponsor_newsletter.create' , compact('study' , 'tags'));

    }

    public function store( Request $request , $id){

        $request->validate([
            'title' => 'required',
            'description' => 'sometimes',
        ]);

        $study = Study::findOrFail($id);
        // $ids = $study->tip_users->pluck('id')->toArray();
        // dd($study->tip_users , $ids);
       

        $news = Newsletter::create([
            'title' => $request->title,
            'body' => $request->description,
            'study_id' => $study->id,
            'created_by' => auth()->user()->id,
        ]);

        $all_users = array();
        if(isset($request->tags)){
        foreach($request->tags as $k => $tag ){
            $curr_tag = Tag::findOrFail($tag);
            $users = $curr_tag->users->pluck('id')->toArray();
            $all_users = array_merge($all_users, $users);
            $all_users = array_unique($all_users); 

        }}
        $news->tags()->sync($request->tags);
        $news->users()->sync( $all_users );

        if(isset($all_users)){
            foreach($all_users as $id){
                Notification::create([
                    'user_id' => $id,
                    'role' => auth()->user()->role_id,
                    'text' => "A new Newsletter has been created",
                ]);
            }
        }

        if($news){
            return redirect("/dashboard/study/$study->id/newsletter")->with('Sucess' , 'Newsletter created successfuully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    public function edit($study , $id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            dd('yes');
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study)->first()->role_detail->permissions->where('name' , 'update')->where('module_id' , 14 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }

        }


        $study = Study::findOrFail($study);
        $newsletter = Newsletter::findOrFail($id);
        // $investigators = Tip_User::where('study_id' , $study->id)->get();
        $tags = Tag::where('sponsor_id' , auth()->user()->sponsor_user->id )->get();

        return view('sponsor_newsletter.update' , compact('study' , 'newsletter' , 'tags'));

    }

    public function update(Request $request , $study_id , $newsletter ){
        
        $request->validate([
            'title' => 'required',
            'description' => 'sometimes',
        ]);
        $newsletter = Newsletter::findOrFail($newsletter);
        
        $newsletter->title = $request->title;
        $newsletter->body = $request->description;
        $newsletter->save();

        $all_users = array();
        if($request->tags){
        foreach($request->tags as $k => $tag ){
            $curr_tag = Tag::findOrFail($tag);
            $users = $curr_tag->users->pluck('id')->toArray();
            $all_users = array_merge($all_users, $users);
            $all_users = array_unique($all_users); 
        }}  

        $newsletter->tags()->sync($request->tags);
        $newsletter->users()->sync( $all_users );


        

        if($newsletter->save()){
            return redirect("/dashboard/study/$study_id/newsletter")->with('Sucess' , 'Newsletter updated successfuully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
        
    }

    public function destroy($study , $id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study)->first()->role_detail->permissions->where('name' , 'update')->where('module_id' , 14 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }
        
        $newsletter = Newsletter::findOrFail($id);

        $newsletter->tags()->sync([]);
        $newsletter->users()->sync([]);

        if(auth()->user()->role_id == 2 && $newsletter->created_by != auth()->user()->id){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }

        if($newsletter->delete()){
            return redirect("/dashboard/study/$study/newsletter")->with('Sucess' , 'Newsletter deleted successfuully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured while deleting');
        }
    }



    public function view($study_id){

        $study = Study::findOrFail($study_id);
        // $newsletters = Newsletter::where('study_id' , $study_id)->get();
        $newsletters = auth()->user()->newsletters->where('study_id' , $study_id);
        return view('investigator_newsletter.index' , compact('study' , 'newsletters'));
    }


    public function viewSingle($study_id , $news_id){

        $study = Study::findOrFail($study_id);
        $newsletter = Newsletter::findOrFail($news_id);
        $curr_news = auth()->user()->newsletters->find($newsletter->id);
        // dd($curr_news);
        if($curr_news->pivot->status == 0){
        $curr_news->pivot->status = 1;
        $curr_news->pivot->read_at = Carbon::now();
        $curr_news->pivot->save();
        }
        // dd( auth()->user()->sponsor_user->id , auth()->user()->sponsor_user->newsletters->find($newsletter->id));

        return view('investigator_newsletter.show' , compact('newsletter' , 'study'));
    }


}
