<?php

namespace App\Http\Controllers;

use App\Study;
use App\Pre_Screening;
use App\Study_Patient;
use Illuminate\Http\Request;
use App\Pre_Screening_Question;

class PreScreeningController extends Controller
{
    public function index($study_id ){

        $screens = Pre_Screening::where('study_id' , $study_id)->get();
        $study = Study::findOrFail($study_id);
        $patients = Study_Patient::where('study_id' , $study_id)->get();
        return view('sponsor_pre_screening.index' ,  compact('screens' , 'study', 'patients'));
    }
    

    public function store($study_id , Request $request){

        $request->validate([
            'screening_name' => 'required',
        ]);


        $pre_screen = Pre_Screening::create([
            'study_id' => $study_id,
            'user_id' => auth()->user()->sponsor_user->id,
            'name' => $request->screening_name,
        ]);

        if($pre_screen){
            return redirect()->back()->with('success' , 'Pre Screening Added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem while adding Pre Screening');
        }
    }

    public function update($study_id , Request $request){

        
        $request->validate([
            'screen_id' => 'required',
            'screen_name' => 'required',
        ]);

        $pre_screen = Pre_Screening::findOrFail($request->screen_id);

        $pre_screen->name = $request->screen_name;
        $pre_screen->save();

        if($pre_screen->save()){
            return redirect()->back()->with('success' , 'Pre Screening updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem while updating Pre Screening');
        }


    }  

    public function destroy( $study_id ,  $screen_id){
        $pre_screen = Pre_Screening::findOrFail($screen_id);


        foreach($pre_screen->questions as $question){
            foreach($question->ans as $ans){
                $ans->delete();
            }
            $question->delete();
        }

        if($pre_screen->delete()){
            return redirect()->back()->with('success' , 'Pre Screening deleted successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem while deleting Pre Screening');
        }
    }


    // For investigators
    public function showScreens($study_id){
        $screens = Pre_Screening::where('study_id' , $study_id)->get();
        $study = Study::findOrFail($study_id);
        return view('investigator_pre_screening.index' ,  compact('screens' , 'study'));
    }
    
   

    
}
