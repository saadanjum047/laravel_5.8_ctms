<?php

namespace App\Http\Controllers;

use DB;
use App\Vote;
use App\Answers_Flag;
use App\Coordinator_Comment;
use App\Study_Diary_Answers;
use Illuminate\Http\Request;
use App\Study_Diary_Question;
use App\Http\Controllers\Controller;

class CoordinatorCommentController extends Controller
{
    public function store( $diary_id , $question_id , $answer_id  , Request $request){

        $question = Study_Diary_Question::findOrFail($question_id);
        $answer = Study_Diary_Answers::findOrFail($answer_id);

        // if($request->flag == 0){
        // if(isset($question->answer->flag)){
        //     $question->answer->flag->delete();
        // }
        // }
        // if($request->flag == 1){
        //     if(!isset($question->answer->flag)){
        //     $flag  = Answers_Flag::create([
        //         'user_id' => auth()->user()->id,
        //         'diary_answer_id' => $question->answer->id,
        //     ]);
        // }
        // }
        $comment = Coordinator_Comment::create([
            'user_id' => auth()->user()->id,
            'comment' => $request->comment,
            'diary_answer_id' => $answer->id,
        ]);
        
        return redirect()->back()->with('success' , 'Comment added successfully');
        // if(@$comment || @$flag)
        // return redirect("/dashboard/study/".$question->diary->study->id."/diary/".$question->diary->id."/questions")->with('success' , 'Comment Added Sucessfully');
        // else
        // return redirect()->back()->with('error' , 'Problem while entring data');
    }

    public function update( $diary_id , $question_id , $answer_id , Request $request){
        $comment = Coordinator_Comment::find($answer_id);
        $question = Study_Diary_Question::findOrFail($question_id);

        $answer = Study_Diary_Answers::findOrFail($answer_id);

        $comment->comment = $request->comment;
        $comment->save();

        if($comment->save())
        return redirect("/dashboard/study/".$question->diary->study->id."/diary/".$question->diary->id."/questions")->with('success' , 'Comment Added Sucessfully');
        else
        return redirect()->back()->with('error' , 'Problem while entring data');

    }

    public function destroy($diary_id , $question_id , $answer_id ){

        $question = Study_Diary_Question::findOrFail($question_id);

        if(Coordinator_Comment::find($answer_id)->delete())
        return redirect()->back()->with('success' , 'Comment Added Sucessfully');
        else
        return redirect()->back()->with('error' , 'Problem while entring data');
        
    }

    public function flag($answer_id){

        $answer = Study_Diary_Answers::find($answer_id);
        
            if(isset($answer->flag)){
                $answer->flag->delete();
                return "Answer is Not flagged";
            }elseif(!isset($answer->flag)){
                    $flag  = Answers_Flag::create([
                    'user_id' => auth()->user()->id,
                    'diary_answer_id' => $answer_id,
                ]);
                return "Answer is flagged";
        }
            
    }

    public function voteUp($diary_id , $question_id , $answer_id ){
        
        
        $answer = Study_Diary_Answers::find($answer_id);


      
        if(!is_null(Vote::where([
            ['user_id' , auth()->user()->id],
            ['answer_id' , $answer_id]
        ])->first())){
            return "Already Voted";
        }else{
            $vote = Vote::create([
                'user_id' => auth()->user()->id,
                'answer_id' => $answer->id,
            ]);
            return Vote::where('answer_id' , $answer_id)->get()->count();
        }
     
    }

    public function voteDown($diary_id , $question_id , $answer_id){

        $answer = Study_Diary_Answers::find($answer_id);

        if($vote = Vote::where([
            ['user_id' , auth()->user()->id],
            ['answer_id' , $answer_id]
        ])->first()){
            $vote->delete();
            return Vote::where('answer_id' , $answer_id)->get()->count();
        }else{
            return "You have not voted";
        }

    }


}
