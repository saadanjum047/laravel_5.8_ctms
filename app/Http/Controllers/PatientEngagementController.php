<?php

namespace App\Http\Controllers;

use App\Study;
use App\Study_Patient;
use App\Patient_Points;
use Illuminate\Http\Request;
use App\Patient_Engagement_Answers;
use App\Patient_Engagement_Question;

class PatientEngagementController extends Controller
{
    public function index($study_id){
        $questions = Patient_Engagement_Question::where('study_id' , $study_id)->get();
        $study = Study::findOrFail($study_id);
        return view('sponsor_patient_engagement.index' , compact('questions', 'study') );
    }

    public function create($study_id){

        $study = Study::findOrFail($study_id);
        
        return view('sponsor_patient_engagement.new_create' , compact('study') );

    }

    public function store($study_id , Request $request){
        // dd($request->all());
      
        
        $request->validate([
            "question" => 'required',
            'choices' => 'sometimes',
            'correct_answer' => 'sometimes',
            'attachment' =>'sometimes',
        ]);

        if ($request->hasFile('attach')) {
            $extension = ".".$request->attach->getClientOriginalExtension();
            $attach = basename($request->attach->getClientOriginalName(), $extension).time();
            $fileName = $attach.$extension;
            $path = public_path().'/engagement_attach';
            $uplaod = $request->attach->move($path , $fileName);
            // dd('fileName');
            }
        
        // dd($request->all() );
        
        $question = Patient_Engagement_Question::create([
            'study_id' => $study_id,
            'question' => $request->question,
            'answers' => isset($request->choices) ? json_encode($request->choices) : NULL,
            'created_by' => auth()->user()->id,
            'attachment' => $fileName ?? NULL,
            'correct_answer' => $request->correct_answer,
        ]);
        

            
        if($question){
            return redirect("/dashboard/study/".$study_id."/patient_engagements")->with('success' , 'Question added successfully');
        }else{
            return redirect()->back()->with('error' , 'Sorry Some problem occourd');
        }
        }

    public function edit($study_id , $question_id){
        $question = Patient_Engagement_Question::findOrFail($question_id);
        $study = Study::findOrFail($study_id);
        return view('sponsor_patient_engagement.new_create' , compact('study' , 'question') );

    }


    public function update($study_id , $question_id , Request $request){

        $request->validate([
            "question" => 'required',
            'choices' => 'sometimes',
            'correct_answer' => 'sometimes',
            'attachment' =>'sometimes',
        ]);

        if ($request->hasFile('attach')) {
            $extension = ".".$request->attach->getClientOriginalExtension();
            $attach = basename($request->attach->getClientOriginalName(), $extension).time();
            $fileName = $attach.$extension;
            $path = public_path().'/engagement_attach';
            $uplaod = $request->attach->move($path , $fileName);
            }

            $question = Patient_Engagement_Question::findOrFail($question_id);

            $question->question = $request->question;
            $question->correct_answer = $request->correct_answer ?? $question->correct_answer;
            $question->answers = isset($request->choices) ? json_encode($request->choices) : NULL;
            $question->attachment = $fileName ?? $question->attachment ;
            
            if($question->save())
            return redirect("/dashboard/study/".$study_id."/patient_engagements")->with('success' , 'Question updated successfully');
        else
            return redirect()->back()->with('error' , 'Sorry Some problem occourd');

    }

    public function remove( $study_id , $question_id){
        $question = Patient_Engagement_Question::findOrFail($question_id);
        $question->delete();
        return redirect()->back()->with('success' , 'Question has been removed');
    }

    public function details($study_id , $question_id){
        $patients = Study_Patient::where('study_id' , $study_id )->get();
        $question = Patient_Engagement_Question::findOrFail($question_id);

        $study = Study::findOrFail($study_id);
        return view('sponsor_patient_engagement.details' , compact('study' , 'question' ,'patients') );

    }

    public function viewQuestion(){
        $questions = Patient_Engagement_Question::where( 'study_id' , auth()->user()->patient->study->id )->get();
        return view('patient_engagement.index' , compact('questions' ) );

        
    }

    public function showQuestion($question_id){
        $question = Patient_Engagement_Question::findOrFail($question_id );
        return view('patient_engagement.show' , compact('question' ) );

    }

    public function saveQuestionAnswer($question_id , Request $request){

        $q = Patient_Engagement_Question::find($question_id);
        activity()->performedOn($q)->log('Answered Patient Engagment Question');

        $choices = json_decode($q->answers);

        $correct_answer = $choices[$q->correct_answer];
        if($request->answer == $correct_answer){
            $status = 1;
        }else{
        $status = 0;
        }

        $points = Patient_Points::where('patient_id' , auth()->user()->patient->id )->first();
        
        $ans = Patient_Engagement_Answers::create([
            'question_id' => $q->id,
            'patient_id' => auth()->user()->patient->id,
            'answer_body' => $request->answer,
            'status' => $status,
        ]);

        $points->answered_questions += 1;
        $points->save();

        if($status == 1){
            $points->earned_score += 10;
            $points->save();
        }

        
     
        if($ans){
            if($ans->answer_body == $correct_answer ){
        return redirect('/dashboard/patient_engagements')->with('success' , 'Good Job! Your Answer was correct. ');}else{

        return redirect('/dashboard/patient_engagements')->with('wrong' , 'Your Answer was not correct');}}
        else{
            return redirect()->back()->with('error' , 'Some problrm occoured while submitting');
        }
  

    }

    public function updateQuestionAnswer($question_id , $ans_id , Request $request){
        
        // dd('we are on our way');

        $ans = Patient_Engagement_Answers::find($ans_id);

        if(isset($request->answerr)){
            $answer = $request->answerr;
        }
        elseif(isset($request->choices)){
            $answer = json_encode($request->choices);

        }
        elseif(isset($request->select)){
            $answer = $request->select;
        }
        $ans->answer_body = isset($answer) ? $answer : NULL ;
        $ans->save();

        if($ans){
            return redirect('/dashboard/patient_engagements')->with('success' , 'Answer added successfully');
            }else{
            return redirect()->back()->with('error' , 'Problem while adding Answer');
            }

    }
}
