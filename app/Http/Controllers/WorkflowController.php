<?php

namespace App\Http\Controllers;

use App\Study;
use App\Workflow;
use Illuminate\Http\Request;

class WorkflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($study_id )
    {
        $study = Study::findOrFail($study_id);
        $workflows = Workflow::where('study_id' , $study_id)->get();
        return view('workflow.index' , compact('workflows', 'study') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($study_id)
    {
        $workflows = Workflow::where('study_id' , $study_id)->get();
        $study = Study::findOrFail($study_id);
        return view('workflow.create' , compact('study', 'workflows') );
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($study_id , Request $request)
    { 
        
        $request->validate([
            'color' => 'required',
            'name' => 'required',
            'parent' => 'sometimes',
            'level' => 'sometimes',
        ]);
        // dd($request->all());

        $workflow = Workflow::create([
            'color' => $request->color,
            'process_name' => $request->name,
            'level' => $request->level,
            'study_id' => $study_id,
            'parent_id' => $request->parent ?? NULL,
        ]);
        if($workflow){
            return redirect('/dashboard/study/'.$study_id.'/workflow')->with('success'  , 'Workflow has been created successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($study_id , $work_id)
    {   
        $workflow = Workflow::findOrFail($work_id);
        $workflows = Workflow::where('study_id' , $study_id)->get();
        $study = Study::findOrFail($study_id);
        return view('workflow.edit' , compact('study', 'workflows' , 'workflow') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $study_id , $work_id , Request $request)
    {
        $workflow = Workflow::findOrFail($work_id);

        $request->validate([
            'color' => 'required',
            'name' => 'required',
            'parent' => 'sometimes',
            'level' => 'sometimes',
        ]);

        
        $workflow->color = $request->color;
        $workflow->process_name = $request->name;
        $workflow->level = $request->level;
        $workflow->parent_id = $request->parent ?? $workflow->parent_id;
        $workflow->save();
                
        if($workflow->save()){
            return redirect('/dashboard/study/'.$study_id.'/workflow')->with('success'  , 'Workflow has been created successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($study_id , $id)
    {
        $workflow = Workflow::findOrFail($id);

        $workflow->delete();

        return redirect()->back()->with('success' , 'Workflow has been deleted');

    }
}
