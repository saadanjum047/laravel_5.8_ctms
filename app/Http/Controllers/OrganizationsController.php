<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Carbon\Carbon;
use App\Organization;

class OrganizationsController extends SiteController
{
    public function __construct()
    {
        $this->template = 'study.frame';
    }

	public function index(){
		$organizations = Organization::where('user_id' , auth()->user()->id )->get();
		return view('organizations.index' , compact('organizations') );
	}

	public function create(){
		return view('organizations.create');
	}

	public function store(Request $request){

		$request->validate([
			'name' => 'required',
			'address' => 'required',
			'city' => 'required',
			'state' => 'required',
			'phone' => 'required',
		]);

		// dd($request->all());
		// if ($request->hasFile('logo')) {
		// $extension = ".".$request->logo->getClientOriginalExtension();
        // $logo = basename($request->logo->getClientOriginalName(), $extension).time();
		// $fileName = $logo.$extension;
		// // $path = $request->logo->storeAs( 'public/organizations/' ,$logo );
		// $path = public_path().'/organization';
        // $uplaod = $request->logo->move($path,$fileName);
		// }
	
		$org = Organization::create([
			'sponsor_name' => $request->name,
			'sponsor_address' => $request->address,
			'sponsor_city' => $request->city,
			'sponsor_state' => $request->state,
			'sponsor_phone' => $request->phone,
			// 'sponsor_logo' => isset($fileName) ? $fileName : null,
			'user_id' => auth()->user()->id
		]);

		if($org)
		return redirect()->route('dashboard.organizations')->with('success' , 'Organization created successfully');
		else
		return redirect()->back()->with('error' , 'Some problem occoured, Try later');
	}

	public function edit($id){
		$org = Organization::findOrFail($id);
		return view('organizations.edit' , compact('org'));
	}

	public function update(Request $request , Organization $org){		

		$request->validate([
			'name' => 'required',
			'address' => 'required',
			'city' => 'required',
			'state' => 'required',
			'phone' => 'required',
			'logo' => 'sometimes',
		]);

		// if ($request->hasFile('logo')) {
		// 	$extension = ".".$request->logo->getClientOriginalExtension();
		// 	$logo = basename($request->logo->getClientOriginalName(), $extension).time();
		// 	$fileName = $logo.$extension;
		// 	// $path = $request->logo->storeAs( 'public/organizations/' ,$logo );

		// 	$fileName = $logo.$extension;
		// 	$path = public_path().'/organization';
		// 	$uplaod = $request->logo->move($path,$fileName);
			
		// }
		
		$org->sponsor_name = $request->name;
		$org->sponsor_address = $request->address;
		$org->sponsor_city = $request->city;
		$org->sponsor_state = $request->state;
		$org->sponsor_phone = $request->phone;
		// $org->sponsor_logo = isset($fileName) ? $fileName : $org->sponsor_logo ;

		if($org->save())
		return redirect()->route('dashboard.organizations')->with('success' , 'Organization updated successfully');
		else
		return redirect()->route('dashboard.organizations')->with('error' , 'Some problem occoured, Try later');

	}

	public function destroy(Organization $org){		

		if(Storage::disk('public')->exists( 'organizations/'. $org->sponsor_logo)){
		Storage::disk('public')->delete( 'organizations/'. $org->sponsor_logo);
		$check = $org->delete();

		if($check)
		return redirect()->back()->with('success' , 'Organization deleted successfully');
		else
		return redirect()->back()->with('error' , 'Some problem occoured, Try later');

		}
	}
	

public function ViewOrganizations(){


$data=['user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];
$response = $this->apiRequest('GetOrganizations',$data);
//dd($response);
	
return view('organizations.index',compact('response'));
}

public function CreateOrganization(){
$data=[
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];
$response = $this->apiRequest('CreateOrganization',$data);
//dd($response);	
return view('organizations.create',compact('response'));
}


public function AddOrganization(Request $request){



if($request->hasfile('logo'))
         {

            foreach($request->file('logo') as $file)
            {
               		$fileName = $file->getClientOriginalName() ;
			$destinationPath ='organization';
			$path_image =  $file->move($destinationPath, $file->getClientOriginalName());  
           }
         }

       $document_path='organization/'.$fileName;
	//dd($document_path);

	$data=[
		'study_id'=>$request->study_id,
		'data'=>$request->except('_token'),
		'organization'=>$document_path,
		'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name')

		];		



$response = $this->apiRequest('AddOrganization',$data);
//dd($response );
	if($response->status=='Success'){
	$Message =$response->Message;
	return back()->with('success', $Message);
}
else{
$Message =$response->Message;
return back()->with('error', $Message);
}

 }





public function EditOrganization($id){
$data=['id'=>$id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];
$response = $this->apiRequest('EditOrganization',$data);
//dd($response);
	
return view('organizations.edit',compact('response'));
}



public function UpdateOrganization(Request $request){

if($request->hasfile('logo'))
         {

            foreach($request->file('logo') as $file)
            {
               		$fileName = $file->getClientOriginalName() ;
			$destinationPath ='organization';
			$path_image =  $file->move($destinationPath, $file->getClientOriginalName());  
           }
         }

       $document_path='organization/'.$fileName;
	//dd($document_path);

	$data=[
		'study_id'=>$request->study_id,
		'data'=>$request->except('_token'),
		'organization'=>$document_path,
		'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name')

		];		
//dd($data);
$response = $this->apiRequest('UpdateOrganization',$data);
//dd($response);
//$Study=$study_id;	
return back()->with('success','Organization Updated Successfully');
}

public function relatestudy(Request $request){



	$data=[
		'data'=>$request->except('_token'),
		'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name')

		];
//dd($data);
$response = $this->apiRequest('relatestudy',$data);
//dd($response );
	if($response->status=='Success'){
	$Message =$response->Message;
	return back()->with('success', $Message);


}

}



	
public function  addsite($sponsor_id){

$sponsor_id=$sponsor_id;	
$data =['user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name')
		];
$response= $this->apiRequest('CreateNewSite', $data);
		
return view('organizations.site.create',compact('sponsor_id','response'));

}

public function  postsite(Request $request){

 $data =$request->except('_token');
//dd($data);
$response= $this->apiRequest('PostNewSite', $data);
//DD($response);
if($response->status =='Success'){
return back()->with('success', $response->Message);
}
else{
return back()->with('error','Sorry Try again');
}

}



public function  viewsite($sponsor_id){

$data =[ 'user_id'=>$sponsor_id,
'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name')
		];

  $response= $this->apiRequest('organizationsite', $data);

//dd($response);

return view('organizations.site.index',compact('sponsor_id','response'));
}



}