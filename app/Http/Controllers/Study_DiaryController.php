<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Study;
use App\Study_Diary;
use App\Notification;
use App\Study_Patient;
use App\Diary_Schedule;
use Illuminate\Http\Request;
use App\Study_Diary_Question;
use App\Http\Controllers\Controller;


class Study_DiaryController extends Controller
{
    public function index(){

        $diaries = Study_Diary::with('study')->where('created_by' , auth()->user()->id)->get();
        $studies = Study::where('created_by' , auth()->user()->id)->get();
        return view('study_diary.index' , compact('diaries' , 'studies') );

    }

    public function ShowDiaryQuestions( $study_id ,$id){

        $study = Study::find($study_id);
        $diary = Study_Diary::find($id);
        $questions = $diary->questions;
        $diaries = Study_Diary::where('created_by' , auth()->user()->id )->get();
        
        return view('PatientDiary.single_diary_question' , compact('questions' , 'diaries' , 'diary' , 'study' ) );
        
    }


    public function ShowDiaryQuestion($diary_id , $question_id ){

        $question = Study_Diary_Question::findOrFail($question_id);
        return view('patientDiary.question_details' , compact('question'));
    }

    public function store(Request $request){

        $request->validate([
            'diary_name' => 'required' ,
            'diary_frequency' => 'required' ,
            'end_duration' => 'required' ,
            'study_name' => 'required' ,
        ]);

        $diary = Study_Diary::create([
            'diary_name' => $request->diary_name,
            'frequency' =>  $request->diary_frequency,
            'end_duration' => $request->end_duration,
            'created_by' => auth()->user()->id,
            'study_id' => $request->study_name,
        ]);

        $patient_ids = $diary->study->patients->pluck('patient_id')->toArray();
        if(isset($patient_ids)){
            foreach($patient_ids as $id){
                Notification::create([
                    'user_id' => $id,
                    'role' => auth()->user()->role_id,
                    'text' => "A new diary has been created $diary->diary_name in your study $diary->study->study_name ",
                ]);
            }
        }

        if($diary)
        return redirect()->back()->with('success' , 'Diary Created Successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occoured,  Try Later');

    }

    public function update(Request $request , Study_Diary $diary){

        $request->validate([
            'diary_name' => 'required' ,
            'diary_frequency' => 'required' ,
            'end_duration' => 'required' ,
            'study_name' => 'required' ,
        ]);

        $diary = Study_Diary::find($request->diary_id)->update([
            'diary_name' => $request->diary_name,
            'frequency' =>  $request->diary_frequency,
            'end_duration' => $request->end_duration,
            'created_by' => auth()->user()->id,
            'study_id' => $request->study_name,
        ]);

        if($diary)
        return redirect()->back()->with('success' , 'Diary Updated Successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occoured,  Try Later');
    
    }

    

    public function remove(Study_Diary $diary){
    
        $diary->questions;
        $diary->diary_schedules;
        foreach($diary->diary_schedules as $schedules){
                $schedules->delete();
        }
        foreach($diary->questions as $question){

            foreach($question->ans as $answer){
                $answer->delete();
            }
                $question->delete();
        }

        
        if($diary->created_by = auth()->user()->id){
        $check = $diary->delete();
    }
        if($diary)
        return redirect()->back()->with('success' , 'Diary Deleted Successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occoured,  Try Later');
    }


    public function viewDiary($id){
        $study = Study::find($id);
        $diaries = Study_Diary::where('study_id' , $id)->get();
        return view('study.diary' , compact('diaries' , 'study') );
    }


    public function changeStatus($diary_id){
        $diary = Study_Diary::findOrFail($diary_id);

        if($diary->status == 1){
        $diary->status = 0;
        $diary->save();
        return redirect()->back()->with('success' , 'Diary Deactiviated successfully');
        }elseif($diary->status == 0){
            $diary->status = 1;
            $diary->save();
        return redirect()->back()->with('success' , 'Diary Activated successfully');
        }      

    }


    public function storeDiary( Request $request , $id){
        
        $study = Study::find($id);
        if(!isset($study)){
            return redirect()->back()->with('error' , "Study Not found");
        }

        $request->validate([
            'diary_name' => 'required' ,
            'diary_frequency' => 'required' ,
            'end_duration' => 'required' ,
        ]);

        // dd($study);

        $diary = Study_Diary::create([
            'diary_name' => $request->diary_name,
            'frequency' =>  $request->diary_frequency,
            'end_duration' => $request->end_duration,
            'created_by' => auth()->user()->id,
            'study_id' => $study->id,
        ]);

        $ids = Study_Patient::where('study_id' , $study->id )->pluck('id')->toArray();
        foreach($ids as $id){
            Diary_Schedule::create([
                'diary_id' => $diary->id,
                'study_id' => $study->id,
                'patient_id' => $id,
                'type' => 0,
            ]);
        }

        $patient_ids = $diary->study->patients->pluck('patient_id')->toArray();
        if(isset($patient_ids)){
            foreach($patient_ids as $id){
                Notification::create([
                    'user_id' => $id,
                    'role' => auth()->user()->role_id,
                    'text' => "A new diary has been created $diary->diary_name in your study".$diary->study->study_name,
                ]);
            }
        }

        if($diary)
        return redirect()->back()->with('success' , 'Diary Created Successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occoured,  Try Later');

    }


    public function updateDiary(Request $request  ){

            $request->validate([
                'diary_name' => 'required' ,
                'diary_frequency' => 'required' ,
                'end_duration' => 'required' ,
            ]);
        
            $diary = Study_Diary::find($request->diary_id)->update([
                'diary_name' => $request->diary_name,
                'frequency' =>  $request->diary_frequency,
                'end_duration' => $request->end_duration,
                'created_by' => auth()->user()->id,
            ]);
    
            if($diary)
            return redirect()->back()->with('success' , 'Diary Updated Successfully');
            else
            return redirect()->back()->with('error' , 'Some problem occoured,  Try Later');   
    }


    

    public function ViewStudyPatients($study_id , $diary_id){
        $diary = Study_Diary::find($diary_id);
        $patients = Study_Patient::where('study_id' , $study_id )->get();

        return view('PatientDiary.users_in_diary' , compact('patients' ,'diary'));
    }

    

    public function ViewStudyPatientDiaries($study_id , $diary_id , $patient_id){
        $diary = Study_Diary::find($diary_id);
        $patient = Study_Patient::find($patient_id);
        return view('PatientDiary.diary_of_user' , compact('diary','patient'));
    }
    
    public function ViewStudyPatientDiaryQuestion($study_id , $diary_id , $patient_id , $freq){
        $study = Study::find($study_id);
        $diary = Study_Diary::find($diary_id);
        $patient = Study_Patient::find($patient_id);
        
        return view('PatientDiary.single_frequency_question' , compact('diary', 'study' ,'freq' ,'patient'));
    }
    

    public function ViewStudyPatientDiaryQuestionAnswer($study_id , $diary_id , $patient_id , $freq , $question_id ){
        $study = Study::find($study_id);
        $diary = Study_Diary::find($diary_id);
        $question = Study_Diary_Question::find($question_id);
        $patient = Study_Patient::find($patient_id);
        
        return view('PatientDiary.single_frequency_question_answer' , compact('diary', 'study', 'question' , 'patient' , 'freq'));
    }

    public function showDiaryDashboard($study_id){
        $study = Study::findOrFail($study_id);
        $scheduled_diaries = Diary_Schedule::where('study_id' , $study->id)->get();
        $patients = Study_Patient::where('study_id' , $study->id )->get();
        return view('diarydashboard.index' , compact('study' , 'scheduled_diaries' , 'patients') );
    }

    public function showScheduledDiary($study_id , $patient_id){
        $study = Study::findOrFail($study_id);
        $patient = Study_Patient::findOrFail($patient_id);

        foreach ($patient->study->diary->where('status' , 1) as $k => $diary){
            for($i = 1; $i<= $diary->frequency; $i++ ){
            $type = $diary->diary_schedules->where('patient_id' , $patient->id )->where('frequency' , $i )->first();
  
            if($diary->questions->count() != 0 ){
            foreach($diary->questions as $question){
              $answer_count = $diary->ans->where('patient_id' , $patient->id )->where('frequency' , $i )->count();
            }
            }else{
              $answer_count = 0;
            }
            
            $questions_count = $diary->questions->count();
  
  
            $due_date = $type->created_at->addDays($diary->end_duration) ;
            $current_date = Carbon::now();
            
            if($due_date > $current_date){
                $type->type = 0;
                $type->save();
            }elseif($due_date < $current_date){
              $type->type = 1;
              $type->save();
            }
            if($questions_count == $answer_count) {
              $type->type = 2;
              $type->save();
            }
          }
          }

        return view('diarydashboard.schedulediaries' , compact('study' , 'patient') );
    }

    public function showMissingDiries($study_id , $patient_id){

        $study = Study::findOrFail($study_id);
        $patient = Study_Patient::findOrFail($patient_id);

        return view('diarydashboard.missingdiaries' , compact('study' , 'patient') );
    }

    public function showPatientCompliances($study_id){
     
        $study = Study::findOrFail($study_id);
        $patients = Study_Patient::all();

        return view('diary.incomplete' , compact('patients' , 'study') );
    }

}



