<?php

namespace App\Http\Controllers;

use App\Study;
use App\Tip_User;
use App\Pre_Screening;
use App\Study_Patient;
use Illuminate\Http\Request;
use App\Pre_Screening_Question;
use App\Screening_Questions_Specification;

class PreScreeningQuestionController extends Controller
{
    public function index($study_id , $screen_id){
        $questions = Pre_Screening_Question::where('screening_id' , $screen_id )->get();
        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);
        return view('sponsor_pre_screening.screening_questions' ,  compact('questions' , 'study' , 'screen'));
    }


    public function viewUsers($study_id , $screen_id , $question_id){
        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);
        $question = Pre_Screening_Question::findOrFail($question_id);
        $invests = Tip_User::where('study_id' , $study->id)->get();
        return view('sponsor_pre_screening.investigators' , compact('study' , 'question' , 'screen' , 'invests'));
        

    }


    public function show($study_id , $screen_id , $question_id , $user_id ){

        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);
        $question = Pre_Screening_Question::findOrFail($question_id);
        $user = Tip_User::findOrFail($user_id);

        return view('sponsor_pre_screening.show_question_answer' , compact('study' , 'question' , 'screen' , 'user' ));

    }

    public function showFrequency($study_id , $screen_id ){
        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);

        return view('sponsor_pre_screening.pre_screens_freq' ,  compact('study' , 'screen'));
    }

    

    public function ViewFrequencyQuestions($study_id , $screen_id , $freq ){

        $questions = Pre_Screening_Question::where('screening_id' , $screen_id )->where('user_id' , auth()->user()->sponsor_user->id)->get();

        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);

        return view('sponsor_pre_screening.questions_freq' ,  compact('study' , 'screen' , 'questions'  , 'freq' ));
    }

    public function create($study_id , $screen_id ){

        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);

        return view('sponsor_pre_screening.create' , compact ('screen' , 'study') ); 
    }

    public function store($study_id , $screen_id , Request $request){

        if(!isset($request->answers) && $request->answer_type != 1 ){
            return redirect()->back()->with('error' , 'You need to select atleast one answer');
        }

        $request->validate([
            "question" => 'required',
            "answer_type" => "required",
            'answers' => 'sometimes',
            'specifications' =>'sometimes',
            'notify' =>'sometimes',
        ]);

    //   dd($request->all());
        $question = Pre_Screening_Question::create([
            'study_id' => $study_id,
            'screening_id' => $screen_id,
            'user_id' => auth()->user()->sponsor_user->id,
            'question' => $request->question,
            'answer_type' => $request->answer_type,
            'notification_response' => $request->notify ?? NULL,
            'answers' => isset($request->answers) ? json_encode($request->answers) : NULL,
        ]);
        
        if(isset($request->specifications)){
            foreach($request->specifications as $spec){
                Screening_Questions_Specification::create([
                    'name' => $spec,
                    'question_id' => $question->id,
                ]);
            }}


        if($question){
            return redirect('dashboard/study/'.$study_id.'/pre_screening/'.$screen_id.'/questions/')->with('success' , 'Pre Screening Question added successfully');;
        }
    }


    public function edit($study_id , $screen_id , $question_id){
        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);
        $question = Pre_Screening_Question::findOrFail($question_id);

        return view('sponsor_pre_screening.create' , compact ('screen' , 'study' , 'question') ); 

    }



    public function update($study_id , $screen_id , $question_id , Request $request){

        $request->validate([
            "question" => 'required',
            "answer_type" => "required",
            'answers' => 'sometimes',
            'specifications' =>'sometimes',
            'notify' =>'sometimes',
        ]);

        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);
        $question = Pre_Screening_Question::findOrFail($question_id);

        // dd($question , $question->specifications);

        $question->question = $request->question;
        $question->notification_response = $request->notify ?? NULL;
        $question->answer_type = $request->answer_type;
        $question->answers = isset($request->answers) ? json_encode($request->answers) : NULL;

        $question->save();
        
        if(isset($question->specifications)){
            foreach($question->specifications as $spec){
                $spec->delete();
                // dump('spec' , Screening_Questions_Specification::where('name' , $spec->name)->first());
                // dump('answer' , Screening_Questions_Specification::where('name' , $spec->name)->first());
                }
            }
            // dd('ruk matherchod');
        if(isset($request->specifications)){
        foreach($request->specifications as $spec){
            Screening_Questions_Specification::create([
                'name' => $spec,
                'question_id' => $question->id,
            ]);
        }
        }

        if($question){
            return redirect('dashboard/study/'.$study_id.'/pre_screening/'.$screen_id.'/questions/')->with('success' , 'Pre Screening Question updated successfully');;
        }else{
            return redirect()->back()->with('error' , 'Some problem while updating Pre Screening Question');
        }

    }

    public function destroy( $study_id , $screen_id , $question_id){
        $question = Pre_Screening_Question::findOrFail($question_id);

        $stat = $question->delete();
        if($stat){
            return redirect()->back()->with('success' , 'Pre Screening Question deleted successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem while deleting Pre Screening Question');
        }
    }


    public function showScreenFrequency($study_id , $screen_id){
        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);

        
        return view('investigator_pre_screening.screening_freq' ,  compact('screen' , 'study'));
    }

    public function showScreenQuestions($study_id , $pat , $screen_id ){

        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);
        $questions = Pre_Screening_Question::where('screening_id' , $screen_id )->get();
        $patient = Study_Patient::findOrFail($pat);
        return view('investigator_pre_screening.questions' ,  compact('screen' , 'study' , 'questions' , 'patient' ));
    }

    public function showScreenQuestionWithAnswer($study_id , $pat ,  $screen_id  , $question_id ){

        $screen = Pre_Screening::findOrFail($screen_id);
        $study = Study::findOrFail($study_id);
        $question = Pre_Screening_Question::findOrFail($question_id);
        $patient = Study_Patient::findOrFail($pat);
        
        return view('investigator_pre_screening.question_answer' ,  compact('screen' , 'study' , 'question' ,'patient'));
    }
    

    public function ChangeStatus($question_id){
        $question = Pre_Screening_Question::findOrFail($question_id);
        if($question->status == 1){
            $question->status = 0;
            $question->save();
        return redirect()->back()->with('success' , 'You have Inactivated the Question');

        }elseif($question->status == 0){
            $question->status = 1;
            $question->save();
        return redirect()->back()->with('success' , 'You have Activated the Question');

        }
        return redirect()->back()->with('error' , 'Some Problem occored');

    }

}
