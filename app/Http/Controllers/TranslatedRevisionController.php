<?php

namespace App\Http\Controllers;

use App\Study;
use App\Revision;
use App\Translated_Revision;
use Illuminate\Http\Request;

class TranslatedRevisionController extends Controller
{
   public function index($study_id , $content_id , $revision_id){

    $revision = Revision::findOrFail($revision_id);
    $study = Study::findOrFail($study_id);
    $translations = $revision->languages;
    return view('contents.translation.index' , compact('translations' , 'study' , 'revision'));

    
   }

   public function create($study_id , $content_id  , $revision_id){

    $revision = Revision::findOrFail($revision_id);
    $study = Study::findOrFail($study_id);

    return view('contents.translation.create' , compact('study' , 'revision'));
   }

   public function store($study_id , $content_id  , $revision_id , Request $request){
   
    $request->validate([
        "revision_header" => 'required' ,
        "revision_des" => 'sometimes',
        "revision_language" => 'required',
        "video_url" => 'sometimes',
    ]);

    // dd($request->all());
    
    // $study = Study::findOrFail($study_id);
    // $content = Content::findOrFail($content_id);
    // dd($content_id , $content );
    $revision = Translated_Revision::create([
        
        "revision_id" => $revision_id,
        "header" => $request->revision_header,
        "description" => $request->revision_des,
        "language" => $request->revision_language,
        "video_url" => $request->video_url,
    ]);
    if($revision){
        return redirect('/dashboard/study/'.$study_id.'/content/'.$content_id.'/version/'.$revision_id.'/translations' )->with('success' , 'Translation added successfully');
    }else{
        return redirect()->back()->with('error' , 'Problem while adding revision');

    }
    }


    public function edit($study_id , $content_id  , $revision_id , $translation_id){

        $revision = Revision::findOrFail($revision_id);
        $study = Study::findOrFail($study_id);
        $translation = Translated_Revision::findOrFail($translation_id);
        return view('contents.translation.edit' , compact('study' , 'revision' , 'translation'));

    }

    public function update($study_id , $content_id  , $revision_id , $translation_id , Request $request){
        
        $request->validate([
            "revision_header" => 'required' ,
            "revision_des" => 'sometimes',
            "revision_language" => 'required',
            "video_url" => 'sometimes',
        ]);
        
        $revision = Translated_Revision::findOrFail($translation_id)->update([
        
            "header" => $request->revision_header,
            "description" => $request->revision_des,
            "language" => $request->revision_language,
            "video_url" => $request->video_url,
        ]);

        $translation = Translated_Revision::findOrFail($translation_id);

        if($translation){
            return redirect('/dashboard/study/'.$study_id.'/content/'.$content_id.'/version/'.$revision_id.'/translations' )->with('success' , 'Translation updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem while adding revision');
    
        }

    }

    public function delete($study_id , $content_id  , $revision_id , $translation_id){
    
        $translation = Translated_Revision::findOrFail($translation_id);
        $translation->delete();
        return redirect()->back()->with('success' , 'Translation has been removed');
    }

}
