<?php

namespace App\Http\Controllers;

use DB;
use App\Group;
use App\Study;
use Carbon\Carbon;
use App\Group_Task;
use App\Study_Patient;
use Illuminate\Http\Request;
use App\Study_Visit_Schedule;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GroupTaskController;

class GroupTaskController extends Controller
{
    public function index($study_id){
        
        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 13 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }

        $groups = Group::where('study_id' , $study_id )->get();
        $study = Study::findOrFail($study_id);
        $schedules = Study_Visit_Schedule::where('study_id' , $study->id)->get();
        return view('group_tasks.index' , compact('groups' , 'study' , 'schedules') );
        
    }


    public function create($study_id){

        // dd('stop');
        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'create')->where('module_id' , 13 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }
        
        $study = Study::findOrFail($study_id);
        return view('group_tasks.create' , compact( 'study') );
        
    }


    public function store($study_id , Request $request){
        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
            'schedules' => 'sometimes',
        ]);

        $study = Study::findOrFail($study_id);


        $group = Group::create([
            'study_id' => $study_id,
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => auth()->user()->id,
        ]);

        $group->schedules()->attach($request->schedules);

        if($group){
            return redirect('/dashboard/study/'.$study_id.'/groups')->with('success' , 'Group added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem while creating group');
        }
    }


    public function edit($study_id , $group_id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'update')->where('module_id' , 13 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }



        $study = Study::findOrFail($study_id);
        $group = Group::findOrFail($group_id);

        return view('group_tasks.edit' , compact('group' , 'study') );
    }

    public function update($study_id , $group_id , Request $request){

        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
            'schedules' => 'sometimes',

        ]);


        $study = Study::findOrFail($study_id);
        $group = Group::findOrFail($group_id);


        $group->schedules()->sync($request->schedules);

        $group = Group::findOrFail($group_id)->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        if($group){
            return redirect('/dashboard/study/'.$study_id.'/groups')->with('success' , 'Group updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem while updating group');
        }
    } 


    public function destroy($study_id , $group_id){
        
        // if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
        //     return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        // }elseif(auth()->user()->role_id == 3)
        // {   
        //     if(auth()->user()->role_id == 3){
        //     if( empty( auth()->user()->study_user->role_detail->permissions->where('name' , 'delete')->where('module_id' , 13 )->toArray() ) ){
        //         return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        //         }
        //     }
        // }
        
        $group = Group::findOrFail($group_id);
        
        // if(auth()->user()->role_id == 2 && $group->user_id != auth()->user()->id){
        //     return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        // }

        if(isset($group->tasks)){
        foreach($group->tasks as $task){
            $task->patients()->sync([]);

            $task->delete();
        }}
        $group->delete();
        
        return redirect()->back()->with('success' , 'Group deleted successfully');
    
    }


    public function tasks($study_id , $group_id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 11 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }

        $study = Study::findOrFail($study_id);
        $group = Group::findOrFail($group_id);
        $tasks = Group_Task::where('group_id' , $group_id )->get();
        $schedules = Study_Visit_Schedule::where('study_id' , $study->id)->get();
        return view('group_tasks.tasks' , compact('tasks' , 'study' , 'group', 'schedules'));
        
    }

    public function CreateTask($study_id , $group_id){


        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 11 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }

        // $schedules = Study_Visit_Schedule::where('study_id' , $study_id)->get();
        $study = Study::findOrFail($study_id);
        $group = Group::findOrFail($group_id);
        return view('group_tasks.create_task' , compact( 'study' , 'group'));
    
    }


    public function StoreTask($study_id , $group_id , Request $request){
        
        $study = Study::findOrFail($study_id);
        $group = Group::findOrFail($group_id);
        $ids = Study_Patient::where('study_id' , $study->id)->pluck('id')->toArray();
        // $schedule = Study_Visit_Schedule::findOrFail($request->schedule_id);


        // dd($request->all() , $schedule , $ids );

        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
        ]);


        $task = Group_Task::create([
            'name' => $request->name,
            'description' => $request->description,
            'group_id' => $group_id,
            'study_id' => $study_id,
            'repeated' => $request->repeated,
        ]);

        $ids = Study_Patient::where('study_id' , $study->id)->pluck('id')->toArray();
         
        $task->patients()->attach($ids);

        // $ids = Study_Patient::where('study_id' , $study_id)->pluck('id')->toArray();
        //     for($i = 1 ; $i<= $request->repeated ; $i++ ){
        //         foreach($ids as $k =>  $id){
        //             $pivots = DB::table('group_task_patient')->insert([
        //                 'patient_id' => $id,
        //                 'task_id' => $task->id,
        //                 'repeated' => $i,
        //             ]);
        //         }
        //     }

        if($task){
        return redirect('/dashboard/study/'.$study_id.'/group/'.$group_id)->with('success' , 'Task added successfully');
        }else{
            return redirect()->back()->with('error' , 'Error whole creating task');

        }
    }

    public function EditTask($study_id , $group_id , $task_id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'update')->where('module_id' , 11 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }

        // $schedules = Study_Visit_Schedule::where('study_id' , $study_id)->get();

        $study = Study::findOrFail($study_id);
        $group = Group::findOrFail($group_id);
        $task = Group_Task::findOrFail($task_id);

        return view('group_tasks.edit_task' , compact( 'study' , 'group' , 'task'));
    
    }


    public function UpdateTask($study_id , $group_id , $task_id , Request $request){
        

        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
        ]);

        $study = Study::findOrFail($study_id);
        $group = Group::findOrFail($group_id);
        $task = Group_Task::findOrFail($task_id);
        $ids = Study_Patient::where('study_id' , $study->id)->pluck('id')->toArray();

        // dd($request->all());


        // if($request->repeated > $task->repeated){
        //     for($i = $task->repeated+1; $i <= $request->repeated; $i++){

        //         foreach($ids as $id){
        //         DB::table('group_task_patient')->insert([
        //             'task_id' => $task->id,
        //             'repeated' => $i,
        //             'patient_id' => $id,
        //         ]);
        //     }

        //     }
        //     // dd($diff , 'greater' );
        // }elseif($request->repeated < $task->repeated) {
        //     for($i = $task->repeated; $i > $request->repeated; $i--){
        //         $pivots = DB::table('group_task_patient')->where('task_id' , $task->id)->where('repeated', $i )->get();
        //         foreach($pivots as $pivot){
        //             DB::table('group_task_patient')->delete($pivot->id);
        //         }

        //     }
        // }

        $task = Group_Task::findOrFail($task_id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'repeated' => $request->repeated,

            ]);



        if($task){
        return redirect('/dashboard/study/'.$study_id.'/group/'.$group_id)->with('success' , 'Task updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Error while updating task');

        }
    }

    public function DeleteTask($study_id , $group_id , $task_id , Request $request){


        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'delete')->where('module_id' , 11 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }

        $task = Group_Task::findOrFail($task_id);
        $task->patients()->sync([]);
        $task->delete();
        return redirect()->back()->with('success' , 'Task deleted successfully');
    }


    public function showTaskDetails($study_id , $group_id , $task_id){
        $group = Group::findOrFail($group_id);
        $task = Group_Task::findOrFail($task_id);
        $study = Study::findOrFail($study_id);
        return view('group_tasks.task_details' , compact('group' , 'task' ,'study'));

    }

    public function showPatientsInTask($study_id , $group_id , $task_id){
        $group = Group::findOrFail($group_id);
        $task = Group_Task::findOrFail($task_id);
        $study = Study::findOrFail($study_id);

        return view('group_tasks.patients_in_task' , compact('group' , 'task' ,'study'));
    }

    public function showPatientDetails($study_id , $group_id , $task_id , $pat_id){

        $group = Group::findOrFail($group_id);
        $task = Group_Task::findOrFail($task_id);
        $study = Study::findOrFail($study_id);
        $patient = Study_Patient::findOrFail($pat_id);

        $pivots = DB::table('group_task_patient')->where('task_id' , $task->id)->where('patient_id', $patient->id )->get();

        return view('group_tasks.patient_details' , compact('group' , 'task' ,'study' , 'pivots' , 'patient'));


    }


    public function attachTaskWithSchedule(Request $request){

        $task = Group_Task::findOrFail($request->task_id);

        if($request->schedule_id == 0){
            $task->schedule_id = NULL;
            $task->save();
            return redirect()->back()->with('success' , 'Task removed from study Schedule');

        }else{
        $task->schedule_id = $request->schedule_id;
        $task->save();
        }   
        return redirect()->back()->with('success' , 'Task assigned to the study Schedule');
        // dd($request->all());
    }
    
    public function attachGroupWithSchedule(Request $request){
        // dd($request->all());
        $group = Group::findOrFail($request->group_id);

        if($request->schedule_id == 0){
            $group->schedule_id = NULL;
            $group->save();
            return redirect()->back()->with('success' , 'Task removed from study Schedule');

        }else{
        $group->schedule_id = $request->schedule_id;
        $group->save();
        }   
        return redirect()->back()->with('success' , 'Task assigned to the study Schedule');
        // dd($request->all());
    }


    public function ViewGroups(){
        // dd('stop');
        return view('patient_tasks.index' );
    }

    public function ViewTasks(){
        
        $tasks = Group_Task::where('study_id' , auth()->user()->patient->study->id )->get();
        return view('patient_tasks.tasks' , compact('tasks'));

    }
    
    public function ViewTask( $task_id){

        $task = Group_Task::findOrFail($task_id);
        $pivots = DB::table('group_task_patient')->where('task_id' , $task->id)->where('patient_id', auth()->user()->patient->id )->orderBy('status' , 'desc')->get();
        
        // $date = Carbon::parse($task->due_date);

        // dd($task->due_date);
        // foreach($pivots as $k => $pivot){
        //     if($k == 0){
        //         if($date < (Carbon::now())->toDateString()){
        //             // dump($date , 'Overdue' , '1st');
        //             DB::table('group_task_patient')->where( 'id' ,$pivot->id)->update([
        //                 'overdue' => 1,
        //             ]);
        //         }
        //     }elseif($k > 0){
        //         if($date < ( Carbon::now())->toDateString())  {
        //             DB::table('group_task_patient')->where( 'id' ,$pivot->id)->update([
        //                 'overdue' => 1,
        //             ]);
        //         }   
        //     }
        //     $date = $date->addDays($task->target);
        //     // dump($pivot , $date);
        // }
            
        return view('patient_tasks.show_task' , compact('task' , 'pivots'));

    }


    public function MarkCompleted($task_id){

      $task = Group_Task::findOrFail($task_id);
      activity()->performedOn($task)->log('Task mark completed');
    //   $pivot = auth()->user()->patient->tasks->pivot;
        $pivot = auth()->user()->patient->tasks->find($task_id)->pivot;
        $pivot->completed_at = Carbon::now();
        $pivot->status = 1;
        $pivot->save();
        
        return redirect()->back()->with('success' , 'You have marked the task completed');
    }

}
