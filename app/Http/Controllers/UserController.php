<?php

namespace App\Http\Controllers;

use Hash;
use Session;
use App\User;
use GuzzleHttp;
use GuzzleHttp\Psr7;
use Illuminate\Http\Request;

use Illuminate\Auth\Access\Gate;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;

class UserController extends SiteController

{
    public function __construct()
    {
        $this->template = 'auth.frame';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setPersonType(Request $request) {
        $data = $request->except('_token');

        if(!empty($data['clear'])){
            return redirect()->back()->withCookies([Cookie::forget('user_role')]);
        }

        $user_type = $data['type'];

        return redirect()->back()->withCookie(Cookie::make('user_role', $user_type));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function login(Request $request) {

        if(Cookie::get('jwt_token')){
            return redirect()->route('dashboard.index');
        }

        if($request->isMethod('post')) {

            $data = $request->except('_token');
            $validator = Validator::make($data, [
                'email' => 'required',
                'password' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }

            if(!in_array(Cookie::get('user_role'), [1,2,3])){
                return abort(404);
            }

		//dd($request->all());
            /**
             * Error Handling not completed!
             */
            $client = new GuzzleHttp\Client();
            try {
                $res = $client->request('POST', env('API_URL').'SignIn', [
                    GuzzleHttp\RequestOptions::JSON => [
                        'email' => $data['email'],
                        'password' => $data['password'],
                        'user_role' => Cookie::get('user_role')
                    ]
                ]);

            }catch (GuzzleHttp\Exception\ClientException $e) {
                if ($e->hasResponse()) {
                    $r = $e->getResponse()->getBody(true);
                    return redirect()->back()->withErrors(json_decode($r)->message);
                }
            }

            if($res->getStatusCode()!==200){
                return abort(404);
            }


            $responseData = (string)$res->getBody();
            $responseData = json_decode($responseData);


            $jwt = Cookie::get('jwt_token');

            if ($responseData->status === 'Success' && !$jwt) {
                $jwt = $responseData->resp->jwt;
                $cookie = cookie('jwt_token', $jwt, 4320);

                $user = $this->apiRequest('GetUser',[],true, $jwt)->data->data;
               
		                if(isset($user->user_role)){
                    $cookie_user = cookie('user_name', $user->fullname, 4320);
		
                 return redirect()->route('dashboard.index')->cookie($cookie)->cookie($cookie_user);
                }else{

                    $cookie_user = cookie('user_name', $user->name, 4320);               
                    Session::put('user_id' ,  $user->id);
                    Session::put('confirmed' ,  $user->confirmed);


                
                return redirect()->route('dashboard.index')->cookie($cookie)->cookie($cookie_user);
                }

            
                
            }elseif($responseData->status==='Fail'){
                return redirect()->back()->withErrors($responseData->message);
            }

        }

        $person_types = $this->apiRequest('GetUserTypes', [], false);

        $person_types = json_decode($person_types);
	$vars['content'] = view('auth.login')->with('person_types', $person_types)->render();

        return $this->render($vars);
    }


    public function verificationPatient(Request $request, $verification_token) {
 
        if($request->isMethod('get')) {
            $verification_token = [
                'verification_token' => $verification_token
            ];

            $check = $this->apiRequest('PatientVerification', $verification_token, false);

            if($check->status!='Success') {
                return abort(404);
            }

            $vars['content'] = view('patient.verification')->with('verification_token', $verification_token)->render();
            return $this->render($vars);
        }


        if($request->isMethod('post')){
            $post_data = $request->except('_token');
            $validator = Validator::make($post_data, [
                'password' => 'required|min:6|confirmed'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }

            $data = [
                'verification_token' => $verification_token,
                'password' => $post_data['password']
            ];

            $response = $this->apiRequest('PatientVerification', $data, false);

            if($response){
                return redirect()->route('login')->with('status', 'Password Successfully Saved!');
            }
        }
    }

    public function recoverId(Request $request) {

        if($request->isMethod('post')){
            $data = $request->except('_token');

            $validator = Validator::make($data, [
                'phone' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }

            $data['user_role'] = Cookie::get('user_role');
            $response = $this->apiRequest('RecoverId', $data, false);

            if(isset($response->status) && $response->status=='Success'){
                return redirect()->back()->with('status', 'Your StudyPal ID Successfully Sent!');
            }
        }

        $vars['content'] = view('auth.recover_id')->render();
        return $this->render($vars);
    }

    public function recoverPassword(Request $request) {

        if($request->isMethod('post')){
            $data = $request->except('_token');
	     $validator = Validator::make($data, [
                'email' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }

            $response = $this->apiRequest('RecoverPassword', $data,false);

            if(isset($response->status) && $response->status=='Success'){

                return redirect()->back()->with('status', 'Your Activation Link Successfully Sent!');
            }
        }

        $vars['content'] = view('auth.recover_password')->render();
        return $this->render($vars);
    }



    public function restorePassword(Request $request, $token, $user_role) {


        if($request->isMethod('post')){
            $data = $request->except('_token');

            $validator = Validator::make($data, [
                'password' => 'required|min:6|confirmed'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }

            $data = [
                'token' => $token,
                'user_role' => $user_role,
                'password' => $data['password']
            ];

            $response = $this->apiRequest('RestorePassword', $data,false);

            if(isset($response->status) && $response->status=='Success'){
                $client = new GuzzleHttp\Client();
                try {
                    $res = $client->request('POST', env('API_URL').'SignIn', [
                        GuzzleHttp\RequestOptions::JSON => [
                            'email' => $response->email,
                            'password' => $response->password,
                            'user_role' => Cookie::get('user_role')
                        ]
                    ]);

                }catch (GuzzleHttp\Exception\ClientException $e) {
                    if ($e->hasResponse()) {
                        $r = $e->getResponse()->getBody(true);
                        return redirect()->back()->withErrors(json_decode($r)->message);
                    }
                }

                if($res->getStatusCode()!==200){
                    return abort(404);
                }


                $responseData = json_decode((string)$res->getBody());

                $jwt = Cookie::get('jwt_token');

                if ($responseData->status === 'Success' && !$jwt) {
                    $jwt = $responseData->resp->jwt;
                    $cookie = cookie('jwt_token', $jwt, 4320);
                    return redirect()->route('dashboard.index')->cookie($cookie);
                }elseif($responseData->status==='Fail'){
                    return redirect()->back()->withErrors($responseData->message);
                }
            }
        }

        $data = [
            'token' => $token,
            'user_role' => $user_role
        ];

        $response = $this->apiRequest('CheckRestorePassword', $data, false);


        if(isset($response->status) && $response->status=='Success' || !empty(session('status'))){
            $vars['content'] = view('auth.restore_password')->with([
                'token' => $token,
                'user_role' => $user_role
            ])->render();
            return $this->render($vars);
        }

        return abort(404);
    }

    public function LoginAndRedirectDiaryQuestions($schedule_id, $auth_token) {

        $jwt_token = Cookie::get('jwt_token');
        $user_role = Cookie::get('user_role');

        if(!empty($jwt_token) && $user_role == 1){
            return redirect()->route('dashboard.diary_questions', $schedule_id);
        }

        $data = [
            'auth_token' => $auth_token
        ];

        $response = $this->apiRequest('GetPatientDataByToken', $data, false);

        if(isset($response->status) && $response->status=='Success'){
            $client = new GuzzleHttp\Client();
            try {
                $res = $client->request('POST', env('API_URL').'SignIn', [
                    GuzzleHttp\RequestOptions::JSON => [
                        'email' => $response->patient->email,
                        'password' => $response->patient->password,
                        'user_role' => 1,
                        'not_md5' => true
                    ]
                ]);

            }catch (GuzzleHttp\Exception\ClientException $e) {
                if ($e->hasResponse()) {
                    $r = $e->getResponse()->getBody(true);
                    return redirect()->back()->withErrors(json_decode($r)->message);
                }
            }

            if($res->getStatusCode()!==200){
                return abort(404);
            }

            $responseData = json_decode((string)$res->getBody());

            $jwt = Cookie::get('jwt_token');

            if ($responseData->status === 'Success' && !$jwt) {
                $jwt = $responseData->resp->jwt;
                $cookie = cookie('jwt_token', $jwt, 4320);
                $user_role = cookie('user_role', 1, 4320);

                return redirect()->route('dashboard.diary_questions', $schedule_id)->withCookies([$cookie, $user_role]);
            }elseif($responseData->status==='Fail'){
                return redirect()->back()->withErrors($responseData->message);
            }

            return abort(404);
        }

        return abort(404);

    }

    public function edit() {
        

        return view('user.edit');
        // $this->template = 'user.frame';

        // $user = $this->apiRequest('GetUser')->data->data;

        // if(isset($user->user_role)){
        //     $username = $user->username;
        // }else{
        //     $username = $user->email;
        // }
		// //dd();

        // $vars['content'] = view('user.edit')->with('username', $username)->render();
        // return $this->render($vars);
    }

    public function update( Request $request ){

        if( ! empty ($request->password) ){
        if($request->password != $request->password_confirmation ){
            return redirect()->back()->with('error' , 'Password does not match' );
        }}
        $request->validate([
            'email' => 'required',
            'password' => 'sometimes',
            'password_confirmation' => 'sometimes',
            'patient_image' => 'sometimes',
        ]);

        
        if ($request->hasFile('image')) {
            $extension = ".".$request->image->getClientOriginalExtension();
            $image = basename($request->image->getClientOriginalName(), $extension).time();
            $fileName = $image.$extension;
            // dd($fileName);
            $path = public_path().'/avatars';
            $uplaod = $request->image->move($path , $fileName);
            }
            // dd($fileName);
        $user = User::findOrFail(auth()->user()->id);
        $user->email = $request->email;
        $user->password = isset($request->password) ? Hash::make($request->password) : $user->password ;
        $user->avatar = isset($fileName) ? $fileName : $user->avatar ;
        $user->save();

       
        if($user){
        return redirect('/dashboard')->with('success' , 'Profile updated successfully' );
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    public function editStore(Request $request) {
        $data = $request->except('_token');

        $validator = Validator::make($data, [
            'email' => 'required|email',
            'password' => 'nullable|min:6|max:255|confirmed'
        ]);


        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }


        $dataToApi = [
            'email' => $data['email'],
            'password' => $data['password']
        ];

        if(isset($data['patient_image'])){
            $dataToApi['patient_image'] = base64_encode(file_get_contents($request->file('patient_image')->getRealPath()));
        }

        $response = $this->apiRequest('EditUserProfile', $dataToApi);

        if(isset($response->status) && $response->status == 'Success'){
            return redirect()->back()->with('status', 'Your changes have been updated.');
        }
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws GuzzleHttp\Exception\GuzzleException
     */
    public function logout() {
        $this->apiRequest('Logout');
        return redirect()->route('login')->cookie(Cookie::forget('jwt_token'));
    }



	public function  ViewUsers($Study_id){

		$data =['study_id'=>$Study_id,
			'user_role'=>Cookie::get('user_role'),
			'user_name'=>Cookie::get('user_name')
			];
		$Study=$Study_id;
	   $response= $this->apiRequest('ViewAllUsers', $data);
	  //dd($response);
	    return view('user.index',compact('response','Study'));
    }

   

    public function  CreateNewUser($Study_id){
	$Study=$Study_id;
    return view('user.create',compact('Study'));
    }



 	public function  PostNewUser(Request $request){
	$data =[
		'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name'),
		'data'=>$request->except('_token')
		];
//dd( $data );
  	 $response= $this->apiRequest('InsertNewUser',$data);

//dd( $response);
   		if($response->status=='Success'){
		return back()->with('success','User Created Successfully');
		}
	else{
	return back()->with('error','Sorry Try again');
	}

    }

	/*
		This is used by sponsor, where they can register a user at a specific site
	*/
		
	public function  Registeruserbysite($study_id, $site_id){
		$Study=$study_id;
		return view('user.create',compact('Study','site_id'));
	}

	/*
		This is used by sponsor, where they can click on a site and
		then click on view users at the selected site
		*/
	public function  ViewUserbysite($study_id, $site_id){
		$Study=$study_id;
		$site=$site_id;
		$data=['study_id'=>$study_id,'site_id'=>$site_id];
		$response= $this->apiRequest('ViewUserBySite', $data);
		//dd($response);
		//die();
	    return view('user.siteusers',compact('response','Study','site'));
    }

	public function edituser($user_id,$study_id,$site_id){
		$data=['user_id'=>$user_id,'study_id'=>$study_id,'site_id'=>$site_id];
		//dd($data);
		 $response= $this->apiRequest('UpdateSingleUser',$data);
		//dd($response);
		$Study=$study_id;
		return view('user.edituser',compact('Study','user_id','response'));
	}

public function updateuser(Request $request){
		$data =[
		'user_role'=>Cookie::get('user_role'),
		'user_name'=>Cookie::get('user_name'),
		'data'=>$request->except('_token')
		];

//dd($data);
 $response= $this->apiRequest('UpdateUser',$data);
 //dd($response);

if($response->status=='Success'){
		return back()->with('success',$response->Message);
		}
	else{
	return back()->with('error','Sorry Try again');
	}

}

public function updateuserstatus($study_id,$user_id,$is_activate){
$data=['study_id'=>$study_id,
'user_id'=>$user_id,
'is_activate'=>$is_activate,
];
// dd($data);
 $response= $this->apiRequest('UpdateUserStatus',$data);
 //dd($response);
if($response->status=='Success'){
		return back()->with('success',$response->Message);
		}
	else{
	return back()->with('error', $response->Message);
	}
}


}
