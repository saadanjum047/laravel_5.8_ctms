<?php

namespace App\Http\Controllers;

use App\User;
use App\Study;
use App\Message;
use App\Tip_User;
use App\Notification;
use App\Study_Patient;
use Illuminate\Http\Request;
use App\Investigator_Message_Reply;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($study_id)
    {   
        $study = Study::findOrFail($study_id);
        $messages = Message::where('investigator_id' , auth()->user()->sponsor_user->id )->where('study_id' , $study_id)->get();
        return view('investigator_msg.index' , compact('messages' , 'study') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($study_id)
    {
        $study = Study::findOrFail($study_id);
        return view('investigator_msg.create' , compact( 'study') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( $study_id, Request $request)
    {
        $study = Study::findOrFail($study_id);

        $request->validate([
            'title' => 'required',
            'body' => 'sometimes',
            'attachment' => 'sometimes',
            'sponsor' => 'required',
        ]);

        if ($request->hasFile('attachment')) {
            $extension = ".".$request->attachment->getClientOriginalExtension();
            $attachment = basename($request->attachment->getClientOriginalName(), $extension).time();
            $fileName = $attachment.$extension;
            // dd($fileName);
            $path = public_path().'/messages';
            $uplaod = $request->attachment->move($path , $fileName);
            }

            $user = User::findOrFail($request->sponsor);

        $msg = Message::create([
            'title' => $request->title,
            'body' => $request->body,
            'attachment' => $fileName ?? NULL ,
            'investigator_id' => auth()->user()->sponsor_user->id ,
            'sponsor_id' =>  $user->sponsor_user->id,
            'study_id' => $study_id ,
        ]);

                Notification::create([
                    'user_id' => $user->id,
                    'role' => $user->role_id,
                    'text' => "You have a new message",
                ]);
        

        if($msg){
            return redirect('/dashboard/study/'. $study_id .'/investigator/messages')->with('success' , 'Message has been sent');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

        
    }

    public function ViewInvestigators($study_id){
        $study = Study::findOrFail($study_id);
        $sites = $study->sites->pluck('site_id')->toArray();
        $investigator_ids = Message::where('study_id' , $study_id)->pluck('investigator_id')->toArray();
        $investigator_ids = array_unique($investigator_ids);

        $investigators = Tip_User::whereIn('id' , $investigator_ids)->get();

        // ->whereIn('site_id' , $sites);
        // $patients = $study->patients->whereIn('site_id' , $sites);
        // $patients = Study_Patient::where('study_id' , auth()->user()->sponsor_user->study->id )->get();
        return view('sponsor_msg.investigators' , compact('investigators' , 'study') );
    
    }

    public function ViewMessages( $study_id , $investigator_id){
        $study = Study::findOrFail($study_id);
        $investigator = Tip_User::findOrFail($investigator_id);

        // $patient = Study_Patient::findOrFail($patient_id);
        // return view('investigator_msg.chat' , compact('patient' , 'study') );
        return view('sponsor_msg.index' , compact('investigator' , 'study') );
    }

    public function showMessage($study_id , $investigator_id , $msg_id){
        $study = Study::findOrFail($study_id);
        $investigator = Tip_User::findOrFail($investigator_id);
        $message = Message::findOrFail($msg_id);
        return view('sponsor_msg.show' , compact('message' , 'investigator' , 'study') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show( $study_id , $message)
    {
        $message = Message::findOrFail($message);
        $study = Study::findOrFail($study_id);

        return view('investigator_msg.show' , compact('message' ,'study') );
    }


    public function replyMessage($study_id , $investigator_id , $msg_id , Request $request){

        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);

        $reply = Investigator_Message_Reply::create([
            'sponsor_id' => auth()->user()->sponsor_user->id,
            'message_id' => $msg_id,
            'title' => $request->title,
            'body' => $request->body,
        ]);

        if($reply)
        return redirect('/dashboard/study/'.$study_id.'/investigator/'.$investigator_id.'/messages/all')->with('success' , 'Your reply to the message has been sent');
        else
        return redirect()->back()->with('error' , 'Sorry ! Some problem occoured');

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
