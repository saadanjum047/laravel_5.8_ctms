<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudyVisitSchedule;
use Validator;

class CreateVisitScheduleController extends Controller
{
    //
    public function createVisitSchedule()
    {
        return view("createVisitSchedule");
    }


    public function createVisitSchedulePost(Request $request)
    {
        $rules = [];


        foreach($request->input('visit_name') as $key => $value) {
            $rules["visit_name.{$key}"] = 'required';
        }


        $validator = Validator::make($request->all(), $rules);


        if ($validator->passes()) {


            foreach($request->input('visit_name') as $key => $value) {
                TagList::create(['visit_name'=>$value]);
            }


            return response()->json(['success'=>'done']);
        }


        return response()->json(['error'=>$validator->errors()->all()]);
    }
    
}
