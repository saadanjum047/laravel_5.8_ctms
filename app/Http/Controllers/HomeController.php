<?php

namespace App\Http\Controllers;

use App\Site;
use App\Study;
use App\Study_Diary;
use App\Study_Patient;
use App\Exports\SiteExport;
use App\Exports\DiaryExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Concerns\FromCollection;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('dashboard.index');
    }

    // public function export($site_id){
    //     $site = Site::find($site_id);
    //     $data = [ 'site' => $site ];
    //     $patients = Study_Patient::all();
    //     return Excel::download(new SiteExport($patients), 'patients.xlsx');

    // }

    public function exportSite($site_id){
        
        $patients = Study_Patient::where('site_id' , $site_id)->get();
        return Excel::download(new SiteExport($patients), 'patients.xlsx');

    }

    public function exportDiary($study_id){
        $study = Study::findOrFail($study_id);

        if(auth()->user()->role_id == 2 ){

        $data = array();
        $patients = Study_Patient::where('study_id' , $study->id)->get();
        $diaeies = Study_Diary::where('study_id' , $study->id)->get();
        }elseif(auth()->user()->role_id == 3){

        $patients = Study_Patient::where('created_by' , auth()->user()->id )->get();

        $diaeies = Study_Diary::where('study_id' , $study->id)->get();

        }
        $data[] = $patients; 
        $data[] = $diaeies;

        return Excel::download(new DiaryExport($data), 'patients.xlsx');
    }


    public function downloadFile($path , $name){

        $path = public_path(). '/' . $path .'/' . $name;
        if(file_exists($path)){
            return Response::download( $path , $name );
        }else{
            return redirect()->back()->with('error' , 'Sorry! the file is not available on server anymore');
        }
    }
}
