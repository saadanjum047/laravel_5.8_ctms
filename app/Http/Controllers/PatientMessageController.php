<?php

namespace App\Http\Controllers;

use App\Study;
use App\Tip_User;
use App\Study_User;
use App\Notification;
use App\Study_Patient;
use App\Patient_Message;
use Illuminate\Http\Request;
use App\Patient_Message_Reply;

class PatientMessageController extends Controller
{
    public function index(){
        $messages = auth()->user()->patient->messages;
        return view('patient_messages.index', compact('messages') );
        
    }

    public function create(){

        // $investigators_ids = Study_User::where('study_id' , auth()->user()->patient->study->id )->pluck('user_id')->toArray();
        // $investigators = Tip_User::whereIn('id' , $investigators_ids)->get();

        $investigators_in_study = Study_User::where('study_id' , auth()->user()->patient->study->id )->pluck('user_id')->toArray();
        $investigators = Tip_User::whereIn(  'user_id' , $investigators_in_study)->get();
      
        return view('patient_messages.create' , compact('investigators'));

    }

    public function store(Request $request){

        $request->validate([
            "title" => "required",
            "body" => "sometimes",
            "investigator_id" => "required",
            "attachment" => "sometimes"
        ]);

        if ($request->hasFile('attachment')) {
            $extension = ".".$request->attachment->getClientOriginalExtension();
            $attachment = basename($request->attachment->getClientOriginalName(), $extension).time();
            $fileName = $attachment.$extension;
            // dd($fileName);
            $path = public_path().'/messages';
            $uplaod = $request->attachment->move($path , $fileName);
            }

        $message = Patient_Message::create([
            'study_id' => auth()->user()->patient->study->id,
            'patient_id' => auth()->user()->patient->id,
            'investigator_id' => $request->investigator_id,
            'title' => $request->title,
            'body' => $request->body,
            'attachment' => $fileName ?? NULL,
        ]);

        activity()->performedOn($message)->log('Patient Has sent a message');

        $user = Tip_User::findOrFail($request->investigator_id);

        Notification::create([
            'user_id' => $user->user->id,
            'role' => $user->user->role_id,
            'text' => "You have a new message from patient",
        ]);

        if($message)
        return redirect('dashboard/patient/messages')->with('success' , 'Message has been sent successfully');
        else
        return redirect()->back()->with('error' , 'Some problem while send message.');

    }

    public function show($message_id){

        $message = Patient_Message::findOrFail($message_id);
        return view('patient_messages.show', compact('message') );

    }

    public function showPatients($study_id){
        $study = Study::findOrFail($study_id);
        $patients = $study->patients;

        return view('investigator_messages.patients', compact('patients' , 'study') );
    }

    public function showPatientMessages($study_id , $patient_id ){

        $study = Study::findOrFail($study_id);
        $patient = Study_Patient::findOrFail($patient_id);
        
        return view('investigator_messages.index', compact('patient' , 'study') );

    }

    public function showMessages($study_id , $patient_id , $message_id){
        $study = Study::findOrFail($study_id);
        $patient = Study_Patient::findOrFail($patient_id);
        $message = Patient_Message::findOrFail($message_id);

        return view('investigator_messages.show', compact('patient' , 'study' , 'message') );

    }
    public function messageReply($study_id , $patient_id , $message_id , Request $request){
        $message = Patient_Message_Reply::create([
            'title' => $request->title,
            'body' => $request->body,
            'investigator_id' => auth()->user()->sponsor_user->id,
            'message_id' => $message_id,
        ]);

        if($message)
        return redirect()->back()->with('success' , 'Reply send successfully');
        else
        return redirect()->back()->with('success' , 'Some problem occoured. plz Try later');
        
    }
}
