<?php

namespace App\Http\Controllers;

use App\Study;
use App\Medicine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedicineController extends Controller
{
    public function index($study_id){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 17 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }

        }
        $study = Study::findOrFail($study_id);
        $mediciens = Medicine::where('study_id' , $study_id)->get();
        return view('medicine.index' , compact('mediciens' , 'study'));

    }

    public function store($study_id , Request $request){


        $request->validate([
            'name' => 'required',
            'dose' => 'required',
        ]);

        $medic = Medicine::create([
            'name' => $request->name,
            'dose' => $request->dose,
            'created_by' => auth()->user()->id,
            'study_id' => $study_id,

        ]);

        if($medic)
        return redirect()->back()->with('success' , 'Medicine added successfully');
        else
        return redirect()->back()->with('error' , 'Problem whilte creating medicine');
    }

    public function update($study_id , Request $request){

        $request->validate([
            'name' => 'required',
            'dose' => 'required',
            'medicine_id' => 'required',
        ]);

        $med =Medicine::findOrFail($request->medicine_id);
        $med->name = $request->name;
        $med->dose = $request->dose;
        $med->save();
        
        if($med)
        return redirect()->back()->with('success' , 'Medicine added successfully');
        else
        return redirect()->back()->with('error' , 'Problem whilte creating medicine');
    }

    public function destroy($study_id , $med_id){


        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $study_id)->first()->role_detail->permissions->where('name' , 'delete')->where('module_id' , 17 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }

        }

        if(Medicine::findOrFail($med_id)->delete())
        return redirect()->back()->with('success' , 'Medicine deleted successfully');
        else
        return redirect()->back()->with('error' , 'Problem whilte deleting medicine');
    }
}
