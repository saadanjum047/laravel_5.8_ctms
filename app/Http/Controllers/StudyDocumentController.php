<?php

namespace App\Http\Controllers;

use App\Study;
use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;


class StudyDocumentController extends Controller
{
    public function index($study_id){

        $documents = Document::where('study_id' , $study_id )->get();
        $study = Study::findOrFail($study_id);
        return view('studydocuments.index' , compact('documents', 'study'));
    
    }


    public function create($study_id){
        $study = Study::findOrFail($study_id);
        return view('studydocuments.create' , compact('study'));

    }


    public function store($study_id  , Request $request){

        if ($request->hasFile('document')) {
            $extension = ".".$request->document->getClientOriginalExtension();
            $doc = basename($request->document->getClientOriginalName(), $extension).time();
            $fileName = $doc.$extension;
    
            $path = public_path().'/documents';
            $uplaod = $request->document->move($path,$fileName);
        }
        

        $doc = Document::create([
            'name' => $request->name,
            'document' => $fileName,
            'document_type' => $request->document_type,
            'study_id' => $study_id,

        ]);
        
        if($doc){
        return redirect("/dashboard/study/".$study_id."/documents" )->with('success' , 'Document addedd successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem occoured while entering document Revision');

        }


    }

    public function edit($study_id , $document_id){
    
    $document = Document::findOrFail($document_id);   
    $study = Study::findOrFail($study_id);

    return view('studydocuments.edit' , compact('document' , 'study'));
    
    }

    public function update($study_id , $document_id , Request $request){
        if ($request->hasFile('document')) {
            $extension = ".".$request->document->getClientOriginalExtension();
            $doc = basename($request->document->getClientOriginalName(), $extension).time();
            $fileName = $doc.$extension;
    
            $path = public_path().'/documents';
            $uplaod = $request->document->move($path,$fileName);

            $document = Document::find($document_id)->update([
                'name' => $request->name,
                'document' => $fileName,
                'document_type' => $request->document_type,
            ]);
        }else{

            $document = Document::find($document_id)->update([
                'name' => $request->name,
                'document_type' => $request->document_type,
    
            ]);
        }

        if($document){
            return redirect("/dashboard/study/".$study_id."/documents" )->with('success' , 'Document addedd successfully');
            }else{
                return redirect()->back()->with('error' , 'Problem occoured while updating document Revision');
    
            }

    }

        public function ViewDocument($study_id){
            $study = Study::findOrFail($study_id);
            $documents = Document::where('study_id' , $study_id)->get();
            return view('investigator_study_document.index' , compact('documents' , 'study'));
        }

        public function showDocument($study_id , $document_id){
            
            $document = Document::findOrFail($document_id);
            $path = public_path().'/documents/'.$document->document;
            if(file_exists($path)){
                return Response::download( $path , $document->document );
            }else{
                return redirect()->back()->with('error' , 'Sorry! the file is not available on server anymore');
            }

            // working fine with pdf and shows the file in the browser
            // return Response::make(file_get_contents($path), 200, [
            //     'Content-Type' => 'application/pdf',
            //     'Content-Disposition' => 'inline; filename="'.$document.'"'
            // ]);

        }

}
