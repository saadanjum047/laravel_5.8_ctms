<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;

class DiaryController extends SiteController
{
    public function __construct()
    {
        $this->template = 'diary.frame';
    }

    public function getDiaryQuestions($schedule_id) {
        $data['schedule_id'] = $schedule_id;
        $response = json_decode($this->apiRequest('GetDiaryQuestions', $data));
                $diary_questions = $response->questions;
        $diary = $response->diary;
        $vars['content'] = view('diary.questions')->with([
            'diary' => $diary,
            'diary_questions' => $diary_questions,
            'unanswered_count' => $response->unanswered_count
        ]);

        return $this->render($vars);
    }


    public function saveAnswers(Request $request) {
        $data = $request->except('_token')['data'];


        $answers = [];
        for($i = 0; $i < (int)$data['answers_length']; $i++){
            $selected = 0;
            if($data['selectedIndex']==$i){
                $selected = 1;
            }
            array_push($answers, $selected);
        }

        $saveData = [
            'question_id' => $data['question_id'],
            'answers' => array_values($answers),
            'text' => $data['text']
        ];

        $response = $this->apiRequest('SaveDiaryAnswers', $saveData);

        return response()->json($response);
    }

    public function confirmDiary($schedule_id) {
        $data['schedule_id'] = $schedule_id;

        $this->apiRequest('ConfirmDiary',$data);


        return redirect()->back();
    }

    public function incompleteDiary($study_id, $site_id = null) {
        $data['study_id'] = $study_id;

        if($site_id !== null){
            $data['site_id'] = 1;
        }
		
		if(Cookie::get('user_role') == 2){
        $patients = $this->apiRequest('GetStudy', $data);
        $patients = json_decode($patients);
		}
		else
		if(Cookie::get('user_role') == 3){
		$patients = $this->apiRequest('GetPatientsList', $data);
		}
		

        if(!$patients){
            $patients = collect([]);
        }
		
        $vars['content'] = view('diary.incomplete')->with(['patients' => $patients]);


        return $this->render($vars);
    }









}
