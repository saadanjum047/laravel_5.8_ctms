<?php

namespace App\Http\Controllers;

use DB;
use Hash;
use App\Site;
use App\User;
use App\Study;
use App\SiteUser;
use App\Tip_User;
use Carbon\Carbon;
use App\Study_site;
use App\Study_User;
// use Illuminate\Support\Facades\DB;
use App\Sponsor_Role;
use App\Study_Patient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Study_Site_Controller extends Controller
{
    public function viewSites($id){
        $study = Study::find($id);
        
        return view('site.index',compact('study'));
    }


    public function create($study_id){
        $study = Study::find($study_id);
        return view('site.create' , compact('study'));
    }

    public function store(Request $request , $study_id){

        $request->validate([
            "site_name" => "required",
            "site_nbr" => "required",
            "address1" => "required",
            "address2" => "sometimes",
            "city" => "required",
            "state" => "required",
            "country" => "required",
            "planned_patients" => "sometimes",
            "pat_lo" => "sometimes",
            "pat_hi" => "sometimes",
        ]);

        $study = Study::find($study_id);

        $site = Site::create([
            "site_name" =>  $request->site_name ,
            "site_nbr" =>  $request->site_nbr ,
            "address1" =>  $request->address1 ,
            "address2" =>  $request->address2 ,
            "city" =>  $request->city ,
            "state" =>  $request->state ,
            "country" =>  $request->country ,
            "pat_lo" =>  $request->pat_lo ,
            "pat_hi" =>  $request->pat_hi ,
            "created_by" => auth()->user()->id,
            "user_id" =>  auth()->user()->id,
            "date_created" => Carbon::now(),
        ]);

        $site_user = SiteUser::create([
            "site_id" => $site->site_id,
            "user_id" =>  auth()->user()->id,
            "created_by" => auth()->user()->id,
            "date_created" => Carbon::now(),

        ]);

        $study_site = Study_site::create([
           'study_id' => $study->id,
           'site_id' => $site->site_id,
           'site_nbr'  => $site->site_nbr,
           'planned_patients' => $request->planned_patients,
           'create_date' => Carbon::now(),
           "created_by" => auth()->user()->id,

        ]);
 
        if($site && $site_user && $study_site )
        return redirect("/dashboard/study/$study->id/sites")->with('success' , 'Site has been registered');
        else
        return redirect()->back()->with('error' , 'Problem while registring ');

    }


    public function edit($study_id , $site){

        $study = Study::find($study_id);
        $site  = Site::find($site);
        return view('site.edit' , compact('study' , 'site'));
    }

    public function update(Request $request , $study_id , $site_id){

        // dd($request->all() , $study_id , $site);

        $request->validate([
            "site_name" => "required",
            "site_nbr" => "required",
            "address1" => "sometimes",
            "address2" => "sometimes",
            "city" => "sometimes",
            "state" => "sometimes",
            "country" => "sometimes",
            "planned_patients" => "sometimes",
            "pat_lo" => "sometimes",
            "pat_hi" => "sometimes",
        ]);

        $study = Study::find($study_id);
        $site = Site::find($site_id);
    
        $site->site_name =  $request->site_name;
        $site->site_nbr =  $request->site_nbr;
        $site->address1 =  $request->address1;
        $site->address2 =  $request->address2;
        $site->city =  $request->city;
        $site->state =  $request->state;
        $site->country =  $request->country;
        $site->pat_lo =  $request->pat_lo;
        $site->pat_hi =  $request->pat_hi;
        
        $site->save();

        $site->site->site_nbr = $request->site_nbr;
        $site->site->planned_patients = $request->planned_patients;
        $site->site->save(); 

         return redirect("/dashboard/study/$study->id/sites")->with('success' , 'Site has been registered');

        if($site->save() && $site->site->save() )
        return redirect("/dashboard/study/$study->id/sites")->with('success' , 'Site has been registered');
        else
        return redirect()->back()->with('error' , 'Problem while updating');



    }


    public function ViewUsersBySite($study_id , $site_id ){
        $study = Study::findOrFail($study_id);
        $site = Site::findOrFail($site_id);
        // $patients = Study_Patient::where('site_id' , $site_id)->get();
        $users = Tip_User::where([
            ['study_id' , $study->id ],
            ['site_id' , $site->site_id ]
            ] )->get();

        return view( 'site.users' , compact('users' , 'study' , 'site') );
    }

    public function CreateUsersBySite(  $study_id , $site_id ){
        $sponsor_roles = Sponsor_Role::where('sponsor_id' , auth()->user()->id )->get();
        
        $study = Study::findOrFail($study_id);
        $site = Site::findOrFail($site_id);
        return view('site.create_user' , compact('study' , 'site' ,'sponsor_roles'));
        
    }



    public function StoreUsersBySite($study_id , $site_id , Request $request){

        $request->validate([
            "firstname_input" => "sometimes",
            "lastname_input" => "sometimes",
            "email_input" => "required | unique:users,email,",
            "pass_input" => "required",
            "con_pass_input" => "required",
            "phone_input" => "sometimes",
            "addr_input" => "sometimes",
            "addr2_input" => "sometimes",
            "city_input" => "sometimes",
            "state_input" => "sometimes",
            "country_input" => "sometimes",
            "zip_input" => "sometimes",
            "add_new" => "sometimes",
        ]);


        if($request->pass_input != $request->con_pass_input ){
            return back()->with('error', 'Password does not match');
        }

        $user = User::create([
            'name' => $request->firstname_input,
            'email' => $request->email_input,
            'created_by' => auth()->user()->id ,
            'password' => Hash::make($request->pass_input),
            'role_id' => 3,
        ]);

        $tip_user = Tip_User::create([
            'username' => $request->firstname_input,
            'password' => Hash::make($request->pass_input),
            'fullname' => $request->firstname_input .' '.$request->lastname_input ,
            'address1' => $request->addr_input,
            'address2' => $request->addr2_input,
            'city' => $request->city_input,
            'zip' => $request->zip_input,
            'phone' => $request->phone_input,
            'country' => $request->country_input,
            'email' => $request->email_input,
            'state' => $request->state_input,
            'sponsor_id' => auth()->user()->id,
            'user_id' => $user->id,
            'study_id' => $study_id,
            'site_id' => $site_id,
            'user_role' => $request->role_id,
            'is_activate' => 1,
        ]);

            
        $study = Study::findOrFail($study_id);
        
        $role = Study_User::create([
            'study_id' => $study_id,
            'user_id' => $user->id,
            'created_by'  => auth()->user()->id,
            'role' => $request->role_id,
            'date_created' => Carbon::now(),
        ]);


        $site_user = SiteUser::create([
            'user_id' => $user->id,
            'site_id' => $site_id,
            'date_created' => Carbon::now(),
            'created_by'  => auth()->user()->id,

        ]);

        if($user && $tip_user){
            return redirect("/dashboard/study/".$study_id."/sites/".$site_id."/users")->with('success','User created successfully');
        }else{
            return redirect()->back()->with('error', 'Problem while creating user');
        }
    }


    public function edituser( $study_id , $site_id , Tip_User $user){
        
        $study = Study::findOrFail($study_id);
        $sponsor_roles = Sponsor_Role::where('sponsor_id' , auth()->user()->id )->get();
        $site = Site::findOrFail($site_id);
        return view('site.edit_user' , compact('study','user', 'site' , 'sponsor_roles'));
    }

    
    public function Updateuser( $study_id , $site_id , Tip_User $user , Request $request){

        $request->validate([
            "firstname_input" => "sometimes",
            "lastname_input" => "sometimes",
            "email_input" => "required",
            "phone_input" => "sometimes",
            "addr_input" => "sometimes",
            "addr2_input" => "sometimes",
            "city_input" => "sometimes",
            "state_input" => "sometimes",
            "country_input" => "sometimes",
            "zip_input" => "sometimes",
            "add_new" => "sometimes",
        ]);
            
        $study_id = $user->study->id;
        
        $tip_user = Tip_User::find($user->id)->update([
            'username' => $request->firstname_input,
            'fullname' => $request->firstname_input .' '.$request->lastname_input ,
            'address1' => $request->addr_input,
            'address2' => $request->addr2_input,
            'city' => $request->city_input,
            'zip' => $request->zip_input,
            'phone' => $request->phone_input,
            'country' => $request->country_input,
            'email' => $request->email_input,
            'state' => $request->state_input,
            'study_id' => $study_id,
            'user_role' => $request->role_id,
            
        ]);

        $user = User::find($user->user->id);

        $user->name = $request->firstname_input;
        $user->email = $request->email_input;
        $user->save();

        $role =  $user->study_user->where('study_id' , $study_id )->first();
        $role->role = $request->role_id;
        $role->save();

        if($user && $tip_user){
            return redirect("/dashboard/study/$study_id/sites/$site_id/users")->with('success' , 'User updated successfully');
        }else{
            return back()->with('error' , 'Problem while updating user');
        }
    }

    public function changeUserStatus($user_id){
        $user = Tip_User::findOrFail($user_id);

       

        if($user->is_activate == 1)
        $user->is_activate = 0;
        else
        $user->is_activate = 1;

        // dd($user);

        $user->save();
        return redirect()->back()->with('success', "User status has been changed successfully");
    }

}

