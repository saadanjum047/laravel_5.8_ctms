<?php

namespace App\Http\Controllers;

use App\User;
use App\Study;
use App\Tip_User;
use App\Site_Task;
use Carbon\Carbon;
use App\Study_User;
use Illuminate\Http\Request;
use App\Investigator_Task_Group;

class InvestigatorTaskGroupController extends Controller
{
    // Investigator_Task_Group

    public function index($study_id){
        $groups = Investigator_Task_Group::where('study_id' , $study_id)->get();
        $study = Study::findOrFail($study_id);
        return view('investigator_group_tasks.index' , compact('groups' , 'study') );
    }

    public function create($study_id){

        $study = Study::findOrFail($study_id);

        return view('investigator_group_tasks.create' , compact( 'study') );

    }

    public function store($study_id , Request $request ){

        $group = Investigator_Task_Group::create([
            'sponsor_id' => auth()->user()->sponsor_user->id,
            'study_id' => $study_id,
            'name' => $request->name,
            'description' => $request->description,
        ]);

        if($group){
            return redirect('/dashboard/study/'.$study_id.'/investigator/groups')->with('success' , 'Group added successfully');
        }
        else{
            return redirect()->back()->with('error' , 'Some problem while adding group');

        }

    }

    public function edit($study_id , $group_id){
        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);
        return view('investigator_group_tasks.edit' , compact( 'study' ,'group') );

    }

    public function update($study_id , $group_id , Request $request){
        
        $group = Investigator_Task_Group::findOrFail($group_id)->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        if($group){
            return redirect('/dashboard/study/'.$study_id.'/investigator/groups')->with('success' , 'Group added successfully');
        }
        else{
            return redirect()->back()->with('error' , 'Some problem while adding group');

        }        

    }

    public function destroy( $study_id ,  $group_id){
        $group = Investigator_Task_Group::findOrFail($group_id)->delete();
        // foreach($group->tasks as $task){
        //     $task->delete();
        // }
        return redirect()->back()->with('success' , 'Group has been removed');

    }


    //Tasks

    public function tasks($study_id , $group_id) {
        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);

        $tasks = $group->tasks;
        return view('investigator_group_tasks.tasks.index' , compact('group' , 'study' , 'tasks' ) );
    }

    public function taskCreate($study_id , $group_id){
        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);

        return view('investigator_group_tasks.tasks.create' , compact('group' , 'study' , 'tasks' ) );

    }

    public function taskStore($study_id , $group_id , Request $request){
        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);
        
        $investigators_in_study = Study_User::where('study_id' , $study_id)->pluck('user_id')->toArray();
        $investigators_ids = Tip_User::whereIn(  'user_id' , $investigators_in_study)->pluck('id')->toArray();
        // dd($investigators_ids);
        $task  = Site_Task::create([
            'study_id' => $study_id,
            'group_id' => $group->id,
            'name' => $request->name,
            'description' => $request->description,
            'repeated' => $request->repeated,

        ]);

        $task->investigators()->sync($investigators_ids);

        if($task)
        return redirect('dashboard/study/'.$study_id.'/investigator/group/'.$group_id.'/tasks')->with('success' , 'Task added successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occoured');

    }




    public function taskEdit($study_id , $group_id , $task_id ){
        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);
        $task = Site_Task::findOrFail($task_id);
        return view('investigator_group_tasks.tasks.edit' , compact('group' , 'study' , 'task' ) );

    }

    public function taskUpdate($study_id , $group_id , $task_id , Request $request){

        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);
        $investigators_in_study = Study_User::where('study_id' , $study_id)->pluck('user_id')->toArray();
        $investigators_ids = Tip_User::whereIn(  'user_id' , $investigators_in_study)->pluck('id')->toArray();
        
        // dd($investigators_ids);
        $task = Site_Task::findOrFail($task_id);

        $task->investigators()->sync($investigators_ids);

        $task  = Site_Task::findOrFail($task_id)->update([
            'study_id' => $study_id,
            'group_id' => $group->id,
            'name' => $request->name,
            'description' => $request->description,
            'repeated' => $request->repeated,
        ]);

        if($task)
        return redirect('dashboard/study/'.$study_id.'/investigator/group/'.$group_id.'/tasks')->with('success' , 'Task updated successfully successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occoured');

    }

    public function taskDelete($study_id , $group_id , $task_id , Request $request){

        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);

        $task = Site_Task::findOrFail($task_id);
        // dd($task);
        $task->investigators()->sync([]);
        $task->delete();

        return redirect()->back()->with('success' , 'Task delete successfully');


    }
    public function taskDetails($study_id , $group_id , $task_id , Request $request){

        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);
        $task = Site_Task::findOrFail($task_id);

        return view('investigator_group_tasks.tasks.details' , compact('group' , 'study' , 'task' ) );

    }

    public function showGroups($study_id){
    
    $study = Study::findOrFail($study_id);
    $groups = Investigator_Task_Group::where('study_id' , $study_id)->get();
    return view('investigator_group_tasks_investigators.index' , compact( 'study' , 'groups') );
    
    }

    public function showGroupTasks($study_id , $group_id ){
        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);
        $tasks = $group->tasks;

        return view('investigator_group_tasks_investigators.tasks' , compact( 'study' , 'group' , 'tasks') );


    }

    public function showTask($study_id , $group_id , $task_id ){

        $group = Investigator_Task_Group::findOrFail($group_id);
        $study = Study::findOrFail($study_id);
        $task = Site_Task::findOrFail($task_id);

        return view('investigator_group_tasks_investigators.show' , compact( 'study' , 'group' , 'task') );
    }

    public function markComplete($task_id){

        $pivot = auth()->user()->sponsor_user->tasks->find($task_id)->pivot;
        if($pivot->status == 0){
        $pivot->status = 1;
        $pivot->complete_at = Carbon::now();
        $pivot->save();}

        return redirect()->back()->with('success', 'Task is marked completed');
        
    }


}
