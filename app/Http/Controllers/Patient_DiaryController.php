<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Study;
use App\Study_Diary;
use App\Study_Diary_Answers;
use Illuminate\Http\Request;
use App\Study_Diary_Question;
use App\Http\Controllers\Controller;
use App\Study_Question_Specification;
use Illuminate\Support\Facades\Cookie;
use App\Study_Diary_Answer_Specification;

class Patient_DiaryController extends Controller
{
    public function index($study_id){
        $study = Study::find($study_id);   
        $questions = Study_Diary_Question::where('created_by' , auth()->user()->id )->get();
        $diaries = Study_Diary::where('created_by' , auth()->user()->id )->get();

        return view('PatientDiary.index' , compact('questions' , 'diaries' , 'study' ) );
    }

    public function create($study_id , $diary_id ){
     $study = Study::find($study_id);   
     $diaries = Study_Diary::where([
         ['created_by' , auth()->user()->id ],
         ['study_id' , $study->id ],
     ])->get();
    $diary = Study_Diary::findOrFail($diary_id);
     return view('PatientDiary.create' , compact('diaries', 'study' , 'diary') );
    }

    public function store(Request $request){


        if(!isset($request->answers) && $request->answer_type != 1 ){
            return redirect()->back()->with('error' , 'You need to select atleast one answer');
        }
        // dd($request->all() , json_encode($request->integer));
        $request->validate([
            "question" => 'required',
            "diary_id" => 'required',
            "answer_type" => "required",
            'answers' => 'sometimes',
            'specifications' =>'sometimes',
            'notify' =>'sometimes',
            'integer' =>'sometimes',
        ]);
        // dd('sss');
        $study = Study_Diary::findOrFail($request->diary_id);
    
        $question = Study_Diary_Question::create([
            'question' => $request->question,
            'diary_id' => $request->diary_id,
            'study_id' => $study->study_id,
            'answer_type' => $request->answer_type,
            'notification_response' => isset($request->notify) ? $request->notify : NULL,
            'answers' => isset($request->answers) ? json_encode($request->answers) : NULL,
            'created_by' => auth()->user()->id,
            'integer' => json_encode($request->integer) ?? NULL,
        ]);
        if(isset($request->specifications)){
        foreach($request->specifications as $spec){
            Study_Question_Specification::create([
                'name' => $spec,
                'question_id' => $question->id,
            ]);
        }}
      

        if($question){
            return redirect("/dashboard/study/".$question->diary->study->id."/diary/".$question->diary->id."/questions")->with('success' , 'Question added successfully');
        }else{
            return redirect()->back()->with('error' , 'Sorry Some problem occourd');
        }
    }

    public function edit( $study_id , $diary_id , Study_Diary_Question $question){
        $study = Study::find($study_id);
        $diary = Study_Diary::findOrFail($diary_id);
        $diaries = Study_Diary::where('created_by' , auth()->user()->id )->get();
        // return view('PatientDiary.create' , compact('diaries' , 'question' , 'study' , 'diary') );
        return view('PatientDiary.create' , compact('diaries' , 'question' , 'study' , 'diary') );
    }

    public function update(Study_Diary_Question $question , Request $request){

        if(!isset($request->answers) && $request->answer_type != 1 ){
            return redirect()->back()->with('error' , 'You need to select atleast one answer');    
        }

        $request->validate([
            "question" => 'required',
            "answer_type" => "required",
            'answers' => 'sometimes',
            'integer' =>'sometimes',

        ]);

            $study = Study_Diary::findOrFail($request->diary_id);
            $question->question = $request->question;
            $question->study_id = $study->study_id;
            $question->notification_response = isset($request->notify) ? $request->notify : NULL ;

            $question->answer_type = $request->answer_type;
            $question->answers = isset($request->answers) ? json_encode($request->answers) : NULL;
            $question->integer = json_encode($request->integer) ?? NULL ;

            if(isset($question->specifications)){
                foreach($question->specifications as $spec){
                    $spec->delete();
                    }
                }
                
            if(isset($request->specifications)){
            foreach($request->specifications as $spec){
                Study_Question_Specification::create([
                    'name' => $spec,
                    'question_id' => $question->id,
                ]);
            }
            }
            
            if($question->save())
            return redirect("/dashboard/study/".$question->diary->study->id."/diary/".$question->diary->id."/questions")->with('success' , 'Question added successfully');
        else
            return redirect()->back()->with('error' , 'Sorry Some problem occourd');
        
    }


    public function destroy( $id){
        $question = Study_Diary_Question::findOrFail($id);

        if(isset($question->specifications)){
            foreach($question->specifications as $spec){
                $spec->delete();
                }
            }
            
        if($question->delete()){
            return redirect()->back()->with('success' , 'Question removed successfully');
        }else{
            return redirect()->back()->with('error' , 'Sorry Some problem occourd');
        }
        
    }



    public function viewDiaryQuestion($id){
        $diary = Study_Diary::find($id);
        // dd($diary , $diary->questions);
        return view('patient_question.index' , compact('diary'));
    }

    public function viewQuestion($id , $freq){
        $question = Study_Diary_Question::find($id);

        // dd($question  , $question->answer , $question->ans);
        return view('patient_question.question' , compact('question' , 'freq'));
    }

    public function storeAnswer(Request $request , $id , $freq){
        
        $q = Study_Diary_Question::find($id);
        if(isset($request->answer)){
            $answer = $request->answer;
        }
        elseif(isset($request->choices)){
            $answer = json_encode($request->choices);
        }
        elseif(isset($request->select)){
            $answer = $request->select;
        }
        // dd($request->all() , $answer );
        $ans = Study_Diary_Answers::create([
            'question_id' => $q->id,
            'study_id' => $q->diary->study->id,
            'diary_id' =>  $q->diary->id,
            'patient_id' => auth()->user()->patient->id,
            'answers' => $answer,
            'frequency' => $freq ?? 1 ,
        ]);
        
        if(isset($request->choices) && isset($request->specifications) ){
            foreach($request->choices as $k=> $opt){
                // dump($opt , $request->specifications[$k] );
                Study_Diary_Answer_Specification::create([
                    'answer_id' => $ans->id,
                    'name' => $opt,
                    'specification' => isset($request->specifications[$k]) ? $request->specifications[$k] : '',
                ]);
            }    
        }
        elseif(isset($request->select)){
            if(isset($request->specifications)){
            Study_Diary_Answer_Specification::create([
                'answer_id' => $ans->id,
                'name' => $request->select,
                'specification' => isset($request->specifications) ? $request->specifications : '' ,
            ]);
        }
        }


        if($ans){
            // return redirect('/dashboard/study/diary/questions/'.$q->diary->id)->with('success' , 'Answer submitted successfully');
            return redirect('/dashboard/diaries/'.$q->diary->id.'/all/questions/'.$freq)->with('success' , 'Answer submitted successfully');
        }
        else{
            return redirect()->back()->with('success' , 'Problem while submitting answer');

        }
    }

    public function storeDiary( $diary_id , $freq , Request $request ){
        
        // dd($request->all());
        
        foreach($request->questions as $q){
            
            $question = Study_Diary_Question::findOrFail($q);
            
            if(isset($request['answer'.$q] )){

            $answer = $request['answer'.$q];

            if($question->answer_type == 3){
                $answer = json_encode($answer);
            }
            
            // @dump($question . '  ' . $answer);

            if(!is_null($question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first())){
                $ans = $question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first();

                if($question->answer_type != 1 ){
                    foreach($ans->specifications as $spec){
                        $spec->delete();
                        // @dump('specs delete');
                    }
                }
                $ans->answers = $answer;
                $ans->save();
                // @dump('update answer');
                $ans = $question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first();

            }else{
                // @dump('create answer');

            $ans = Study_Diary_Answers::create([
                'question_id' => $question->id,
                'study_id' => $question->diary->study->id,
                'diary_id' =>  $question->diary->id,
                'patient_id' => auth()->user()->patient->id,
                'answers' => $answer,
                'frequency' => $freq ?? 1 ,
            ]);
            }

            if($question->answer_type == 3){
            if(isset($request['answer'.$q] ) && isset($request['specifications'.$q] ) ){
                foreach($request['answer'.$q] as $k=> $opt){
                    Study_Diary_Answer_Specification::create([
                        'answer_id' => $ans->id,
                        'name' => $opt,
                        'specification' => $request['specifications'.$q][$k] ??  '',
                    ]);
                }    
            // @dump('check specs create');

            }
        }

        if($question->answer_type == 2){
            if(isset($request['answer'.$q] )){
                if(isset($request['specifications'.$q] )){
                Study_Diary_Answer_Specification::create([
                    'answer_id' => $ans->id,
                    'name' => $request['answer'.$q] ,
                    'specification' => $request['specifications'.$q] ?? '' ,
                ]);
            }
                // @dump('radio specs create');

            }
        }


        }else{
            $answer = $question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first();
            if(isset($answer)){
                $answer->delete();
            }
        }
    }

    return redirect()->back()->with('success' , 'Answered Saved successfully');

    }

    public function updateAnswer(Request $request , $id){

        // dd($request->all());
        $ans = Study_Diary_Answers::find($id);
        // $ans = $q->ans->where('patient_id' , auth()->user()->patient->id)->first();


        if(isset($request->choices)){        
            foreach($ans->specifications as $spec){
                $spec->delete();
            }
        }
        if(isset($request->select)){
        
            foreach($ans->specifications as $spec){
                $spec->delete();
            }
        }


        if(isset($request->answer)){
            $answer = $request->answer;
        }
        elseif(isset($request->choices)){
            $answer = json_encode($request->choices);
        }
        elseif(isset($request->select)){
            $answer = $request->select;
        }

        $ans->answers = isset($answer) ? $answer : NULL ;
        $ans->save();

        if(isset($ans->specifications)){
        foreach($ans->specifications as $spec){
            $spec->delete();
        }}
        
        if(isset($request->choices) ){
            // foreach($ans->specifications as $spec){
            //     $spec->delete();
            // }
            if(isset($request->specifications)){
            foreach($request->choices as $k=> $opt){
                Study_Diary_Answer_Specification::create([
                    'answer_id' => $ans->id,
                    'name' => $opt,
                    'specification' => isset($request->specifications[$k]) ? $request->specifications[$k] : '' ,
                ]);}
            }    
        }
    
        elseif(isset($request->select)){
            // foreach($ans->specifications as $spec){
            //     $spec->delete();
            // }
            if(isset($request->specifications)){
            Study_Diary_Answer_Specification::create([
                'answer_id' => $ans->id,
                'name' => $request->select,
                'specification' => isset($request->specifications) ? $request->specifications : '',
            ]);
        }
    }

    return redirect()->back()->with('success' , 'Done!');

    // if($ans){
    //     return redirect('/dashboard/diaries/'.$ans->question->diary->id.'/all/questions/'.$ans->frequency)->with('success' , 'Answer submitted successfully');
    //     }
    //     else{
    //         return redirect()->back()->with('error' , 'Problem while submitting answer');

    //     }
    }







    // Functions for the different diaries

    public function viewDiaries( $study_id , $diary_id , $status ){
        $diary = Study_Diary::find($diary_id);

        return view('patient_question.diaries' , compact( 'diary' , 'status'));
    }

    public function viewDiaryQuestions($diary_id , $freq , $status ){

        $diary = Study_Diary::find($diary_id);
        
        // return view('patient_question.index' , compact( 'diary' , 'freq'));
        return view('patient_question.new_index' , compact( 'diary' , 'freq' , 'status'));
    }
}
