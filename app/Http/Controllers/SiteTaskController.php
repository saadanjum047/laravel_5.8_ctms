<?php

namespace App\Http\Controllers;

use App\Site;
use App\Study;
use App\Tip_User;
use App\Site_Task;
use Carbon\Carbon;
use App\Study_Patient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteTaskController extends Controller
{
    public function index($study_id  , $site_id ){
        $tasks = Site_Task::where('site_id' , $site_id)->get();
        $study = Study::find($study_id);
        $site = Site::find($site_id);

        return view('site_tasks.index' , compact('tasks' , 'study' , 'site'));
    }


    public function create($study_id , $site_id  ){

        $study = Study::find($study_id);
        $site = Site::find($site_id);
        $investigators = Tip_User::where('site_id' , $site_id )->get();
        return view('site_tasks.create' , compact('study' , 'investigators' , 'site') );
    }


    public function store($study_id , $site_id , Request $request){

        $request->validate([
            'name' => 'required',
            'investigators' => 'required',
            'description' => 'sometimes',
            'document' => 'sometimes',
        ]);
   
        if ($request->hasFile('document')) {
            $extension = ".".$request->document->getClientOriginalExtension();
            $document = basename($request->document->getClientOriginalName(), $extension).time();
            $fileName = $document.$extension;
            // dd($fileName);
            $path = public_path().'/site_tasks/documents';
            $uplaod = $request->document->move($path , $fileName);
            }

        $task = Site_Task::create([
            'name' => $request->name,
            'study_id' => $study_id,
            'site_id' => $site_id,
            'description' => $request->description ?? NULL,
            'document' => $fileName ?? NULL,
        ]);
            // dd('stop');
        $task->investigators()->sync($request->investigators);


        if($task){
            return redirect('/dashboard/study/'.$study_id.'/site/'.$site_id.'/tasks')->with('success' , 'Task added successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem while creating task');
        }

    }


    public function edit($study_id , $site_id , $task_id){
        
        $study = Study::find($study_id);
        $task = Site_Task::find($task_id);
        $investigators = Tip_User::where('site_id' , $site_id )->get();
        $site = Site::find($site_id);

        return view('site_tasks.edit' , compact('task' , 'study' , 'investigators' , 'site') );
    }


    public function update($study_id , $site_id , $task_id , Request $request){

        $request->validate([
            'name' => 'required',
            'investigators' => 'required',
            'description' => 'sometimes',
            'document' => 'sometimes',
        ]);

        if ($request->hasFile('document')) {
            $extension = ".".$request->document->getClientOriginalExtension();
            $document = basename($request->document->getClientOriginalName(), $extension).time();
            $fileName = $document.$extension;
            // dd($fileName);
            $path = public_path().'/site_tasks/documents';
            $uplaod = $request->document->move($path , $fileName);
            }

        $study = Study::find($study_id);
        $task = Site_Task::find($task_id);

        $task->investigators()->sync($request->investigators);
        // dd($request->patients);

        $task = Site_Task::find($task_id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'document' => $fileName ?? $task->document ,
        ]);

        if($task){
            return redirect('/dashboard/study/'.$study_id.'/site/'.$site_id.'/tasks')->with('success' , 'Task added successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem while creating task');
        }
    }

    public function destroy($study_id ,  $site_id , $task_id){

        $task = Site_Task::find($task_id);
        $task->investigators()->sync([]);
        $task->delete();

        return redirect()->back()->with('success' , 'Task deleted successfully');
        
    }

    public function showDetails($study_id , $site_id , $task_id ){

        $study = Study::find($study_id);
        $task = Site_Task::find($task_id);
        $site = Site::find($site_id);

        return view('site_tasks.details' , compact('task' , 'study' , 'site' ) );
    }


    public function viewTasksToInvestigators(){
        return view('investigators_tasks.index');
        // dd(auth()->user()->patient->tasks );
    }

    public function ViewTask($task_id){
        $task = Site_Task::find($task_id);

        return view('investigators_tasks.show' ,compact('task') );

    }

    public function taskCompleted($task_id ){


        $task = Site_Task::find($task_id);
        $pivot = $task->investigators->find(auth()->user()->sponsor_user->id);

        $pivot->pivot->status = 1;
        $pivot->pivot->complete_at = Carbon::now();

        $pivot->pivot->save();

        return redirect()->back()->with('success' , 'Task marked completed');
    }

}
