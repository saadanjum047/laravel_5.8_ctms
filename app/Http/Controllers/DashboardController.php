<?php

namespace App\Http\Controllers;

use DB;
use App\Site;
use App\Study;
use Carbon\Carbon;
use App\Study_site;
use App\Study_Diary;
use App\DiarySchedule;
use App\Study_Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Spatie\Activitylog\Models\Activity;

class DashboardController extends SiteController
{
    public function __construct()
    {
        $this->template = 'frame';
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index() {
      // dd(auth()->user()->name);
        // $studies = Study::with('sites')->get();

        // foreach($studies as $study){
        //   foreach($study->sites as $site){
            // The site relationship works fine here but not in blade 
        //     dd($study->sites , $site , $site->site);
        //   }

        // }

        // $studies = $this->apiRequest('GetStudies');
        // $information  = $this->apiRequest('GetPatientsInformation');
        // $diary_types = $this->apiRequest('GetDiaryTypes');
        
        // $studies = Study::with('sites')->get();
        // $information  = $this->apiRequest('GetPatientsInformation');
        // $diary_types = $this->apiRequest('GetDiaryTypes');


        // $user  = auth()->user();
        // $sponsor = DB::table('tip_sponsor_users')->where('user_id', $user->id)->first(['sponsor_id']);
        
        // $studies_ids = Study::where('sponsor_id',$sponsor->sponsor_id)->pluck('id')->toArray();
        // $patients_ids = DB::table('tip_study_patient')->whereIn('study_id', $studies_ids)->pluck('patient_id')->toArray();

        // $incomplete_diary = DiarySchedule::groupBy('patient_id')->where('type', '!=', 2)->whereIn('patient_id', $patients_ids)->count();

        // $patients_count = Patient::whereIn('id', $patients_ids)->count();

        // dd($sponsor , $studies_ids , $patients_ids , $incomplete_diary); 
        
        // $diary_types = json_decode($diary_types);
        
        // dd($studies , $information , $diary_types);


//         $data['diary_type'] = 0;
//         $current_diary = json_decode($this->apiRequest('GetDiary', $data));
//         if(!isset($this->apiRequest('GetPatientStudy')->status)) {
//             $patient_study = json_decode($this->apiRequest('GetPatientStudy'));
//         }else{
//             $patient_study = [];
//         }
// $data=['user_role'=>Cookie::get('user_role'),
// 'user_name'=>Cookie::get('user_name')
// ];
// $response = $this->apiRequest('GetSponserStudies',$data);




      //   $vars['content'] = view('index')->with([
      //       'studies'=> $studies,
      // //       'information' => $information,
      // //       'diary_types' => $diary_types,
      // //       'current_diary' => $current_diary,
      // //       'patient_study' => $patient_study,
      // //  'response'=>$response,
      //   ]);

      //   return $this->render($vars);
      // {{dd(auth()->user()->sponsor_user->role)}}
      
      // dd(auth()->user()->sponsor_user );
     
      if(auth()->user()->role_id == 2){

          $studies = Study::with('sites')->where('created_by' , auth()->user()->id)->get();
          return view('index' , compact('studies') );

      }elseif(auth()->user()->role_id == 3){

      
          $study_ids = auth()->user()->study_user->pluck('study_id')->toArray();
          $studies = Study::with('sites')->whereIn('id' , $study_ids)->get();
          
          // $study = Study::findOrFail(auth()->user()->sponsor_user->study->id);

        // $all_sites =  Study_site::where('study_id' , $study->id)->pluck('site_id')->toArray();
            
        // if(count($all_sites) > 0 ){
        // $all_patients = Study_Patient::whereIn('site_id' , $all_sites )->get();
        // }

        // $studies = Study::with('sites')->where('created_by' , auth()->user()->sponsor_user->sponsor_id)->get();
        return view('index' , compact('studies') );

        // return view('index' , compact('studies' , 'all_patients') );
      }
      elseif(auth()->user()->role_id == 4 ){      

        $patient = Study_Patient::where('patient_id' , auth()->user()->id )->first();
       
        if( $patient->confirmed== 0){
              return view('patient.account_not_verified' , compact('patient') );
            }

        $patient_study = $patient->study;
      
        foreach (auth()->user()->patient->study->diary->where('status' , 1) as $k => $diary){
          for($i = 1; $i<= $diary->frequency; $i++ ){
          $type = $diary->diary_schedules->where('patient_id' , auth()->user()->patient->id )->where('frequency' , $i )->first();

          if($diary->questions->count() != 0 ){
          foreach($diary->questions as $question){
            $answer_count = $diary->ans->where('patient_id' , auth()->user()->patient->id )->where('frequency' , $i )->count();
          }
          }else{
            $answer_count = 0;
          }
          
          $questions_count = $diary->questions->count();


          $due_date = $type->created_at->addDays($diary->end_duration) ;
          $current_date = Carbon::now();
          
          if($due_date > $current_date){
              $type->type = 0;
              $type->save();
          }elseif($due_date < $current_date){
            $type->type = 1;
            $type->save();
          }
          if($questions_count == $answer_count) {
            $type->type = 2;
            $type->save();
          }
        }
        }
        // dd('stop');
        $diaries = Study_Diary::where('study_id' , $patient_study->id )->get();
        return view('index' , compact('patient_study' , 'patient' ) );

      }elseif(auth()->user()->role_id > 4 )
      {
        // dd(auth()->user() , auth()->user()->sponsor_user , auth()->user()->sponsor_user->study);
        return view('index' );
      }
      // else{
        // return redirect('/')->with('error' , 'You are not allows')
      // }

    }




    public function diaryPage($diary_type) {
        $data['diary_type'] = $diary_type;
        $diary_types = $this->apiRequest('GetDiaryTypes');
        $diary_types = json_decode($diary_types);
        $current_diary = json_decode($this->apiRequest('GetDiary', $data));

        $vars['content'] = view('diary.diary_page')->with([
            'type' => $diary_type,
            'diary_types' => $diary_types,
            'current_diary' => $current_diary
        ]);

        return $this->render($vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveStudy(Request $request) {
        $data = $request->except('_token');

        $result = $this->apiRequest('setStudyForInvestigator', $data);

        if($result->status=='Success'){
            return redirect()->back()->with('status', $result->message);
        }

        return redirect()->back()->withErrors($result->message);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registerPatient() {
        $vars['content'] = view('register_patient');
        return $this->render($vars);
    }
	
	
	
	 /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function diarydashboard($study_id) {
		  $DiarySchedule = $this->apiRequest('countdiaryschedule',$study_id);
//dd($DiarySchedule);
		  $study_id=$study_id;
		  return view('diarydashboard.index',compact('DiarySchedule','study_id'));
    }
	
	
	 public function CronSchedule() {
		
		 $DiarySchedule = $this->apiRequest('CronSchedule');
		return   $DiarySchedule;
    }
	


	 public function CronCheckUsersRegistered() {

		 $CronCheckUsersRegistered= $this->apiRequest('CronCheckUsersRegistered');
		//dd($CronCheckUsersRegistered);
		 return   $CronCheckUsersRegistered;
    }
	

 public function aswerdiaries($patient_id,$study_id) {
  $data=["patient_id"=>$patient_id,
 "study_id"=>$study_id
  ];

  $DiarySchedule = $this->apiRequest('Allanswerdiaries',$data);
 //dd( $DiarySchedule);
  return view('diarydashboard.answerdiaries',compact('DiarySchedule','study_id'));
		
}


public function missingdiaries($patient_id,$study_id) {
  $data=["patient_id"=>$patient_id,
 "study_id"=>$study_id
  ];

	//dd($patient_id);
 $Missingdiaries= $this->apiRequest('Missingdiaries',$data);
 // dd( $DiarySchedule);
  return view('diarydashboard.missingdiaries',compact('Missingdiaries','study_id'));
		
		
}

public function shecdulediaries($patient_id,$study_id) {
  $data=["patient_id"=>$patient_id,
 "study_id"=>$study_id
  ];

  //dd($patient_id);
  $DiarySchedule = $this->apiRequest('Scheduleddiaries',$data);
 // dd( $DiarySchedule);
  return view('diarydashboard.schedulediaries',compact('DiarySchedule','study_id'));
		

		
}


public function contents($study_id) {
  $data=[ "study_id"=>$study_id];

$response = $this->apiRequest('ViewContents',$data);
 //dd( $response );
$Study=$study_id;
  return view('contents.index',compact('Study','response'));

}	

public function Addcontents($study_id){
$data=[ "study_id"=>$study_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];


$response = $this->apiRequest('Addcontents',$data);
//dd($response);
$Study=$study_id;
return view('contents.createcontent',compact('Study','response'));
}




public function storecontents(Request $request){

//dd($request->all());
$data=[ 'data' => $request->except('_token'),
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];

$response = $this->apiRequest('storecontents',$data);
if($response){
return back()->with("success","content added successfully");
}else{
return back()->with("error","content Not Added");
}
}






public function AddUrlcontents($study_id){
$data=[ "study_id"=>$study_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];


$response = $this->apiRequest('AddcontentsUrl',$data);
$Study=$study_id;
//dd($response);
return view('contents.addurl',compact('Study','response'));
}

public function Calculatorcontents($study_id){
$data=[ "study_id"=>$study_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];
$response = $this->apiRequest('AddcontentCalculator',$data);
$Study=$study_id;


return view('contents.calculator',compact('Study','response'));
}

public function Documentcontents($study_id){
$data=[ "study_id"=>$study_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];


$response = $this->apiRequest('AddcontentCalculator',$data);
$Study=$study_id;
//dd($response);
return view('contents.document',compact('Study','response'));
}	


public function Solutioncontents($study_id){
$data=[ "study_id"=>$study_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];


$response = $this->apiRequest('AddcontentCalculator',$data);
$Study=$study_id;
//dd($response);
return view('contents.solutions',compact('Study','response'));
}	

public function PostDocumentcontents(Request $request){

dd($request->all());
$data=[ 
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];


$response = $this->apiRequest('PostDocumentcontents',$data);
$Study=$study_id;

if($response){
return back()->with("success","content added successfully");
}else{
return back()->with("error","content Not Added");
}

}	

public function getstudies(Request $request){
 $User=DB::table('tip_study')->where('sponsor_id',$request->sponsor_id)->get();
if($User !=[]){
$html = '';
        foreach ($User as $value) {


                  $html .= "<option value='" . $value->id. "'>" . $value->study_name . "</option>";
        }
  return response()->json(['html' => $html]);
}
else{
$html = '';
$html .= "<option value=''>" . "Sorry No Study Found Select Some Other Organization" . "</option>";
        }
  return response()->json(['html' => $html]);

}
	

	
public function getstudiesss(){
 $User=DB::table('tip_study')->where('sponsor_id',116)->get();

if($User !=[]){
$html= '';
        foreach ($User as $value) {


                  $html.= "<option value='" . $value->id. "'>" . $value->study_name . "</option>";
        }
  return response()->json(['html' => $html]);
}
else{
$html= '';
$html.= "<option>" . "Sorry No Study Found Select Some Other Organization" . "</option>";
 return  $html;
 }


dd();
	

}



public function addrevisioncontent($study_id, $content_id){
  
$data=[ 
'study_id'=>$study_id,
'content_id'=>$content_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];

$response= $this->apiRequest('addrevisioncontent',$data);
$Study=$study_id;
 $content= $content_id;
$created_by=$response->user_id;
//dd($response);
  return view('contents.revision',compact('Study','content','created_by','response'));

}


public function viewrevisioncontent($study_id, $content_id){
$data=[ 
'study_id'=>$study_id,
'content_id'=>$content_id,
'user_role'=>Cookie::get('user_role'),
'user_name'=>Cookie::get('user_name')
];


 $response= $this->apiRequest('viewrevisioncontent',$data);
//dd($response);
 $Study=$study_id;
 $content= $content_id;

  return view('contents.viewrevision',compact('Study','content','response'));

}


public function postrevision(Request $request){
$data= $request->except('_token');
//dd($data);
 $response= $this->apiRequest('PostRevision',$data);
//dd($response);
 if($response->status="Success"){
return back()->with("success","Revision added successfully");
}else{
return back()->with("error","content Not Added");
}

}

public function Editcontent($study_id, $content_id){
	$response=DB::table('tip_content')->where('content_id',$content_id)->get()->toarray();
	//dd($response);
	$studyname =DB::table('tip_study')->where('id',$study_id)->get()->toarray();
	//dd($studyname);
	$tip_users=DB::table('tip_users')->where('fullname', Cookie::get('user_name'))->get()->ToArray();
	$AllStudies=DB::table('tip_study')->where('created_by',$tip_users[0]->id)->get();
	$Organizations =DB::table('tip_sponsor')->where('user_id',$tip_users[0]->id)->get();
	$contentstab =DB::table('tip_content_tab')->get();
	$Study=$study_id;
	
	
	return view('contents.editcontent',compact('Study','studyname','response','Organizations','AllStudies','contentstab'));
	

}

public function Postupdatecontent(Request $request){
//dd($request->all());

$update=DB::table('tip_content')
            ->where('content_id', $request->content_id)
            ->update([
  "content_tab_id" => $request->content_tab_id,
  "content_header" => $request->content_header,
  "content_body" => $request->content_body,
  "content_lang" => $request->content_lang,
  "content_type" =>$request->content_type,
  "active" => $request->active,
  "public" =>$request->public
]);


if($update=true){
	return back()->with("success","content Updated successfully");
	}
	else{
	return back()->with("error","content Not Added");
	}

}

Public function postcontentsurl(Request $request){
//dd($request->all());
	$tip_users=DB::table('tip_users')->where('fullname', Cookie::get('user_name'))->get()->ToArray();
	 $contentUrl=DB::table('tip_content')->Insert([
	 "content_header"=>$request->content_header,
	 "video_url"=>$request->video_url,
	 "content_body"=>$request->content_body,
	"study_id"=>$request->study_id,
	 "created_by"=>$tip_users[0]->id,
	 "content_type"=>$request->content_type,
	"active"=>$request->active,
	"public"=>$request->public,
	"content_lang"=>$request->content_lang,
	 "parent_id"=>$request->parent_id,
	 "date_created"=>\Carbon\Carbon::now(),
	"content_tab_id"=>$request->content_tab_id]);
	
	if($contentUrl=true){
	return back()->with("success","content Url added successfully");
	}
	else{
	return back()->with("error","content Not Added");
	}

}



Public function postcontentcalculator(Request $request){
//dd($request->all());
$tip_users=DB::table('tip_users')->where('fullname', Cookie::get('user_name'))->get()->ToArray();
 $contentUrl=DB::table('tip_content')->Insert([
 "content_header"=>$request->content_header,
  "content_body"=>$request->content_body,
  "study_id"=>$request->study_id,
  "created_by"=>$tip_users[0]->id,
  "content_lang"=>$request->content_lang,
"active"=>$request->active,
"content_tab_id"=>0
]);

if($contentUrl=true){
return back()->with("success","content Url added successfully");
}
else{
return back()->with("error","content Not Added");
}

}

 public function savePatientData1(Request $request)
    {
	
        $data = $request->except('_token');
	
       // $data['patient_id'] = $id;

        $save_data = $this->apiRequest('savePatientData', $data);
        $save_data = json_decode($save_data);
        if (isset($save_data->success)) {
            return redirect()->back();
        }
    }


}
