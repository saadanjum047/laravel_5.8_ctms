<?php

namespace App\Http\Controllers;

use App\Study;
use Carbon\Carbon;
use App\Study_Patient;
use App\Patient_Consent;
use Illuminate\Http\Request;

class PatientConsentController extends Controller
{
    public function index(){
        $study = auth()->user()->patient->study;
        return view('patient_consent.index' , compact('study') );
    } 

    public function SubmitConsent(Request $request){

        // dd($request->signature);
        $encoded_image = explode(",", $request->signature)[1];
        $decoded_image = base64_decode($encoded_image);
        // dd($decoded_image);

        $signature = auth()->user()->id.auth()->user()->name.".png";
        // dd($signature);
        file_put_contents( public_path()."/patient_signatures/".$signature , $decoded_image);
        
        // dd($request->all() , $sign);
        
        if(auth()->user()->patient->consent){
            $consent = Patient_Consent::findOrFail(auth()->user()->patient->consent->id)->update([
                'acceptence_date' => Carbon::now(),
                'status' => 1,
                'signature' => $signature,
                
            ]);
        }else{
        $consent = Patient_Consent::create([
            'study_id' => auth()->user()->patient->study->id,
            'patient_id' => auth()->user()->patient->id,
            'signature' => $signature,
            'acceptence_date' => Carbon::now(),
            'status' => 1,
        ]);
        }

        activity()->log('Patient Submitted his consent');

        if($consent)
        return redirect()->back()->with('success' , 'You have successfully submitted your agreement');
        else
        return redirect()->back()->with('error' , 'Some problem occoured');

    }

    public function showPatientConsents($study_id){
        $study = Study::findOrFail($study_id);
        $patients = Study_Patient::where('study_id' , $study_id)->get();

        return view('patient_consent.patient' , compact('patients' , 'study') );
    }

    public function deletePatientConsents(){
        // dd(auth()->user()->patient->consent);
        $consent = auth()->user()->patient->consent;
        $consent->signature = NULL;
        $consent->acceptence_date = NULL;
        $consent->status = 0;
        $consent->save();

        return redirect()->back()->with('success' , 'Consent has been deleted successfully');
    }
}
