<?php

namespace App\Http\Controllers;

use App\Site;
use App\Study_Patient;
use App\Exports\SiteExport;
use Illuminate\Http\Request;
use App\Exports\SchedulesExport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;


class ExportController extends Controller
{
    public function exportSchedules($patient_id){
        $patient = Study_Patient::findOrFail($patient_id);
        $schedules = $patient->patient_schedule;
        return Excel::download(new SchedulesExport($schedules) , 'Schedules.xlsx');
    }

    public function exportSite($site_id){
        
        $patients = Study_Patient::where('site_id' , $site_id)->get();
        return Excel::download(new SiteExport($patients), 'patients.xlsx');

    }

}
