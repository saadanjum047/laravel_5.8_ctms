<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pre_Screening_Answer;
use App\Pre_Screening_Question;
use App\Screening_Answers_Specification;

class PreScreeningAnswerController extends Controller
{
    public function store($question_id , Request $request){
        // dd($request->all() , $request->answer );

        $q = Pre_Screening_Question::find($question_id);

        if(isset($request->answerr)){
            $answer = $request->answerr;
        }
        elseif(isset($request->choices)){
            $answer = json_encode($request->choices);
        }
        elseif(isset($request->select)){
            $answer = $request->select;
        }

        $ans = Pre_Screening_Answer::create([
            'question_id' => $q->id,
            'screening_id' =>  $q->pre_screen->id,
            'investigator_id' => auth()->user()->sponsor_user->id,
            'answer_body' => $answer,
        ]);
        
        if(isset($request->choices) && isset($request->specifications) ){
            foreach($request->choices as $k=> $opt){
                // dump($opt , $request->specifications[$k] );
                Screening_Answers_Specification::create([
                    'answer_id' => $ans->id,
                    'name' => $opt,
                    'specification' => isset($request->specifications[$k]) ? $request->specifications[$k] : '',
                ]);
            }    
        }
        elseif(isset($request->select)){
            if(isset($request->specifications)){
                Screening_Answers_Specification::create([
                'answer_id' => $ans->id,
                'name' => $request->select,
                'specification' => isset($request->specifications) ? $request->specifications : '' ,
            ]);
        }
        }

        // $question = Pre_Screening_Question::findOrFail($question_id);
        
        // $ans = Pre_Screening_Answer::create([
        //     'screening_id' => $question->pre_screen->id,
        //     'question_id' => $question->id,
        //     'investigator_id' => auth()->user()->sponsor_user->id ,
        //     'answer_body' => $request->answer,
        // ]);

        if($ans){
        return redirect()->back()->with('success' , 'Answer added successfully');
        }else{
        return redirect()->back()->with('error' , 'Problem while adding Answer');
        }

    }


    public function update($question_id , $ans_id , Request $request){
        
        // dd('we are on our way');

        $ans = Pre_Screening_Answer::find($ans_id);
        // $ans = $q->ans->where('patient_id' , auth()->user()->patient->id)->first();


        if(isset($request->choices)){        
            foreach($ans->specifications as $spec){
                $spec->delete();
            }
        }
        if(isset($request->select)){
        
            foreach($ans->specifications as $spec){
                $spec->delete();
            }
        }


        if(isset($request->answerr)){
            $answer = $request->answerr;
        }
        elseif(isset($request->choices)){
            $answer = json_encode($request->choices);

        }
        elseif(isset($request->select)){
            $answer = $request->select;
        }
        $ans->answer_body = isset($answer) ? $answer : NULL ;
        $ans->save();

        if(isset($ans->specifications)){
        foreach($ans->specifications as $spec){
            $spec->delete();
        }}
        
        if(isset($request->choices) ){
            // foreach($ans->specifications as $spec){
            //     $spec->delete();
            // }
            if(isset($request->specifications)){
            foreach($request->choices as $k=> $opt){
                Screening_Answers_Specification::create([
                    'answer_id' => $ans->id,
                    'name' => $opt,
                    'specification' => isset($request->specifications[$k]) ? $request->specifications[$k] : '' ,
                ]);}
            }    
        }
    
        elseif(isset($request->select)){
            // foreach($ans->specifications as $spec){
            //     $spec->delete();
            // }
            if(isset($request->specifications)){
                Screening_Answers_Specification::create([
                'answer_id' => $ans->id,
                'name' => $request->select,
                'specification' => isset($request->specifications) ? $request->specifications : '',
            ]);
        }
    }
    
        // $question = Pre_Screening_Question::findOrFail($question_id);
        // $ans = $question->ans->where('investigator_id' , auth()->user()->sponsor_user->id )->first();

        // // $ans = Pre_Screening_Answer::findOrFail($request->answer_id);
        // $ans->answer_body = $request->answer;
        // $ans->save();    

        if($ans->save()){
        return redirect()->back()->with('success' , 'Answer updated successfully');
        }else{
        return redirect()->back()->with('error' , 'Problem while updating Answer');
        }
    }


    public function destroy($answer_id){
        $ans = Pre_Screening_Answer::findOrFail($answer_id);

        if($ans->delete()){
            return redirect()->back()->with('success' , 'Answer deleted successfully');
            }else{
            return redirect()->back()->with('error' , 'Problem while deleting Answer');
            }
    }


    public function StoreAllAnswers(Request $request){
    
    //  foreach($request->q as $q){
    //      if($request['answer_id'.$q] == '0' ){
    //          dump('answer not found');
    //      }
    //     $ans = $request['answer'.$q] ;
    //     if(is_array($ans)){
    //         dump($ans  );

    //     }
    //     elseif(!is_null($ans)){
    //         dump( $ans . ' : not null');
    //     }else{
    //         dump($ans);
    //     }
    //  }
        // $question_number = intval(preg_replace('/[^0-9]+/', '', $string), 10);
            // dd('stop');
        // dd($request->all());
    

    foreach($request->q as $q){

        if(!isset($request['answer'.$q]) || is_null($request['answer'.$q]) ){

            if($request['answer_id'.$q] != '0'){
                $delete_answer = Pre_Screening_Answer::findOrFail($request['answer_id'.$q])->delete();
            }
    
            // dump('answeer found but not here -> DELETEION : ' .$request['answer_id'.$q] );
        }elseif(isset($request['answer'.$q])){

        // dump($request['answer'.$q]);
        
        $ans = $request['answer'.$q];

        if(is_array($ans)){
            $ans = json_encode($ans);
        }
        if($request['answer_id'.$q] == '0' ){
            // dump('create' . $q);
            $q = Pre_Screening_Question::findOrFail($q);
            $answer = Pre_Screening_Answer::create([
                'question_id' => $q->id,
                'screening_id' =>  $q->pre_screen->id,
                'investigator_id' => auth()->user()->sponsor_user->id,
                'answer_body' => $ans,
            ]);

        }else{
            // dump('update answer_id : '.$request['answer_id'.$q] );
            Pre_Screening_Answer::findOrFail($request['answer_id'.$q])->update([
                'answer_body' => $ans,
            ]);
        }


    }
    }
    return redirect()->back()->with('success' , 'Answers are updated');
    // loop ends
        // dd($request->all());


}

}
