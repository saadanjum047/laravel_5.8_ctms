<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Study;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index($study_id){
        $study = Study::findOrFail($study_id);
        $tags = Tag::where('sponsor_id' , auth()->user()->sponsor_user->id )->get();

        return view('sponsor_newsletter.user_tags.index' , compact('study' , 'tags'));
        
    }

    public function create($study_id){

        $study = Study::findOrFail($study_id);
        $users = User::all();
        return view('sponsor_newsletter.user_tags.create' , compact('study' , 'users'));

    }

    public function store($study_id , Request $request){
        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
            'users' => 'sometimes',
        ]);
            // dd($request->all());
        $tag = Tag::create([
            'sponsor_id' => auth()->user()->sponsor_user->id ,
            'name' => $request->name,
            'description' => $request->description,
        ]);

        $tag->users()->sync($request->users);

        if($tag){
            return redirect('/dashboard/study/'.$study_id.'/tags')->with('success' , 'Tag added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

    }


    public function edit($study_id , $tag_id){
        $study = Study::findOrFail($study_id);
        $tag = Tag::findOrFail($tag_id);
        $users = User::all();

        return view('sponsor_newsletter.user_tags.edit' , compact('study' , 'tag' , 'users'));

    }

    public function update($study_id , $tag_id , Request $request){
        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
            'users' => 'sometimes',
        ]);
            // dd($request->all());
        $tag = Tag::findOrFail($tag_id)->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        $tag = Tag::findOrFail($tag_id);

        $tag->users()->sync($request->users);

        if($tag){
            return redirect('/dashboard/study/'.$study_id.'/tags')->with('success' , 'Tag added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

    }

    public function delete( $study_id , $tag_id){
        $tag = Tag::findOrFail($tag_id);
        
        // dd($tag);
        
        $tag->users()->sync([]);

        if($tag->delete()){
        return redirect()->back()->with('success' , 'Tag deleted successfully');
        }else{
        return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }


}
