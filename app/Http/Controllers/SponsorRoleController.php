<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Modules;
use App\Organization;
use App\Sponsor_Role;
use App\Sponsor_Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SponsorRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role_id != 2 ){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }


        $roles = Sponsor_Role::with('orgs')->where('sponsor_id' , auth()->user()->id )->get();
        $organizations = Organization::where('user_id' , auth()->user()->id)->get();
        return view('sponsor_roles.index' ,compact('roles' , 'organizations') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->role_id != 2 ){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }

        $modules = Modules::all();
        return view('sponsor_roles.permissions' , compact('modules') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'role' => 'required',
        ]);

        $role = Sponsor_Role::create([
            'name' => $request->role,
            'sponsor_id' => auth()->user()->id,
        ]);

        $role->permissions()->sync($request->per);
    
        if($role)
        return redirect('/dashboard/sponsor_roles')->with('success' , 'Roles created successfully');
        else
        return redirect()->back()->with('error' , 'Some problem while creating roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sponsor_Role  $sponsor_Role
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sponsor_Role  $sponsor_Role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   

        if(auth()->user()->role_id != 2 ){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }

        $role = Sponsor_Role::findOrFail($id);
        if($role->sponsor_id != auth()->user()->id){
            return redirect()->back()->with('error' , 'You can not edit that permissions');
        }
        $roles = Sponsor_Role::where('sponsor_id' , auth()->user()->id)->get();
        $modules = Modules::all();
        
        return view('sponsor_roles.permissions' , compact('role' ,'roles', 'modules') );

    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sponsor_Role  $sponsor_Role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   

        $request->validate([
            'role_id' => 'required',
            'role' => 'required',
        ]);

        $role = Sponsor_Role::findOrFail($request->role_id);
        $role->name = $request->role;

        $role->permissions()->sync($request->per);
        $role->save();
        
        if($role)
        return redirect('/dashboard/sponsor_roles')->with('success' , 'Roles updated successfully');
        else
        return redirect()->back()->with('error' , 'Some problem while updating roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sponsor_Role  $sponsor_Role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sponsor_Role $role)
    {    
        if(auth()->user()->role_id != 2 ){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }

        // dd($role , $role->permissions);
        if($role->permissions){
            $role->permissions()->sync([]);
    }
        if($role->delete())
        return redirect()->back()->with('success' , 'Role Deleted successfully');
        else
        return redirect()->back()->with('error' , 'Some problem while deleting role');
    
    }

    public function modules(){
        if(auth()->user()->role_id != 2 ){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }

        $modules = Modules::all(); 
        return view('sponsor_roles.modules' , compact('modules'));
    }

    public function save(Request $request){


        $request->validate([
            'module' => 'required | string'
        ]);

        $module = Modules::create([
            'name' => $request->module,
            'user_id' => auth()->user()->id,
        ]);


        if($module){
            $per = Sponsor_Permission::create([
                'module_id' => $module->id,
                'name' => 'view',
            ]);
         $per = Sponsor_Permission::create([
                'module_id' => $module->id,
                'name' => 'update',
            ]);

            $per = Sponsor_Permission::create([
                'module_id' => $module->id,
                'name' => 'create',
            ]);
            
         $per = Sponsor_Permission::create([
                'module_id' => $module->id,
                'name' => 'delete',
            ]);
        

            if($per)
                return redirect()->back()->with('success' , 'Roles created successfully');
                else
                return redirect()->back()->with('error' , 'Some problem while creating roles');
            
        }

    }
    public function delete(Modules $mod){
        
        if(auth()->user()->role_id != 2 ){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }

        if(count($mod->permissions) > 0){
            foreach($mod->permissions as $per){
                $per->delete();
            }
      }

        if($mod->delete())
        return redirect()->back()->with('success' , 'Modules deleted successfully');
        else
        return redirect()->back()->with('error' , 'Some problem while deleting modules');
        
    }


}
