<?php

namespace App\Http\Controllers;

use App\Study;
use App\Study_Diary;
use Illuminate\Http\Request;
use App\Study_Diary_Question;
use App\Translated_Questions;
use App\Translated_Question_Specification;

class TranslatedQuestionController extends Controller
{
    public function index($study_id , $diary_id , $question_id ){

    $study = Study::findOrFail($study_id);        
    $diary = Study_Diary::findOrFail($diary_id);        
    $question = Study_Diary_Question::findOrFail($question_id);   
    
    return view('PatientDiary.translated_question.index' , compact('study' , 'diary' , 'question') );
    }

    public function create($study_id , $diary_id , $question_id){
        $real_question = Study_Diary_Question::findOrFail($question_id);   

    return view('PatientDiary.translated_question.create' , compact('real_question') );

    }

    public function store($study_id , $diary_id , $real_question , Request $request){

        if(!isset($request->answers) && $request->answer_type != 1 ){
            return redirect()->back()->with('error' , 'You need to select atleast one answer');
        }

        $request->validate([
            "question" => 'required',
            "diary_id" => 'required',
            "answer_type" => "required",
            'answers' => 'sometimes',
            'specifications' =>'sometimes',
            'notify' =>'sometimes',
            'integer' =>'sometimes',
            'lang' =>'required',
        ]);
        
        // dd($request->all());
        
        $question = Translated_Questions::create([

            'question_id' => $real_question,
            'question' => $request->question,
            'answer_type' => $request->answer_type,
            'notification_response' => isset($request->notify) ? $request->notify : NULL,
            'answers' => isset($request->answers) ? json_encode($request->answers) : NULL,
            'created_by' => auth()->user()->id,
            'score' => json_encode($request->integer) ?? NULL,
            'language' => $request->lang ?? NULL,
        ]);
        if(isset($request->specifications)){
        foreach($request->specifications as $spec){
            Translated_Question_Specification::create([
                'name' => $spec,
                'question_id' => $question->id,
            ]);
        }}
      

        if($question){
            return redirect("/dashboard/study/".$study_id."/diary/".$diary_id."/question/".$real_question."/versions")->with('success' , 'Question added successfully');
        }else{
            return redirect()->back()->with('error' , 'Sorry Some problem occourd');
        }
    }

    public function edit($study_id , $diary_id , $real_question , $question_id ){
    $question = Translated_Questions::findOrFail($question_id);
    $real_question = Study_Diary_Question::findOrFail($real_question);   
    $study = Study::findOrFail($study_id);        
    $diary = Study_Diary::findOrFail($diary_id);   

    return view('PatientDiary.translated_question.create' , compact('real_question' , 'question', 'diary' , 'study') );
        
    }


    public function update( $study_id , $diary_id ,  $real_question_id , $question_id , Request $request ){
        if(!isset($request->answers) && $request->answer_type != 1 ){
            return redirect()->back()->with('error' , 'You need to select atleast one answer');    
        }

        $request->validate([
            "question" => 'required',
            "answer_type" => "required",
            'answers' => 'sometimes',
            'integer' =>'sometimes',

        ]);

            $question = Translated_Questions::findOrFail($question_id);

            $study = Study_Diary::findOrFail($request->diary_id);
            $question->question = $request->question;
            $question->notification_response = isset($request->notify) ? $request->notify : NULL ;
            $question->answer_type = $request->answer_type;
            $question->answers = isset($request->answers) ? json_encode($request->answers) : NULL;
            $question->score = json_encode($request->integer) ?? NULL ;

            if(isset($question->specifications)){
                foreach($question->specifications as $spec){
                    $spec->delete();
                    }
                }
                
            if(isset($request->specifications)){
            foreach($request->specifications as $spec){
                Study_Question_Specification::create([
                    'name' => $spec,
                    'question_id' => $question->id,
                ]);
            }
            }
            
            if($question->save())
            return redirect("/dashboard/study/".$study_id."/diary/".$diary_id."/question/".$real_question_id."/versions")->with('success' , 'Question updated successfully');
        else
            return redirect()->back()->with('error' , 'Sorry Some problem occourd');

    }



    public function remove($question_id){
        $question = Translated_Questions::findOrFail($question_id);
        $question->delete();
        return redirect()->back()->with('success' , 'Question version deleted successfiully');
    }

}
