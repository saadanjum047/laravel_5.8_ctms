<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class SiteController extends Controller
{
    protected $template;

    /**
     * @return mixed|string
     * @throws GuzzleException
     */
    public function geUser()
    {
        return $this->apiRequest([], 'GetUser');
    }

    /**
     * @param $action
     * @param array $data
     * @param array $attributes
     * @param bool $send_token
     * @param bool $jwt_token
     * @return mixed|string
     * @throws GuzzleException
     */
    public function apiRequest($action, $data = [], $send_token = true, $jwt_token = false, $multipart = null)
    {
        if ($send_token) {
            if (!Cookie::get('jwt_token') && !$jwt_token) {
                return 'Token not found!';
            }

            if (!$jwt_token) {
                $jwt_token = Cookie::get('jwt_token');
            }

            $headers = [
                'Authorization' => 'Bearer ' . $jwt_token,
                'Content-Type' => 'multipart/form-data',
                'X-API-Key' => 'abc345'
            ];
        } else {
            $headers = [
                'Content-Type' => 'multipart/form-data',
                'X-API-Key' => 'abc345'
            ];
        }

        $client = new GuzzleHttp\Client();
        try {

            if ($multipart) {
                $res = $client->request('POST', env('API_URL') . $action, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $jwt_token,
                        'Content-Type' => 'multipart/form-data',
                    ],
                    'multipart' => $multipart
                ]);

            } else {
                $res = $client->request('POST', env('API_URL') . $action, [
                    GuzzleHttp\RequestOptions::HEADERS => $headers,
                    GuzzleHttp\RequestOptions::JSON => $data,
                ]);
            }

        } catch (GuzzleHttp\Exception\RequestException $e) {
            if ($e->hasResponse()) {
                return json_encode($e->getResponse());
            } else {
                return 'yeah';
            }
        }

        if ($res->getStatusCode() !== 200) {
            return 'Status Code !200';
        }

        $responseData = json_decode((string)$res->getBody());

        if (isset($responseData->status) && ($responseData->status === 'Success' || $responseData->status === 'Fail')) {
            return $responseData;
        } else {
            return (string)$res->getBody();
        }
    }

    /**
     * @param array $vars
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render($vars = [])
    {
        $user = $this->apiRequest('GetUser');

        if(isset($user->data)) {
            $vars['user'] = $user->data->data;
        }

        return view($this->template)->with($vars);
    }


    public function  ViewSites($Study_id){
		$data =['study_id'=>$Study_id,
			'user_role'=>Cookie::get('user_role'),
			'user_name'=>Cookie::get('user_name')
			];
		$Study=$Study_id;
	    $response= $this->apiRequest('ViewAllSites', $data);
	//dd($response);
	    return view('site.index',compact('response','Study'));
	    }
	

    

    public function  CreateNewSite($Study_id){
		$data =['study_id'=>$Study_id,
			'user_role'=>Cookie::get('user_role'),
			'user_name'=>Cookie::get('user_name')
			];
		$Study=$Study_id;
		$response= $this->apiRequest('CreateNewSite', $data);
		// dd($response->data[0]->uuid);
		// dd($response);
	
		if($response->status == "Success"){		
		return view('site.create',compact('Study','response'));
    }
}
		





 public function  PostNewSite(Request $request){
 $data =$request->except('_token');
//dd($data);
$response= $this->apiRequest('PostNewSite', $data);
//dd($response);
if($response->status =='Success'){
return back()->with('success', $response->Message);
}
else{
return back()->with('error','Sorry Try again');
}
    
}


	public function  editsite($study_id, $site_id){
		$data =['study_id'=>$study_id,
			'user_role'=>Cookie::get('user_role'),
			'user_name'=>Cookie::get('user_name')
			];
		$Study=$study_id;
		$response= $this->apiRequest('CreateNewSite', $data);
		$site= $this->apiRequest('GetSinglesite', $site_id);
		$tipstudysite=$site->tipstudysite;
		$site=$site->site;
	        
		return view('site.edit',compact('Study','site_id','response','site','tipstudysite'));

	}
	
	
	public function  updatesite(Request $request){
		$data =[
			'user_role'=>Cookie::get('user_role'),
			'user_name'=>Cookie::get('user_name'),
			'site'=>$request->except('_token')
			];
		//dd($data);
		//$Study=$study_id;
		$response= $this->apiRequest('UpdateSingleSite', $data);
		//$site= $this->apiRequest('GetSinglesite', $site_id);
		//$tipstudysite=$site->tipstudysite;
		//$site=$site->site;
	       // dd($response);
	
	if($response->status =='Success'){
	return back()->with('success', $response->Message);
	}
	else{
	return back()->with('error','Sorry Try again');
	}
	
		
	
	}




}

