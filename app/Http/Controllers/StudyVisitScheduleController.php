<?php

namespace App\Http\Controllers;

use App\Study;
use App\Patient;
use App\Study_Patient;
use App\Patient_Schedule;
use Illuminate\Http\Request;
use App\Study_Visit_Schedule;
use App\Http\Controllers\Controller;
use URL;

class StudyVisitScheduleController extends Controller
{
    public function index(){
        $study_ids = Study::where('created_by' , auth()->user()->id)->pluck('id')->toArray();
        if(auth()->user()->role_id == 2){
        $schedules = Study_Visit_Schedule::whereIn('study_id' , $study_ids)->get();
        }elseif(auth()->user()->role_id == 3){
        $schedules = Study_Visit_Schedule::all();
        }

        $studies = Study::where('created_by' , auth()->user()->id)->get();
        $patients = Study_Patient::with('user')->get();
        return view('study_schedule.index' , compact('schedules' , 'studies','patients' ));

    }


    public function store(Request $request , $id){
        // dd($request->all());
        if(auth()->user()->role_id == 3){               
        if( auth()->user()->study_user->where('study_id' , $id)->first()->role_detail->permissions->count() > 0 ){
            $array = auth()->user()->study_user->where('study_id' , $id)->first()->role_detail->permissions->where('name' , 'create')->where('module_id' , 12 )->first();
            if(is_null($array)){
            return redirect()->back()->with('error', 'You are not allowed to create visits');
            }
        }}
        // $request->validate([
        //     "visit_name" => "required",
        //     "low_target" => "required",
        //     "target" => "required",
        //     "high_target" => "required",
        // ]);
        $study = Study::findOrFail($id);
            
       
        if(isset($request->visit_name) ){

        foreach($request->visit_name as $k => $name){
            $ids = $study->schedules->whereNotIn('id', $request->schedule_id)->pluck('id')->toArray();
           
            $end = $k;
            if(isset($request->schedule_id[$k])){
                $schedule = Study_Visit_Schedule::find($request->schedule_id[$k])->update([
                    'visit_name' => !is_null($name) ? $name : ' ' ,
                    'target' => !is_null($request->target[$k]) ? $request->target[$k] : 1 ,
                    'window_low' => !is_null($request->low_target[$k]) ? $request->low_target[$k] : 1,
                    'window_high' => !is_null($request->high_target[$k]) ? $request->high_target[$k] : 1,
                ]);
            }elseif(!isset($request->schedule_id[$k])){
            // dump($name , $request->low_target[$k] , $request->target[$k] , $request->high_target[$k] );
            $schedule = Study_Visit_Schedule::create([
                'visit_name' => !is_null($name) ? $name : ' ' ,
                'study_id' => $study->id,
                'target' => !is_null($request->target[$k]) ? $request->target[$k] : 1 ,
                'window_low' => !is_null($request->low_target[$k]) ? $request->low_target[$k] : 1,
                'window_high' => !is_null($request->high_target[$k]) ? $request->high_target[$k] : 1,
            ]);
        }

        }
        }else{
        foreach($study->schedules as $schedule){   
                $schedule->delete();
            }        
        }
        // $sch = $study->schedules->whereNotIn('id' , $ids);
        if(isset($ids)){
        for($i = 0 ; $i < count($ids) ; $i++){
           $sch = Study_Visit_Schedule::findOrFail($ids[$i]);
           $sch->delete();
        }
        }

        // if($schedule)
        return redirect()->back()->with('success' , 'Patient scheduled successfully');
        // else
        // return redirect()->back()->with('error' , 'Problem while scheduling');

    }



    // public function store(Request $request, $id){

    //     $request->validate([
    //                 "name" => "required",
    //                 "target" => "required",
    //                 "window_low" => "required|integer",
    //                 "window_high" => "required|integer",
    //             ]);

    //             $schedule = Study_Visit_Schedule::create([
    //                 'visit_name' => json_encode($request->name),
    //                 'study_id' => $id,
    //                 'target' => json_encode($request->target),
    //                 'window_low' => $request->window_low,
    //                 'window_high' => $request->window_high,
    //             ]);
    //             if($schedule)
    //             return redirect()->back()->with('success' , 'Patient scheduled successfully');
    //             else
    //             return redirect()->back()->with('error' , 'Problem while scheduling');
    // }

    //     public function update(Request $request , $id){
    //     $request->validate([
    //         "name" => "required",
    //         "target" => "required",
    //         "window_low" => "required|integer",
    //         "window_high" => "required|integer",
    //         'schedule' => 'required',
    //     ]);

    //     $schedule = Study_Visit_Schedule::find($request->schedule)->update([
    //         'visit_name' => json_encode($request->name),
    //         'target' => json_encode($request->target),
    //         'window_low' => $request->window_low,
    //         'window_high' => $request->window_high,
    //     ]);

    //     if($schedule)
    //     return redirect()->back()->with('success' , 'Patient scheduled updated successfully');
    //     else
    //     return redirect()->back()->with('error' , 'Problem while updateing schedule');
    // }


    // public function store(Request $request){
    // //    dd($request->all());
    //     $request->validate([
    //         "name" => "required",
    //         "study" => "required|integer",
    //         "target" => "required",
    //         "window_low" => "required|integer",
    //         "window_high" => "required|integer",
    //     ]);

    //     $schedule = Study_Visit_Schedule::create([
    //         'visit_name' => json_encode($request->name),
    //         'study_id' => $request->study,
    //         'target' => json_encode($request->target),
    //         'window_low' => $request->window_low,
    //         'window_high' => $request->window_high,
    //     ]);
    //     if($schedule)
    //     return redirect()->back()->with('success' , 'Patient scheduled successfully');
    //     else
    //     return redirect()->back()->with('error' , 'Problem while scheduling');
    // }



    // public function update(Request $request){

    //     $request->validate([
    //         "name" => "required",
    //         "study" => "required|integer",
    //         "target" => "required",
    //         "window_low" => "required|integer",
    //         "window_high" => "required|integer",
    //         'schedule' => 'required',
    //     ]);

    //     $schedule = Study_Visit_Schedule::find($request->schedule)->update([
    //         'visit_name' => json_encode($request->name),
    //         'patient_id' => $request->patient,
    //         'study_id' => $request->study,
    //         'target' => json_encode($request->target),
    //         'window_low' => $request->window_low,
    //         'window_high' => $request->window_high,
    //     ]);

    //     if($schedule)
    //     return redirect()->back()->with('success' , 'Patient scheduled updated successfully');
    //     else
    //     return redirect()->back()->with('error' , 'Problem while updateing schedule');
    

    // }



    public function Schedules($id){

     


        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if( empty( auth()->user()->study_user->where('study_id' , $id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 12 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }


        $study = Study::find($id);

        // return view('studies.schedules' , compact('study'));
        return view('study.schedule' , compact('study'));

    }


    public function destroy(Study_Visit_Schedule $schedule){

        if(auth()->user()->role_id != 2 && auth()->user()->role_id != 3){
            return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
        }elseif(auth()->user()->role_id == 3)
        {   
            if(auth()->user()->role_id == 3){
            if(  empty( auth()->user()->study_user->where('study_id' , $id)->first()->role_detail->permissions->where('name' , 'delete')->where('module_id' , 12 )->toArray() ) ){
                return redirect()->back()->with('error' , 'Sorry you are not allowed to visit');
                }
            }
        }

        if($schedule->delete()){
            return redirect()->back()->with('success' , 'Patient schedule deleted successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem while deleting');
        }
    }

    public function showScheduledPatients(){
        $study_ids = Study::where('created_by' , auth()->user()->id)->pluck('id')->toArray();
        $schedules = Patient_Schedule::whereIn('study_id' , $study_ids)->get();
        // dd($schedules);
        return view('patient_schedule.index' , compact('schedules'));
    }


}
