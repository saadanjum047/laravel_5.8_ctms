<?php

namespace App\Http\Controllers;
use DB;
use Hash;
use Excel;

use App\Site;
use App\User;
use App\Study;
use GuzzleHttp;
use App\Patient;
use Carbon\Carbon;
use App\Group_Task;
use App\Study_site;
use App\Study_Diary;
use App\Study_Patient;
use App\Diary_Schedule;
use App\Patient_Points;
use Twilio\Rest\Client;
use App\Patient_Consent;
use App\Content_Language;
use App\Patient_Schedule;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Study_Visit_Schedule;
use App\Exports\PatientsExport;
use App\Exports\SchedulesExport;
use App\Patient_Schedule_Change;
use App\Patient_Engagement_Question;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\FromCollection;


class PatientsController extends SiteController
{
    public function __construct()
    {
        $this->template = 'patient.frame';
    }

    public function index(){
        $patients = Study_Patient::with('user')->where('created_by' , auth()->user()->id)->get();
        // dd($patients);
        return view('patient.list' , compact('patients'));

    }

    public function view(){

        $study_ids = auth()->user()->study_user->pluck('study_id')->toArray() ;
        $studies = Study::whereIn('id' , $study_ids )->get();        
        $languages = Content_Language::all();
        return view('patient.register_patient' , compact('studies' , 'languages'));
    }




    public function store(Request $request){

        $request->validate([
            "name" => "required",
            "study_id" => "required",
            "language" => "required",
            "study_patient_id" => "required",
            "phone" => "sometimes",
            "email" => "required | unique:users",
            "enrollment_date" => "required",
        ]);

        $study = Study::find($request->study_id);
        $site = Study_site::where('study_id' , $study->id)->pluck('site_nbr')->first();
        if(!isset($site)){
        $site = 0;
        }
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => 4,
        ]);


        $study_patient = Study_Patient::create([
            'patient_id' => $user->id,
            'study_id' => $study->id,
            'site_id' => auth()->user()->sponsor_user->site_id ,
            'phone' => $request->phone,
            'language' => $request->language,
            'patient_nbr' =>   $site."-".$request->study_patient_id ,
            'enrollment_date' => Carbon::parse($request->enrollment_date) ,
            'created_by' => auth()->user()->id,
            'confirmation_token' => Hash::make(Str::random()),
        ]);

        $consent = Patient_Consent::create([
            'study_id' => $study->id,
            'patient_id' => $study_patient->id,
            'status' => 0,
            ]);

        $ids = Study_Patient::where('study_id' , $study->id )->pluck('id')->toArray();
        $tasks = Group_Task::where('study_id' , $study->id)->get();
        foreach($tasks as $task){
            if(isset($task->patients)){
                $task->patients()->sync($ids);
            }
        }

        $total_questions = Patient_Engagement_Question::where('study_id' , $study->id)->get()->count();
        $points = Patient_Points::create([
            'patient_id' => $study_patient->id,
            'earned_score' => 0,
            'total_score' => 10 * $total_questions ,
            'total_questions' => $total_questions,
            'answered_questions' => 0,
        ]);

        if($study->diary->count() > 0 ){
            foreach($study->diary as $diary){
            for($i = 1; $i <= $diary->frequency; $i++ ){
                $patient_diary = Diary_Schedule::create([
                    'study_id' => $study->id,
                    'patient_id' => $study_patient->id,
                    'diary_id' => $diary->id,    
                    'frequency' => $i,    
                ]);
            }
            }
        }

        $date = Carbon::parse($request->enrollment_date);

        Patient_Schedule::create([
            'visit_name' => "First Visit" ,
            'patient_id' => $study_patient->id,
            'study_id' => $study->id,
            'visit_date' => $date
        ]);

        if($study->schedules->count() > 0){
        
            foreach($study->schedules as $k => $schedule){

                $date->addDays($schedule->target);
                Patient_Schedule::create([
                    'visit_name' => $schedule->visit_name,
                    'patient_id' => $study_patient->id,
                    'study_id' => $study->id,
                    'visit_date' => $date
                ]);
    
                // $new = Carbon::parse($order[0]->expire_date)->addMonths(6);
            }
            }


            // working

            $sid    = "AC02e773c869e004f461302f2508d5c8f8";
            $token  = "bf92cf43e8c0c69aec28ddfc2e27b548";
            $from_number = '+18566444445';
            $url = url('/').'/patient/'.$study_patient->id.'/confirmation/'.$study_patient->confirmation_code;
            $client = new Client($sid, $token);

            $client->messages->create(
                ''.$study_patient->phone, // Text this number
                [
                'from' => $from_number, // From a valid Twilio number
                'body' => 'Email verification link from StudyPal! <br>
                    Hello'.$user->name.'. Your Email: '.$user->email . '. To activate your account, click on the link. '. $url
                ]
            );

        if(isset($patient) && isset($study_patient)){
            return redirect('/dashboard/patient')->with('success' , 'Patient Registered successfully');
        }
        else{
        return redirect('dashboard/patient')->with('error' , '!Some problem while registring patient');
        }
           
    }




    public function show($study_id , $id){
        $study = Study::findOrFail($study_id);
        $patient = Study_Patient::with('user' , 'study', 'site')->findOrFail($id);
        $schedules = Study_Visit_Schedule::where('patient_id' , $patient->id)->get();


        $patient_study = $patient->study;
      
        foreach ($patient->study->diary->where('status' , 1) as $k => $diary){
            for($i = 1; $i<= $diary->frequency; $i++ ){
            $type = $diary->diary_schedules->where('patient_id' , $patient->id )->where('frequency' , $i )->first();
  
            if($diary->questions->count() != 0 ){
            foreach($diary->questions as $question){
              $answer_count = $diary->ans->where('patient_id' , $patient->id )->where('frequency' , $i )->count();
            }
            }else{
              $answer_count = 0;
            }
            
            $questions_count = $diary->questions->count();
  
  
            $due_date = $type->created_at->addDays($diary->end_duration) ;
            $current_date = Carbon::now();
            
            if($due_date > $current_date){
                $type->type = 0;
                $type->save();
            }elseif($due_date < $current_date){
              $type->type = 1;
              $type->save();
            }
            if($questions_count == $answer_count) {
              $type->type = 2;
              $type->save();
            }
          }
          }

        // $visits = Study_Visit_Schedule::where('study_id' , $study->id)->where('visit_name' , $schedule->visit_name )->get();

        
        // dd($id);
        return view('patient.show' , compact('patient' , 'schedules' , 'study'));
    }

    public function showOtherPage($id){
        
        $patient = Study_Patient::with('user' , 'study', 'site')->findOrFail($id);
        $schedules = Study_Visit_Schedule::where('patient_id' , $patient->id)->get();
        return view('patient.test1_show' , compact('patient' , 'schedules'));
    }




    public function update( Study_Patient $patient , Request $request){
        $request->validate([
            "patient_id" => "required",
            "patient_nbr" => "required",
            "name" => "required",
            "phone" => "required",
            "email" => "required",
            "exit_date" => "sometimes",
        ]);

        $patient->phone = $request->phone;
        $patient->patient_nbr = $request->patient_nbr;
        $patient->exit_date = $request->exit_date;
        $patient->save();
        
        $user = $patient->user;
        $user->name = $request->name;
        $user->email = $request->email;


        if($user->save())
            return redirect()->back()->with('success' , 'Patien updated successfully');
            else
            return redirect()->back()->with('error' , 'Problem while updateing data');

    }

    
    public function getNBR( $id  ){
        $study = Study::find($id);
        // $site = Study_site::where('study_id' , $id)->pluck('site_nbr')->first();
        $site = Study_site::where('study_id' , $id)->pluck('site_nbr')->first();
        if(!isset($site))
        $site = 0;
        return $site;
    }
    
    
    public function checkNBR( $id , $nbr){
        $study = Study::find($id);
        $site = Study_Patient::where('study_id' , $id)->pluck('patient_nbr')->toArray();

        // list($part1, $part2) = explode('-', $string);
        $sites = array_map( function($val) { 
        list($part1, $part2) = explode('-', $val);
        return $part2;
        } , $site );

       if(in_array( $nbr , $sites)){
            return 'used';
       }else{
        return 'available';

       }
    }

    public function patientimages($study_id){

        $patient_images = DB::table('patientsimages')->get();
        // dd($user );
        $study = Study::findOrFail($study_id);
        $patients = Study_Patient::with('user')->where('study_id' , $study_id)->get(); 

        return view('patientimages.index',compact( 'patients' ,'patient_images' , 'study'));
        }
        public function patientimagesupload($study_id){
            $study = Study::findOrFail($study_id);
            $patients = Study_Patient::where('study_id' , $study->id)->get();
            return view('patientimages.create' , compact('study' , 'patients'));

        }

        public function savePatientImage(Request $request){

            // if(file_exists(Input::file('image')->move(getcwd() . '/files/img/' . $post-    >image)))    
            // {
            //     unlink(Input::file('image')->move(getcwd() . '/files/img/' . $post-    >image));}
            
                if ($request->hasFile('patient_image')) {
                $extension = ".".$request->patient_image->getClientOriginalExtension();
                $image = basename($request->patient_image->getClientOriginalName(), $extension).time();
                $fileName = $image.$extension;
                $path = public_path().'/pat_images';
                // dd($fileName , $request->patient_image , $path);

                $uplaod = $request->patient_image->move($path,$fileName);
            }

            $img = DB::table('patientsimages')->insert([
                'patient_id' => $request->patient_id ,
                'patient_image' => $fileName ?? " " ,
            ]);

            if($img){
                return redirect()->back()->with('success' , 'Picture Saved successfully');
            }else{
                return redirect()->back()->with('success' , 'Picture Saved successfully');
            }
            

        }

    public function changeSchedule($study_id , $patient_id , Request $request){
        $visit = Patient_Schedule::findOrFail($request->visit_id);
        $visit->visit_date = $request->enrollment_date;
        $visit->save();
        if($visit->save())
        return redirect()->back()->with('success' , 'Visit Date has been changed successfully');
        else
        return redirect()->back()->with('error' , 'Some problem while changing visit ');

    }


    public function changePatientSite($study_id , $pat_id , Request $request){

        $patient = Study_Patient::findOrFail($pat_id);
        $patient->site_id = $request->site_id;
        $patient->save();
        if($patient->save())
        return redirect()->back()->with('success' , 'Site has been changed successfully');
        else
        return redirect()->back()->with('error' , 'Some problem while changing site ');
    }

    public function RequestChangePatientVisit( Request $request){

        Patient_Schedule_Change::create([
            'visit_id' => $request->visit_id,
            'date' => $request->date,
         ]);
        return redirect()->back()->with('success' , 'Visit Date request sent successfully');
    }

    public function ResetPatientVisit( $change_id){
        
        $change_visit = Patient_Schedule_Change::findOrFail($change_id);
        $change_visit->status = 1;
        $change_visit->save();
        $change_visit->schedule->visit_date = $change_visit->date;
        $change_visit->schedule->save();
        $change_visit->delete();
        return redirect()->back()->with('success' , 'Patient Visit date changed successfully');
    }




















    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        $this->template = 'patient.register_frame';

        $studies = json_decode($this->apiRequest('GetStudies'));
        $user = $this->apiRequest('GetUserWithSite');
        $user = json_decode($user);

        $vars['content'] = view('patient.register_patient')->with([
            'studies' => (array)$studies,
            'user' => $user
        ])->render();

        return $this->render($vars);
    }

    public function registerStore(Request $request)
    {
	//json_encode($request->all());
         //  dd($request->all());
		
        $data = $request->except('_token');
		

        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required|phone:AUTO',
            'study_patient_id' => 'required',
            'enrollment_date' => 'required',
            'study_id' => 'required',
            'language' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        //$data['patient_image'] = base64_encode(file_get_contents($request->file('patient_image')->getRealPath())) ;

        $response = $this->apiRequest('RegisterPatient', $data, true, false);
		
//dd( $response);

        if ($response->status ==='Success') {
            return redirect()->back()->with(['status' => 'Patient Successfully Added!']);
        } elseif ($response->status === 'Fail') {
            return redirect()->back()->withErrors($response->errors);
        }

    }

    // nbr function
    public function getLanguagesAndSiteNbr($study_id)
    {
        $data['study_id'] = $study_id;
        $languages = $this->apiRequest('GetLanguagesAndSiteNbr', $data);

        if (!isset($languages->status)) {
            return response()->json(json_decode($languages));
        } else {
            return response()->json([]);
        }
    }

    public function patientsList()
    {

        $response = $this->apiRequest('GetPatientsList');

        $patients = [];
        if (isset($response->status) && $response->status === 'Success') {
            $patients = $response->patients;
        }


        $vars['content'] = view('patient.list')->with('patients', $patients)->render();
        return $this->render($vars);

    }

    public function patientsNotes()
    {
        $response = $this->apiRequest('GetPatientsList');

        $patients = [];
        if (isset($response->status) && $response->status === 'Success') {
            $patients = $response->patients;
        }

        $response = $this->apiRequest('GetNotesList');

        $notes = [];
        if (isset($response->status) && $response->status === 'Success') {
            $notes = $response->notes;
        }

        $vars['content'] = view('patient.notes')->with(compact('patients', 'notes'))->render();
        return $this->render($vars);

    }

    public function patientsAddNotes(Request $request)
    {
        $data = $request->except('_token');
        $data['patient_image'] = base64_encode(file_get_contents($request->file('patient_image')->getRealPath()));

        $response = $this->apiRequest('CreatePatientNote', $data);

        if ($response->status === 'Success') {
            return redirect()->back()->with(['status' => $response->message]);
        } elseif ($response->status === 'Fail') {
            return redirect()->back()->withErrors($response->errors);
        }
    }

    // public function show($patient_id, $site_id = NULL)
    // {

    //     $data = [
    //         'patient_id' => $patient_id
    //     ];

    //     $response = $this->apiRequest('GetPatient', $data);
    //     if (!$response->patient) {
    //         return abort(404);
    //     }

    //     $site = null;
    //     if ($site_id !== NULL) {
    //         $data = [
    //             'site_id' => $site_id
    //         ];

    //         $site = $this->apiRequest('GetSiteById', $data);
    //         $site = json_decode($site);
    //     }
	

    //     $patient = $response->patient;

    //     $data = [
    //         'patient_id' => $patient->id
    //     ];
    //     $activity = $this->apiRequest('GetPatientActivity', $data);
	
    //     $activity = json_decode($activity);

    //     $schedules = $response->schedules;


    //     if (Cookie::get('user_role') == 3) {
    //         $patient->visit_history = collect($patient->visit_history);
    //     }


	// //dd($activity);
    //     $vars['content'] = view('patient.show')->with([
    //         'site' => $site,
    //         'patient' => $patient,
    //         'schedules' => $schedules,
    //         'activity' => $activity,
	//     //'sites'=>$activity->sites
    //     ])->render();
	
    //     return $this->render($vars);
    // }



public function SavePatientIntoSite(Request $request){
	$data = [
            'site_id' => $request->site_id,
            'patient_id' => $request->patient_id,
	    'table_id' => $request->table_id,
	    'old_site_id'=>$request->old_site_id,
	    'audit_id'=>$request->audit_id,
 	    'created_by' => $request->created_by


	 ];
	//dd($data);

	$response = $this->apiRequest('TransferPatientIntoSite', $data);
	//dd($response);
	$response =json_decode($response );
	

        if($response->status ="success"){
		
	return back()->with('ok','Patient Is Transferred Into Site Successfully'); 
	}else{
	return back()->with('error','try again'); 
	}
	}


    public function savePatientData(Request $request, $id)
    {
        $data = $request->except('_token');
        $data['patient_id'] = $id;

        $save_data = $this->apiRequest('savePatientData', $data);
        $save_data = json_decode($save_data);

        if (isset($save_data->success)) {
            return redirect()->back();
        }
    }

    public function SaveVisitDate(Request $request)
    {

        $data = $request->except('_token');

        $request->validate([
            'patient_id' => 'required',
            'enrollment_date' => 'required'
        ]);

        $response = $this->apiRequest('SaveVisitDate', $data);

        if ($response->status == 'Success') {
            return redirect()->back()->with('status', 'Visit Date Successfully Saved!');
        }
    }

    public function exportPatientsList($site_id = null, $study_id = null)
    {

        $data = [
            'site_id' => $site_id,
            'study_id' => $study_id
        ];

        $response = $this->apiRequest('GetPatientsList', $data);

        $patients = collect();
        if (isset($response->status) && $response->status === 'Success') {
            $patients = $response->patients;
            $patients_array = collect([]);
            foreach ($patients as $patient) {
                $schedules = collect($patient->schedules);
                $data = collect([
                    'id' => $patient->id,
                    'name' => $patient->name,
                    'phone' => $patient->phone,
                    'email' => $patient->email,
                    'created_at' => $patient->created_at,
                    'date_confirmed' => $patient->date_confirmed,
                    'info' => collect($patient->info),
					'site'=>collect($response->site),
                    'schedules' => $schedules,
                    'count_questions' => count($schedules)
                ]);

                $patients_array->push($data);
            }
            $patients = $patients_array->where('count_questions', '>', 0);
        }


        $schedules = collect();
        $questions = collect();
        $patients->each(function ($patient) use ($schedules, $questions) {
            foreach ($patient['schedules'] as $schedule) {
                foreach ($schedule->questions as $question) {
                    $questions->push($question);
                }
                $schedules->push($schedule);
            }
        });


        $data = [
            'patients' => $patients,
            'questions' => $questions->groupBy('question'),
            'schedules' => $schedules,
		'site' => collect($response->site)

        ];

        return Excel::download(new PatientsExport($data), 'patients-list.xlsx');
    }

    public function exportPatientAndSchedules($patient_id)
    {

        $data = [
            'patient_id' => $patient_id
        ];

        $response = $this->apiRequest('GetPatient', $data);

        if (!$response->patient) {
            return abort(404);
        }

		//dd($response->patient->info->patient_nbr);
        $patient = $response->patient;
		
        $schedules = $response->schedules;

        $schedules_data = [];
        foreach ($schedules as $schedule) {
            switch ($schedule->type) {
                case 0:
                    $schedule->type = 'Current';
                    break;
                case 1:
                    $schedule->type = 'Overdue';
                    break;
                case 2:
                    $schedule->type = 'Completed';
                    break;
            }

            $questions = [];
            foreach ($schedule->questions as $question) {
                $answers = [];

                foreach ($question->answers as $answer) {
                    $selected = '';
                    if ($answer->selected) {
                        $selected = ' ✔';
                    }
                    $answers[] = [
                        'answer' => $answer->answer . $selected,
                        'text' => $answer->text !== false ? $answer->text : '',
                    ];
                }

                $questions[] = [
                    'id' => $question->id,
                    'question' => $question->question,
                    'answers' => $answers,
                ];
            }

            $schedules_data[] = [
                'id' => $schedule->id,
				'patient_nbr'=>$response->patient->info->patient_nbr,
                'created_at' => $schedule->created_at,
                'type' => $schedule->type,
                'questions' => $questions
            ];
        }

        $data = $schedules_data;

        return Excel::download(new SchedulesExport($data), 'patient-schedules.xlsx');
    }



  public function PatientsDiaries($id){

   //dd($id);
  
     $response = $this->apiRequest('alldiaries', $id);
     //dd( $response);
 $activity = json_decode($response);

//dd($activity);



     $vars['content'] = view('patient.alldiaries')->with(['schedules' =>  $activity ])->render();

     // dd($vars);
        return $this->render($vars);

 }

// public function patientimages(){

// $patient_images = DB::table('patientsimages')->get();
// //dd($user );

// $response = $this->apiRequest('GetPatientsList'); 
// return view('patientimages.index',compact('response','patient_images'));
// }


public function addpatientimage(Request $request){
//dd( $Request->all());

		if ($request->hasFile('patient_image')) {
        $image = $request->file('patient_image');
        $name = $image->getClientOriginalName();
        $size = $image->getClientSize();
        $destinationPath = public_path('/pat_images');
        $image->move($destinationPath, $name);

           }


$save= DB::table('patientsimages')->insert(
    ['patient_id' => $request->patient_id, 'patient_image' =>  $name]
);

if($save=true){
return back()->with('success','Patient Image added Successfully');
}
else{
return back()->with('error','Sorry Patient not Image added ');
}



}

public function PatientSendSms( $study_id , $diary_id , $patient_id){
    
    // dd($study_id , $diary_id , $patient_id);
    $study = Study::findOrFail($study_id);

    $diary = Study_Diary::findOrFail($diary_id);


    $sid    = "AC02e773c869e004f461302f2508d5c8f8";
    $token  = "bf92cf43e8c0c69aec28ddfc2e27b548";
    $from_number = '+18566444445';
    // $client = new Client($sid, $token);
    $client = new \Twilio\Rest\Client($sid, $token);

    // $message = $client->messages("MM800f449d0399ed014aae2bcc0cc2f2ec")
    //                   ->fetch();
    // dd($message->body);
    
    $client->messages->create(
        // ''.$patient_phone[0], // Text this number
        '+923147637613', // Text this number
        [
            // 'from' => $this->config['twilio']['from_number'], // From a valid Twilio number
            'from' => $from_number, // From a valid Twilio number
            'body' => 'StudyPal Message: '. $diary->study->study_name . ',
        click here to answer '  
        // . $this->config['client_url'] . 'diary/' . $id . '/' . $auth_token[0] . ''
        ]
    );

    // dd($client);
// $response=$this->apiRequest('PatientToSendSms',$id);
//dd($response);
return redirect()->back()->with('success', 'Message Sent Successfully');

}



















    public function patient_confirmation($patient_id , $confirmation_token){
        // dd($confirmation_token);
        $patient = Study_Patient::findOrFail($patient_id);
        if($patient->confirmation_token == $confirmation_token){
            $patient->confirmation_date = Carbon::now();
            $patient->confirmed = 1;
            $patient->save();
            return redirect('/dashboard');
        }else{
            $url = url('/').'/patient/'.$patient_id.'/reset_confirmation';
            echo 'Your confirmation code does not matches <a href='.$url.'>Click Here to resend</a> ' ;
        }
    }

    public function patient_confirmation_reset($patient_id){

        $patient = Study_Patient::findOrFail($patient_id);

        $patient->confirmation_token = Hash::make(Str::random());
        $patient->confirmation_date = null;
        $patient->confirmed = 0;
        $patient->save();

        $sid    = "AC02e773c869e004f461302f2508d5c8f8";
        $token  = "bf92cf43e8c0c69aec28ddfc2e27b548";
        $from_number = '+18566444445';
        $url = '/patient/'.$patient->id.'/confirmation/'.$patient->confirmation_code;
        $client = new Client($sid, $token);

        $client->messages->create(
            ''.$patient->phone, // Text this number
            [
                // 'from' => $this->config['twilio']['from_number'], // From a valid Twilio number
                'from' => $from_number, // From a valid Twilio number
                'body' => 'You have been registered in : '. $study->study_name . ',
            click here to confirm your information'  
            . $url
            ]
        );

        echo "A message is send . Plz check inbox";
    }

}
