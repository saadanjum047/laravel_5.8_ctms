<?php

namespace App\Http\Controllers;
use DB;
use Hash;
use Excel;
use Session;
use App\Site;
use App\User;
use App\Study;
use App\Content;
use App\SiteUser;
use App\Tip_User;
use Carbon\Carbon;
use App\Study_site;
use App\Study_User;
use App\Study_Audit;
use App\Organization;
use App\Sponsor_Role;
use App\DiarySchedule;
use App\Study_Patient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Exports\PatientsExport;
use Spatie\ArrayToXml\ArrayToXml;
use App\Exports\StudyScheduleExports;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

class StudyController extends SiteController
{
    public function __construct()
    {
        $this->template = 'study.frame';
    }


    public function index(){
        $studies = Study::where('created_by' , auth()->user()->id)->get();
        return view('studies.index' , compact('studies'));
    }

    public function create(){
        $organizations = Organization::where('user_id' , auth()->user()->id)->get();
        return view('studies.create' , compact('organizations'));
    }

    public function store(Request $request){

        $request->validate([
            "organization" => "required",
            "protocol_id" => "required",
            "study_name" => "required",
            "study_title" => "required",
            "study_description" => "sometimes",
            "startdate" => "sometimes",
            "enddate" => "sometimes",
            "access_code" => "required",
            "background_img" => 'sometimes',
        ]);


        // some problem while uploading
        if ($request->hasFile('background_img')) {
            $extension = ".".$request->background_img->getClientOriginalExtension();
            $background_img = basename($request->background_img->getClientOriginalName(), $extension).time();

            $fileName = $background_img.$extension;
            // $path = $request->logo->storeAs( 'public/studies/' , $background_img );

            $path = public_path().'/studies';
            $uplaod = $request->background_img->move($path,$fileName);

            }

        
        $study = Study::create([
            'study_name' => $request->study_name,
            'study_title'  =>  $request->study_title,
            'study_description' =>  $request->study_description,
            'created_by' =>  auth()->user()->id,
            'protocol_id' =>  $request->protocol_id,
            'startdate' =>  $request->startdate,
            'enddate'  =>  $request->enddate,
            'sponsor_id'  =>  $request->organization,
            'bkground_img'  =>  $fileName ?? NULL,
            'access_code'  =>  $request->access_code,
            'protocol'  =>  null,
            'planned_patients'  =>  1,
            'workflow_level'  =>   1,
            'date_created' =>  (Carbon::now())->toDateString(),

              ]);
              
        if($study)
        return redirect()->route('dashboard.studies')->with('success' , 'Study Created Successfully');
        else
        return redirect()->route('dashboard.studies')->with('error' , 'Some problrm occoured while creating study');
    }


    public function edit(Study $study){
        $organizations = Organization::where('user_id' , auth()->user()->id)->get();
        return view('studies.edit' , compact('organizations' , 'study'));
    }




    public function update(Request $request , Study $study){

        // dd($request->all());
        $request->validate([
            "organization" => "required",
            "study_name" => "required",
            "study_title" => "required",
            "study_description" => "required",
            "startdate" => "required",
            "enddate" => "required",
            "background_img" => 'sometimes',
        ]);


        if ($request->hasFile('background_img')) {
            $extension = ".".$request->background_img->getClientOriginalExtension();
            $background_img = basename($request->background_img->getClientOriginalName(), $extension).time();

            $fileName = $background_img.$extension;
            // $path = $request->logo->storeAs( 'public/studies/' , $background_img );

            $path = public_path().'/studies';
            $uplaod = $request->background_img->move($path,$fileName);

            }

        

        $study = Study::find($study->id)->update([
            'study_name' => $request->study_name,
            'study_title'  =>  $request->study_title,
            'study_description' =>  $request->study_description,
            'protocol_id' =>  $request->protocol_id,
            'startdate' =>  $request->startdate,
            'enddate'  =>  $request->enddate,
            'sponsor_id'  =>  $request->organization,
            'bkground_img'  =>  $fileName ?? NULL,
            'access_code'  =>  $request->access_code,
            'protocol'  =>  null,
            'planned_patients'  =>  1,
            'workflow_level'  =>   1,
            'date_created' =>  (Carbon::now())->toDateString(),

              ]);

            if($study)
            return redirect()->route('dashboard.studies')->with('success' , 'Study Created Successfully');
            else
            return redirect()->back()->with('error' , 'Some problrm occoured while creating study');

    }



    public function viewUsers($study_id){
        
        $users = Tip_User::with('user')->where('study_id',$study_id )->get();
        $study = Study::findOrFail($study_id);
        return view('user.index' , compact('users' , 'study'));
    }

    public function CreateNewUser($study_id){
        $sponsor_roles = Sponsor_Role::where('sponsor_id' , auth()->user()->id )->get();
        $study = Study::findOrFail($study_id);
        return view('user.create' , compact('study', 'sponsor_roles'));
    }

    public function StoreNewUser(Request $request , $study_id){
        $request->validate([
            "firstname_input" => "sometimes",
            "lastname_input" => "sometimes",
            "email_input" => "required | unique:users,email,",
            "pass_input" => "required",
            "con_pass_input" => "required",
            "phone_input" => "sometimes",
            "addr_input" => "sometimes",
            "addr2_input" => "sometimes",
            "city_input" => "sometimes",
            "state_input" => "sometimes",
            "country_input" => "sometimes",
            "zip_input" => "sometimes",
            "add_new" => "sometimes",
            'role_id' => 'required',
        ]);


        if($request->pass_input != $request->con_pass_input ){
            return back()->with('error', 'Password does not match');
        }
        $user = User::create([
            'name' => $request->firstname_input,
            'email' => $request->email_input,
            'created_by' => auth()->user()->id ,
            'password' => Hash::make($request->pass_input),
            'role_id' => 3,
        ]);

        $tip_user = Tip_User::create([
            'username' => $request->firstname_input,
            'password' => Hash::make($request->pass_input),
            'fullname' => $request->firstname_input .' '.$request->lastname_input ,
            'address1' => $request->addr_input,
            'address2' => $request->addr2_input,
            'city' => $request->city_input,
            'zip' => $request->zip_input,
            'phone' => $request->phone_input,
            'country' => $request->country_input,
            'email' => $request->email_input,
            'state' => $request->state_input,
            'sponsor_id' => auth()->user()->id,
            'user_id' => $user->id,
            'study_id' => $study_id,
            'user_role' => $request->role_id,
        ]);

        $study = Study::findOrFail($study_id);
        
        if($tip_user){
        foreach($study->newsletters as $news ){            
          DB::table('newsletter_tip__user')->insert([
              'newsletter_id' => $news->id,
              'tip__user_id' => $tip_user->id,
          ]); 
        }
        }
        

        $role = Study_User::create([
            'study_id' => $study_id,
            'user_id' => $user->id,
            'created_by'  => auth()->user()->id,
            'role' => $request->role_id,
            'date_created' => Carbon::now(),
        ]);

        if($user && $tip_user){
            return redirect('/dashboard/study/'.$study_id.'/viewusers')->with('success','User created successfully');
        }else{
            return redirect()->back()->with('error', 'Problem while creating user');
        }
    }


    public function edituser( $study_id ,Tip_User $user){
        
        $sponsor_roles = Sponsor_Role::where('sponsor_id' , auth()->user()->id )->get();

        $study = Study::findOrFail($study_id);
        return view('user.edituser' , compact('study','user' ,'sponsor_roles'));
    }

    public function updateuser(Request $request , Tip_User $user){

        $request->validate([
            "firstname_input" => "sometimes",
            "lastname_input" => "sometimes",
            "email_input" => "required",
           
            "phone_input" => "sometimes",
            "addr_input" => "sometimes",
            "addr2_input" => "sometimes",
            "city_input" => "sometimes",
            "state_input" => "sometimes",
            "country_input" => "sometimes",
            "zip_input" => "sometimes",
            "add_new" => "sometimes",
        ]);
            
        $study_id = $user->study->id;
        
        $tip_user = Tip_User::find($user->id)->update([
            'username' => $request->firstname_input,
            'fullname' => $request->firstname_input .' '.$request->lastname_input ,
            'address1' => $request->addr_input,
            'address2' => $request->addr2_input,
            'city' => $request->city_input,
            'zip' => $request->zip_input,
            'phone' => $request->phone_input,
            'country' => $request->country_input,
            'email' => $request->email_input,
            'state' => $request->state_input,
            'user_role' => $request->role_id,

        ]);
        $user = User::find($user->user->id);

        $user->name = $request->firstname_input;
        $user->email = $request->email_input;
        $user->save();
        $user->study_user->role = $request->role_id;
        $user->study_user->save();

        
        if($user && $tip_user){
            return redirect('/dashboard/study/'.$study_id.'/viewusers')->with('success' , 'User updated successfully');
        }else{
            return back()->with('error' , 'Problem while updating user');
        }
    }



    public function viewSites($id){
        $study = Study::find($id);
        return view('site.index',compact('study'));

    }


    public function destroy($study_id , $user_id){

        $user = Tip_User::find($user_id);
        // dd($user , $user->user);
        if(!is_null($user->user->study_user)){
            foreach($user->user->study_user as $user){
            $user->delete();
        }
        }
        if(!is_null($user->user->site_user)){
        foreach($user->user->site_user as $user){
            $user->delete();
        }
            // dump('site_user');
        }
        $user->user->delete();
        $user->delete();

        return redirect()->back()->with('success' , 'User has been removed successfully');
    }

    public function showStudyGuide($study_id){
        // $document = Document::findOrFail($document_id);
        // $path = public_path().'/documents/'.$document->document;
        // if(file_exists($path)){
        //     return Response::download( $path , $document->document );
        // }else{
        //     return redirect()->back()->with('error' , 'Sorry! the file is not available on server anymore');
        // }
    }

















    // Old Functions


	public function studyschedule($study_id) {
	$data =[
	'study_id'=>$study_id,
	'user_role'=>Cookie::get('user_role')
	];

	$response= $this->apiRequest('StudySchedules', $data);
	//dd($response);
	if($response->status == "Success"){		
		return view('studyschedule.index',compact('response','study_id'));
		} else {
			return 'error';
		}
	
	}
	
	public function createschedule($study_id) {
		$data =[
		'study_id'=>$study_id,
		'user_role'=>Cookie::get('user_role')
		];
		
		$response= $this->apiRequest('CreateSchedule', $data);
		
		
			return view('studyschedule.createschedule',compact('response','study_id'));
			
	}
		
	public function StudyScheduleExport($study_id) {
		$data =[
		'study_id'=>$study_id,
		'user_role'=>Cookie::get('user_role')
		];
	
		 $data = $this->apiRequest('StudyScheduleExports',$data);
		 return Excel::download(new StudyScheduleExports($data), 'StudySchedule.xlsx');
	}
    
    public function show($id , $lang = 'English'){
        

        if(auth()->user()->role_id == 4 ){
            return redirect()->back()->with('error' , 'Sorry You cant access this page');
        }

        $tip_users = Tip_User::all();
        
        
        
        // $pat = Study_Patient::where('study_id' , $id)->get();
        // $patients_ids = Study_Patient::where('study_id' , $id)->toArray();
        $study = Study::findOrFail($id);
        
        if(auth()->user()->role_id == 2 ){
        $contents = Content::where('created_by' , auth()->user()->id )->get();
        }elseif(auth()->user()->role_id == 3 ){
        $contents = Content::where('created_by' , auth()->user()->created_by )->get();
        }
        
        $site_id = SiteUser::where('user_id' , auth()->user()->id)->first();
        
        $all_sites =  Study_site::where('study_id' , $study->id)->pluck('site_id')->toArray();
            
        if(count($all_sites) > 0 ){
        $all_patients = Study_Patient::whereIn('site_id' , $all_sites )->get();
        }

        if(!isset($site_id)){
            return view('study.show' , compact( 'study' , 'tip_users' , 'all_patients' , 'contents' ));   
        }
            
        $site_id = $site_id->site_user_id;

        // By the logic here must be role 3 
        if(auth()->user()->role_id == 3){
            
        $all_patients_ids = Study_Patient::where([
            ['study_id', $study->id],
            ['site_id', $site_id],
        ])->pluck('patient_id')->toArray();

        $incomplete_diary = DiarySchedule::where([
            ['study_id', $study->id],
            ['type', '!=', 2],
        ])->whereIn('patient_id', $all_patients_ids)
        // ->groupBy('patient_id')
        ->get()->count();

        $patients_ids = Study_Patient::where([
            ['study_id', $study->id],
        ])->pluck('patient_id')->toArray();


        // From 2nd DB
        $all_patients_count = Study_Patient::whereIn('id', $all_patients_ids)->count();
            // $all_patients_count = 100;

            // Already There
            // $patients_ids = Study_Patient::where([
            //         ['study_id', $study->id],
            //         ['site_id', $site_id],
            // ])->pluck('patient_id')->toArray();

            // Already Have + From other DB
            // $patients_count = Patient::whereIn('id', $patients_ids)->count();

            
            $patients_incomplete_diary = Diary_Schedule::where([
                ['study_id', $study->id] ,
                ['type', '!=', 2] ,
            ])->whereIn('patient_id' , $patients_ids)->get()->count();


        }elseif (auth()->user()->role_id == 2) {
            $patients_ids = Study_Patient::where([
                ['study_id', $study->id],
            ])->pluck('patient_id')->toArray();


            $incomplete_diary = DiarySchedule::where([
                ['study_id', $study->id],
                ['type', '!=', 2],
            ])->whereIn('patient_id', $patients_ids)
            // ->groupBy('patient_id')
            ->get()->count();
            // From other DB
            $patients_count = Study_Patient::whereIn('id', $patients_ids)->count();
            // $patients_count = 100;
            // $all_patients_count = 100;

            $patients_type_counts = [
                // Overdue
                DiarySchedule::groupBy('patient_id')
                    ->where('study_id', $study->id)
                    ->whereIn('patient_id', $patients_ids)
                    ->where('type', 1)->count(),
                // Current
                DiarySchedule::groupBy('patient_id')
                    ->where('study_id', $study->id)
                    ->whereIn('patient_id', $patients_ids)
                    ->where('type', 0)->count(),
                // Success
                DiarySchedule::groupBy('patient_id')
                    ->where('study_id', $study->id)
                    ->whereIn('patient_id', $patients_ids)
                    ->where('type', 2)->count()
            ];

            
            $patients_incomplete_diary = DiarySchedule::where([
                ['study_id', $study->id] ,
                ['type', '!=', 2] ,
            ])->whereIn('patient_id' , $patients_ids)->get()->count();


            $sites = Site::with('site')->where('created_by' ,auth()->user()->id)->get();
            $study_audit = Study_Audit::all();
            $user = User::with('site')->findOrFail(auth()->user()->id);
          
            }



            // $patients_incomplete_diary = DiarySchedule::where([
            //     ['study_id', $study->id] ,
            //     ['type', '!=', 2] ,
            // ])->whereIn('patient_id' , $patients_ids)->get()->count();


              // That we dont have any
            $procent_patients = 122;
            $patients = Study_Patient::where('study_id' , $study->id)->get();

            // foreach ($study->sites as $k => $site){
            //     $items[]= $site->site->site_name;
            //     if($k == 5)
            //     break;
            //     }
            //   dd($patients_incomplete_diary);

            // $all_sites =  Study_site::where('study_id' , $study->id)->pluck('site_id')->toArray();
            
            // if(count($all_sites) > 0 ){
            // $all_patients = Study_Patient::whereIn('site_id' , $all_sites )->get();
            // }
            return view('study.show' , compact( 'study' , 'procent_patients' , 'sites' , 'incomplete_diary' , 'patients_type_counts' , 'patients_incomplete_diary' , 'patients_count' , 'user' , 'patients' , 'tip_users' , 'study_audit' , 'all_patients' , 'contents' ));


        }








        public function old_show(){

        // $patients = Study_Patient::get()->map(function ($patient){

        //     $checkTypeCurrent = DiarySchedule::where([
        //             ['type', '=', 0],
        //             [ 'type', '!=', 1],
        //             [ 'type', '!=', 2],
        //             ['patient_id', $patient->id], 
        //             ])->get()->count();
                
        //         $checkTypeOverdue = DiarySchedule::where([
        //            [ 'type', 1],
        //             ['type', '!=', 0],
        //            [ 'type', '!=', 2],
        //            ['patient_id', $patient->id],
        //         ])->get()->count();

        //         if ($checkTypeCurrent) {
        //             $patient->type = 0;
        //         } elseif ($checkTypeOverdue) { // Overdue
        //             $patient->type = 1;
        //         } else {
        //             $patient->type = 2;
        //         }

        //         return $patient;
        // })->toArray();

        dd($patients);
        // if(auth() && auth()->user()->role_id === 3 )    
        $content = Content::where([
            ['study_id' , $id],
            ['content_lang' , $lang]
            ])->get();

        $sites = Site::with('site')->get();
        $patients_ids = Study_Patient::where([
            ['study_id', $study->id],
            // (isset($site_id)) ? ['site_id', $site_id] : null,
        ])->pluck('patient_id')->toArray();


        // $incomplete_diary = DiarySchedule::where([
        //     ['study_id', $study->id],
        //     ['type', '!=', 2]
        // ])->whereIn('patient_id', $patients_ids)->groupBy('patient_id')->get(['id', 'patient_id'])->count();

        $incomplete_diary = DiarySchedule::where([
            ['study_id', $study->id],
            ['type', '!=', 2],
            ])->whereIn('patient_id', $patients_ids)->get(['id', 'patient_id'])->count();



        // $patients_count = Patient::whereIn('id', $patients_ids)->count();
        // $all_patients_count = $patients_count;
        

        $patients_type_counts = [
            // Overdue
            DiarySchedule::groupBy('patient_id')
                ->where('study_id', $study->id)
                ->whereIn('patient_id', $patients_ids)
                ->where('type', 1)->count(),
            // Current
            DiarySchedule::groupBy('patient_id')
                ->where('study_id', $study->id)
                ->whereIn('patient_id', $patients_ids)
                ->where('type', 0)->count(),
            // Success
            DiarySchedule::groupBy('patient_id')
                ->where('study_id', $study->id)
                ->whereIn('patient_id', $patients_ids)
                ->where('type', 2)->count()
        ];

        // $all_patients_count = Patient::whereIn('id', $all_patients_ids)->count();
        $all_patients_count = 300;

        $study_audit = Study_Audit::all();

        if(auth()->user()->role_id ==3) {
            $procent_patients = ($all_patients_count != 0 && $patients_count != 0) ? ($patients_count * 100) / $all_patients_count : 0;
        }else{
            $procent_patients = ($study->planned_patients == 0 || $all_patients_count > $study->planned_patients || $patients_count == 0) ? 100 : $study->planned_patients*100/$patients_count;
        }
        $tip_users=DB::table('tip_users')->select('fullname','id','email')->get();
        $user = User::with('site')->findOrFail(auth()->user()->id);

        dd($patients_ids);
        

        $patients_incomplete_diary = DiarySchedule::where([
            ['study_id', $study->id] ,
            ['type', '!=', 2] ,
        ])->whereIn('patient_id' , $patients_ids)->get();
        
        $patients_type_counts = 100;

        dd($patients_incomplete_diary);
        // $patients_incomplete_diary = 10;
        $patients_count = 100; 
        return view('study.show' , compact( 'study' , 'procent_patients',  'patients' , 'sites' , 'incomplete_diary' , 'patients_type_counts' , 'patients_incomplete_diary' , 'patients_count' , 'user'));

    // dd($patients , $sites , $incomplete_diary , $patients_type_counts)  ;
        
        // $checkTypeCurrent = DiarySchedule::where([
        //     ['type', '=', 0],
        //    [ 'type', '!=', 1],
        //    [ 'type', '!=', 2] ])->whereIn('patient_id', $pat)->get()->count();


        // $checkTypeOverdue = DiarySchedule::where([
        //    [ 'type', 1],
        //     ['type', '!=', 0],
        //    [ 'type', '!=', 2],
        // ])->whereIn('patient_id', $pat)->get()->count();

        

        // dd($checkTypeCurrent , $checkTypeOverdue);

        // dd('show' , $lang , $pat);




    }
    

    // public function show($study_id, $language_or_site) {
    //     $data = [
    //         'study_id' => $study_id,
    //         'language_or_site' => $language_or_site,
	   	
    //     ];

    //     $study = $this->apiRequest('GetStudy', $data);


    //     $dataStudy = [
    //         'study_id' => $study_id,
    //         'language_or_site' => $language_or_site
    //     ];

    //     $information  = $this->apiRequest('GetStudyInformation', $dataStudy);
	// //dd($information);
    //     $study = json_decode($study);


    //     if((int)Cookie::get('user_role')===3) {
    //         $procent_patients = ($information->all_patients_count != 0 && $information->patients_count != 0) ? ($information->patients_count * 100) / $information->all_patients_count : 0;
    //     }else{
    //         $procent_patients = ($study->study->planned_patients == 0 || $information->all_patients_count > $study->study->planned_patients || $information->patients_count == 0) ? 100 : $study->study->planned_patients*100/$information->patients_count;
    //     }

    //     $user = $this->apiRequest('GetUserWithSite');
    //     $user = json_decode($user);
    //     return view('study.show')->with([
    //         'study_id' => $study_id,
    //         'study' => $study,
    //         'information' => $information,
    //         'procent_patients' => $procent_patients,
    //         'user' => $user,
	//     'sites'=>$information->sites,
 	//     'tip_study_audit'=>$information->tip_study_audit

    //     ])->render();
    // }


        
    public function showSite( $study_id , $site_id) {

        $study = Study::findOrFail($study_id);
        $site = Site::findOrFail($site_id );
        $patients = Study_Patient::where('study_id' , $study->id)->where('site_id' , $site->site_id)->get();
        $patient_count = Study_Patient::where('site_id' , $site->site_id)->where('study_id' , $study_id )->get();

        $patients_ids = Study_Patient::where([
            ['study_id', $study->id],
            // (isset($site_id)) ? ['site_id', $site_id] : null,
        ])->pluck('patient_id')->toArray();


        $incomplete_diary = DiarySchedule::where([
            ['study_id', $study->id],
            ['type', '!=', 2],
        ])->whereIn('patient_id', $patients_ids)
        // ->groupBy('patient_id')
        ->get()->count();

        return view('study.site' , compact('site' , 'study' ,'patients', 'patient_count' , 'incomplete_diary'));

        // $data = [
        //     'site_id' => $site_id,
        //     'study_id' => $study_id
        // ];

        // $site = $this->apiRequest('GetSiteById', $data);

        // $site = json_decode($site);

        // $dataStudy = [
        //     'site_id' => $site_id,
        //     'study_id' => $study_id,
        //     'language_or_site' => 'English'
        // ];
        // $information  = $this->apiRequest('GetStudyInformation', $dataStudy);


        // if((int)Cookie::get('user_role')===3) {
        //     $procent_patients = ($information->all_patients_count != 0 && $information->patients_count != 0) ? ($information->patients_count * 100) / $information->all_patients_count : 0;
        // }else{
        //     $procent_patients = 0;
        // }

        // $vars['site'] = $site;

        // $vars['content'] = view('study.site')->with([
        //     'study_id' => $study_id,
        //     'site' => $site,
        //     'information' => $information,
        //     'procent_patients' => $procent_patients,
        // ])->render();

        // return $this->render($vars);
    }

    public function showChart($study_id) {
        $study = Study::findOrFail($study_id);
        return view('study.showChart' , compact('study') );
    }
    


    public function exportPatientsList($site_id = null, $study_id = null)
    {


        if($study_id == null) {
            $data = [
                'study_id' => $site_id
            ];
        }else{
            $data = [
                'site_id' => $site_id,
                'study_id' => $study_id
            ];
        }

        $response = $this->apiRequest('GetPatientsList', $data);

		$patients = collect();
        if (isset($response->status) && $response->status == 'Success') {
            $patients = $response->patients;
            $patients_array = collect([]);
            foreach ($patients as $patient) {
                $schedules = collect($patient->schedules);
              
                 $data = collect([
                    'id' => $patient->id,
                    'name' => $patient->name,
                    'phone' => $patient->phone,
                    'email' => $patient->email,
                    'created_at' => $patient->created_at,
                    'date_confirmed' => $patient->date_confirmed,
                    'info' => collect($patient->info),
					'site'=>collect($response->site),
                    'schedules' => $schedules,
                    'count_questions' => count($schedules)
                ]);



                $patients_array->push($data);
            }
            $patients = $patients_array->where('count_questions', '>', 0);
            
        }


        $schedules = collect();
        $questions = collect();
        $patients->each(function ($patient) use ($schedules, $questions) {
            foreach ($patient['schedules'] as $schedule) {
                foreach ($schedule->questions as $question) {
                    $questions->push($question);
                }
                $schedules->push($schedule);
            }
        });


        $data = [
            'patients' => $patients,
            'questions' => $questions->groupBy('question'),
            'schedules' => $schedules
        ];

//dd( $data);
        return Excel::download(new PatientsExport($data), 'patients-list.xlsx');
    }


    public function recursive($workflows) {
        $workflows = $workflows->map(function($item){
            $newItem = [];
            if(count($item->child) > 0) {
                $newItem['children'] = $this->recursive(collect($item->child));
            }
            unset($item->child);

            $newItem['id'] = $item->workflow_id;
            $newItem['name'] = $item->process_name;
            $newItem['color'] = $item->color;

            return $newItem;
        });
        return $workflows;
    }

    // public function showChart($study_id) {

    //     $data = [
    //         'study_id' => $study_id,
    //         'language_or_site' => 'English'
    //     ];

    //     $study = $this->apiRequest('GetStudy', $data);
    //     $study = json_decode($study);



    //     $workflows = $this->apiRequest('GetWorkFlows', $data);
    //     $workflows = json_decode($workflows);

    //     $workflows = collect($workflows);

    //     $workflows = $this->recursive($workflows);

    //     $vars['content'] = view('study.showChart')->with([
    //         'study' => $study,
    //         'workflows' => collect($workflows)
    //     ]);

    //     return $this->render($vars);
    // }

    public function showChartForms($study_id, $workflow_id) {
        $data = [
            'study_id' => $study_id,
            'language_or_site' => 'English',
            'workflow_id' => $workflow_id
        ];

        $study = $this->apiRequest('GetStudy', $data);
        $study = json_decode($study);

        $workflows = $this->apiRequest('GetWorkFlows', $data);
        $workflows = json_decode($workflows);



        $vars['content'] = view('study.showChartForms')->with([
            'study' => $study,
            'workflows' => $workflows
        ]);

        return $this->render($vars);
    }

    

	
  public function GetStudyDocuments($study_id) {
	$Study=$study_id;
	$data=['study_id'=>$study_id];
  	$response = $this->apiRequest('GetAllStudyDocuments',$data);
        //dd($response->studyname[0]);
	return view('studydocuments.index',compact('response','Study'));

 	}

	public function CreateStudyDocuments($study_id) {
		$Study=$study_id;
		$data=['study_id'=>$study_id];
	  	$response = $this->apiRequest('GetAllStudyDocuments',$data);
	
		return view('studydocuments.create',compact('response','Study'));
	
	 }


public function StoreStudyDocuments(Request $request) {


   if($request->hasfile('document'))
     {

        foreach($request->file('document') as $file)
        {
           		$fileName = $file->getClientOriginalName() ;
		$destinationPath ='documents';
		$path_image =  $file->move($destinationPath, $file->getClientOriginalName());  
       }
     }

	$document_path='documents/'.$fileName;
	//dd($document_path);

	$data=[
		'study_id'=>$request->study_id,
		'name'=>$request->name,
		'document'=>$document_path
		];		

		$response = $this->apiRequest('CreateNewStudyDocument',$data);
	if($response->status=='Success'){
		$Message =$response->Message;
		return back()->with('success', $Message);
	} else {
	$Message =$response->Message;
	return back()->with('error', $Message);
	}

 }


public function EditStudyDocuments($study_id,$id) {
	$Study=$study_id;
	$data=['id'=>$id,
	'study_id'=>$study_id];
  	$response = $this->apiRequest('GetSingleStudyDocuments',$data);
	//dd($response);
	return view('studydocuments.edit',compact('response','Study'));

 }

public function UpdateStudyDocuments(Request $request) {

//dd($request->all());
	if($request->hasfile('document'))
         {

            foreach($request->file('document') as $file)
            {
               		$fileName1 = $file->getClientOriginalName() ;
			$destinationPath ='documents';
			$path_image =  $file->move($destinationPath, $file->getClientOriginalName());  
           }

         }

       $document_path='documents/'.$fileName1;
		

	$data=['id'=>$request->id,'study_id'=>$request->study_id,'name'=>$request->name,'document'=>$document_path];		

	//dd($data);
	$response = $this->apiRequest('StoreUpdatedDocument',$data);
	
	if($response->status=='Success'){
		$Message =$response->Message;
		return back()->with('success', $Message);
	}
	else{
	$Message =$response->Message;
	return back()->with('error', $Message);
	}

 }


}