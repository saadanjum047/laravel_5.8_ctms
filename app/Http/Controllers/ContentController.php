<?php

namespace App\Http\Controllers;

use App\Site;
use App\Study;
use App\Content;
use App\Document;
use App\Revision;
use Carbon\Carbon;
use App\Study_site;
use App\Content_Tab;
use App\Content_Site;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    public function index($study_id){

        $contents = Content::where('study_id', $study_id )->get();
        $study = Study::findOrFail($study_id);
        return view('contents.index' , compact('contents' , 'study'));

    }

    public function create($study_id){
        $study = Study::findOrFail($study_id);
        $organizations = Organization::where('user_id' , auth()->user()->id)->get();
        $contentstab = Content_Tab::all();
        return view('contents.createcontent' , compact('study' , 'organizations' , 'contentstab'));

    }

    public function store($study_id , Request $request){
        
        // dd($request->all() , $study_id );
        $request->validate([
 
            "content_tab_id" => "required",
            "content_header" => "required",
            "content_body" => "sometimes",
            "content_lang" => "required",
            "content_type" => "required",
            "active" => "sometimes",
            "public" => "sometimes",
        ]);
    //    dd($request->all());


    
        $content = Content::create([
            'content_header'	 => $request->content_header,
            'content_body'	 => $request->content_body,	
            'content_lang'	 => $request->content_lang,	
            'created_by'	 => auth()->user()->id,
            'date_created'	 => Carbon::now(),	
            'content_type'	 => $request->content_type,
            'active'	 => isset($request->active) ? $request->active : 0 ,
            'public'	 => isset($request->public) ? $request->public : 0 ,
            'content_tab_id' => $request->content_tab_id,
            "study_id" => $study_id,

        ]);

        $revision = Revision::create([
            "version" => 'V 1.0',
            "revision_header" => $request->content_header,
            "revision_des" => $request->content_body,
            "revision_language" => $request->content_lang,
            "created_by" => auth()->user()->id,
            "content_id" => $content->content_id,
            "study_id" => $study_id,
            "active" => 1 ,
        ]);

        if($content->content_type == 'consent' && $content->active == 1 ){
            $activ_consents = Content::where('study_id' , $study_id )->where('active' , 1)->where('content_type' , 'consent')->get();
            if(isset($activ_consents)){
            foreach($activ_consents as $consent){
                $consent->active = 0;
                $consent->save();
            }}
        }

        $content->active = 1;
        $content->save();

        if($content){
            return redirect('/dashboard/study/'.$study_id.'/contents')->with('success' , 'Content added successfully');}
            else{
            return redirect()->back()->with('errpr' , 'Problem while adding content');
    }
        
		
    }

    // calculator
    public function CreateContentByCalculator($study_id){

        $study = Study::findOrFail($study_id);
        $organizations = Organization::where('user_id' , auth()->user()->id)->get();
        $contentstab = Content_Tab::all();
        return view('contents.calculator' , compact('study' , 'organizations' , 'contentstab'));
    }


    public function StoreContentByCalculator($study_id , Request $request){

        // dd($request->all());

        $request->validate([
            "content_tab_id" => "required",
            "content_header" => "required",
            "content_body" => "sometimes",
            "content_lang" => "required",
            "active" => "sometimes",
            "public" => "sometimes",
        ]);

    
        $content = Content::create([
            'content_header'	 => $request->content_header,
            'content_body'	 => $request->content_body,
            'content_lang'	 => $request->content_lang,	
            'created_by'	 => auth()->user()->id,
            'date_created'	 => Carbon::now(),	
            'content_tab_id' => $request->content_tab_id,
            'active'	 => $request->active ?? 0 ,
            "study_id" => $study_id,
            'public'	 => $request->public ?? 0 ,
        ]);

        $revision = Revision::create([
            "version" => 'V 1.0',
            "revision_header" => $request->content_header,
            "revision_des" => $request->content_body,
            "revision_language" => $request->content_lang,
            "created_by" => auth()->user()->id,
            "content_id" => $content->content_id,
            "study_id" => $study_id,
            "active" => 1 ,
        ]);

        if($content){
            return redirect('/dashboard/study/'.$study_id.'/contents')->with('success' , 'Content added successfully');}
            else{
            return redirect()->back()->with('errpr' , 'Problem while adding content');
    }
    }


    public function CreateContentByURL($study_id){
        $study = Study::findOrFail($study_id);
        $organizations = Organization::where('user_id' , auth()->user()->id)->get();
        $contentstab = Content_Tab::all();
        return view('contents.addurl' , compact('study' , 'organizations' , 'contentstab'));
    }


    public function StoreContentByURL($study_id , Request $request){

        // dd($request->all());
        $request->validate([
            "content_tab_id" => "required",
            "content_header" => "required",
            "content_body" => "sometimes",
            "content_lang" => "required",
            "content_type" => "required",
            "video_url" => "sometimes",
            "active" => "sometimes",
            "public" => "sometimes",
        ]);
       
        $content = Content::create([
            'content_header'	 => $request->content_header,
            'content_body'	 => $request->content_body,	
            'content_lang'	 => $request->content_lang,	
            'video_url'	 => $request->video_url,	
            'created_by'	 => auth()->user()->id,
            'date_created'	 => Carbon::now(),	
            'content_type'	 => $request->content_type,
            'active'	 => $request->active ?? 0 ,
            'public'	 => $request->public ?? 0 ,
            'content_tab_id' => $request->content_tab_id,
            "study_id" => $study_id,

        ]);

        $revision = Revision::create([
            "version" => 'V 1.0',
            "revision_header" => $request->content_header,
            "revision_des" => $request->content_body,
            "revision_language" => $request->content_lang,
            "created_by" => auth()->user()->id,
            "content_id" => $content->content_id,
            "study_id" => $study_id,
            "active" => 1 ,
        ]);

        if($content){
            return redirect('/dashboard/study/'.$study_id.'/contents')->with('success' , 'Content added successfully');}
            else{
            return redirect()->back()->with('errpr' , 'Problem while adding content');
    }

    }


    public function CreateSolution($study_id){
        $study = Study::findOrFail($study_id);
        $studies = Study::all();

        return view('contents.solutions' , compact('studies' , 'study'));
    }


    public function StoreSolution($study_id, Request $request){
        dd($request->all());
    }



    public function edit($study_id , $content_id){
        $contentstab = Content_Tab::all();
        $study = Study::findOrFail($study_id);
        $content = Content::findOrFail($content_id);
        $organizations = Organization::where('user_id' , auth()->user()->id)->get();
        return view('contents.editcontent', compact('content' , 'study' ,'organizations' , 'contentstab'));
    }



    public function update($study_id , $content_id , Request $request){

        $request->validate([
            "content_tab_id" => "required",
            "content_header" => "required",
            "content_body" => "sometimes",
            "content_lang" => "required",
            "content_type" => "required",
            "active" => "sometimes",
            "public" => "sometimes",
        ]);
            
        $content = Content::findOrFail($content_id);
        $content->content_header = $request->content_header;
        $content->content_body = $request->content_body;
        $content->content_lang = $request->content_lang;
        $content->content_type = $request->content_type;
        $content->active = $request->active ?? 0;
        $content->public = $request->public ?? 0;
        $content->content_tab_id = $request->content_tab_id;
        $content->save();
        if($content->save()){
            return redirect('/dashboard/study/'.$study_id.'/contents')->with('success' , 'Content deleted successfully');
        }else{
            return redirect()->back()->with('errpr' , 'Problem while deleting content');
    
        }
    }


    public function assignSite($study_id , $content_id , Request $request){
        
        $site_revision = Revision::find($request->revision_id);
        
        $cntn_site = Content_Site::where([
            ['site_id' , $request->current_site_id ],
            ['content_id' , $content_id] ,   
            ['revision_id' , $request->revision_id] ])->first();


        if($cntn_site != NULL){
            // dd($cntn_site , $request->all());
            $cntn_site->site_id = $request->site_id ?? NULL ;
            $cntn_site->save();
        }else{
            // dd('stop' , $request->all() , $cntn_site);
        $site_rev = Content_Site::create([
            'content_id' => $content_id,
            'revision_id' => $request->revision_id,
            'site_id' => $request->site_id,
            'created_by' => auth()->user()->id,
        ]);
        }

        return redirect()->back()->with('success', 'Revision assigned to the site');
    }

    public function removeSite($content_id , $revision_id){

        $content = Content_Site::where([ ['revision_id' , $revision_id ], ['content_id' , $content_id] ])->first();
        $content->delete();
        return redirect()->back()->with('success', 'Revision has been removed from site');

    }


    public function destroy($study_id , $content_id){
        $content = Content::find($content_id);
        if($content->delete()){
        return redirect('/dashboard/study/'.$study_id.'/contents')->with('success' , 'Content deleted successfully');
    }else{
        return redirect()->back()->with('error' , 'Problem while deleting content');

    }

    }

    // Revisions methods 

    public function addRevision($study_id , $content_id){
        $study = Study::findOrFail($study_id);
        
        $content = Content::findOrFail($content_id);
        return view('contents.revision' , compact('content' , 'study'));
    }


    public function StoreRevision($study_id , $content_id , Request $request){
        $request->validate([
            "revision_name" => 'required' ,
            "revision_header" => 'required' ,
            "revision_des" => 'sometimes',
            "revision_language" => 'required',
            "video_url" => 'sometimes',
            "active" => 'sometimes'
        ]);

        $content = Content::findOrFail($content_id);
            // dd($content_id);
        if($request->active == 1){
            $revisions = $content->revisions->where('active' , 1);
        foreach($revisions as $rev){
            $rev->active = 0;
            $rev->save();
        }
        }
        
        $study = Study::findOrFail($study_id);
        $content = Content::findOrFail($content_id);
        // dd($content_id , $content );
        $revision = Revision::create([
            "version" => $request->revision_name,
            "revision_header" => $request->revision_header,
            "revision_des" => $request->revision_des,
            "revision_language" => $request->revision_language,
            "video_url" => $request->video_url,
            "created_by" => auth()->user()->id,
            "content_id" => $content->content_id,
            "study_id" => $study_id,
            "active" => $request->active ?? 0 ,
        ]);
        if($revision){
            return redirect('/dashboard/study/'.$study_id.'/contents')->with('success' , 'Revision added successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem while adding revision');
    
        }

    }

    public function ViewRevisions($study_id , $content_id){

        $study = Study::findOrFail($study_id);
        $content = Content::findOrFail($content_id);
        $revisions = Revision::where('content_id', $content_id)->get();
        $sites = Study_site::where('study_id' , $study_id )->get();
        // dd($study);
        return view('contents.viewrevision' ,  compact('study' , 'content' , 'revisions' , 
        'sites') );
    }


    public function editRevisions($study_id , $content_id , $revision_id){
        $study = Study::findOrFail($study_id);
        $content = Content::findOrFail($content_id);
        $revision = Revision::find($revision_id);

        return view('contents.edit_revision' ,  compact('study' , 'content' , 'revision'));      
    }


    public function UpdateRevisions($study_id , $content_id , $revision_id , Request $request){

        $request->validate([
        "revision_header" => "required",
        "revision_des" => "sometimes",
        "revision_language" => "required",
        "video_url" => "sometimes",
        "active" => "sometimes",
        "revision_name" => 'sometimes',

        ]);
        $content = Content::findOrFail($content_id);

        if($request->active == 1){
            $revisions = $content->revisions->where('active' , 1);
            foreach($revisions as $rev){
                $rev->active = 0;
                $rev->save();
            }
            }

        $revision = Revision::find($revision_id);
        $revision->revision_des = $request->revision_des;
        $revision->version = $request->revision_name;
        $revision->revision_header = $request->revision_header;
        $revision->revision_language = $request->revision_language;
        $revision->video_url = $request->video_url;
        $revision->active = $request->active ?? 0 ;

        if($revision->save()){
            return redirect('/dashboard/study/'.$study_id.'/viewrevision/'.$content_id)->with('success' , 'The revision has been updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem occoured while updating Revision');

        }

    }
    


    public function destroyRevisions($study_id  , $content_id , $revision_id){

        $revision = Revision::find($revision_id);
        if($revision->delete()){
            return redirect()->back()->with('success' , 'Revision deleted successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem occoured while deleting Revision');
        }

    }

    public function Document($study_id){
        $study = Study::findOrFail($study_id);
        $studies = Study::where('created_by' , auth()->user()->id )->get();
        return view('contents.document' ,  compact('study' ,'studies' ) );

    }


        

    public function SaveDocument($study_id , Request $request){

    if ($request->hasFile('document')) {
        $extension = ".".$request->document->getClientOriginalExtension();
        $doc = basename($request->document->getClientOriginalName(), $extension).time();
        $fileName = $doc.$extension;

        $path = public_path().'/documents';
        $uplaod = $request->document->move($path,$fileName);
    }

        $doc = Document::create([
            'name' => $request->name,
            'study_id' => $request->study_id,
            'document' => $fileName,
        ]);
        
        if($doc){
        return redirect("/dashboard/study/".$study_id."/contents" )->with('success' , 'Document addedd successfully');
        }else{
            return redirect()->back()->with('error' , 'Problem occoured while entering document Revision');

        }
    }

    







    public function test($study_id){
       
        dd('test method');
    }

    public function getstudies($sponsor_id){
        $studies = Study::where('sponsor_id' , $sponsor_id)->get();
        if(count($studies) > 0 ){
        return $studies;
    
    }else{
        return 0;
    }
    }
}
