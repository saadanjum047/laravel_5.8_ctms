<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'user_pat';
    protected $guarded = [];

    public function schedule(){
        return $this->hasMany('App\Study_Visit_Schedule');
    }

    public function info(){
        return $this->hasOne('App\Study_Patient');
    }
}
