<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investigator_Task_Group extends Model
{
    protected $table  = 'investigator_task_groups';
    protected $guarded = [];

    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function sponsor(){
        return $this->belongsTo('App\Tip_User' , 'sponsor_id');
    }

    public function tasks(){
        return $this->hasMany('App\Site_Task' , 'group_id');
    }
}
