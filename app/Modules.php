<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $table = 'modules';
    
    protected $guarded = [];

    public function permissions(){
        return $this->hasMany('App\Sponsor_Permission' , 'module_id' )->orderBy('id');
    }
}
