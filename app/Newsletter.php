<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $guarded = [];

    public function study(){
        return $this->belongsTo('App\Study');
    }

    public function users(){
        return $this->belongsToMany('App\User' , 'newsletter_user' , 'newsletter_id' , 'user_id' )->withPivot('status' , 'read_at') ;
    }

    public function tags(){
        return $this->belongsToMany('App\Tag' ,'newsletters_tags' , 'newsletter_id' , 'tag_id');
    }
}
