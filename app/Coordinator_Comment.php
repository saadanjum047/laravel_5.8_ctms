<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator_Comment extends Model
{
    protected $guarded = [];

    public function answer(){
        return $this0->belongsTo('App\Study_Diary_Answers' , 'diary_answer_id');
    }
}
