<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Message_Reply extends Model
{
    protected $table = 'patient_messages_replies';
    protected $guarded = [];

    public function investigator(){
        return $this->belongsTo('App\Tip_User' , 'investigator_id');
    }

    public function message(){
        return $this->belongsTo('App\Patient_Message' , 'message_id');
    }
}
