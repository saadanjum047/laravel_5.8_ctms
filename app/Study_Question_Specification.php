<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study_Question_Specification extends Model
{
    protected $table = 'study_questions_specifications';
    protected $guarded = [];

    public function question(){
        return $this->belongsTo('App\Study_Diary_Question');
    }
}
