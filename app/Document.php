<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'study_documents';
    protected $guarded  = [];

    public $timestamps = false;

}