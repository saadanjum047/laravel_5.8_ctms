<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_Consent extends Model
{
    protected $table = 'patient_consents';
    protected $guarded = [];
    protected $dates = ['acceptence_date' , 'created_at', 'updated_at'];

}
