<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
Artisan::call('cache:clear');
Artisan::call('config:clear');
dd(auth()->user()->patient);
return "Cache is cleared";
});
use Carbon\Carbon;
Route::get('currenttimestamp', function () {
$now = Carbon::now();
return $now;
});

Route::get('tip_users', function () {
$now = DB::table('tip_users')->get();
dd($now);
});



Route::get('notification_response', ['uses'=>'NotificationResponseController@NotificationResponse']);

Route::get('/', 'HomeController@index')->name('home');

Route::post('/set-person-type', ['uses'=>'UserController@setPersonType', 'as'=>'set_person_type']);
// Route::match(['get', 'post'], '/login', ['uses'=>'UserController@login', 'as'=>'login']);

Route::match(['post', 'get'], '/verification/patient/{token}', ['uses'=>'UserController@verificationPatient', 'as'=>'verification.patient']);
Route::match(['get','post'], '/recover/id', ['uses'=>'UserController@recoverId', 'as'=>'recover.id']);
Route::match(['get','post'], '/recover/password', ['uses'=>'UserController@recoverPassword', 'as'=>'recover.password']);
Route::match(['get', 'post'], '/recover/password/{token}/{user_role}', ['uses'=>'UserController@restorePassword', 'as'=>'restore.password'])->where('user_role', '[1-3]');


Route::get('/diary/food_diary', function() 
{ 
    return view('diary.food_diary');    
}
);

/*
* Diary
*/
//  Route::get('/diary/{schedule_id}/{auth_token}', ['uses'=>'UserController@LoginAndRedirectDiaryQuestions']);

Route::group(['prefix'=>'dashboard', 'as'=>'dashboard.', 'middleware'=>'auth'], function(){
	
// Route::get('/user/edit', ['uses'=>'UserController@edit', 'as'=>'user.edit']);
// Route::post('/user/edit', ['uses'=>'UserController@editStore', 'as'=>'user.edit']);

//conetnts
// Route::get('/study/{study_id}/content', ['uses'=>'DashboardController@contents', 'as'=>'study.contents']);
// Route::get('/contents', ['uses'=>'DashboardController@contents', 'as'=>'contents']);
// Route::get('/study/{study_id}/content/add', ['uses'=>'DashboardController@Addcontents', 'as'=>'study.contents.add']);
// Route::Post('/study/content/add', ['uses'=>'DashboardController@storecontents', 'as'=>'store.contents']);
// Route::get('/study/{study_id}/content/{content_id}/Editcontent', ['uses'=>'DashboardController@Editcontent', 'as'=>'study.Editcontent']);
// Route::Post('/study/content/urls', ['uses'=>'DashboardController@urls', 'as'=>'store.urls']);
// Route::get('/study/{study_id}/content/add/url', ['uses'=>'DashboardController@AddUrlcontents', 'as'=>'study.contents.add.url']);
// Route::post('/study/postcontentsurl', ['uses'=>'DashboardController@postcontentsurl', 'as'=>'study.postcontentsurl']);
// Route::post('/study/postcontentcalculator', ['uses'=>'DashboardController@postcontentcalculator', 'as'=>'study.postcontentcalculator']);
// Route::post('/study/Postupdatecontent', ['uses'=>'DashboardController@Postupdatecontent', 'as'=>'content.Postupdatecontent']);
// Route::get('/study/{study_id}/content/calculator', ['uses'=>'DashboardController@Calculatorcontents', 'as'=>'study.contents.calculator']);
// Route::get('/study/{study_id}/content/document', ['uses'=>'DashboardController@Documentcontents', 'as'=>'study.contents.document']);
// Route::get('/study/{study_id}/content/solutions', ['uses'=>'DashboardController@Solutioncontents', 'as'=>'study.contents.solutions']);
// // Route::post('/study/getstudies', ['uses'=>'DashboardController@getstudies', 'as'=>'study.getstudies']);
// // Route::get('/study/getstudiesss', ['uses'=>'DashboardController@getstudiesss', 'as'=>'study.getstudiesss']);
// Route::get('/study/{study_id}/content/{content_id}/addrevision', ['uses'=>'DashboardController@addrevisioncontent', 'as'=>'study.addrevision']);
// Route::Post('/study/postrevision', ['uses'=>'DashboardController@postrevision', 'as'=>'study.postrevision']);
// Route::get('/study/{study_id}/content/{content_id}/viewrevision', ['uses'=>'DashboardController@viewrevisioncontent', 'as'=>'study.viewrevision']);




//diary dashboard

// Route::get('/study/{study_id}/diarydashboard', ['uses'=>'DashboardController@diarydashboard', 'as'=>'study.diarydashboard']);

Route::get('/patient/{patient_id}/study/{study_id}/aswerdiaries', ['uses'=>'DashboardController@aswerdiaries', 'as'=>'answerdiaries']);
Route::get('/patient/{patient_id}/study/{study_id}/missingdiaries', ['uses'=>'DashboardController@missingdiaries', 'as'=>'missingdiaries']);
Route::get('/patient/{patient_id}/study/{study_id}/shecdulediaries', ['uses'=>'DashboardController@shecdulediaries', 'as'=>'shecdulediaries']);

// Schedule
Route::get('/study/{study_id}/studyschedule', ['uses'=>'StudyController@studyschedule', 'as'=>'study.studyschedule']);
Route::get('/study/{study_id}/CreateSchedule', ['uses'=>'StudyController@CreateSchedule', 'as'=>'study.CreateSchedule']);
Route::get('/study/{study_id}/StudyScheduleExport', ['uses'=>'StudyController@StudyScheduleExport', 'as'=>'study.StudyScheduleExport']);

Route::get('/CronSchedule', ['uses'=>'DashboardController@CronSchedule', 'as'=>'CronSchedule']);
Route::get('/CronCheckUsersRegistered', ['uses'=>'DashboardController@CronCheckUsersRegistered', 'as'=>'CronCheckUsersRegistered']);

Route::get("addVisits","VisitScheduleController@createVisitSchedule");
Route::post("addVisits","VisitScheduleConroller@createVisitSchedulePost")->name('createvisitschedulepost');


//end



/**
 * Sites
 */
Route::get('/study/{study_id}/ViewSites', ['uses'=>'SiteController@ViewSites', 'as'=>'site.ViewSites']);
Route::get('/study/{study_id}/CreateNewSite', ['uses'=>'SiteController@CreateNewSite', 'as'=>'site.CreateNewSite']);
Route::post('/study/PostNewSite', ['uses'=>'SiteController@PostNewSite', 'as'=>'site.PostNewSite']);
Route::get('/study/{study_id}/site/{site_id}/editsite', ['uses'=>'SiteController@editsite', 'as'=>'editsite']);
Route::post('/study/updatesite', ['uses'=>'SiteController@updatesite', 'as'=>'updatesite']);






/**
 * Users
 */
// Route::get('/study/{study_id}/ViewUsers', ['uses'=>'UserController@ViewUsers', 'as'=>'user.ViewUsers']);
// Route::get('/study/{study_id}/CreateNewUser', ['uses'=>'UserController@CreateNewUser', 'as'=>'user.CreateNewUser']);
Route::post('/study/PostNewUser', ['uses'=>'UserController@PostNewUser', 'as'=>'user.PostNewUser']);
Route::get('/study/{study_id}/site/{site_id}/Registeruserbysite', ['uses'=>'UserController@Registeruserbysite', 'as'=>'Registeruserbysite']);
Route::get('/study/{study_id}/site/{site_id}/ViewUser', ['uses'=>'UserController@Viewuserbysite', 'as'=>'Viewuserbysite']);
Route::get('/study/{study_id}/user/{user_id}/edituser', ['uses'=>'UserController@edituser', 'as'=>'edituser']);
Route::post('/study/updateuser', ['uses'=>'UserController@updateuser', 'as'=>'updateuser']);

// Route::get('/study/{study_id}/user/{user_id}/status/{is_activate}/updateuserstatus', ['uses'=>'UserController@updateuserstatus', 'as'=>'updateuserstatus']);


/**
 * StudyDocuments
 */


// Route::get('/study/{study_id}/StudyDocuments', ['uses'=>'StudyController@GetStudyDocuments', 'as'=>'study.StudyDocuments']);
// Route::get('/study/{study_id}/CreateStudyDocuments', ['uses'=>'StudyController@CreateStudyDocuments', 'as'=>'study.CreateStudyDocuments']);
// Route::post('/study//StoreStudyDocuments', ['uses'=>'StudyController@StoreStudyDocuments', 'as'=>'study.StoreStudyDocuments']);
// Route::get('/study/{study_id}/document/{id}/EditStudyDocuments', ['uses'=>'StudyController@EditStudyDocuments', 'as'=>'study.EditStudyDocuments']);
// Route::post('/study/UpdateStudyDocuments', ['uses'=>'StudyController@UpdateStudyDocuments', 'as'=>'study.UpdateStudyDocuments']);


/**
 * Organizations
 */
// Route::get('/Organizations', ['uses'=>'OrganizationsController@ViewOrganizations', 'as'=>'Organizations']);
Route::get('/Organizations/Create', ['uses'=>'OrganizationsController@CreateOrganization', 'as'=>'CreateOrganization']);
Route::Post('/study/AddOrganization', ['uses'=>'OrganizationsController@AddOrganization', 'as'=>'AddOrganization']);	
Route::get('/EditOrganization/{id}', ['uses'=>'OrganizationsController@EditOrganization', 'as'=>'EditOrganization']);
Route::Post('/Organizations/Update', ['uses'=>'OrganizationsController@UpdateOrganization', 'as'=>'UpdateOrganization']);
Route::Post('/Organizations/relatestudy', ['uses'=>'OrganizationsController@relatestudy', 'as'=>'relatestudy']);
Route::get('/Organizations/{id}/addsite', ['uses'=>'OrganizationsController@addsite', 'as'=>'addsite']);
Route::post('/Organizations/postsite', ['uses'=>'OrganizationsController@postsite', 'as'=>'postsite']);
Route::get('/Organizations/{id}/viewsite', ['uses'=>'OrganizationsController@viewsite', 'as'=>'viewsite']);	

/**
 * Studies
 */
//  Route::get('/Studies', ['uses'=>'StudiesController@ViewStudies', 'as'=>'Studies']);
//  Route::get('/Studies/Create', ['uses'=>'StudiesController@CreateStudy', 'as'=>'CreateStudy']);
//  Route::Post('/AddStudy', ['uses'=>'StudiesController@AddStudy', 'as'=>'AddStudy']);
//  Route::get('/EditStudy/{id}', ['uses'=>'StudiesController@EditStudy', 'as'=>'EditStudy']);
//  Route::Post('/Studies/Update', ['uses'=>'StudiesController@UpdateStudy', 'as'=>'UpdateStudy']);
// // Route::get('/', ['uses'=>'DashboardController@index', 'as'=>'index']);
// Route::get('/diary-page/{diary_type}', ['uses'=>'DashboardController@diaryPage', 'as'=>'diaryPage']);
// Route::post('/save-study', ['uses'=>'DashboardController@saveStudy', 'as'=>'save-study']);
// Route::get('/logout', ['uses'=>'UserController@logout', 'as'=>'logout']);


/**
* Study
*/
// Route::get('/study/{id}/chart/{workflow_id}/forms', ['uses'=>'StudyController@showChartForms', 'as'=>'showChartForms']);
// Route::get('/study/{id}/chart', ['uses'=>'StudyController@showChart', 'as'=>'showChart']);
// Route::get('/study/{id}/{language}', ['uses'=>'StudyController@show', 'as'=>'study']);
Route::get('/study//export/{study_id}', ['uses'=>'StudyController@exportPatientsList', 'as'=>'study.exportPatients']);
// Route::get('/site/{site_id}/{study_id}', ['uses'=>'StudyController@showSite', 'as'=>'site.show']);
/**
* Diary
*/
Route::post('/diary/save/answers', ['uses'=>'DiaryController@saveAnswers']);
Route::get('/diary/{schedule_id}', ['uses'=>'DiaryController@getDiaryQuestions'])->name('diary_questions');
Route::get('/diary/confirm/diary/{schedule_id}', ['uses'=>'DiaryController@confirmDiary'])->name('confirm-diary');
// Route::get('/diary/incomplete/{study_id}/all/{site_id?}', ['uses'=>'DiaryController@incompleteDiary', 'as'=>'incompleteDiary']);
/**
* Patients
*/
Route::group(['prefix'=>'patient', 'as'=>'patient.'], function() {
Route::get('/register', ['uses'=>'PatientsController@register', 'as'=>'register']);
Route::post('/register', ['uses'=>'PatientsController@registerStore', 'as'=>'register']);
Route::post('/get-languages-and-site-nbr/{study_id}', ['uses'=>'PatientsController@getLanguagesAndSiteNbr', 'as'=>'get-languages']);
Route::post('/save-visit-date', ['uses'=>'PatientsController@SaveVisitDate', 'as'=>'save-visit-date']);
Route::get('/list', ['uses'=>'PatientsController@patientsList', 'as'=>'list']);
Route::get('/export-patients-list/{site_id?}/{study_id?}', ['uses'=>'PatientsController@exportPatientsList', 'as'=>'export-patients-list']);
Route::get('/show/{id}/{site?}', ['uses'=>'PatientsController@show', 'as'=>'show']);
Route::post('/save', 'DashboardController@savePatientData1')->name('save');
Route::get('/export-patient-and-schedules/{id}', ['uses'=>'PatientsController@exportPatientAndSchedules', 'as'=>'export-patient-and-schedules']);
Route::get('/notes', ['uses'=>'PatientsController@patientsNotes', 'as'=>'notes']);
Route::post('/add-notes', ['uses'=>'PatientsController@patientsAddNotes', 'as'=>'add-notes']);
// Route::get('/Diaries/{id}/AllDiaries', ['uses'=>'PatientsController@PatientsDiaries', 'as'=>'Diaries']);
// Route::get('/Diaries/{id}', ['uses'=>'PatientsController@PatientSendSms', 'as'=>'sendsms']);
// Route::get('/images', 'PatientsController@patientimages')->name('images');
Route::post('/addpatientimage', ['uses'=>'PatientsController@addpatientimage', 'as'=>'addpatientimage']);
Route::post('/SavePatientIntoSite', ['uses'=>'PatientsController@SavePatientIntoSite', 'as'=>'SavePatientIntoSite']);
});
































// All the additional routes for the updated modules that require authentication

Route::get('/', 'DashboardController@index')->name('index');

Route::get('/user/edit' , 'UserController@edit' )->name('user.edit');
Route::post('/user/edit' , 'UserController@update' )->name('user.edit');

// For Diary Question
Route::get('/patientDiary' , 'Patient_DiaryController@index')->name('patientdiary');
Route::post('/patientDiary' , 'Patient_DiaryController@store')->name('patientdiary');
Route::get('/study/{study}/diary/{diary}/question/create' , 'Patient_DiaryController@create')->name('study.diary.question.create');
// Route::get('/patientDiary/edit/{question}' , 'Patient_DiaryController@edit')->name('patientDiary.edit');
Route::get('study/{study}/diary/{diary}/edit/{question}' , 'Patient_DiaryController@edit')->name('study.diary.edit');
Route::post('/patientDiary/update/{question}' , 'Patient_DiaryController@update')->name('patientDiary.update');
Route::get('/patientDiary/remove/{id}' , 'Patient_DiaryController@destroy')->name('patientdiary.remove');

// For diaries 
Route::get('/diaries' , 'Study_DiaryController@index')->name('diaries');
// Route::get('/diary_questions/{id}' , 'Study_DiaryController@ShowDiaryQuestions')->name('diary_questions');


Route::get('study/{study}/diary/{id}/questions' , 'Study_DiaryController@ShowDiaryQuestions')->name('study.diary.questions');
// Route::get('/diary/{diary}/question/{q}' , 'Study_DiaryController@ShowDiaryQuestion')->name('diary.questions');
Route::post('/diaries' , 'Study_DiaryController@store')->name('diaries');
Route::get('/diaries/remove/{diary}' , 'Study_DiaryController@remove')->name('diaries.remove');
Route::post('/diaries/update' , 'Study_DiaryController@update')->name('diaries.update');


// Routes for comments and flags
Route::post('/diary/{diary}/question/{question}/{answer}' , 'CoordinatorCommentController@store')->name('diary.question.comment');
Route::post('/diary/{diary}/question/{question}/update/{answer}' , 'CoordinatorCommentController@update')->name('diary.question.comment.update');
Route::get('/diary/{diary}/question/{question}/delete/{answer}' , 'CoordinatorCommentController@destroy')->name('diary.question.comment.delete');


// Routes for votes
Route::get('/diary/{diary}/question/{que}/vote/{answer}' , 'CoordinatorCommentController@voteUp')->name('diary.question.vote');
Route::get('/diary/{diary}/question/{que}/unvote/{answer}' , 'CoordinatorCommentController@voteDown')->name('diary.question.unvote');
Route::get('/flaging/{answer}' , 'CoordinatorCommentController@flag')->name('flaging');



// Specific study diaries
Route::get('/study/{id}/diary' , 'Study_DiaryController@viewDiary')->name('study.diary');

Route::get('/diary/{diary}/status' , 'Study_DiaryController@changeStatus')->name('diary.status');


Route::post('/study/{study}/diary/create' , 'Study_DiaryController@storeDiary')->name('study.diary.create');
Route::post('/study/diary/update' , 'Study_DiaryController@updateDiary')->name('study.diary.update');


// displaying users in a diary
Route::get('/study/{study}/diary/{diary}/patients' , 'Study_DiaryController@ViewStudyPatients')->name('study.diary.patients');

//Displaying diaries of a single users
Route::get('/study/{study}/diary/{diary}/patients/{patient}/diaries' , 'Study_DiaryController@ViewStudyPatientDiaries')->name('study.diary.patients.diaries');

//displaying questions and answers of the particular user
Route::get('/study/{study}/diary/{diary}/patient/{patient}/frequency/{freq}/questions' , 'Study_DiaryController@ViewStudyPatientDiaryQuestion')->name('study.diary.patient.frequency.questions');


//Displaying a single answer of a single patient
Route::get('/study/{study}/diary/{diary}/patient/{patient}/frequency/{freq}/question/{question}/answer' , 'Study_DiaryController@ViewStudyPatientDiaryQuestionAnswer')->name('study.diary.patient.frequency.question.answer');


// routes for translated questions

Route::get('study/{study}/diary/{diary}/question/{question}/versions' , 'TranslatedQuestionController@index')->name('study.diary.question.versions');

Route::get('study/{study}/diary/{diary}/question/{question}/version/create' , 'TranslatedQuestionController@create')->name('study.diary.question.version.create');

Route::post('study/{study}/diary/{diary}/question/{question}/version/create' , 'TranslatedQuestionController@store')->name('study.diary.question.version.store');

Route::get('study/{study}/diary/{diary}/question/{question}/version/{ver}/edit' , 'TranslatedQuestionController@edit')->name('study.diary.question.version.edit');


Route::post('study/{study}/diary/{diary}/question/{question}/version/{ver}/update' , 'TranslatedQuestionController@update')->name('study.diary.question.version.update');


Route::get('study/diary/question/version/{ver}/remove' , 'TranslatedQuestionController@remove')->name('study.diary.question.version.remove');



// Patient engaements

Route::get('study/{study}/patient_engagements' , 'PatientEngagementController@index' )->name('study.patient_engagements');

Route::get('study/{study}/patient_engagements/create' , 'PatientEngagementController@create' )->name('study.patient_engagements.create');

Route::post('study/{study}/patient_engagements/store' , 'PatientEngagementController@store' )->name('study.patient_engagements.store');

Route::get('study/{study}/patient_engagements/{question}/edit' , 'PatientEngagementController@edit' )->name('study.patient_engagements.edit');

Route::post('study/{study}/patient_engagements/{question}/update' , 'PatientEngagementController@update' )->name('study.patient_engagements.update');

Route::get('study/{study}/patient_engagements/{question}/remove' , 'PatientEngagementController@remove' )->name('study.patient_engagements.remove');

Route::get('study/{study}/patient_engagements/{question}/details' , 'PatientEngagementController@details' )->name('study.patient_engagements.details');


// patient end
Route::get('patient_engagements' , 'PatientEngagementController@viewQuestion' )->name('patient_engagements');
Route::get('patient_engagements/{question}/show' , 'PatientEngagementController@showQuestion' )->name('patient_engagements.show');

Route::post('patient_engagements/{question}/answer/save' , 'PatientEngagementController@saveQuestionAnswer' )->name('patient_engagements.answer.save');

Route::post('patient_engagements/{question}/answer/{answer}/update' , 'PatientEngagementController@updateQuestionAnswer' )->name('patient_engagements.answer.update');


// specific sites in a study
Route::get('/study/{id}/sites' , 'Study_Site_Controller@viewSites' )->name('study.sites');
// Routes for sites
Route::get('/study/{id}/sites/create' , 'Study_Site_Controller@create')->name('study.sites.create');
Route::post('/study/{id}/sites/create' , 'Study_Site_Controller@store')->name('study.sites.create');
Route::get('/study/{id}/sites/edit/{site}' , 'Study_Site_Controller@edit')->name('study.sites.edit');
Route::post('/study/{id}/sites/update/{site}' , 'Study_Site_Controller@update')->name('study.sites.update');

Route::get('change_user_status/{user}' , 'Study_Site_Controller@changeUserStatus')->name('change_user_status');

Route::get('/study/{id}/sites/{site}/users' , 'Study_Site_Controller@ViewUsersBySite')->name('study.sites.users');
Route::get('/study/{study}/sites/{site}/users/create' , 'Study_Site_Controller@CreateUsersBySite')->name('study.sites.user.create');
Route::post('/study/{study}/sites/{site}/users/store' , 'Study_Site_Controller@StoreUsersBySite')->name('study.sites.user.store');
Route::get('/study/{study}/sites/{site}/users/edit/{user}' , 'Study_Site_Controller@edituser')->name('study.sites.user.edit');
Route::post('/study/{study}/sites/{site}/users/update/{user}' , 'Study_Site_Controller@Updateuser')->name('study.sites.user.update');

// Routes for the site Tasks
Route::get('study/{study}/site/{site}/tasks' , 'SiteTaskController@index')->name('study.site.tasks');
Route::get('study/{study}/site/{site}/task/create' , 'SiteTaskController@create')->name('study.site.task.create');
Route::post('study/{study}/site/{site}/task/store' , 'SiteTaskController@store')->name('study.site.task.store');
Route::get('study/{study}/site/{site}/task/{task}/edit' , 'SiteTaskController@edit')->name('study.site.task.edit');
Route::post('study/{study}/site/{site}/task/{task}/update' , 'SiteTaskController@update')->name('study.site.task.update');
Route::get('study/{study}/site/{site}/task/{task}/delete' , 'SiteTaskController@destroy')->name('study.site.task.delete');
Route::get('study/{study}/site/{site}/task/{task}/details' , 'SiteTaskController@showDetails')->name('study.site.task.details');


//  Site Tasks for Investigators
Route::get('/tasks' , 'SiteTaskController@viewTasksToInvestigators')->name('tasks' );
Route::get('/task/{task}/view' , 'SiteTaskController@ViewTask')->name('task.view' );
Route::get('/task/{task}/completed' , 'SiteTaskController@taskCompleted')->name('task.completed' );




// Group Task routes

Route::get('study/{study}/groups' , 'GroupTaskController@index')->name('study.groups');
Route::get('study/{study}/group/create' , 'GroupTaskController@create')->name('study.group.create');
Route::post('study/{study}/group/store' , 'GroupTaskController@store')->name('study.group.store');
Route::get('study/{study}/group/{group}/edit' , 'GroupTaskController@edit')->name('study.group.edit');
Route::post('study/{study}/group/{group}/update' , 'GroupTaskController@update')->name('study.group.update');
Route::get('study/{study}/group/{group}/delete' , 'GroupTaskController@destroy')->name('study.group.delete');

// for group tasks 

Route::get('study/{study}/group/{group}' , 'GroupTaskController@tasks' )->name('study.group');
Route::get('study/{study}/group/{group}/task/create' , 'GroupTaskController@CreateTask' )->name('study.group.task.create');
Route::post('study/{study}/group/{group}/task/store' , 'GroupTaskController@StoreTask' )->name('study.group.task.store');
Route::get('study/{study}/group/{group}/task/{task}/edit' , 'GroupTaskController@EditTask' )->name('study.group.task.edit');
Route::post('study/{study}/group/{group}/task/{task}/update' , 'GroupTaskController@UpdateTask' )->name('study.group.task.update');
Route::get('study/{study}/group/{group}/task/{task}/delete' , 'GroupTaskController@DeleteTask' )->name('study.group.task.delete');
Route::get('study/{study}/group/{group}/task/{task}/details' , 'GroupTaskController@showTaskDetails' )->name('study.group.task.details');

Route::get('study/{study}/group/{group}/task/{task}/pateints' , 'GroupTaskController@showPatientsInTask' )->name('study.group.task.pateints');

Route::get('study/{study}/group/{group}/task/{task}/pateint/{pat}' , 'GroupTaskController@showPatientDetails' )->name('study.group.task.pateint');

Route::post('task/attach' , 'GroupTaskController@attachTaskWithSchedule' )->name('task.attach');

Route::post('group/attach' , 'GroupTaskController@attachGroupWithSchedule' )->name('group.attach');

//for group tasks for pateints 
// Route::get('groups/all' , 'GroupTaskController@ViewGroups')->name('groups.all');
// Route::get('groups/{group}/tasks/all' , 'GroupTaskController@ViewGroupTasks')->name('group.tasks.all');
// Route::get('groups/{group}/tasks/{task}/view' , 'GroupTaskController@ViewGroupTask')->name('group.tasks.view');
// Route::get('mark/task/{task}/status/{repeated}' , 'GroupTaskController@MarkCompleted')->name('mark.task.status');

Route::get('/tasks/all' , 'GroupTaskController@ViewTasks')->name('tasks.all');

Route::get('/tasks/{task}/view' , 'GroupTaskController@ViewTask')->name('group.tasks.view');

Route::get('mark/task/{task}' , 'GroupTaskController@MarkCompleted')->name('mark.task');



// For study documents 
Route::get('study/{study}/documents' , 'StudyDocumentController@index')->name('study.documents');
Route::get('study/{study}/document/create' , 'StudyDocumentController@create')->name('study.document.create');
Route::post('study/{study}/document/store' , 'StudyDocumentController@store')->name('study.document.store');

Route::get('study/{study}/document/{doc}/edit' , 'StudyDocumentController@edit')->name('study.document.edit');
Route::post('study/{study}/document/{doc}/update' , 'StudyDocumentController@update')->name('study.document.update');

// view study documents on investigator side

Route::get('study/{study}/documents/view' , 'StudyDocumentController@ViewDocument')->name('study.documents.view');
Route::get('study/{study}/document/{doc}/show' , 'StudyDocumentController@showDocument')->name('study.documents.show');

// for diary on patient side
Route::get('study/diary/questions/{id}' , 'Patient_DiaryController@viewDiaryQuestion' )->name('study.diary.question');
Route::get('/study/diary/question/{id}/view/{freq}' , 'Patient_DiaryController@viewQuestion' )->name('study.diary.question.view');
Route::post('/diary/question/{id}/store/{freq}' , 'Patient_DiaryController@storeAnswer' )->name('diary.question.store');
Route::post('/diary/question/update/{id}' , 'Patient_DiaryController@updateAnswer' )->name('diary.question.update');


Route::post('/diary/{diary}/questions/storeDiary/{freq}' , 'Patient_DiaryController@storeDiary' )->name('diary.question.storeDiary');

// diaries for change
// Route::get('study/{study}/diaries/{diary}' , 'Patient_DiaryController@viewDiaries' )->name('study.diaries');
Route::get('study/{study}/diaries/{diary}/status/{status}' , 'Patient_DiaryController@viewDiaries' )->name('study.diaries.status');
// Route::get('/diaries/{diary}/all/questions/{freq}' , 'Patient_DiaryController@viewDiaryQuestions' )->name('diary.all.questions');
Route::get('/diaries/{diary}/all/questions/{freq}/status/{status}' , 'Patient_DiaryController@viewDiaryQuestions' )->name('diary.all.questions.status');


// For Organizations
Route::get('/organizations', 'OrganizationsController@index')->name('organizations');
Route::get('/organizations/create', 'OrganizationsController@create')->name('organizations.create');
Route::post('/organizations', 'OrganizationsController@store')->name('organizations');
Route::get('/organizations/edit/{id}', 'OrganizationsController@edit')->name('organizations.edit');
Route::post('/organizations/update/{org}', 'OrganizationsController@update')->name('organizations.update');
Route::get('/organizations/remove/{org}', 'OrganizationsController@destroy')->name('organizations.remove');


// For study
Route::get('/studies' , 'StudyController@index' )->name('studies');
Route::get('/study/create' , 'StudyController@create' )->name('study.create');
Route::post('/studies' , 'StudyController@store' )->name('studies');
Route::get('/studies/edit/{study}' ,  'StudyController@edit')->name('studies.edit');
Route::post('/studies/update/{study}' ,  'StudyController@update')->name('studies.update');
Route::get('/study/view/{id}/{lang?}' , 'StudyController@show' )->name('study.view');

// For Sites
Route::get('study/{study_id}/site/{site_id}' , 'StudyController@showSite')->name('study.site');


Route::get('study/{study_id}/chart' , 'StudyController@showChart')->name('study.chart');



// For Sponsor Roles
Route::get('/sponsor_roles' , 'SponsorRoleController@index')->name('sponsor_roles');
Route::get('/sponsor_roles/create' , 'SponsorRoleController@create' )->name('sponsor_roles.create');
Route::post('/sponsor_roles' , 'SponsorRoleController@store')->name('sponsor_roles');
Route::get('/sponsor_roles/remove/{role}' , 'SponsorRoleController@destroy')->name('sponsor_roles.remove');
Route::get('sponsor_roles/edit/{id}' , 'SponsorRoleController@edit')->name('sponsor_roles.edit');

Route::post('/sponsor_roles/update' , 'SponsorRoleController@update' )->name('sponsor_roles.update');

// for modules
Route::get('/modules' , 'SponsorRoleController@modules')->name('modules');
Route::post('/modules' , 'SponsorRoleController@save')->name('modules');
Route::get('/modules/remove/{mod}' , 'SponsorRoleController@delete')->name('modules.remove');



//For patients schedule
Route::get('/study_schedule' , 'StudyVisitScheduleController@index' )->name('study_schedule');
Route::post('/study_schedule/{id}' , 'StudyVisitScheduleController@store' )->name('study_schedule');
Route::post('/study_schedule/update' , 'StudyVisitScheduleController@update' )->name('study_schedule.update');
// Route::get('/study_schedule/delete/{schedule}' , 'StudyVisitScheduleController@destroy' )->name('study_schedule.delete');

Route::get('schedules_Patients' , 'StudyVisitScheduleController@showScheduledPatients' )->name('schedules_Patients');

//for patients 

Route::get('patient' , 'PatientsController@index')->name('patient');
Route::get('patient/register' , 'PatientsController@view' )->name('patient.register');
Route::post('patient/register' , 'PatientsController@store' )->name('patient.register');
Route::get('study/{study}/patient/{id}/show' , 'PatientsController@show' )->name('study.patient.show');
Route::get('study/{study}/diary/{diary}/patient/{id}/sendSMS' , 'PatientsController@PatientSendSms' )->name('study.patient.sendSMS');

Route::post('patient/update/{patient}' , 'PatientsController@update' )->name('patient.update');

Route::post('patient/request_reset_schedule' , 'PatientsController@RequestChangePatientVisit' )->name('patient.request_reset_schedule');

Route::get('patient/reset_schedule/{id}' , 'PatientsController@ResetPatientVisit' )->name('patient.reset_schedule');



//Ajax route for site nbr
Route::get('/get_nbr/{id}' , 'PatientsController@getNBR' );
Route::get('/check_nbr_value/{study_id}/{nbr}' , 'PatientsController@checkNBR' );



// for study users
Route::get('/study/{study_id}/viewusers', 'StudyController@viewUsers')->name('study.viewusers');
Route::get('/study/user/{study_id}/CreateNewUser', 'StudyController@CreateNewUser')->name('study.user.create');
Route::post('/study/user/{study_id}/StoreNewUser', 'StudyController@StoreNewUser')->name('study.user.store');
Route::get('/study/user/{study_id}/edit/{user}', 'StudyController@edituser')->name('study.user.edit');
Route::post('/study/user/update/{user}', 'StudyController@updateuser')->name('study.user.update');

Route::get('/study/user/{study_id}/delete/{user}', 'StudyController@destroy')->name('study.user.delete');



// study schedules
Route::get('/study/schedules/{id}', 'StudyVisitScheduleController@Schedules')->name('study.schedules');
Route::post('/study/schedules/{id}', 'StudyVisitScheduleController@store')->name('study.schedules');
Route::post('/study/schedules/update/{id}', 'StudyVisitScheduleController@update')->name('study.schedules.update');
Route::get('/study/schedules/delete/{schedule}', 'StudyVisitScheduleController@destroy')->name('study.schedules.delete');


// Routes for newsletter on sponsor side
Route::get('/study/{study}/newsletter', 'NewsletterController@index')->name('study.newsletter');
Route::get('/study/{id}/newsletter/create', 'NewsletterController@create')->name('study.newsletter.create');
Route::post('/study/{id}/newsletter/store', 'NewsletterController@store')->name('study.newsletter.store');
Route::get('/study/{study}/newsletter/edit/{newsletter}', 'NewsletterController@edit')->name('study.newsletter.edit');
Route::post('/study/{study}/newsletter/update/{id}', 'NewsletterController@update')->name('study.newsletter.update');
Route::get('/study/{study}/newsletter/delete/{newsletter}', 'NewsletterController@destroy')->name('study.newsletter.delete');
Route::get('/study/{study}/newsletter/{id}', 'NewsletterController@show')->name('study.newsletter.show');
Route::get('/study/{study}/newsletter/{id}/details', 'NewsletterController@showDetails')->name('study.newsletter.details');


// Routes for user tags 

Route::get('/study/{study}/tags' , 'TagController@index')->name('study.tags');
Route::get('/study/{study}/tags/create' , 'TagController@create')->name('study.tags.create');
Route::post('/study/{study}/tags/store' , 'TagController@store')->name('study.tags.store');
Route::get('/study/{study}/tags/{tag}/edit' , 'TagController@edit')->name('study.tags.edit');
Route::post('/study/{study}/tags/{tag}/update' , 'TagController@update')->name('study.tags.update');
Route::get('/study/{study}/tags/{tag}/delete' , 'TagController@delete')->name('study.tags.delete');



// Routes for the medicien
Route::get('study/{study}/medicines' , 'MedicineController@index' )->name('study.medicines');
Route::post('study/{study}/medicines.create' , 'MedicineController@store' )->name('study.medicines.create');
Route::post('study/{study}/medicines.update' , 'MedicineController@update' )->name('study.medicines.update');
Route::get('study/{study}/medicines/{med}' , 'MedicineController@destroy' )->name('study.medicines.delete');


// Newsletter for ivestigators
Route::get('/study/{study}/newsletters' , 'NewsletterController@view' )->name('study.newsletters');
Route::get('/study/{study}/newsletter/{news}/show' , 'NewsletterController@viewSingle' )->name('study.newsletters.view');



// Routes for content 
Route::get('study/{study}/contents' , 'ContentController@index')->name('study.contents');
Route::get('study/{study}/contents/add' , 'ContentController@create')->name('study.contents.add');
Route::post('study/{study}/store/contents' , 'ContentController@store')->name('study.store.contents');
Route::get('study/{study}/Editcontent/{content}' , 'ContentController@edit')->name('study.Editcontent');
Route::post('study/{study}/Postupdatecontent/{content}' , 'ContentController@update')->name('study.Postupdatecontent');
Route::get('study/{study}/contents/delete/{content}' , 'ContentController@destroy')->name('study.contents.delete');

// assigning the revision to the site
Route::post('study/{study}/contents/{content}/revision' , 'ContentController@assignSite')->name('study.content.assignSite');
Route::get('contents/{content}/revision/{revision}/remove' , 'ContentController@removeSite')->name('content.revision.remove');



Route::get('study/{study}/addrevision/{content}' , 'ContentController@addRevision')->name('study.addrevision');
Route::post('study/{study}/postrevision/{content}/' , 'ContentController@StoreRevision')->name('study.postrevision');
Route::get('study/{study}/viewrevision/{content}' , 'ContentController@ViewRevisions')->name('study.viewrevision');

Route::get('study/{study}/content/{content}/editRevision/{revision}' , 'ContentController@editRevisions')->name('study.content.editRevision');
Route::post('study/{study}/content/{content}/updateRevision/{revision}' , 'ContentController@UpdateRevisions')->name('study.content.updateRevision');

Route::get('study/{study}/content/{content}/deleteRevision/{revision}' , 'ContentController@destroyRevisions')->name('study.content.deleteRevision');


// for translated revisions
Route::get('study/{study}/content/{content}/version/{revsision}/translations', 'TranslatedRevisionController@index')->name('study.content.version.translation');

Route::get('study/{study}/content/{content}/version/{revsision}/translations/create', 'TranslatedRevisionController@create')->name('study.content.version.translation.create');

Route::post('study/{study}/content/{content}/version/{revsision}/translations/store', 'TranslatedRevisionController@store')->name('study.content.version.translation.store');

Route::get('study/{study}/content/{content}/version/{revsision}/translations/{tran}/edit', 'TranslatedRevisionController@edit')->name('study.content.version.translation.edit');

Route::post('study/{study}/content/{content}/version/{revsision}/translations/{tran}/update', 'TranslatedRevisionController@update')->name('study.content.version.translation.update');

Route::get('study/{study}/content/{content}/version/{revsision}/translations/{tran}/delete', 'TranslatedRevisionController@delete')->name('study.content.version.translation.delete');


// Calculator
Route::get('study/{study}/contents/calculator' , 'ContentController@CreateContentByCalculator')->name('study.contents.calculator');
Route::post('study/{study}/contents/calculator' , 'ContentController@StoreContentByCalculator')->name('study.contents.calculator');


// Add URL
Route::get('study/{study}/contents/add/URL' , 'ContentController@CreateContentByURL')->name('study.contents.add.url');
Route::post('study/{study}/contents/add/URL' , 'ContentController@StoreContentByURL')->name('study.contents.add.url');


// document
Route::get('study/{study}/contents/document' , 'ContentController@Document')->name('study.contents.document');
Route::post('study/{study}/contents/document' , 'ContentController@SaveDocument')->name('study.contents.document');


// Solution
Route::get('study/{study}/contents/solutions' , 'ContentController@CreateSolution')->name('study.contents.solutions');
Route::post('study/{study}/contents/solutions' , 'ContentController@StoreSolution')->name('study.contents.solutions');

// The ajax route for getting studies in an Organization
Route::get('/getstudies/{sponsor}' , 'ContentController@getstudies')->name('getstudies');



// Patient images
Route::get('study/{study}/patient/images' , 'PatientsController@patientimages')->name('study.patient.images');
Route::get('study/{study}/patient/images/upload' , 'PatientsController@patientimagesupload')->name('study.patient.images.upload');
Route::post('patient/images/store' , 'PatientsController@savePatientImage')->name('patient.images.store');

// Changeing visit date for the patient
Route::post('/study/{study}/patient/{pat}/schedule/save' , 'PatientsController@changeSchedule' )->name('study.patient.schedule.save');

Route::post('/study/{study}/patient/{pat}/site/change' , 'PatientsController@changePatientSite' )->name('study.patient.site.change');


// Patient End
// Pre Screening Routes
Route::get('study/{study}/pre_screening' , 'PreScreeningController@index')->name('study.pre_screening');
Route::post('study/{study}/pre_screening' , 'PreScreeningController@store')->name('study.pre_screening');

Route::post('study/{study}/pre_screening/update' , 'PreScreeningController@update')->name('study.pre_screening.update');


Route::get('study/{study}/pre_screening/{screen}/delete' , 'PreScreeningController@destroy')->name('study.pre_screening.delete');





// For Pre Screening Questions
Route::get('study/{study}/pre_screening/{screen}/questions' , 'PreScreeningQuestionController@index')->name('study.pre_screening.questions');


Route::get('study/{study}/pre_screening/{screen}/questions/{question}/users' , 'PreScreeningQuestionController@viewUsers')->name('study.pre_screening.question.users');


Route::get('study/{study}/pre_screening/{screen}/questions/{question}/user/{user}/answer' , 'PreScreeningQuestionController@show')->name('study.pre_screening.question.user.answer');




Route::get('study/{study}/pre_screening/{screen}/frequency' , 'PreScreeningQuestionController@showFrequency')->name('study.pre_screening.frequency');


Route::get('study/{study}/pre_screening/{screen}/frequency/{freq}/questions' , 'PreScreeningQuestionController@ViewFrequencyQuestions')->name('study.pre_screening.frequency.questions');

Route::get('study/{study}/pre_screening/{screen}/frequency/{freq}/question/{question}/view' , 'PreScreeningQuestionController@showFrequencyQuestions')->name('study.pre_screening.frequency.question.view');


Route::get('study/{study}/pre_screening/{screen}/frequency/{freq}/question/{question}' , 'PreScreeningQuestionController@show')->name('study.pre_screening.question.show');



Route::get('study/{study}/pre_screening/{screen}/question/create' , 'PreScreeningQuestionController@create')->name('study.pre_screening.question.create');

Route::post('study/{study}/pre_screening/{screen}/question/store' , 'PreScreeningQuestionController@store')->name('study.pre_screening.question.store');


Route::get('study/{study}/pre_screening/{screen}/question/{question}/edit' , 'PreScreeningQuestionController@edit')->name('study.pre_screening.question.edit');

Route::post('study/{study}/pre_screening/{screen}/question/{question}/update' , 'PreScreeningQuestionController@update')->name('study.pre_screening.question.update');


Route::get('study/{study}/pre_screening/{screen}/question/{question}/delete' , 'PreScreeningQuestionController@destroy')->name('study.pre_screening.question.delete');


Route::get('question/{question}/status' , 'PreScreeningQuestionController@ChangeStatus')->name('question.status');


// Investigator site
// Pre Screening 

Route::get('study/{study}/pre_screening/all' , 'PreScreeningController@showScreens')->name('study.pre_screening.all');

// Route::get('study/{study}/pre_screening/{screen}/viewFrequency' , 'PreScreeningQuestionController@showScreenFrequency')->name('study.pre_screening.viewFrequency');


Route::get('study/{study}/pre_screening/{screen}/viewQuestions' , 'PreScreeningQuestionController@showScreenQuestions')->name('study.pre_screening.viewQuestions');

//for single patient
Route::get('study/{study}/patient/{pat}/pre_screening/{screen}/viewQuestions' , 'PreScreeningQuestionController@showScreenQuestions')->name('study.pre_screening.viewQuestions');

// Route::get('study/{study}/pre_screening/{screen}/viewQuestion/{question}' , 'PreScreeningQuestionController@showScreenQuestionWithAnswer')->name('study.pre_screening.viewQuestion');


Route::get('study/{study}/patient/{pat}/pre_screening/{screen}/viewQuestion/{question}' , 'PreScreeningQuestionController@showScreenQuestionWithAnswer')->name('study.pre_screening.viewQuestion');

Route::post('question/{question}/answer/store' , 'PreScreeningAnswerController@store')->name('question.answer.store');

Route::post('question/{question}/answer/{answer}/update' , 'PreScreeningAnswerController@update')->name('question.answer.update');

Route::get('pre_screening/answer/{answer}/delete' ,'PreScreeningAnswerController@destroy')->name('pre_screening.answer.delete');

// storing all pre screenings answers in patient page
Route::post('question/answered' , 'PreScreeningAnswerController@StoreAllAnswers')->name('question.answered');


// investigator messages

Route::get('study/{study}/investigator/messages' , 'MessageController@index' )->name('study.investigator.messages');
Route::get('study/{study}/investigator/message/{msg}/view' , 'MessageController@show' )->name('study.investigator.message.view');
Route::get('study/{study}/investigator/messages/create' , 'MessageController@create' )->name('study.investigator.messages.create');
Route::post('study/{study}/investigator/messages/store' , 'MessageController@store' )->name('study.investigator.messages.store');



// investigator messages on Sponsor end
Route::get('study/{study}/messages/investigators' , 'MessageController@ViewInvestigators' )->name('study.messages.investigators');
Route::get('study/{study}/investigator/{investigator}/messages/all' , 'MessageController@ViewMessages' )->name('study.investigator.messages.all');
Route::get('study/{study}/investigator/{investigator}/message/{msg}/show' , 'MessageController@showMessage' )->name('study.investigator.message.show');


// Reply of the messages
Route::post('study/{study}/investigator/{investigator}/message/{msg}/reply' , 'MessageController@replyMessage' )->name('study.investigator.message.reply');


// workflow
Route::get('study/{study}/workflow', 'WorkflowController@index')->name('study.workflow');
Route::get('study/{study}/workflow/create', 'WorkflowController@create')->name('study.workflow.create');
Route::post('study/{study}/workflow/store', 'WorkflowController@store')->name('study.workflow.store');
Route::get('study/{study}/workflow/{work}/edit', 'WorkflowController@edit')->name('study.workflow.edit');
Route::post('study/{study}/workflow/{work}/update', 'WorkflowController@update')->name('study.workflow.update');
Route::get('study/{study}/workflow/{work}/delete', 'WorkflowController@destroy')->name('study.workflow.delete');


// investigator task groups

Route::get('study/{study}/investigator/groups' , 'InvestigatorTaskGroupController@index')->name('study.investigator.groups');

Route::get('study/{study}/investigator/group/create' , 'InvestigatorTaskGroupController@create')->name('study.investigator.group.create');

Route::post('study/{study}/investigator/group/store' , 'InvestigatorTaskGroupController@store')->name('study.investigator.group.store');

Route::get('study/{study}/investigator/group/{group}/edit' , 'InvestigatorTaskGroupController@edit')->name('study.investigator.group.edit');

Route::post('study/{study}/investigator/group/{group}/update' , 'InvestigatorTaskGroupController@update')->name('study.investigator.group.update');

Route::get('study/{study}/investigator/group/{group}/delete' , 'InvestigatorTaskGroupController@destroy')->name('study.investigator.group.delete');


// Investigator tasks

Route::get('study/{study}/investigator/group/{group}/tasks' , 'InvestigatorTaskGroupController@tasks')->name('study.investigator.group.tasks');
Route::get('study/{study}/investigator/group/{group}/task/create' , 'InvestigatorTaskGroupController@taskCreate')->name('study.investigator.group.task.create');
Route::post('study/{study}/investigator/group/{group}/task/store' , 'InvestigatorTaskGroupController@taskStore')->name('study.investigator.group.task.store');
Route::get('study/{study}/investigator/group/{group}/task/{task}/edit' , 'InvestigatorTaskGroupController@taskEdit')->name('study.investigator.group.task.edit');

Route::post('study/{study}/investigator/group/{group}/task/{task}/update' , 'InvestigatorTaskGroupController@taskUpdate')->name('study.investigator.group.task.update');

Route::get('study/{study}/investigator/group/{group}/task/{task}/delete' , 'InvestigatorTaskGroupController@taskDelete')->name('study.investigator.group.task.delete');

Route::get('study/{study}/investigator/group/{group}/task/{task}/details' , 'InvestigatorTaskGroupController@taskDetails')->name('study.investigator.group.task.details');


// Investigator tasks in investigator
Route::get('study/{study}/investigator/groups/all' , 'InvestigatorTaskGroupController@showGroups')->name('study.investigator.groups.all');

Route::get('study/{study}/investigator/group/{group}/task/all' , 'InvestigatorTaskGroupController@showGroupTasks')->name('study.investigator.group.task.all');

Route::get('study/{study}/investigator/group/{group}/task/{task}/show' , 'InvestigatorTaskGroupController@showTask')->name('study.investigator.group.task.show');

Route::get('/investigator/task/{task}/markComplete' , 'InvestigatorTaskGroupController@markComplete')->name('investigator.mark.task.markComplete');



// Investigator Pattient Messages
Route::get('patient/messages' , 'PatientMessageController@index')->name('patient.messages');

Route::get('patient/message/create' , 'PatientMessageController@create')->name('patient.message.create');
Route::post('patient/message/store' , 'PatientMessageController@store')->name('patient.message.store');
Route::get('patient/message/{msg}/show' , 'PatientMessageController@show')->name('patient.message.show');


Route::get('study/{study}/patients/messages' , 'PatientMessageController@showPatients')->name('study.patients.messages');

Route::get('study/{study}/patient/{patient}/messages' , 'PatientMessageController@showPatientMessages')->name('study.patient.messages');

Route::get('study/{study}/patient/{patient}/message/{msg}/show' , 'PatientMessageController@showMessages')->name('study.patient.message.show');

Route::post('study/{study}/patient/{patient}/message/{msg}/reply' , 'PatientMessageController@messageReply')->name('study.patient.message.reply');


Route::get('study/{study}/diaryDashboard' , 'Study_DiaryController@showDiaryDashboard')->name('study.diarydashboard');

Route::get('study/{study}/patient/{patient}/scheduled_diareis' , 'Study_DiaryController@showScheduledDiary')->name('study.patient.scheduledDiary');

Route::get('study/{study}/patient/{patient}/missingDiareis' , 'Study_DiaryController@showMissingDiries')->name('study.patient.missingDiareis');

Route::get('study/{study}/diary/incomplete' , 'Study_DiaryController@showPatientCompliances')->name('study.diary.incomplete');



// patient consent

Route::get('study/{study}/patient/consent' , 'PatientConsentController@showPatientConsents')->name('study.patients.consent');
Route::get('patient/consent/delete' , 'PatientConsentController@deletePatientConsents')->name('patients.consent.delete');
Route::get('patient/consent' , 'PatientConsentController@index')->name('patient.consent');

Route::POST('patient/consent/submit' , 'PatientConsentController@SubmitConsent')->name('patient.consent.submit');



// Export Routes here
Route::get('export/sitepatients/{id}' , 'HomeController@exportSite')->name('export.sitepatients');

Route::get('/export/pateint_schedules/{patient}' , 'ExportController@exportSchedules')->name('export.pateint_schedules');

Route::get('/study/{study}/export' , 'HomeController@exportDiary')->name('study.export');

Route::get('download/{path}/file/{file}' , 'HomeController@downloadFile' )->name('download.file');


Route::get('test' , 'PatientController@PatientSendSms' );

});


Route::get('patient/{id}/confirmation/{code}' , 'PatientsController@patient_confirmation' )->name('patient.confirmation');

Route::get('patient/{id}/reset_confirmation' , 'PatientsController@patient_confirmation_reset' )->name('patient.reset_confirmation');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

