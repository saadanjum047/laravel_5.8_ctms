@extends('layouts.site')

@section('styles')



{{-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> --}}
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<style>
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{

    overflow-y: scroll;
    height: 250px;
}

::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}
</style>
@endsection

@section('content')


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id ) }}">{{$study->study_name}}</a></li>

        <li class="breadcrumb-item"><a href="{{ route('dashboard.messages.patients' , $study->id ) }}"> Patients </a></li>

        <li class="breadcrumb-item active"> {{$patient->patient_nbr}} </li>
    </ol>
</nav>
    

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif








<div class="container">
    <div class="row">
        <div class="col-md-10 offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> Messages
                   
                </div>
                <div class="panel-body">
                    <ul class="chat bg-white" style="padding:20px">
                        @foreach($patient->messages->where('investigator_id' , auth()->user()->sponsor_user->id ) as $k => $msg)
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img @if(isset($patient->user->avatar)) src="{{url('/')}}/avatars/WhatsApp Image 2020-02-14 at 3.08.09 PM1581695491.jpeg" @else src="{{url('/')}}/images/msg-m.png" @endif alt="User Avatar" class="img-circle" style="border-radius:200px; max-width:50px; max-height:50px" />
                            {{-- <img  src="{{url('/')}}/avatars/WhatsApp Image 2020-02-14 at 3.08.09 PM1581695491.jpeg"  class="img-circle"  /> --}}
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                </div>
                                <p>
                                   {{strip_tags($msg->body)}}
                                </p>
                            </div>
                        </li>
                        
                        @endforeach
                        
                    </ul>
                </div>
              
            </div>
        </div>
    </div>
</div>





{{-- <div class="bg-white" style="padding:20px">

    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Patient Messages
        </div>

    <table class="table table-hover">
    <tr>
        <th>#</th>
        <th width="40%" >Title</th>
        <th width="30%" >Attachment</th>
        <th width="20%" >Patient</th>
        <th width="10%" >Created</th>
    </tr>

    
    @foreach ($patient->messages->where('investigator_id' , auth()->user()->sponsor_user->id ) as $k => $msg)
    <tr>
        <td> {{$k+1}} </td>
        <td>
            <a href="{{route('dashboard.patient.message.show' , [ $study->id , $patient->id , $msg->id] )}} "> {{$msg->title}} </a>
             </td>
        <td> {{$msg->attachment}} </td>
        <td> {{$msg->patient->user->name}} </td>
        <td> {{($msg->created_at)->toDateString()}} </td>
    </tr>
    @endforeach
    

</table>

</div>
</div> --}}
    
@endsection