@extends('layouts.site')


@section('content')


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif
@if ($message = Session::get('wrong'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item" aria-current="page">
    Patient Engagement Questions</li>
   </ol>
</nav>


<div class="bg-white">
  
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif


    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Mystery Questions </h3>
        </div>

<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Question</th>
    <th >Answer</th>
    <th >Answer Status</th>
    <th>Created</th>
</tr>

@foreach ($questions as $k => $question)
    @php
        $answer = $question->ans->where('patient_id' , auth()->user()->patient->id )->first();
    @endphp
<tr>
    <td> {{$k+1}} </td>
    @if(!isset($answer))
    <td>  <a href="{{route('dashboard.patient_engagements.show' , $question->id )}} ">{{$question->question }} </a>  </td>
    @else
    <td> {{$question->question }} </td>
    @endif
    <td> @if(isset($answer)) {{$answer->answer_body}} @endif </td>
    <td> @if(isset($answer->status)) @if($answer->status == 1) <span class="badge badge-success">Correct</span>  @elseif($answer->status == 0) <span class="badge badge-danger">Wrong</span> @endif @endif </td>
    <td> {{($question->created_at)->toDateString() }} </td>
        
    </tr>

@endforeach

</table>

</div>
</div>





@endsection

