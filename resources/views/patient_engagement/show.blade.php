
@extends('layouts.site')


@section('content')


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.patient_engagements')}}">Questions</a></li>
   <li class="breadcrumb-item" aria-current="page">
    Patient Engagement Questions</li>
   </ol>
</nav>


<div class="bg-white " style="padding:20px">

    <div class="bg-white" style="padding:20px">
        
{{--         
          
        @if(session()->has('success'))
        <p class="alert alert-primary"> {{session('success')}} </p>
        @endif
        @if(session()->has('error'))
        <p class="alert alert-danger"> {{session('error')}} </p>
        @endif
         --}}
        <div>
            @php
            if(isset($question->ans)){
            if($question->ans){
                $answer = $question->ans->where('patient_id' , auth()->user()->patient->id )->first();
                
            }
        }
        @endphp

            <img src="{{url('/')}}/engagement_attach/{{$question->attachment}}" style="max-width:650px; max-height:650px" >
            <hr>
            <form action="@if(isset($answer)) {{route('dashboard.patient_engagements.answer.update' , [ $question->id , $answer->id] )}} @else {{route('dashboard.patient_engagements.answer.save' , $question->id  )}} @endif" method="POST">

               <h4> {{$question->question}} </h4>
               <hr>
            @csrf
                @php
                    $choices = json_decode($question->answers);
                @endphp
            <div class="form-group">
                <input type="radio" value="{{$choices[0]}}" name="answer" id="choice1"> 
                <label class="col-form-label" for="choice1"> {{$choices[0]}} </label>
            </div>
            <div class="form-group">
                <input type="radio" value="{{$choices[1]}}" name="answer" id="choice2"> 
                <label class="col-form-label" for="choice2" > {{$choices[1]}} </label>
            </div>
            <div class="form-group">
                <input type="radio" value="{{$choices[2]}}" name="answer" id="choice3"> 
                <label class="col-form-label" for="choice3"> {{$choices[2]}} </label>
            </div>
            <div class="form-group">
                <input type="radio" value="{{$choices[3]}}" name="answer" id="choice4"> 
                <label class="col-form-label" for="choice4"> {{$choices[3]}} </label>
            </div>
        
            <button class="btn btn-success">Save</button>
            </form>
        
            <br>
            <hr>
        
        
        </div>
        </div>
</div>





@endsection

