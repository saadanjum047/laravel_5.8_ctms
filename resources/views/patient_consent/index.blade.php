

@extends('layouts.site')


@section('content')

{{-- <style>
  .content_body{
    background-color: white;

  }
  .content_body > div {
    background-color: white;
  }
</style> --}}

{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

@if ($message = Session::get('success'))
   <script>
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: "{{$message}}",
      showConfirmButton: false,
      timer: 2500
    });
  </script>

@endif

@if ($message = Session::get('error'))
<script>
  Swal.fire({
    position: 'top-end',
    icon: 'error',
    title: "{{$message}}",
    showConfirmButton: false,
    timer: 2500
  });
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     {{-- <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
    --}}
     <li class="breadcrumb-item active" aria-current="page">
     <a href="#" >{{$study->study_name}}</a></li>
     
   
      <li class="breadcrumb-item" aria-current="page">Patient Consent</li>
     </ol>
 </nav>



@php
    $consent = $study->consent->where('active' , 1)->first();
@endphp
{{-- @dd($consent->revisions) --}}

@if($consent && $consent->revisions->where('active' , 1)->first())
<div class="bg-white" style="padding:20px">
  
        <div class="card-title" >
            <h2 class="text-center" >{{$consent->revisions->where('active' , 1)->first()->revision_header}} </h2>
        </div>
        <br>
        <hr>
          {!! ($consent->revisions->where('active' , 1)->first()->revision_des) !!}

          <br>

            <span class="badge badge-success float-right" style="margin-top:30px"> Date: <strong> {{ Carbon\Carbon::parse($consent->date_created)->format('F d-Y ')}} </strong> </span>

            <br>
    
            @if(isset($consent->video_url))
            <p class="alert alert-success" style="margin-top:40px"> <a href="#"> Click Here </a> for Video  </p>
            @endif
    
            <hr>
    
        @if(!isset(auth()->user()->patient->consent) || auth()->user()->patient->consent->status == 0 )
        <p class="alert alert-info"> <i class="fa fa-info"></i>  We will need your signature to confirm! <strong> If You have any concerns, Plz <a href="{{route('dashboard.patient.message.create')}}"> message here <i class="fa fa-envelope" ></i>  </a> ! </strong> </p>
        
        <div style="width:auto; height:200px;margin-top:30px; ">
            <div class="wrapper">
                <canvas id="signature-pad" style="border:1px black solid; border-radius:5px; height:100%; width: 100%; max-width:500px; max-height:200px" class="signature-pad"></canvas>
              </div>
              {{-- width=480 height=200 --}}
              <button id="save-png" class="btn btn-success"> <i class="fa fa-save"></i> Save</button>
              {{-- <button id="save-jpeg">Save as JPEG</button>
              <button id="save-svg">Save as SVG</button>
              <button id="draw">Draw</button>
              <button id="erase">Erase</button>
              <button id="clear">Clear</button> --}}
            </div>
    
        <form action="{{route('dashboard.patient.consent.submit')}}" method="POST" style="display:none" id="main_form">
        {{ csrf_field() }}
        <div class="form-group">        
            <input type="checkbox" id="agreement" value="1" onchange="toggle_button()"  />
            <label for="agreement">I Agree to the terms and services</label>
        </div>
        <input type="hidden" onchange="get_sign()" name="signature" id="signature" value=""   />
    
        <button type="submit" id="submit_btn" class="btn btn-success" disabled >I Agree</button>
        </form>
    @else
    
    <h2>Your Signature</h2>
    
    <img style="border:1px solid #ddd; height:100%; width: 100%; max-width:400px; max-height:200px" src="{{asset('/patient_signatures/'.auth()->user()->patient->consent->signature )}}" />
    
    <p style="margin-top:10px" class="alert alert-info"> <i class="fa fa-info-circle fa-lg"></i> You had submitted agreement on <strong> {{Carbon\Carbon::parse(auth()->user()->patient->consent->acceptence_date)->format('F d-Y ') }} </strong> </p>
    
    <br>
    <a class="btn btn-danger" href="{{route('dashboard.patients.consent.delete')}}" > Delete Signature </a>
    
    @endif
          </div>

@else
    <p class="alert alert-info">No Consent Found</p>
@endif



{{-- <div style="width:500px; height:300px;margin-top:30px; ">
<div class="wrapper">
    <canvas id="signature-pad" style="border:1px black solid" class="signature-pad" width=480 height=200></canvas>
  </div>
  
  <button id="save-png">Save as PNG</button>
  <button id="save-jpeg">Save as JPEG</button>
  <button id="save-svg">Save as SVG</button>
  <button id="draw">Draw</button>
  <button id="erase">Erase</button>
  <button id="clear">Clear</button>
</div> --}}
</div>

<script>
    
    function toggle_button(){
        $('#agreement').val();
        if ($('#agreement').is(":checked")){
            $("#submit_btn").prop("disabled",false);

        }else{
            $("#submit_btn").prop("disabled",true);

        }
    }
    function get_sign(){
        console.log('basic')
        if($('#signature').val() != null ){
            $('#main_form').show();

        }else{
            $('#main_form').hide();

        }

    }

</script>

    <script>
        var canvas = document.getElementById('signature-pad');

        // Adjust canvas coordinate space taking into account pixel ratio,
        // to make it look crisp on mobile devices.
        // This also causes canvas to be cleared.
        function resizeCanvas() {
            // When zoomed out to less than 100%, for some very strange reason,
            // some browsers report devicePixelRatio as less than 1
            // and only part of the canvas is cleared then.
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
        }
        
        window.onresize = resizeCanvas;
        resizeCanvas();
        
        var signaturePad = new SignaturePad(canvas, {
          backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
        });
        
        document.getElementById('save-png').addEventListener('click', function () {
          if (signaturePad.isEmpty()) {
            return alert("Please provide a signature first.");
          }
          
          var data = signaturePad.toDataURL('image/png');
          //console.log(data);
          $('#signature').val(data);
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your signature has been recorded',
            showConfirmButton: false,
            timer: 1500
          });
          $('#main_form').show();

          //window.open(data);
        });
        
        document.getElementById('save-jpeg').addEventListener('click', function () {
          if (signaturePad.isEmpty()) {
            return alert("Please provide a signature first.");
          }
        
          var data = signaturePad.toDataURL('image/jpeg');
          //console.log(data);
          $('#signature').val(data);
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your signature has been recorded',
            showConfirmButton: false,
            timer: 1500
          });
          $('#main_form').show();

          //window.open(data);
        });
        
        document.getElementById('save-svg').addEventListener('click', function () {
          if (signaturePad.isEmpty()) {
            return alert("Please provide a signature first.");
          }
        
          var data = signaturePad.toDataURL('image/svg+xml');
          $('#signature').val(data);
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Your signature has been recorded',
            showConfirmButton: false,
            timer: 2500
          });
          $('#main_form').show();

          //console.log(atob(data.split(',')[1]));
          //window.open(data);
        });
        
        document.getElementById('clear').addEventListener('click', function () {
          signaturePad.clear();
          $('#signature').val('');
          $('#main_form').hide();
        });
        
        document.getElementById('draw').addEventListener('click', function () {
          var ctx = canvas.getContext('2d');
          console.log(ctx.globalCompositeOperation);
          ctx.globalCompositeOperation = 'source-over'; // default value
        });
        
        document.getElementById('erase').addEventListener('click', function () {
          var ctx = canvas.getContext('2d');
          ctx.globalCompositeOperation = 'destination-out';
        });
        
    </script>
    

    
@endsection