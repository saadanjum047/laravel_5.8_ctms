

@extends('layouts.site')


@section('content')

{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

@if ($message = Session::get('success'))
   <script>
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: "{{$message}}",
      showConfirmButton: false,
      timer: 2500
    });
  </script>

@endif

@if ($message = Session::get('error'))
<script>
  Swal.fire({
    position: 'top-end',
    icon: 'error',
    title: "{{$message}}",
    showConfirmButton: false,
    timer: 2500
  });
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
     
   
      <li class="breadcrumb-item" aria-current="page">Patient Consent</li>
     </ol>
 </nav>



<div class="bg-white" style="padding:20px">

    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title"> Patient Consents </h3>
        </div>

<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Patient Name</th>
    <th >Enrolment Date</th>
    <th >Acceptance Status</th>
    <th >Acceptance Date</th>
</tr>

@foreach($patients as $k => $patient)
<tr>
    <td> {{$k+1}} </td>
    <td> {{$patient->user->name}} </td>
    <td> {{ Carbon\Carbon::parse($patient->enrollment_date)->format('F d-Y')}} </td>
    <td> @if(isset($patient->consent)) @if($patient->consent->status == 0) <span class="badge badge-danger">Not Accepted</span> @elseif($patient->consent->status ==1 )<span class="badge badge-success">Accepted</span> @endif  @else <span class="badge badge-danger">Not Accepted</span> @endif  </td>
    <td> @if(isset($patient->consent->acceptence_date)) {{$patient->consent->acceptence_date->toDateString() ?? ''}} @endif </td>
</tr>
@endforeach

</table>

</div>   

</div>


@endsection