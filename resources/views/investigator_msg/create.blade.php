@extends('layouts.site')

@section('content')



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id) }}">{{ $study->study_name}}</a></li>

        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.investigator.messages' , $study->id) }}">Messages</a></li>
        
        <li class="breadcrumb-item active">Create New Message</li>


    </ol>
</nav>
    

<div class="bg-white " style="padding:20px">
    
    @if($errors->any())
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    <form class="col-md-8" action="{{route('dashboard.study.investigator.messages.store' , $study->id)}}" method="POST" enctype="multipart/form-data" >
        @csrf
        <div class="form-group">
            <label class="col-form-label"> Title: </label>
            <input type="text" class="form-control" placeholder="message title" name="title" >
        </div>
        <div class="form-group">
            <label class="col-form-label"> Body: </label>
            <textarea id="summernote" name="body" ></textarea>
        </div>
        <div class="form-group">
            <label class="col-form-label"> Attachment: </label>
            <input class="form-control" type="file" name="attachment" >
        </div>
        <input type="hidden" name="sponsor" value="{{$study->creator->id}}" />

        <button type="submit" class="btn btn-success"> Send Message </button>
    </form>
    
</div>
    
@endsection