@extends('layouts.site')


@section('content')
    

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id)}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item" aria-current="page">
    Protocol Medications
</li>

   </ol>
</nav>

<div class="bg-white">

@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger">{{$error}}</p>
@endforeach
@endif
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Protocol Medications
          <button href="#" style="float:right" class="btn btn-success" data-toggle="modal" data-target="#create_model" ><i class="fa fa-plus"></i> Add New Protocol Medication</button></h3>
        </div>

        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Medicine Name</th>
                <th>Medicine Dose</th>
                <th>Date Created</th>
                <th>Actions</th>
            </tr>

            @foreach ($mediciens as $k=> $med)
            <tr>

                <td>{{$k+1}} </td>
                <td>{{$med->name}} </td>
                <td>{{$med->dose}} </td>
                <td>{{$med->created_at}} </td>
                <td> 
                    <a class="text-primary" onclick="update_model({{$med}})"  ><i class="fa fa-edit fa-lg"></i> </a> |
                    <a class="text-danger" href="{{route('dashboard.study.medicines.delete' , [$study->id , $med->id] )}}"><i class="fa fa-trash fa-lg"></i> </a>
                </td>
              </tr>
            @endforeach

        </table>
    </div>


</div>


<div class="modal fade" id="create_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Protocol Medications</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('dashboard.study.medicines.create' , $study->id)}}" method="POST">
          {{ csrf_field() }}
          <div class="modal-body">
          <div class="form-group">
            <label for="name" class="col-form-label">Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Medicine Name" name="name">
          </div>
          <div class="form-group">
            <label for="dose" class="col-form-label">Dose:</label>
            <input type="text" class="form-control" id="dose" placeholder="Medicine Dose" name="dose">
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Create</button>
        </div>
    </form>
    </div>
  </div>
</div>


<div class="modal fade" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Protocol Medication</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('dashboard.study.medicines.update' , $study->id)}}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="medicine_id" value="" id="medicine_id" />
            <div class="modal-body">
            <div class="form-group">
              <label for="name_up" class="col-form-label">Name:</label>
              <input type="text" class="form-control" id="name_up" placeholder="Medicine Name" name="name">
            </div>
            <div class="form-group">
              <label for="dose_up" class="col-form-label">Dose:</label>
              <input type="text" class="form-control" id="dose_up" placeholder="Medicine Dose" name="dose">
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Update</button>
          </div>
      </form>
      </div>
    </div>
  </div>

  <script>
      function update_model(med){
          $('#update_model').modal('show');
          $('#medicine_id').val(med.id);
          $('#name_up').val(med.name);
          $('#dose_up').val(med.dose);
      }
  </script>

@endsection