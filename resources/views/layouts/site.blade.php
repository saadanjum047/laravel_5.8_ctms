<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>StudyPal</title>
    <link rel="stylesheet" href="{{ asset('node_modules/font-awesome/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('styles')


    <style>
        .notification_rows {
            overflow-wrap: break-word;
            word-wrap: break-word;
            hyphens: auto;
            white-space: normal !important;
          
          }

          .arrow-right {
            width: 0; 
            height: 0; 
            border-top: 10px solid transparent;
            border-bottom: 10px solid transparent;
            border-left: 10px solid #d9edf7;
            position: absolute;
            right:0; 
            margin-right:-8px;
        }
        .arrow-left {
            width: 0; 
            height: 0; 
            left:0;
            position: absolute;
            margin-left: -8px;
            border-top: 10px solid transparent;
            border-bottom: 10px solid transparent; 
            border-right:10px solid #dff0d8; 
          }
    </style>

</head>
<body>
<div class=" container-scroller">
@hasSection('navbar')
    @yield('navbar')
@else
{{--  {{ dd(auth()->user()->patient->study->diary) }}  --}}




    <!--Navbar-->
        <nav class="navbar bg-primary-gradient col-lg-12 col-12 p-0 fixed-top navbar-inverse d-flex flex-row">
            <div class="bg-white text-center navbar-brand-wrapper">
              
                @if( auth()->user()->role_id == 2 )
                @if(isset($study))
                <a class="navbar-brand brand-logo" href="{{ route('dashboard.study.view' , [$study->id , 'English']) }}"><img  @if(isset($study->bkground_img)) src="{{url('/')}}/studies/{{ $study->bkground_img }}" @else src="{{ asset('images/sp_logo_back.png') }}" @endif style="max-width:auto; max-height:50px" /></a>  
                @else
                <a class="navbar-brand brand-logo"  href="{{ route('home') }}" ><img src="{{url('/')}}/images/sp_logo_back.png" style="max-width:auto; max-height:50px" /></a>
                @endif
                @endif

                @if(auth()->user()->role_id == 4 )
                <a class="navbar-brand brand-logo"  href="{{ route('home') }}" ><img
                     @if(isset(auth()->user()->patient->study->bkground_img)) src="{{ asset('studies/'.auth()->user()->patient->study->bkground_img) }}" @else src="{{ asset('images/sp_logo_back.png') }}" @endif  style="max-width:auto; max-height:50px" /></a>
                @elseif(auth()->user()->role_id == 3 )
                    <a class="navbar-brand brand-logo"  href="{{ route('home') }}" ><img
                    @if(isset(auth()->user()->sponsor_user->study->bkground_img)) src="{{ asset('studies/'.auth()->user()->sponsor_user->study->bkground_img) }}" @else src="{{ asset('images/sp_logo_back.png') }}" @endif style="max-width:auto; max-height:50px" /></a>

               
                @endif
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center">
                <button class="navbar-toggler navbar-toggler hidden-md-down align-self-center mr-3" type="button"
                        data-toggle="minimize">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <form class="form-inline mt-2 mt-md-0 hidden-md-down">
                    <input class="form-control mr-sm-2 search" type="text" placeholder="Search">
                </form>
                <ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
                    <li class="nav-item">
                        <a class="nav-link profile-pic" href="#">
                            @isset(auth()->user()->avatar )
                            <img class="rounded-circle" src="{{ asset('/avatars/'.auth()->user()->avatar) }}" alt="">
                            @else
                            <img class="rounded-circle" src="{{ asset('images/face.jpg') }}" alt="">

                            @endif
                        </a>
                    </li>
                    <li class="nav-item dropdown ">
                        {{--  notification icon  --}}
                        <a class="nav-link" onclick="toggle_notification()" href="#"><i class="fa fa-bell"></i> <div style="background-color:red; radius-border:200px; " ></div> </a>
                        
                        {{--  notification details  --}}
                        
                        <div id="notification_panel" onblur="close_notification" style="width:300px;" class="dropdown-menu dropdown-menu-lg dropdown-menu-right show" >
                            <span class="dropdown-item dropdown-header">{{count(auth()->user()->notifications) }} Notifications</span>
                            @if(count(auth()->user()->notifications) > 0 )
                            @foreach (auth()->user()->notifications->sortByDesc('created_at') as $k=> $notification)
                            
                            <div class="dropdown-divider" ></div>
                            {{-- <a href="@if(!is_null(auth()) && isset(auth()->user()->patient->study )) {{route('dashboard.study.view' , auth()->user()->patient->study->id )}} @else {{'#'}} @endif" class="dropdown-item"> --}}
                            
                            <a href="#" class="dropdown-item notification_rows" style=" " >
                            
                                <i class="text-primary fa fa-bell mr-2"></i>
                              {{$notification->text}}
                              <span class="float-right text-muted text-sm ">
                                  @php
                                  $mints = \Carbon\Carbon::now()->diffInMinutes($notification->created_at);
                                  $hours = \Carbon\Carbon::now()->diffInHours($notification->created_at);
                                  $days = \Carbon\Carbon::now()->diffInDays($notification->created_at);
                                  @endphp
                                  @if($mints < 60 ) {{$mints}} mints ago
                                  @elseif($hours < 24 )  {{$hours}} Hours ago
                                  @else {{$days}} days ago
                                  @endif
                                </span>
                            </a>  
                            @php
                            if($k > 4){
                                break;
                            }
                            @endphp
                            
                            @endforeach
                            <div class="dropdown-divider"></div>                            

                            {{--  <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>  --}}
                          </div>
                          
                          @endif
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right hidden-lg-up align-self-center" type="button"
                        data-toggle="offcanvas">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <!--End navbar-->
    @endif
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-right">
            <nav class="bg-white sidebar sidebar-fixed sidebar-offcanvas" id="sidebar">
                <div class="user-info" >
                    @isset(auth()->user()->avatar )
                    {{-- <img src="{{ asset('images/face.jpg') }}" alt=""> --}}

                        <img src="/avatars/{{ auth()->user()->avatar }}" alt="Avatar" style="max-width:85px; max-height:100px" >
                    @else
                        <img src="{{ asset('images/face.jpg') }}" alt="">
                    @endisset
                    <div class="">
                    <p class="name">
                    
                        {{--  @if(Cookie::get('user_name'))
                            {{ Cookie::get('user_name') }}
                        @endif  --}}
                        
                         @if(auth()->user()->name)
                            {{ auth()->user()->name }} 
                            @if(auth()->user()->role_id == 4)
                            
                            {{--  @dd(auth()->user()->patient->points->earned_score)  --}}
                            @if(  auth()->user()->patient->points->earned_score > 49 && auth()->user()->patient->points->earned_score < 100 )
                            <img src="{{url('/')}}/medals/medal.png" style="width:50px; height:50px" />
                            @elseif(auth()->user()->patient->points->earned_score > 99 && auth()->user()->patient->points->earned_score < 150)
                            <img src="{{url('/')}}/medals/silver-medal.png" style="width:50px; height:50px" />
                            @elseif(auth()->user()->patient->points->earned_score > 149 )
                            <img src="{{url('/')}}/medals/gold-medal.png" style="width:50px; height:50px" />

                            @endif
                            @endif
                        @endif

                    </p>
                </div>
                    <p class="designation">
                        {{--  @if((int)Cookie::get('user_role')==3)  --}}
                        @if(auth()->user()->role_id ==3)
                            Investigator
                        {{--  @elseif((int)Cookie::get('user_role')==2)  --}}
                        @elseif(auth()->user()->role_id ==2)
                            Sponsor
                        @elseif(auth()->user()->role_id > 4)
                            Side Role
                        @endif
                    </p>
                    <span class="online"></span>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();" class="btn btn-sm">Logout</a>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                    <a class="btn btn-sm" href="{{ route('dashboard.user.edit') }}">
                        <!-- <i class="fa fa-wpforms"></i> -->
                        <i class="fa fa-edit text-successDashboard"></i>
                        <span class="menu-title">Edit Profile</span>
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('dashboard.index') }}">
                            <!-- <i class="fa fa-dashboard"></i> -->
                            <img src="{{ asset('images/icons/1.png') }}" alt="Dashboard">
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    {{--  {{dd(auth()->user()->sponsor_user->study->id) }}  --}}
                {{--  if user is a side role  --}}
                @if(auth()->user()->role_id > 4)
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('dashboard.study.view' , auth()->user()->sponsor_user->study->id) }}">
                        <img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Dashboard">
                        <span class="menu-title">Study</span>
                    </a>
                </li>
                @endif
				<!-- If user is a patient -->
				{{--  @if((int)Cookie::get('user_role')!=2 && (int)Cookie::get('user_role')!=3)  --}}
				@if( auth()->user()->role_id == 1 ||  auth()->user()->role_id == 4)
				{{--  <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.patient.notes') }}">
                                <img src="{{ asset('images/icons/notes.png') }}" alt="">
                                <span class="menu-title">Patient Notes</span>
                            </a>
                        </li>  --}}

                        <li class="nav-item">
                            <a class="nav-link" href="{{route('dashboard.patient.consent')}}">
                                <img src="{{ asset('images/icons/notes.png') }}" alt="">
                                <span class="menu-title">Patient Consent</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.tasks.all') }}">
                                <img src="{{ asset('images/icons/completed-task.png') }}" alt="">
                                <span class="menu-title">Patient Tasks</span>
                            </a>
                        </li>
                      
                      
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.patient_engagements') }}">
                                <img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="">
                                <span class="menu-title">Patient Engagements</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.patient.messages') }}"> <img src="{{ asset('images/icons/messages.png') }}" alt="">
                                <span class="menu-title">Messages</span>
                            </a>
                        </li>
                     @endif
					{{--  @if((int)Cookie::get('user_role')==2)  --}}
					@if(auth()->user()->role_id == 2)
					@isset($study)	
					{{--  <li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.site.ViewSites',  $study->id) }}">
						<img src="{{ asset('images/icons/hospital.svg') }}" alt=" Sites" >Sites </a>
					</li>  --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.chart', $study->id) }}">
                            <img src="{{ asset('images/icons/chart.png') }}" alt="">
                            <span class="menu-title">See Chart</span>
                        </a>
                    </li>

                    <li class="nav-item">	
                        <a class="nav-link" href="{{ route('dashboard.study.sites', $study->id) }}"><img src="{{ asset('images/icons/hospital.svg') }}" alt="Sites ">Sites </a>
                    </li>

					<li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.study.viewusers',  $study->id) }}">
						<img src="{{ asset('images/icons/team.svg') }}" alt=" Users">Users </a>
                    </li>
                    <li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.study.newsletter' , $study->id ) }}">
						<img src="{{ asset('images/icons/email.png') }}" alt=" Users">Newsletter </a>
                    </li>
                    <li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.study.investigator.groups' , $study->id ) }}">
						<img src="{{ asset('images/icons/completed-task.png') }}" alt=" Users">Investigator Tasks </a>
                    </li>
                     <li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.study.groups' , $study->id ) }}">
						<img src="{{ asset('images/icons/completed-task.png') }}" alt=" Users">Patient Tasks </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.pre_screening' , $study->id) }}">
                            <img src="{{ asset('images/icons/monitor.png') }}" alt="">
                            <span class="menu-title">Pre Screening</span>
                        </a>
                    </li> 
                    <li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.study.medicines' , $study->id ) }}">
						<img src="{{ asset('images/icons/drug.png') }}" alt=" Users">Protocol Medications </a>
					</li>
					{{-- <li class="nav-item">	
						<a  class="nav-link" href="#">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Study Documents</a>
                    </li> --}}
                    <li class="nav-item">	
                    <a class="nav-link" href="{{ route('dashboard.study.patient.images' , $study->id) }}">
                    <img src="{{ asset('images/icons/notes.png') }}" alt="">
                    <span class="menu-title">Patient images</span>
                    </a>
                    </li>
                    <li class="nav-item">	
						<a  class="nav-link" href="{{route('dashboard.study.documents' , $study->id)}}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Study Documents</a>
					</li>
					<li class="nav-item">	
						<a  class="nav-link" href="{{ route('dashboard.study.schedules' , $study->id) }}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Visit Schedule">Visit Schedule</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.messages.investigators' , $study->id ) }}">
                            <img src="{{ asset('images/icons/messages.png') }}" alt="">
                            <span class="menu-title">Investigator Messages </span>
                        </a>
                    </li>

        {{-- @dd(url()->current()) --}}
                    
					<li class="nav-item" style="">	
						<a class="nav-link dropdown-btn @if(str_contains(url()->current() , 'contents')) active @endif" onclick="change_arrow()" >
						<img src="{{ asset('images/icons/1055672.svg') }}" alt="">
						Protocol
                        <i id="protocol_arr" class="fa fa-caret-up"></i> </a>
                        
						<ul class="dropdown-container" @if(str_contains(url()->current() , 'contents')) style="display:block" @endif >
							<a class="nav-link pull-left" href="{{ route('dashboard.study.contents',$study->id) }}"><i class="fa fa-eye" aria-hidden="true"></i>View contents</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.add',$study->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Add contents</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.add.url',$study->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Add Url contents</a>
							<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.calculator',$study->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Add  Calculator</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.document',$study->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Add  Document</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.document',$study->id) }}"><i class="fa fa-snowflake-o" aria-hidden="true"></i>Trial Solutions</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.solutions',$study->id) }}"><i class="fa fa-medkit" aria-hidden="true"></i>Devices</a>
						</ul>
					</li>
					{{--  <li class="nav-item">	
						<a  class="nav-link" href="{{ route('dashboard.study.studyschedule',  $study->id) }}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Patient Diary</a>
                    </li>  --}}
                    <li class="nav-item">	
						<a  class="nav-link" href="{{ route('dashboard.study.diary',  $study->id) }}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Patient Diary</a>
					</li>
					{{-- <li class="nav-item">	
						<a  class="nav-link" href="{{ route('dashboard.study.studyschedule',  $study->id) }}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Reported Outcomes</a>
					</li> --}}
					{{--  <li class="nav-item">	
						<a  class="nav-link" >
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Patient Engagement">Patient Engagement</a>
                    </li>  --}}

                    <li class="nav-item" style="">	
						<a class="nav-link dropdown-btn @if(str_contains( url()->current() , 'engagements')) active @endif" onclick="change_patient_arrow()" >
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="">
						Patient Engaemens
                        <i id="pateint_eng_arr" class="fa fa-caret-up"></i> </a>
                        
						<ul class="dropdown-container" @if(str_contains(url()->current() , 'engagements')) style="display:block" @endif >
                            <a class="nav-link pull-left" href="{{ route('dashboard.study.patient_engagements',  $study->id) }}" ><i class="fa fa-question-circle" aria-hidden="true"></i>Mystrey Questions</a>
						</ul>
					</li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('dashboard/study/'.$study->id.'/workflow')}}">
                            <img src="{{ asset('images/icons/eye.png') }}" alt="">
                            <span class="menu-title">Visit Guide</span>
                        </a>
                    </li>
					 	@endif
					@endif
					{{--  @if((int)Cookie::get('user_role')==2)  --}}
                    @if(auth()->user()->role_id ==2)
                   
 						@isset($Study)
							<li class="nav-item">	
								<a class="nav-link" href="{{ route('dashboard.site.ViewSites', $Study) }}"><img src="{{ asset('images/icons/hospital.svg') }}" alt="Sites ">Sites </a>
                            </li>
                          
							<li class="nav-item">	
								<a  class="nav-link" href="{{ route('dashboard.study.viewusers', $Study) }}">
								<img src="{{ asset('images/icons/team.svg') }}" alt="Users ">Users </a>
							</li>
							<li class="nav-item">	
								<a  class="nav-link" href="#">
 								<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Study Documents</a>
							</li>
					<li class="nav-item" style="">	
						<a class="nav-link dropdown-btn">
						<img src="{{ asset('images/icons/1055672.svg') }}" alt="">Protocol
						<i class="fa fa-caret-down"></i></a>
						<ul class="dropdown-container">
							<a class="nav-link pull-left" href="{{ route('dashboard.study.contents',$Study) }}">View contents</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.add',$Study) }}">Add contents</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.add.url',$Study) }}">Add Url contents</a>
							<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.calculator',$Study) }}">Add Calculator</a>
				 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.document',$Study) }}">Add Document</a>
						</ul>
                    </li>
                  
		 @endif
			
 		@endif


                    {{--  @if((int)Cookie::get('user_role')==3)  --}}
                    @if( auth()->user()->role_id ==3)
                    @if(isset($study))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.pre_screening' , $study->id) }}">
                            <img src="{{ asset('images/icons/monitor.png') }}" alt="">
                            <span class="menu-title">Pre Screening</span>
                        </a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.investigator.messages' , $study->id) }}">
                            <img src="{{ asset('images/icons/messages.png') }}" alt="">
                            <span class="menu-title">Messages</span>
                        </a>
                    </li>
                    <li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.study.investigator.groups.all' , $study->id ) }}">
						<img src="{{ asset('images/icons/completed-task.png') }}" alt=" Users">Investigator Tasks </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.patients.messages' , $study->id) }}"><img src="{{ asset('images/icons/messages.png') }}" alt=""><span class="menu-title">Patient Messages</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.patient.images.upload' , $study->id) }}">
                        <img src="{{ asset('images/icons/notes.png') }}" alt="">
                        <span class="menu-title">Patient images</span>
                    </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.patient.register') }}">
                            <!-- <i class="fa fa-wpforms"></i> -->
                            <img src="{{ asset('images/icons/3.png') }}" alt="">
                            <span class="menu-title">Register Patient</span>
                        </a>
                    </li>
                  
                
                    @if(isset($study->id))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('dashboard.study.patients.consent' , $study->id)}}">
                            <img src="{{ asset('images/icons/notes.png') }}" alt="">
                            <span class="menu-title">Patient Consent</span>
                        </a>
                    </li>
                    <li class="nav-item">	
						<a  class="nav-link" href="{{route('dashboard.study.documents.view' , $study->id)}}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Study Documents</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.export', $study->id) }}"><img src="{{ asset('images/icons/export.png') }}" alt="">
                            <span class="menu-title">Export Diary</span>
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.pre_screening.all' , $study->id) }}">
                            <!-- <i class="fa fa-wpforms"></i> -->
                            <img src="{{ asset('images/icons/monitor.png') }}" alt="">
                            <span class="menu-title">Pre Screenings</span>
                        </a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.newsletters' , $study->id ) }}">
                            <!-- <i class="fa fa-wpforms"></i> -->
                            <img src="{{ asset('images/icons/email.png') }}" alt="">
                            <span class="menu-title">Newsletters</span>
                        </a>
                    </li>
                    @endif
                
                    {{--  Roles and permission tabs here  --}}
                    @if(isset($study))

                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 12 )->toArray() ) )

                    <li class="nav-item">	
						<a  class="nav-link" href="{{ route('dashboard.study.schedules' , $study->id) }}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Visit Schedule</a>
					</li>
                    @endif
                    
                    
                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 13 )->toArray() ) )
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.study.groups' , $study->id) }}">
                            <img src="{{ asset('images/icons/completed-task.png') }}" alt="">
                            <span class="menu-title">Patient Tasks</span>
                        </a>
                    </li>
                    @endif

                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 14 )->toArray() ) )
                    <li class="nav-item">	
						<a class="nav-link" href="{{ route('dashboard.study.newsletter' , $study->id ) }}">
						<img src="{{ asset('images/icons/email.png') }}" alt=" Users"> Study Newsletter </a>
                    </li>
                    @endif

                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 15 )->toArray() ) )
                    <li class="nav-item">	
						<a  class="nav-link" href="{{ route('dashboard.study.diary',  $study->id) }}">
						<img src="{{ asset('images/icons/document-library-icon_309078.png') }}" alt="Study Documents">Patient Diary</a>
					</li>
                    @endif
                   

                    {{--  study if  --}}
                    @endif

                    @if(isset(auth()->user()->sponsor_user->tasks ))

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.tasks') }}">
                                <!-- <i class="fa fa-wpforms"></i> -->
                                <img src="{{ asset('images/icons/completed-task.png') }}" alt="">
                                <span class="menu-title">Tasks</span>
                            </a>
                        </li>
                        @endif
                        
                        {{--  <li class="nav-item">			 
                            <a  class="nav-link" href="{{ route('dashboard.study_schedule')}}">
                            <img src="{{ asset('images/icons/time.png') }}" alt=" Organizations">
                            Visit Schedule</a>
                        </li>  --}}
                    @endif

                    {{--  @if((int)Cookie::get('user_role')!==1 && (int)Cookie::get('user_role')!==2)  --}}
                        {{-- for investigator menu --}}
                    @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.patient.list') }}">
                                <!-- <i class="fa fa-wpforms"></i> -->
                                <img src="{{ asset('images/icons/group.png') }}" alt="">
                                <span class="menu-title">Patients List</span>
                            </a>
                        </li> --}}
                        {{-- <li class="nav-item">			 
                            <a  class="nav-link" href="{{ route('dashboard.patient') }}">
                            <img src="{{ asset('images/icons/patients.png') }}" alt=" Organizations">
                            Patients</a></li> --}}
                            
                    @endif

                    @isset($study)
                   
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.study.export', $study->id) }}"><img src="{{ asset('images/icons/export.png') }}" alt="">
                                <span class="menu-title">Export Diary</span>
                            </a>
                        </li> --}}
									
                    @endif





               {{--  @if((int)Cookie::get('user_role')==2 ||(int)Cookie::get('user_role')==3)  --}}
             
            {{-- Sponsor Dashbaord --}}

			<!-- If logged in user is a Sponsor -->
			 {{--  @if((int)Cookie::get('user_role')==2)  --}}
			 @if(auth()->user()->role_id==2)
 			@isset($study)
			@else


				<li class="nav-item">	
						 
				<a  class="nav-link" href="{{ route('dashboard.organizations') }}">
				<img src="{{ asset('images/icons/healthcare.svg') }}" alt=" Organizations">
				Organizations</a></li>
				<li class="nav-item">			 
				{{--<a  class="nav-link" href="{{ route('dashboard.Studies') }}">
				 <img src="{{ asset('images/icons/study.svg') }}" alt=" Studies ">
                Studies </a></li>--}}
                
                {{--  @if((int)Cookie::get('user_role')==2)  --}}
                @if(auth()->user()->role_id ==2)


				{{--  <li class="nav-item">	
						 
				<a  class="nav-link" href="{{ route('dashboard.patientdiary') }}">
				<img src="{{ asset('images/icons/question.png') }}" alt=" Organizations">
                Diary Questions</a></li>
                  --}}
                {{--  <li class="nav-item">			 
				<a  class="nav-link" href="{{ route('dashboard.diaries') }}">
				<img src="{{ asset('images/icons/3.png') }}" alt=" Organizations">
                Diary</a></li>
                  --}}
                <li class="nav-item">			 
				<a  class="nav-link" href="{{ route('dashboard.sponsor_roles') }}">
				<img src="{{ asset('images/icons/key.png') }}" alt=" Organizations">
                Roles</a></li>
                
                {{--  <li class="nav-item">			 
				<a  class="nav-link" href="{{ route('dashboard.modules') }}">
				<img src="{{ asset('images/icons/motherboard.png') }}" alt=" Organizations">
                Modules</a></li>  --}}
                
                {{-- <li class="nav-item">			 
				<a  class="nav-link" href="{{ route('dashboard.patient.register') }}">
				<img src="{{ asset('images/icons/clipboard.png') }}" alt=" Organizations">
                Register Patients</a></li> --}}
                
                {{--  <li class="nav-item">			 
				<a  class="nav-link" href="{{ route('dashboard.schedules_Patients') }}">
				<img src="{{ asset('images/icons/clipboard.png') }}" alt=" Organizations">
                Scheduled Patients</a></li>
                  --}}
                   
                {{-- <li class="nav-item">			 
                <a  class="nav-link" href="{{ route('dashboard.patient') }}">
                <img src="{{ asset('images/icons/patients.png') }}" alt=" Organizations">
                Patients</a></li> --}}
                
                    
                {{--  <li class="nav-item">			 
				<a  class="nav-link" href="{{ route('dashboard.study_schedule') }}">
				<img src="{{ asset('images/icons/time.png') }}" alt=" Organizations">
                Study Schedules </a></li>
               --}}
              
                
				<li class="nav-item">			 
				</li>
            @endif
            
			@endif
			@endif


			
            {{--  @if((int)Cookie::get('user_role')==2 ||(int)Cookie::get('user_role')==3)  --}}
            @if(auth()->user()->role_id == 2 || auth()->user()->role_id == 3 )
            @isset($study)
                
                <li class="nav-item" style="margin-bottom:60px">	
            
                    <a class="nav-link dropdown-btn">
                        <img src="{{ asset('images/icons/report_magnify.png') }}" alt="">
                        Reports 
                        <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-container">
                        <a class="nav-link pull-right" href="{{ route('dashboard.study.diarydashboard', $study->id) }}">Diary Dashboard</a>
                    <a class="nav-link pull-right" href="{{ route('dashboard.study.schedules' , $study->id) }}">Study Schedule</a>
                        </ul>
                </li>
            
                                        @endif

        
                    @isset($Study)
                                    

                <li class="nav-item" style="margin-bottom:60px">	
                
                    <a class="nav-link dropdown-btn">
                        <img src="{{ asset('images/icons/report_magnify.png') }}" alt="">
                        Reports 
                        <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-container">
                        <a class="nav-link pull-right" href="{{ route('dashboard.study.diarydashboard', $Study) }}">Diary Dashboard</a>
                        <a class="nav-link pull-right" href="{{ route('dashboard.study.studyschedule', $Study) }}">Study Schedule</a>
                        </ul>
                </li>

            @else
                
        
                    
                @endif
            @endif


            






		<?php $patient_id = Session::get('user_id');
		$get_study_id=DB::table('tip_study_patient')->where('patient_id', $patient_id)->get();?>
        @if($get_study_id->count()>0)
        
        {{--  @if((int)Cookie::get('user_role')==2 ||(int)Cookie::get('user_role')==3)  --}}
        
		@if(auth()->user()->role_id ==2 ||auth()->user()->role_id==3)
			<li class="nav-item" style="display:none">	
				<a class="nav-link dropdown-btn">
		 		<img src="{{ asset('images/icons/1055672.svg') }}" alt="">
				Protocol
				<i class="fa fa-caret-down"></i></a>
				<ul class="dropdown-container">
				<a class="nav-link pull-left" href="{{ route('dashboard.study.contents',$get_study_id[0]->study_id) }}">View contents</a>
	 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.add',$get_study_id[0]->study_id) }}" >Add contents</a>
	 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.add.url',$get_study_id[0]->study_id) }}">Add Url contents</a>
				<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.calculator',$get_study_id[0]->study_id) }}">Add Calculator</a>
	 			<a class="nav-link pull-left" href="{{ route('dashboard.study.contents.document',$get_study_id[0]->study_id) }}">Add Document</a>
				</ul>
		    </li>
		      
		
     
           @endif
 @endif
 



<script>
function issue(){
alert("Dear Bro You Know on Sponser we were adding protocols into the study i mean we were getting studyID first then adding protocol"+"\r\n"+ 
"So here on patient do you want to add protocol with out studyID if yes then i have to remove study id from url and have to make new route without studyID. OK");

}
</script>

    </ul> 
				

            </nav>
            <!-- SIDEBAR ENDS -->

            <div class="content-wrapper" style="margin-top: 20px">

                @yield('content')
            </div>

            <footer class="footer">
                <div class="container-fluid clearfix">
                      <span class="float-right">
                          <a href="#">StudyPal</a> &copy; 2017
                      </span>
                </div>
            </footer>
        </div>
    </div>
</div>

<script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('node_modules/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<script src="{{ asset('node_modules/chart.js/dist/Chart.min.js') }}"></script>
{{--<script src="{{ asset('node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>--}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5NXz9eVnyJOA81wimI8WYE08kW_JMe8g&callback=initMap" async
        defer></script>
<script src="{{ asset('js/off-canvas.js') }}"></script>
<script src="{{ asset('js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('js/misc.js') }}"></script>
<script src="{{ asset('js/chart.js') }}"></script>
<script src="{{ asset('js/maps.js') }}"></script>
<style>
.dropdown-container {
  display: none;
  
  padding-left: 8px;
}

.fa-caret-down {
  float: right;
  padding-right: 8px;
}

</style>
<script>
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;
for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>

 <script>

    function close_notification(){
        console.log('blur');
        $('#notification_panel').hide();
    }


    $(document).ready(function() {
        $('#summernote').summernote();
    });
    
      function toggle_notification(){
          console.log('notify');
          $('#notification_panel').toggle();
      }

     function change_arrow(){
        var c1 = 'fa fa-caret-up' ;
        var c2 = 'fa fa-caret-down' ;

         console.log( $('#protocol_arr').attr('class') );

        if( $('#protocol_arr').attr('class') == c1 ){
            $('#protocol_arr').removeClass(c1).addClass(c2);
        }else{
            $('#protocol_arr').removeClass(c2).addClass(c1);

        }

     }

     change_patient_arrow()
     {
        var c1 = 'fa fa-caret-up' ;
        var c2 = 'fa fa-caret-down' ;

         console.log( $('#pateint_eng_arr').attr('class') );

        if( $('#pateint_eng_arr').attr('class') == c1 ){
            $('#pateint_eng_arr').removeClass(c1).addClass(c2);
        }else{
            $('#pateint_eng_arr').removeClass(c2).addClass(c1);

        }

     }

  </script>


@yield('scripts')

</body>
</html>
