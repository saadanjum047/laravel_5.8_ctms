@extends('layouts.site')

@section('content')



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        
        {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , auth()->user()->patient->study->id) }}">{{ auth()->user()->patient->study->study_name}}</a></li> --}}

        <li class="breadcrumb-item"><a href="#">{{ auth()->user()->patient->study->study_name}}</a></li>
    </ol>
</nav>
    

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<div class="bg-white" style="padding:20px">

    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Messages
          <a href="{{ route('dashboard.patient.message.create') }}" style="float:right" class="btn btn-primary"  ><i class="fa fa-envelope"></i> Send New Message </a></h3>
        </div>

    <table class="table table-hover">
    <tr>
        <th>#</th>
        <th width="40%" >Title</th>
        <th width="30%" >Attachment</th>
        <th width="20%" >Investigator</th>
        <th width="10%" >Created</th>
    </tr>

    
    @foreach ($messages as $k => $msg)
    <tr>
        <td> {{$k+1}} </td>
        <td>
            <a href="{{route('dashboard.patient.message.show', $msg->id )}}"> {{$msg->title}} </a>
             </td>
        <td> {{$msg->attachment}} </td>
        <td> {{$msg->investigator->user->name}} </td>
        <td> {{($msg->created_at)->toDateString()}} </td>
    </tr>
    @endforeach
    

</table>

</div>
</div>
    
@endsection