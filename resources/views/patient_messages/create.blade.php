@extends('layouts.site')

@section('content')



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , auth()->user()->patient->study->id) }}">{{ auth()->user()->patient->study->study_name}}</a></li> --}}

        <li class="breadcrumb-item"><a href="{{ route('dashboard.patient.messages') }}">Messages</a></li>
        
        <li class="breadcrumb-item active">Create New Message</li>


    </ol>
</nav>
    

<div class="bg-white " style="padding:20px">
    
    @if($errors->any())
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    <form class="col-md-8" action="{{route('dashboard.patient.message.store')}}" method="POST" enctype="multipart/form-data" >
        @csrf
        <div class="form-group">
            <label class="col-form-label"> Title: </label>
            <input type="text" class="form-control" placeholder="message title" name="title" >
        </div>
        <div class="form-group">
            <label class="col-form-label"> Body: </label>
            <textarea id="summernote" name="body" ></textarea>
        </div>
        
        <div class="form-group">
            <label class="col-form-label"> Attachment: </label>
            <input type="file" class="form-control"  name="attachment" >
        </div>

        <div class="form-group">
            <label class="col-form-label"> Investigator: </label>
            <select class="form-control" name="investigator_id">
                <option value=""> Select Any Option </option>
                @foreach($investigators as $investigator)
                @if(isset($investigator->user))
                <option value="{{$investigator->id}}">{{$investigator->user->name ?? '-'}} </option>
                @endif
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-success"> Send Message </button>
    </form>
    
</div>
    
@endsection