@extends('layouts.site')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
@section('content')
@if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')!==1)
  <?php $Study=$study_id;?>
<div class="col col-lg-12">
	<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
        <li class="breadcrumb-item active" aria-current="page">
		<a href="{{ route('dashboard.study', [$study_id,'English']) }}" >{{$response->study_name->study_name}}</a></li>
		<li class="breadcrumb-item" aria-current="page">Study Schedule</li>
		</ol>
	</nav>
    <div class="card">
             <div class="card-block">
		<h5 class="card-title mb-4 pull-left">Study Schedule</h5>
			
			   <a class="btn btn-primary pull-right"  href="{{ route('dashboard.study.StudyScheduleExport', $study_id) }}"><i class="fa fa-file-excel-o"></i> Overview Report</a>
			  <div class="table-responsive" >
                  <table  class="table table-striped table-bordered" cellspacing="0" width="100%">
                       
                        <form method="POST" id="visit_schedule" >
						    <div class="alert alert-danger print-error-msg" style="display:none">
				            <ul></ul>
				            </div>
				
				
				            <div class="alert alert-success print-success-msg" style="display:none">
				            <ul></ul>
				            </div>
			                 <table class="table table-bordered table-striped" id="dynamic_field">
			               <thead>
	                        <tr class="text-primary">
	                            <th>Visit Name</th>
	                            <th>Low Target</th>
	                            <th>Target</th>
	                            <th>High Target</th>
	                        </tr>
			               </thead>
			               <tbody>
			
			               </tbody>
			               <tfoot>
			                <tr>
	                            <td><input type="text" name="visit_name[]" placeholder="Enter Visit Name" class="form-control name_list" /></td>  
	                            <td><input type="text" name="low_target[]" placeholder="Enter Low Target" class="form-control name_list" /></td>  
	                            <td><input type="text" name="target[]" placeholder="Enter Visit Target" class="form-control name_list" /></td>  
	                            <td><input type="text" name="high_target[]" placeholder="Enter High Target" class="form-control name_list" /></td>  
								<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>
			                 
			                 
			                </tr>
			               </tfoot>
			           </table>
			            @csrf
			                  <input type="submit" name="save" id="save" class="btn btn-primary" value="Save" />
						</form>
                       
                        <?php 
	                        
	                       // dd($response);
	                       // die();
	                       //{{ route("createvisitschedulepost") }}
	                       ?>
                       
            </table>
        </div>
    </div>
</div>
   
</div>


@endif
@endsection

<script type="text/javascript">
    $(document).ready(function(){      
      var postURL = "<?php echo url('addmore'); ?>";
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="visit_name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input type="text" name="low_target[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input type="text" name="target[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input type="text" name="high_target[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                    }
                }  
           });  
      });  


      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      }
    });  
</script>