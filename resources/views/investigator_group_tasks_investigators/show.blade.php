
@extends('layouts.site')


@section('content')
    
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    


     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.investigator.groups.all' , $study->id )}}">Investigator Tasks</a></li>    

     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.investigator.groups.all' , $study->id )}}">Groups</a></li>    
     
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.investigator.group.task.all' , [$study->id , $group->id] )}}">Tasks</a></li>    
     
     
      <li class="breadcrumb-item" aria-current="page">Task Details</li>
     </ol>
 </nav>


<div class="bg-white" style="padding:10px">
    <h2> {{$task->name}} </h2>  <br>
    {{$task->description}}
    <hr>
</div>
@endsection
