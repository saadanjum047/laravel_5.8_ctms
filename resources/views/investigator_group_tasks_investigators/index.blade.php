


@extends('layouts.site')


@section('content')



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     
      <li class="breadcrumb-item" aria-current="page">Investigators Tasks</li>
     </ol>
 </nav>

    
<div class="bg-white" style="padding:10px">
<div class="card">

    
    <div class="card-title">
        <h2>Groups</h2>
    </div>
    <table class="table">
        <tr> 
            <th> # </th>
            <th> Group Name </th>
            <th> Group Description </th>
            <th> Created </th>
            <th> No. of Tasks </th>
        </tr>
        @if($groups)
        @foreach ($groups as $k=> $group)
        <tr>    
            <td> {{$k+1}} </td>
            <td> <a href="{{route('dashboard.study.investigator.group.task.all' , [ $study->id , $group->id])}}">{{$group->name}}</a> </td>
            <td> {{ str_limit(strip_tags($group->description) , 80)}} </td>
            <td> {{($group->created_at)->toDateString()}} </td>
            <td> {{$group->tasks->count()}}  </td>
        </tr>
        @endforeach
        @endif
    </table>

</div>
</div>
@endsection