@extends('layouts.site')


@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{ $message }}", "success");
</script>
@endif

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , [$study->id , 'English'] )}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item" aria-current="page">
    Pre Screenings
</li>

   </ol>
</nav>

<div class="bg-white">
    <div class="card">
    <div class="card-header bg-white">
            
        <h3 class="card-title">Pre Screenings</h3>


            @if($errors->any())
            @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @endif
         
      </div>
      <table class="table table-hover">
    <tr>
        <th>#</th>
        <th>Pre Screenings Name</th>
        <th>Study</th>
        <th>End Duration</th>
    </tr>
    @foreach ($screens as $i => $screen)
        
    <tr>
        <td>{{$i+1}} </td>
        <td>
          {{-- <a href="{{route('dashboard.study.diary.questions' ,[ $study->id , $diary->id ])}}"> --}}
          {{-- <a href="{{route('dashboard.study.diary.patients' ,[ $study->id , $diary->id ])}}"> --}}
          <a href="{{route('dashboard.study.pre_screening.viewQuestions' , [$screen->study->id , $screen->id])}}">
             {{$screen->name}} </a> </td>
        <td> {{$screen->study->study_name}} </td>
        <td> {{$screen->end_duration}} </td>
       
    </tr>

    @endforeach

</table>
</div>

</div>

{{--  <div class="modal fade" id="create_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Pre Screenings</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.study.pre_screening' , $study->id)}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <label for="screening-name" class="col-form-label">Pre Screen Name:</label>
              <input type="text" class="form-control" id="screening-name" name="screening_name">
            </div>
            <div class="form-group">
                <label for="screening-frequency" class="col-form-label">Frequency:</label>
                <input type="number" class="form-control" id="screening-frequency" name="screening_frequency">
              </div>
              <div class="form-group">
                <label for="end_duration" class="col-form-label">End Duration:</label>
                <input type="number" class="form-control" id="end_duration" name="end_duration">
              </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Create</button>
        </div>
        </form>

      </div>
    </div>
  </div>  --}}



  {{--  <div class="modal fade" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Pre Screening</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.study.pre_screening.update' , $study->id )}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <input type="hidden" value="" id="screen_id" name="screen_id" />
              
              <label for="screen-name" class="col-form-label">Pre Screening Name:</label>
              <input type="text" class="form-control" id="screen-name1" name="screen_name">
            </div>
            <div class="form-group">
                <label for="screen-frequency" class="col-form-label">Frequency:</label>
                <input type="number" class="form-control" id="screen-frequency1" name="screen_frequency">
              </div>
              <div class="form-group">
                <label for="end_duration" class="col-form-label">End Duration:</label>
                <input type="number" class="form-control" id="end_duration1" name="end_duration">
              </div>
              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>

      </div>
    </div>
  </div>  --}}

@endsection

@section('scripts')
<script>
    function update_model_data(screen , study){
        console.log(screen , study);
        $('#update_model').modal('show');
        $("#screen-name1").val(screen.name);
        $("#screen_id").val(screen.id);
        $("#screen-frequency1").val(screen.frequency);
        $("#end_duration1").val(screen.end_duration);
         
        
    }
</script>
@endsection