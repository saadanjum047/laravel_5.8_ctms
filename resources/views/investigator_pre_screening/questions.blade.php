@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , [$study->id , 'English'] )}}">{{$study->study_name}}</a></li>
   @if(isset($patient))
   <li class="breadcrumb-item"><a href="{{ route('dashboard.patient.show' , $patient->id )}}">{{$patient->user->name}}</a></li>
    @endif
   {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.all' , $study->id )}}">{{$screen->name}}</a></li> --}}


   <li class="breadcrumb-item" aria-current="page">
Pre Screening Questions
</li>

   </ol>
</nav>


<div class="bg-white" style="padding:20px">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}

<div>

    @foreach ($questions as $question)
    <h4>  
        <a href="{{route('dashboard.study.pre_screening.viewQuestion' , [$study->id , $patient->id , $screen->id , $question->id ] )}}"  >
        {{$question->question}}  </a>
        </h4>

        @php
        if($question->ans){
            $answer = $question->ans->where('investigator_id' , auth()->user()->sponsor_user->id )->first();
        }
        @endphp

        
            @if(isset($answer))
            <p> {{ str_limit(trim(str_replace(array( '[' , ']' , '"' ) , '' , $answer->answer_body )) , 200) }} </p>
            @endif
            <br>
            <hr>

    @endforeach

</div>


</div>





@endsection

