@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , [$study->id , 'English'] )}}">{{$study->study_name}}</a></li>
   @if(isset($patient))
   <li class="breadcrumb-item"><a href="{{ route('dashboard.patient.show' , $patient->id )}}">{{$patient->user->name}}</a></li>
    @endif

   {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.all' , $study->id )}}">{{$screen->name}}</a></li> --}}
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.viewQuestions' ,[$study->id , $patient->id , $screen->id] )}}">{{$screen->name}} Questions </a></li>



   <li class="breadcrumb-item" aria-current="page">
Pre Screening Questions
</li>

   </ol>
</nav>


<div class="bg-white" style="padding:20px">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}

<div>
    @php
    if($question->ans){
        $answer = $question->ans->where('investigator_id' , auth()->user()->sponsor_user->id )->first();
        
    }

if(count($question->specifications) > 0 )
{
    foreach($question->specifications as $spec){
        $specs[] = $spec->name;

    }
}
@endphp
    <form action="@if(isset($answer)) {{route('dashboard.question.answer.update' , [$question->id , $answer->id] )}} @else {{route('dashboard.question.answer.store' , $question->id  )}} @endif" method="POST">
    @csrf

    <div class="container bg-white" style="padding-top:20px" >
        <div class="row" style="margin-top:20px">
            <div class="col-md-8" >
                <h4> {{$question->question}} </h4>
            <br>
            @if(!is_null($question->answer_type) && $question->answer_type == 1 )
            <div class="form-group">
                <label class="col-form-label"> Answer </label>
                <textarea name="answerr" class="form-control" rows="8" placeholder="Detail you Answer here" >@if(isset($answer)) {{$answer->answer_body}}  @endif</textarea>
               
            </div>
            
            @elseif(!is_null($question->answer_type) && $question->answer_type == 3)
            <div class="form-group">
                <label class="col-form-label"> Select below answers</label><br>
                @php
                $tagsArray =  explode( ',' , $question->answers);
                if(isset($answer))  {
                $answersArray = explode( ',' , $answer->answer_body);  
                foreach($answersArray as $arr)
                $array[] = trim(str_replace(array( '[' , ']' , '"' ) , '' , $arr ));
                }   
                @endphp
                {{--  {{dd($answersArray , $array)}}  --}}
                @foreach( $tagsArray as $i=> $ans) 
                {{-- @dd($ans , $array , $answer , $question) --}}
                {{-- {{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} ,  --}}
                <input type="checkbox" id="check{{$i}}" value="{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}}" name="choices[]"  
                @if(isset($answer))

                @if(in_array( trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) , $array )) {{"checked"}} @endif
                @endif
                @if(isset($specs) && in_array( trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) , $specs ) ) onclick="make_checkbox_description(this.id)" @endif  
                             
                />
                <label class="col-form-label" for="check{{$i}}">{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} </label>
                <br>
                <div id="additional_fields{{$i}}">
                @if(isset($answer->specifications))
                @if(count($answer->specifications) > 0)
                    @foreach($answer->specifications as $spec)
                    @if($spec->name == trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) )
                    <textarea class="form-control" name="specifications[]" id="specs{{$i}}" placeholder="Plz elaborate your answer"> {{$spec->specification}} </textarea>
                    @endif
                    @endforeach
                    @endif
                    @endif  
                </div>
                @endforeach
            </div>
            @elseif(!is_null($question->answer_type) && $question->answer_type == 2)
            <div class="form-group">
                <label class="col-form-label"> Select below answers</label><br>
                @php
                $tagsArray =  explode( ',' , $question->answers);
                @endphp
                @foreach( $tagsArray as $i=> $ans) 
                <input type="radio" id="radio{{$i}}" name="select" value="{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}}" 
                {{-- @if(isset($answer) && $answer->answers == trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) ) --}}
                  @if(isset($answer) && trim(str_replace(array( '[' , ']' , '"' ) , '' , $answer->answer_body )) == trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) )
                    {{"checked"}}
                @endif
                @if(isset($specs) && in_array( trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) , $specs ) ) onclick="make_radio_description(this.id)" @else 
                onclick="remove_radio_description(this.id)" @endif />

                <label class="col-form-label" for="radio{{$i}}">{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} </label>

                <div id="additional_fields{{$i}}">
                @if(isset($answer->specifications))
                @if(count($answer->specifications) > 0)
                    @foreach($answer->specifications as $spec)
                    @if($spec->name == trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) )
                    <textarea class="form-control" name="specifications" id="specs{{$i}}" placeholder="Plz elaborate your answer"> {{$spec->specification}} </textarea>
                    @endif
                    @endforeach
                    @endif
                    @endif
                </div>
                <br>
                @endforeach
            </div>
            @endif
        </div>
        
    </div>
    @if(isset($answer))
    <button type="submit" class="btn btn-primary" style="margin-bottom:20px">Update</button>
    @else
    <button type="submit" class="btn btn-success" style="margin-bottom:20px">Save</button>
    @endif
    </div>

    
    </form>

    <br>
    <hr>


</div>
</div>

<script>
    @php
    if(!isset($i)) 
    $i = 0 ; 
    @endphp
    
    var n = {{$i}};
    var i = 1;
    function make_radio_description( id){
        var radio_id = id.replace( /^\D+/g, ''); 


        if($('#radio'+radio_id).prop("checked") == true){
        if (!$('#specs'+radio_id).length){
            console.log('checked but not exists');

            $('#additional_fields'+radio_id).append('<textarea class="form-control" name="specifications" id="specs'+radio_id+'" placeholder="Plz elaborate your answer"></textarea>');
            i = i + 1;
        }
        }
    }
    
    
    function remove_radio_description(id){
        var radio_id = id.replace( /^\D+/g, ''); 

        console.log(id , n )
        for(i = 0 ; i <= n ; i++){
        if ($('#specs'+i).length ){
            console.log('unchecked but exists');
            $('#specs'+i).remove();
    }
}
}
    


    function make_checkbox_description(id){
        
        var check_id = id.replace( /^\D+/g, ''); 
        console.log( 'make_checkbox_description' , id , check_id , $('#specs'+check_id).length);
        if($('#check'+check_id).prop("checked") === false){
            if ($('#specs'+check_id).length){
                console.log('unchecked but exists');
            $('#specs'+check_id).remove();
        }
        }

        if($('#check'+check_id).prop("checked") === true){
        if (!$('#specs'+check_id).length){
            console.log('checked but not exists');

            $('#additional_fields'+check_id).append('<textarea class="form-control" name="specifications[]" id="specs'+check_id+'" placeholder="Plz elaborate your answer"></textarea>');
            i = i + 1;
        }
        }
      
    }


    function remove_checkbox_description(id){
        var check_id = id.replace( /^\D+/g, ''); 
        console.log( 'remove_checkbox_description' , id ,check_id , ) ;
        if($('#check'+check_id).prop("checked") == true){
            console.log('checked');
        }
        if($('#check'+check_id).prop("checked") == false){
            console.log('unchecked');
        }
    }

</script>


</div>


@endsection


