@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view', [$study->id , 'English'] )}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening', $study->id )}}">{{$screen->name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.questions', [$study->id , $screen->id] )}}">Questions </a></li>
   <li class="breadcrumb-item" aria-current="page">
Investigators</li>

<a class="btn btn btn-outline-warning float-right" href="{{URL::previous()}}"> <i class="fa fa-arrow-left"></i> Back</a>

   </ol>
</nav>


<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Investigators
        </div>
        
<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Name</th>
    <th >Email</th>
    <th >Study</th>
   
</tr>
@php
$i = 1;
@endphp
@foreach ($invests as $k => $invest)
<tr>
    <td> {{$k+1}} </td>
    <td> 
      <a href="{{route('dashboard.study.pre_screening.question.user.answer' ,  [$study->id , $screen->id , $question->id , $invest->id] )}}" >
      {{$invest->user->name ?? ''}} </a> </td>
    <td> {{$invest->user->email ?? ''}} </td>
    <td> {{$invest->study->study_name ?? ''}} </td>
</tr>
@php
$i += 1;
@endphp
@endforeach
</table>

</div>
</div>





@endsection

