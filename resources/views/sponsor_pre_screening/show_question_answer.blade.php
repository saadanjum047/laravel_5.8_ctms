@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , [$study->id , 'English'] )}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening' , $study->id )}}">{{$screen->name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.questions' ,[ $study->id , $screen->id ])}}"> Questions </a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.question.users' ,[ $study->id , $screen->id , $question->id ])}}"> Users </a></li>

   <li class="breadcrumb-item" aria-current="page">Question</li>

   
   <a class="btn btn btn-outline-warning float-right" href="{{URL::previous()}}"> <i class="fa fa-arrow-left"></i> Back</a>

   </ol>
</nav>


<div class="bg-white" style="padding:20px">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}
  
<h2> {{$question->question}} </h2>
<hr>

{{--  dd($user);  --}}
@php

if($question->ans){
      $answer = $question->ans->where('investigator_id' , $user->id)->first();

    } 
@endphp

@if(isset($answer))
<div>    
{{$answer->answer_body}}
<br>

<span class="small badge badge-success"> Answered By: {{$answer->investigator->user->name}} </span>

<hr>
</div>

@endif

</div>


@endsection

