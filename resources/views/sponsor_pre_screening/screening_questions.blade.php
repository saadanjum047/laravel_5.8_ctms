@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , [$study->id , 'English'] )}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening' , $study->id )}}">{{$screen->name}}</a></li>

   @if(isset($freq))
   {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.frequency' , [$study->id , $screen->id ])}}">Frequency {{$freq}} </a></li> --}}

   @endif

   <li class="breadcrumb-item" aria-current="page">
Pre Screening Questions
</li>
<a class="btn btn btn-outline-warning float-right" href="{{URL::previous()}}"> <i class="fa fa-arrow-left"></i> Back</a>

   </ol>
</nav>


<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Pre Screening Questions
          <a href="{{ route('dashboard.study.pre_screening.question.create' , [$study->id , $screen->id ])}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Question</a></h3>
        </div>
        
<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Question</th>

    <th >Pre Screening</th>
    {{--  <th>Notification Response</th>  --}}
    <th>Status</th>
    <th>Created</th>
    <th>Action</th>
</tr>
@php
$i = 1;
@endphp
@foreach ($questions as $k => $question)
<tr>
  {{--  {{ dd($question->diary) }}  --}}
    <td>{{$k+1}} </td>
    <td> 
      <a href="{{route('dashboard.study.pre_screening.question.users' , [$study->id , $screen->id , $question->id ] )}}" >
      {{$question->question}}</td> </a>
       {{--  <td> @if(isset($question->answers))
      @php
      $tagsArray =  explode( ',' , $question->answers);
      @endphp
      @foreach( $tagsArray as $ans) 
      {{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} , 
      @endforeach
     @endif </td>  --}}
    <td> @if(isset($question->pre_screen)) {{$question->pre_screen->name}} @endif </td>
    {{--  <td> {{$question->notification_response}} </td>  --}}
    <td> @if($question->status == 0) <span class="badge badge-warning">Inactive</span> @elseif($question->status == 1) <span class="badge badge-success">Active</span> @endif </td>
    <td> {{($question->created_at)}} </td>
  
    <td> 

      @if($question->ans->count() == 0 )
      <a href="{{route('dashboard.question.status' , $question->id )}}" title="Inactive the question" > <i class="@if($question->status == 1) text-danger @elseif($question->status == 0) text-success @endif  fa fa-power-off fa-lg"></i></a> |

      @endif
      @if(auth()->user()->role_id == 3 &&  auth()->user()->sponsor_user->id == $question->user_id )
      <a href="{{route('dashboard.study.pre_screening.question.delete' , [$study->id , $screen->id , $question->id ])}}"> <i class="text-danger fa fa-trash fa-lg"></i></a>
      <a href="{{route('dashboard.study.pre_screening.question.edit', [$study->id , $screen->id , $question->id] )}} " class="text-primary" > <i class="fa fa-edit fa-lg" ></i> </a>    
        
      @endif

      @if(auth()->user()->role_id == 2 )
      <a href="{{route('dashboard.study.pre_screening.question.delete' , [$study->id , $screen->id , $question->id ])}}"> <i class="text-danger fa fa-trash fa-lg"></i></a>
      <a href="{{route('dashboard.study.pre_screening.question.edit', [$study->id , $screen->id , $question->id] )}} " class="text-primary" > <i class="fa fa-edit fa-lg" ></i> </a>    

      @endif
    </td>
</tr>

@endforeach
</table>

</div>
</div>





@endsection

