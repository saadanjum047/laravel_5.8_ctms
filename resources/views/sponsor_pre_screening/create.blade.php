@extends('layouts.site')


@section('content')

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening' , $study->id )}}">Pre Screenings</a></li>

   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening.questions' ,[ $study->id , $screen->id] )}}"> Questions </a></li>
   @if(isset($question))

   <li class="breadcrumb-item"><a href="{{route('dashboard.study.pre_screening.questions' ,[ $question->pre_screen->study->id , $question->pre_screen->id] )}}">Pre Screen</a></li>
   @endif
   <li class="breadcrumb-item" aria-current="page">
    @if(isset($question))
    Update Question
    @else
    Pre Screening Questions
    @endif
</li>
<a class="btn btn btn-outline-warning float-right" href="{{URL::previous()}}"> <i class="fa fa-arrow-left"></i> Back</a>

   </ol>
</nav>



<div class="bg-white" style="padding:20px">
      
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif

@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif

    <h2> @if(isset($question )) Update Question  @else
         Create Question @endif </h2>
    <form action="@if(isset($question )) {{route('dashboard.study.pre_screening.question.update'  , [$study->id , $screen->id , $question->id])}}  @else
    {{route('dashboard.study.pre_screening.question.store' , [$study->id , $screen->id] )}} @endif" method="post">
        {{ csrf_field() }}

      <div class="modal-body">
          <div class="form-group">
            <label for="question" class="col-form-label">Question:</label>
            <input type="text" name="question" class="form-control" id="question" @if(isset($question )) value="{{$question->question}}"  @endif placeholder="Your Question here" required />
          </div>
          {{--  <div class="form-group">
            <label for="question" class="col-form-label">Notify me Answer (optional):</label>
            <input type="text" name="notify" class="form-control" id="question" @if(isset($question )) value="{{$question->notification_response}}"  @endif placeholder="Notify me Answer (optional)" >
          </div>  --}}
          <div class="form-group">
            <label for="answer_type" class="col-form-label">Answer Type:</label>
            <select name="answer_type"  class="form-control" id="answer_type">
            <option @if(isset($question->answer_type) && $question->answer_type == 1)) {{"selected"}} @endif value="1">Text</option>  
            <option @if(isset($question->answer_type) && $question->answer_type == 2)) {{"selected"}} @endif value="2">Radio Buttons</option>  
            <option @if(isset($question->answer_type) && $question->answer_type == 3)) {{"selected"}} @endif value="3">Check Boxes</option>  
            </select> 
          </div>
         

          <div class="row">
          <button class="btn btn-primary offset-11"  @if(!isset($question ))style="display:none" @elseif(isset($question) && $question->answer_type == 1)) style="display:none" @else {{"selected"}} @endif type="button" id="add_div" ><i class="fa fa-plus"></i> </button></div>
          
            {{--  {{dd($question->specifications)}}  --}}
        <div id="answers">
          @if(isset($question ))  
          @if(isset($question->answers))
        @php
        $i = 0;
        $tagsArray =  json_decode($question->answers);
        @endphp
        @if($question->specifications->count() > 0 )
        @php
            
        $specs = $question->specifications->pluck('name')->toArray(); 
        @endphp
        @endif

        @foreach( $tagsArray as $k=> $ans) 
        @php
        $i++;
        @endphp
                {{-- <input type="hidden" name="spec_id" value="" > --}}

            <div class="row" id="ans{{$k}}">
            <div class="form-group col-md-6" style="display:inline-block">
            <lable class="col-form-label">Choice{{$k+1}}</lable> 
              <input type="text"  class="form-control" name="answers[]" value="{{$tagsArray[$k]}}" onchange="update_radio(this.id)" id="choice{{$k}}" /> </div>  

            <div class="col-md-2" style="margin-top:20px" >  
              <input type="checkbox" id="super{{$k}}" name="specifications[]" onchange="check_value(this.id)" @if(isset($specs)) @if(in_array( $tagsArray[$k] , $specs ) ) checked value="{{$tagsArray[$k]}}" @endif @endif   /> 
              <label class="col-form-label" for="super{{$k}}">Specification</label></div> 
            
            <div class="col-md-2" style="margin-top:20px" >  
              <input type="radio" name="notify" id="{{$k}}" onchange="radio_value(this.id)" @if(isset($question->notification_response) && ($question->notification_response  == $tagsArray[$k] ) ) checked value={{$tagsArray[$k]}} @endif  /> 
              <label class="col-form-label" for="{{$k}}">Notification</label></div> 
            
            <div class="col-md-1 float-right"> 
              <button type="button" style="margin-left:10px; margin-top:20px" class="btn btn-danger float-right" id="ans{{$k}}" onclick="div_delete(this.id)" ><i class="fa fa-minus" ></i> </button></div> </div>
            
            @endforeach

          @endif 
          @endif
        </div>


          {{-- <div class="form-group">
            <label for="diary-select" class="col-form-label">Diary:</label>
            <select class="form-control" id="diary-select" name="diary">
              <option value="" index="-1">Select Any Diary</option>
              @foreach($diaries as $diary)
              <option @if(isset($question )  && $question->diary_id == $diary->id ) {{'selected'}}   @endif
              value="{{$diary->id}} "> {{$diary->diary_name}} </option>
              @endforeach
            </select>
          </div> --}}
          
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-success">@if(isset($question )) Update Question @else 
            Add Question @endif</button>
      </div>
    </form>

</div>

@endsection


@section('scripts')
    

<script>

  $('#answer_type').change( function (){
    var select = $('#answer_type').val();
    if(select == 2 || select == 3){
     $('#add_div').show();
    }
    if(select == 1){
      $('#add_div').hide();
      $('#answers div').remove();
    }
    console.log(select);

  } );
 
  var i = @if(isset($question) && isset($question->answers) )  {{$i+1}} @else 1  @endif;
  $("#add_div").click( function (){
    console.log('cehck');
    $('#answers').append( '  <div class="row"  id="ans'+i+'"> <div class="form-group col-md-6" style="display:inline-block;"  > <lable class="col-form-label">Choice '+i+' </lable> <input type="text"  class="form-control" name="answers[]" id="choice'+i+'" onchange="update_radio(this.id)"  /> </div> <div class="col-md-2" style="margin-top:20px" >  <input type="checkbox" id="super'+i+'" name="specifications[]" onchange="check_value(this.id)" /> <label class="col-form-label" for="super'+i+'">Specification</label></div> <div class="col-md-2" style="margin-top:20px" >  <input type="radio" name="notify" id="'+i+'" onchange="radio_value(this.id)"  /> <label class="col-form-label" for="'+i+'">Notification</label></div> <div class="col-md-1 float-right">  <button type="button" style="margin-left:10px; margin-top:20px" class="btn btn-danger " id="ans'+i+'"  onclick="div_delete(this.id)" ><i class="fa fa-minus" ></i> </button> </div></div>');
    i = i + 1;
  } );

  function div_delete(id){
    $('#'+id).remove();
  }
  function radio_value(id){
    console.log(id);
    var value ;
     value = $('#choice'+id).val();
     console.log(value);
     $('#'+id).val(value);
  }

  function check_value(id){
    console.log('check');
    var value;
    id = id.replace(/\D/g, "");
    value = $('#choice'+id).val();
    $('#super'+id).val(value);
    
  }

  function update_radio(id){
    console.log('radio');

    id = id.replace(/\D/g, "");
    $('#'+id).val($('#choice'+id).val());
    if($('#super'+id).prop('checked') == true){
      $('#super'+id).val($('#choice'+id).val())
      console.log('cehcked');
    }
    console.log(id);
  }
  


  </script>



@endsection