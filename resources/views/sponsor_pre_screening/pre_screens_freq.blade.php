@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , [$study->id , 'English'] )}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.pre_screening' , $study->id )}}">{{$screen->name}}</a></li>
   <li class="breadcrumb-item" aria-current="page">
Pre Screening Questions
</li>
<a class="btn btn btn-outline-warning float-right" href="{{URL::previous()}}"> <i class="fa fa-arrow-left"></i> Back</a>

   </ol>
</nav>


<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Pre Screening Questions</h3>
        </div>
        
<table class="table table-hover">
<tr>
    <th>#</th>
    <th> Pre Screen</th>
    <th> Frequency</th>
  
</tr>
@for($i = 1 ; $i <= $screen->frequency ; $i++)
<tr>
    <th> {{$i}} </th>
    <th> 
        <a href="{{route('dashboard.study.pre_screening.frequency.questions' , [$study->id , $screen->id , $i ])}}"> {{$screen->name}} </a> </th>
    <th> {{$i}} </th>
</tr>
@endfor

</table>

</div>
</div>





@endsection

