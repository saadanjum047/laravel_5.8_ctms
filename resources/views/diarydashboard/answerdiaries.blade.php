@extends('layouts.site')

@section('content')
@if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')!==1)


	  <div class="col col-lg-12">
	<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">
		<a href="{{route('dashboard.study.diarydashboard',$study_id)}}">
		{{$DiarySchedule->studyname->study_name}}</a></li>
		</ol>
	</nav>
	

 
   
   
				

	
	<div class="row mt-5">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title font-weight-normal">Answered Diaries</h4>
                    
                    <div class="table-responsive" id="patients-notes">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            
                            <th>schedule_id</th>
                            <th>question_id</th>
			     <th>diary_id</th>
				<th>study_id</th>
				<th>Answered</th>
                        </tr>
                        </thead>
                        <tbody>
						
						@foreach($DiarySchedule->DiaryAnswer as $dt)
						
						<tr style="background-color:#DADADA">
                            
                            <td>{{$dt->schedule_id}}</td>
							
							  <td>{{$dt->question_id}}</td>
							   <td>{{$dt->diary_id}}</td>
							    <td>{{$dt->study_id}}</td>
								@foreach($dt->answers as $ans)
								 <td>{{$ans->selected}}</td>
								 @endforeach

                        </tr>
				
			@endforeach


						

                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
      
                </div>
				
            </div>
        
   
	
	
@endif
@endsection