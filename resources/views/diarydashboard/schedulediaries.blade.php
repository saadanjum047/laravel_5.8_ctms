@extends('layouts.site')

@section('content')
{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')!==1)  --}}
@if( auth()->user()->role_id != 1)

	  <div class="col col-lg-12">
	<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">
		<a href="{{route('dashboard.study.view',[$study->id,'English'])}}">
		{{$study->study_name}}</a></li>
        <li class="breadcrumb-item " aria-current="page">
        <a href="{{route('dashboard.study.diarydashboard',$study->id)}}">Diary Dashboard
        </a>
        </li>
        <li class="breadcrumb-item " aria-current="page">
        Scheduled Diaries
        </li>
		</ol>
	</nav>
	

 
   
   
				

	
	<div class="row mt-5">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title font-weight-normal">Scheduled Diaries</h4>
                    
                    <div class="table-responsive" id="patients-notes">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            
                           <th>#</th>
                        
			     <th>type</th>				
				<th>Created_at</th>
                        </tr>
                        </thead>
                        <tbody>
						
						@foreach($patient->diary_schedule as $diary)
						
						<tr style="background-color:#DADADA">
                            
                               <td>{{$diary->id}}</td>
							  <td>@if($diary->type==2)
                                <label class="badge badge-success">completd</label>
                                @elseif($diary->type==1)
                                <label class="badge badge-danger">Overdue</label>
                                @else
                                <label class="badge badge-warning">current</label>
                                @endif
                                </td>
							    <td>{{$diary->created_at->format('F , D , m/Y') }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
      
                </div>
				
            </div>
        
   
	
	
@endif
@endsection