@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')!==1) --}}
@if(auth() && auth()->user()->role_id != 1 )


<div class="col col-lg-12">
	<nav aria-label="breadcrumb">
       	<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
	        <li class="breadcrumb-item active" aria-current="page">
			<a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a>
			</li>
			 <li class="breadcrumb-item" aria-current="page">Diary Dashboard
			</li>
		</ol>
	</nav>
	 


   <!-- <h3 class="text-primary mb-4">Diary Dashboard</h3>-->
   
   	<div class="row mt-5">
        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 mb-4">
            <div class="card">
                <div class="card-block">
				 <i class="fa fa-user-o fa-2x pull-right"></i><br>
                    <h4 class="card-title font-weight-normal text-success">{{$scheduled_diaries->count()}}</h4>

                    <p class="card-text ">Total Scheduled Diaries</p>
                    <div class="progress">
                        <div class="progress-bar  progress-slim bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 mb-4">
            <div class="card">
                <div class="card-block">
				<i class="fa fa-user-o fa-2x pull-right"></i><br>
                    <h4 class="card-title font-weight-normal text-success"> {{$scheduled_diaries->where('type' , 2)->count() }} </h4>
                    <p class="card-text ">Total Answer Diaries</p>
                    <div class="progress">
                        <div class="progress-bar  progress-slim bg-success" role="progressbar" style="width: {{$scheduled_diaries->where('type' , 2)->count()/$scheduled_diaries->count() * 100}}%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
	
	<div class="row mt-5">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title font-weight-normal">Scheduled Diaries</h4>
                    
                    <div class="table-responsive" id="patients-notes">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            
                            <th>Patient#</th>
                            <th>#Scheduled Diaries</th>
			     <th>Missed Diaries</th>
				<!--<th>Number Of Diaries</th>-->
                        </tr>
                        </thead>
                        <tbody>
						
						@foreach($patients as $patient)
						@if($patient->diary_schedule->count() > 0 )
						<tr style="background-color:#DADADA">
                            <td>{{$patient->patient_nbr}}</td>
                            <td><a href="{{route('dashboard.study.patient.scheduledDiary', [ $study->id , $patient->id ])}}">{{$patient->diary_schedule->count() }}</a></td>
 	                    	<td><a href="{{route('dashboard.study.patient.missingDiareis', [ $study->id, $patient->id ])}}"> Missing diaries</a></td> 
                        </tr>
                        @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
       {{--  <!--- <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 mb-4">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title font-weight-normal">
                        Answer Diaries
                    </h4>
                    
                    <div class="table-responsive" id="patients-notes">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            
                            <th>Patient#</th>
                            <th>Answer Diaries</th>
                            
                        </tr>
                        </thead>
                        <tbody>
						@foreach($DiarySchedule->data_answer as $dt)
						
						<tr style="background-color:#DADADA">
                            
                            <td>{{$dt->patient_nbr}}</td>
                            <td>{{$dt->answer_diary}}</td>

                            
                        </tr>
						@endforeach
                        </tbody>
                    </table>
                </div> 
</div>
				
                    </div>--->  --}}
                </div>
            </div>
        </div>
   
	
	
@endif
@endsection