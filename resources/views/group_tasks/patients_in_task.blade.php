
@extends('layouts.site')


@section('content')



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
     
     <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('dashboard.study.groups', $study->id) }}" >{{$group->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('dashboard.study.group', [$study->id , $group->id ]) }}" >{{$group->name}} Tasks </a></li>
      
      <li class="breadcrumb-item" aria-current="page">Patients</li>
     </ol>
 </nav>

    

 <div class="bg-white" style="padding:10px">
    <div class="">
    
        
        <div class="card-title">
            <h2>"{{$task->name}}" Details </h2>
        </div>

        <table class="table table-hover">
            <tr>
                <th> # </th>
                <th> Patient Name </th>
                <th> All Task Status </th>
                <th> Total Tasks </th>
            </tr>

            @php
                $i = 1;
            @endphp
            @foreach ($task->patients as $k => $patient)
            @if($patient->pivot->repeated == 1)
                <tr>
                    <td> {{$i}} </td>
                    <td> 
                        <a href="{{route('dashboard.study.group.task.pateint' , [$study->id , $group->id , $task->id , $patient->id ])}}">
                        {{$patient->user->name}}
                        </a>
                     </td>
                    <td>        
                        @php
                            
                        $pivots = DB::table('group_task_patient')->where('task_id' , $task->id)->where('patient_id', $patient->id )->get();
                        $stat = 1;
                        foreach($pivots as $pivot){
                            if($pivot->status == 0){
                                $stat = 0;
                            }
                        }
                        @endphp
                        @if($stat == 1)
                        <span class="badge badge-success">Complete</span>
                        @else
                        <span class="badge badge-warning">Incomplete</span>
                        @endif
                    </td>
                    <td> {{$task->repeated}} </td>
                </tr>
                @endif
                @php
                    $i++;
                @endphp
            @endforeach
            <tr>

            </tr>
        </table>
        
    
    </div>
    </div>

 @endsection