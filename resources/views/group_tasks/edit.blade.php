

@extends('layouts.site')



@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

    
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
    
     <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.groups', $study->id) }}" >Patient Tasks</a></li>

     <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('dashboard.study.groups', $study->id) }}" >Groups</a></li>
         
        
     <li class="breadcrumb-item" aria-current="page">Update Tasks Group</li>
     </ol>
 </nav>


<div class="bg-white">

    <div class="card">

        @if($errors->any())
        @foreach ($errors->all() as $error)
            <p class="alert alert-danger"> {{$error}} </p>
        @endforeach
        @endif
        
                    <div class="card-header" style="margin-bottom:20px">
                        <h3 class="card-title"> Add A New Task Group </h3>
                    </div>
        
                    <form action="{{route('dashboard.study.group.update' , [$study->id , $group->id] )}} " method="POST">
        
                        {{ csrf_field() }}
        
                <div class="form-group row text-center">
                    <label class="col-md-2 col-form-label"> Group Name: </label>  
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Enter the name of the Group" name="name" style="border-color:#aaa" required value="{{$group->name}}">  
                    </div>
                </div>
                <div class="form-group row text-center">
                    <label class="col-md-2 col-form-label"> Group Description: </label>  
                    <div class="col-md-8">
                        <textarea  id="summernote" style="border-color:#aaa" class="form-control"  name="description"> {{$group->description}} </textarea>  
                    </div>
                </div>
                <div class="form-group row text-center">
                    <label class="col-md-2 col-form-label"> Study Schedules: </label>  
                    <div class="col-md-8">
                        <select class="js-example-basic-multiple" multiple style="width:100%"  name="schedules[]" > 
                            @php
                                $schedule_ids = $group->schedules->pluck('id')->toArray();
                            @endphp
                            @foreach ($study->schedules as $schedule)
                            <option value="{{$schedule->id}}" @if( in_array( $schedule->id , $schedule_ids)) selected @endif >  {{$schedule->visit_name}} </option>
                            @endforeach
                        </select>  
                    </div>
                </div>
                
                    <div class="col-md-10">
                    <button type="submit" style="margin-bottom:20px" class="btn btn-success float-right">Update Task Group</button>
                    </div>
            </form>
        
            </div>


</div>

@endsection




@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script> 
        $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            placeholder : 'Select Schedules',
        }
        );
        });
    </script>
@endsection