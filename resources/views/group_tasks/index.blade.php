

@extends('layouts.site')


@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if ($message = Session::get('success'))
   <script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
      <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.groups', $study->id) }}" >Patient Tasks</a></li>
     
   
      <li class="breadcrumb-item" aria-current="page">Groups</li>
     </ol>
 </nav>



<div class="bg-white" style="padding:10px">


    <div class="">
        <div class="card-title">
            <h2>Groups 
                @if(auth()->user()->role_id == 3)
                @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'create')->where('module_id' , 13 )->toArray() ) )
                <a class="btn btn-success float-right" style="color:white" href="{{route('dashboard.study.group.create' , $study->id )}}" > <i class="fa fa-plus"></i> Add New Task Group</a>
                @endif

                @elseif(auth()->user()->role_id == 2)
                <a class="btn btn-success float-right" style="color:white" href="{{route('dashboard.study.group.create' , $study->id )}}" > <i class="fa fa-plus"></i> Add New Task Group</a>
                @endif
            </h2>
        </div>

        <table class="table table-hover">
            <tr> 
                <th> # </th>
                <th> Group Name </th>
                <th> Group Description </th>
                <th> Schedule </th>
                <th> Created </th>
                <th> Actions </th>
            </tr>
    
            @foreach ($groups as $k=> $group)
                {{--  @dd($group->schedules)  --}}
            <tr>
                <td> {{$k+1}} </td>
                @if(auth()->user()->role_id == 3)
                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'view')->where('module_id' , 11 )->toArray() ) )

                <td> <a href="{{route('dashboard.study.group' , [$study->id , $group->id ])}} "> {{$group->name}} </a> </td>
                @else
                <td> {{$group->name}}  </td>
                @endif
                @elseif(auth()->user()->role_id == 2)
                <td> <a href="{{route('dashboard.study.group' , [$study->id , $group->id ])}} "> {{$group->name}} </a> </td>
                @endif
                <td> {{ str_limit(strip_tags($group->description) , 80)}} </td>
                <td> @foreach($group->schedules as $schedule) {{$schedule->visit_name}} , @endforeach </td>
                <td> {{($group->created_at)->toDateString()}} </td>
                <td> 
           
                    
                    @if(auth()->user()->role_id == 3)
                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'update')->where('module_id' , 13 )->toArray() ) )
                    <a href="{{route('dashboard.study.group.edit' , [$study->id ,  $group->id ])}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> |
                    @endif
                    @elseif(auth()->user()->role_id == 2)
                    <a href="{{route('dashboard.study.group.edit' , [$study->id ,  $group->id ])}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> |
                    @endif

                    @if(auth()->user()->role_id == 3)
                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'delete')->where('module_id' , 13 )->toArray() ) )
                    <a href="{{route('dashboard.study.group.delete' , [$study->id , $group->id ])}} " class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a>

                    @endif

                    @elseif(auth()->user()->role_id == 2)
                    <a href="{{route('dashboard.study.group.delete' , [$study->id , $group->id ])}} " class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </table>
    
    </div>
    
</div>


  <script>

    function modal_open(group , schedule){
        
    document.getElementById("schedule").selectedIndex = 0;
    $("#group").val();

    $('#exampleModal').modal('show');

    $("#group").val(group.id);
    var options = document.getElementById('schedule').options; 
    console.log(options);
    for (var i = 0; i <= options.length; i++) {
        var option = options[i]; 
        if( option.value == schedule.id ){
          var selected = option.index;
          document.getElementById("schedule").selectedIndex = selected;
        }
    }
    }
  </script>

@endsection