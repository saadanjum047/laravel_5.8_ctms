


@extends('layouts.site')


@section('content')



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>

     <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.groups', $study->id) }}" >Patient Tasks</a></li>

     <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('dashboard.study.groups', $study->id) }}" >{{$group->name}}</a></li>
      
      <li class="breadcrumb-item" aria-current="page">{{$group->name}} Tasks</li>
     </ol>
 </nav>

    
<div class="bg-white" style="padding:10px">
<div class="">

    
    <div class="card-title">
        <h2>Tasks

            @if(auth()->user()->role_id == 3)
            @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'create')->where('module_id' , 11 )->toArray() ) )
            <a class="btn btn-success float-right" style="color:white" href="{{route('dashboard.study.group.task.create' , [$study->id , $group->id] )}}" > <i class="fa fa-plus"></i> Add New Task</a>

            @endif

            @elseif(auth()->user()->role_id == 2)

            <a class="btn btn-success float-right" style="color:white" href="{{route('dashboard.study.group.task.create' , [$study->id , $group->id] )}}" > <i class="fa fa-plus"></i> Add New Task</a>
            @endif
    
    </h2>
    </div>
    <table class="table">
        <tr> 
            <th> # </th>
            <th> Task Name </th>
            <th> Task Description </th>
            <th> Created </th>
            <th> Completion Status</th>
            <th> Actions </th>
        </tr>

        @foreach ($tasks as $k=> $task)
        <tr>   
            <td> {{$k+1}} </td>
            <td> {{$task->name}} </td>
            <td> {{ str_limit(strip_tags($task->description) , 80)}} </td>
            <td> {{$task->created_at->toDateString() }} </td>
              @php
              $count = DB::table('group_task_patient')->where('task_id' , $task->id)->where('status' , 1 )->get();
              @endphp
            <td> {{$count->count()}} ({{$task->patients->count()}}) </td>
            <td> 

              {{--  <a href="{{route('dashboard.study.group.task.pateints' , [$study->id , $group->id , $task->id ] )}} " class="text-warning"> <i class="fa fa-info-circle fa-lg"></i> </a>  --}}



                {{--  <a href="javascript:;" class="text-danger" onclick="modal_open({{$task}} , {{$task->schedule ?? NULL }})" > <i class="fa fa-paperclip fa-lg"></i> </a>  --}}
                
                
                
                


                <a href="{{route('dashboard.study.group.task.details' , [$study->id , $group->id , $task->id ])}} " class="text-success"> <i class="fa fa-info-circle fa-lg"></i> </a> |

                    @if(auth()->user()->role_id == 3)
                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'update')->where('module_id' , 11 )->toArray() ) )

                    <a href="{{route('dashboard.study.group.task.edit' , [$study->id , $group->id , $task->id ])}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> |

                    @endif
                    @elseif(auth()->user()->role_id == 2)

                <a href="{{route('dashboard.study.group.task.edit' , [$study->id , $group->id , $task->id ])}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> |
                @endif


                

                @if(auth()->user()->role_id == 3)
                    @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'delete')->where('module_id' , 11 )->toArray() ) )
                    <a href="{{route('dashboard.study.group.task.delete' , [$study->id  , $group->id , $task->id ])}} " class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a> 
                    @endif

                    @elseif(auth()->user()->role_id == 2)
                    <a href="{{route('dashboard.study.group.task.delete' , [$study->id  , $group->id , $task->id ])}} " class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a> 
                    @endif

            </td>
        </tr>
        @endforeach
    </table>

</div>
</div>

  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="{{route('dashboard.task.attach')}} " method="POST">
            @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="task_id" id="task" value="" >
          <div class="form-group">
            <lable class="col-form-label">Schedules</lable>
            <select class="form-control" name="schedule_id" id="schedule" required>
                <option value="" index="-1" >Select Any Schedule</option>
                @foreach ($schedules as $schedule)
                <option value="{{$schedule->id}}">{{$schedule->visit_name}} </option>    
                @endforeach
                <option value="0" class="text-danger">Remove the schedule</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>

      </div>
    </div>
  </div>

  <script>

    function modal_open(task , schedule){
        
    document.getElementById("schedule").selectedIndex = 0;
    $("#task").val();

    $('#exampleModal').modal('show');

    $("#task").val(task.id);
    var options = document.getElementById('schedule').options; 
    console.log(options);
    for (var i = 0; i <= options.length; i++) {
        var option = options[i]; 
        if( option.value == schedule.id ){
          var selected = option.index;
          document.getElementById("schedule").selectedIndex = selected;
        }
    }
    }
  </script>
@endsection