
@extends('layouts.site')


@section('content')



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
     
     <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.groups', $study->id) }}" >Patient Tasks</a></li>


     <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('dashboard.study.groups', $study->id) }}" >{{$group->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('dashboard.study.group', [$study->id , $group->id ]) }}" >{{$group->name}} Tasks </a></li>
      
      <li class="breadcrumb-item" aria-current="page">Tasks Details</li>
     </ol>
 </nav>

    

 <div class="bg-white" style="padding:10px">
    <div class="">
    
        
        <div class="card-title">
            <h2>"{{$task->name}}" Details </h2>
        </div>

        <table class="table table-hover">
            <tr>
                <th> # </th>
                <th> Patient Name </th>
                <th> Task Status </th>
                <th> Completion Time </th>
            </tr>

            @foreach ($task->patients as $k => $patient)
                <tr>
                    <td> {{$k+1}} </td>
                    <td> {{$patient->user->name}} </td>
                    <td>
                    @if($patient->pivot->status == 1)
                    <span class="badge badge-success">Completed</span>
                    @else
                    <span class="badge badge-warning">Incomplete</span>
                    @endif    
                    </td>
                    <td> {{$patient->pivot->completed_at ?? '-'}} </td>
                </tr>
            @endforeach
            <tr>

            </tr>
        </table>
        
    
    </div>
    </div>

 @endsection