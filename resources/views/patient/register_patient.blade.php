<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>StudyPal</title>
    <link rel="stylesheet" href="{{ asset('node_modules/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
	<!---->
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
	<!--<link rel="stylesheet" href="{{ asset('css/demo.css') }}">-->
	<!--end-->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>


<div class="container-scroller">
    <div class="container-fluid">
        <div class="row">
            <div class="content-wrapper full-page-wrapper d-flex align-items-center">
                <div class="card col-lg-4 offset-lg-4">
                    <div class="card-block">
                        <a href="{{ route('dashboard.index') }}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Back to homepage</a>
                        <h3 class="card-title text-primary text-left mb-5 mt-4">Register</h3>
                        <form action="{{ route('dashboard.patient.register') }}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="name" class="form-control p_input" placeholder="Name" required>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input type="password" name="password" class="form-control p_input" placeholder="Passwrod" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                    <select name="study_id" id="select-study" class="form-control" onchange="show_languages()" required>
                                        <option value="">Select study</option>
                                        @foreach($studies as $study)
                                            <option value="{{ $study->id }}">{{ $study->study_name ?? $study->study_title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="language-group" style="display: none">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-language"></i></span>
                                    <select name="language" class="form-control" required>
                                        @foreach ($languages as $lang)
                                        <option value="{{$lang->language}} ">{{$lang->language}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="siteNbr-group" style="display: none">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <span style="position: absolute; display: block; z-index: 1000;top: 8px;left: 50px;font-size: 15px" class="site_nbr"></span>
                                    <input type="text" name="study_patient_id" value="" id="study_patient_id" style="padding-left:42px" class="form-control p_input" placeholder="Study Patient ID" onkeyup="check_nbr(this.value)" required >
                                </div>
                                <p id="status_danger" style="display:none" class="text-danger">This NBR is not available &#10006  </p>
                                <p id="status_success" style="display:none" class="text-success">This NBR is available &#10004		</p>
                            </div>

                            {{--  <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input type="text"  class="form-control p_input" placeholder="Phone Number" required>
                                </div>
                            </div>  --}}
							
							
                            <div class="form-group">
                                <div class="input-group">
                                   <input id="phone" type="tel" style="width: 370px; height: 40px; border: 1px solid rgba(33, 170, 237, 0.1); border-radius: 0.25rem;">
								
								<input id="output" type="text" name="phone" style="display:none">
                                </div>
                            </div>
                                    
								
                               
							

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
                                    <input type="text" name="email" class="form-control p_input" placeholder="Email" style="z-index: 1;" required>
                                </div>
                            </div>

                             <div class="form-group" >
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="enrollment_date" class="datepicker form-control" placeholder="Date of Visit" style="z-index: 1;" required>
                                </div>
                            </div>
                           <!-- <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input type="file" name="patient_image" class="form-control" placeholder="Patient Image">
                                </div>
                            </div>-->


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    {{ $errors->all()[0] }}
                                </div>
                            @elseif(session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-success offset-10">Register</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/intlTelInput.js') }}"></script>
<script>
  // For country code //
	var input = $("#phone"),
        output = $("#output");
		input.intlTelInput
		({
		  nationalMode: true,
		  utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js" // just for formatting/placeholders etc
		});

		// listen to "keyup", but also "change" to update when the user selects a country
		input.on("keyup change", function() {
		var intlNumber = input.intlTelInput("getNumber");
		  if (intlNumber) {
          // Here you get internation number with country code //
        
          
			output.val(intlNumber);
		  } 
        });
        
	 $('ul>li').click(function(){
	
	 var input = $("#phone"),
        output = $("#output");
        input.intlTelInput
		({
		  nationalMode: true,
		  utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js" // just for formatting/placeholders etc
		});
        var intlNumber = input.intlTelInput("getNumber");
		  if (intlNumber) {
          // Here you get internation number with country code //
			output.val(intlNumber);
		  } 
        });
	// $("form").submit(function(){

		// var code = document.getElementById('output').value
		// alert(code);
    // });
  </script>
{{--  <script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>  --}}
<script src="{{ asset('node_modules/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('js/misc.js') }}"></script>
<script>

   

    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxPrefilter(function(options, originalOptions, jqXHR){
        if (options.type.toLowerCase() === "post") {
            // initialize `data` to empty string if it does not exist
            options.data = options.data || "";

            // add leading ampersand if `data` is non-empty
            options.data += options.data?"&":"";

            // add _token entry
            options.data += "_token=" + encodeURIComponent(csrf_token);
        }
    });

    $(function() {
        $( ".datepicker" ).datepicker();
    } );






    function show_languages(){
        $('#language-group').show();
        $('#siteNbr-group').show();
        var val = $('#study_patient_id').val();
        check_nbr(val);
    }


//    $('#select-study').onchange( function(){

    $(document).on('change','#select-study', function(){
        $.ajax({
            url : '/dashboard/get_nbr/'+$(this).val(),
            success: function(response){
                $('.site_nbr').html(response);
            }
        });

    } );

    function check_nbr(value){
        var id = $('#select-study').val();
        if( value.trim() == "" ){
            $('#status_danger').hide();
            $('#status_success').hide();
        }
        $.ajax({
            url : '{{url('/')}}/dashboard/check_nbr_value/'+id +'/'+ value ,
            success:function(response){
                console.log(response);
                if(response == 'available'){
                    $('#status_success').show();
                    $('#status_danger').hide();
                }
                if(response == 'used'){
                    $('#status_danger').show();
                    $('#status_success').hide();
                }
            },
    
        });
    }

 

    /* $(document).on('change','#select-study',function(){
        $.ajax({
            type: 'POST',
            url: "/dashboard",
            //patient/get-languages-and-site-nbr/"+$(this).val(),
            dataType: "text",
            success: function(resultData) {
                resultData = JSON.parse(resultData);

                $('#siteNbr-group').show();
                $('#siteNbr-group .site_nbr').html(resultData.site_nbr+'-');

                if(resultData.contents.length) {
                    $('#language-group').show();
                    $('#languages')
                        .find('option')
                        .remove()
                        .end();
                    $.each(resultData.contents, function (i, item) {
                        $('#languages').append($('<option>', {
                            value: item,
                            text: item
                        }));
                    });
                }else{
                    $('#language-group').hide();
                }
            }
        });
    });  */
</script>

 
</body>
</html>