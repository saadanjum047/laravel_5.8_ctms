@extends('layouts.site')


@section('content')
    
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				@if ($message = Session::get('ok'))
   				<script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a
                    href="{{ route('dashboard.study.view', $study->id) }}">{{ $study->study_name }}</a>
        </li>
        @if($patient->site != null)
            <li class="breadcrumb-item"><a
                        href="{{ route('dashboard.study.site', [ $patient->study->id , $patient->site->site_id ]) }}">{{ $patient->site->site_name }}</a>
            </li>
        @endif
        <li class="breadcrumb-item active" aria-current="page">{{ $patient->patient_nbr }}</li>
    </ol>
</nav>

<a href="{{ route('dashboard.export.pateint_schedules', $patient->id) }}"
   class="btn btn-primary pull-right"><i class="fa fa-file-excel-o"></i> EXPORT</a>
<h3 class="text-primary mb-4">Patient
    №{{ $patient->patient_nbr }} {{--@if((int)Cookie::get('user_role')==2)| {{ $patient->name }}@endif--}}</h3>
<div class="row mb-2">
	 @if(empty($patient->password)) 
        <div class="col-md-12">
			<div class="alert alert-secondary" role="alert">
			Patient has not yet confirmed their registration. Please ask patient to review email confirmation from <u>info@delvehealth.com</u>!
			</div>
        </div>
    @endif
    <div class="col-lg-3 mt-4 mb-5">
        <h5 class="card-title mb-4">Patient Information</h5>
       
    
        <div class="col-md-12">
            {{-- @if((int)Cookie::get('user_role')==3) --}}
            @if(auth()->user()->role_id == 3 )
                <form action="{{ route('dashboard.patient.update' , $patient->id)}}" method="POST">
                    {{ csrf_field() }}

		            <input type="hidden" name="patient_id" value="{{$patient->id}}">
                    <div class="form-group">
                        <label for=""><b>Patient NBR:</b></label>
                        <input type="text" name="patient_nbr" value="{{ $patient->patient_nbr }}"
                               class="form-control">
                    </div>
					
                    <div class="form-group">
                        <label for=""><b>Name:</b></label>
                        <input type="text" name="name" value="{{ $patient->user->name }}" class="form-control">
                    </div>
					
					
					
                    <div class="form-group">
                        <label for=""><b>Phone:</b></label>
                        <input type="text" name="phone" value="{{ $patient->phone }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for=""><b>Email:</b></label>
                        <input type="text" name="email" value="{{ $patient->user->email }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for=""><b>Exit Date:</b></label>
                        <input type="text" name="exit_date" class="datepicker form-control col-md-11" value="
                        @if(isset($patient->exit_date))
                        {{ Carbon\Carbon::make($patient->exit_date)->format('m/d/Y')}}
                        @endif
                        ">
                    </div>
                    <button type="submit" class="btn brn-primary">Save</button>
                </form>
				@else
				<div class="form-group">
                        <label for=""><b>Patient NBR:</b></label>
                        <input type="text" name="patient_nbr" value="{{ $patient->patient_nbr }}"
                               class="form-control">
                    </div>
					
            @endif
			
        </div>
    </div>

    <div class="col-lg-4 mt-4 mb-5">
        <h5 class="card-title mb-4">Visit Schedules</h5>
       <table class="table table-hover table-bordered">
           <tr>
               <th> # </th>
               <th> Visit Name </th>
               <th> Visit Date </th>
               <th> Tasks </th>
               <th> Date Change Request </th>
           </tr>
           @foreach($patient->patient_schedule as $k => $schedule)
           <tr>
               {{-- @dump($schedule->patient_groups) --}}
               @php
               
                $visit = App\Study_Visit_Schedule::where('study_id' , $study->id)->where('visit_name' , $schedule->visit_name )->first();
                if(isset($visit->patient_groups)){
                $groups = $visit->patient_groups ?? '';
                }
               @endphp
               <td> {{$k+1}} </td>
               <td> {{$schedule->visit_name}} </td>
               <td> {{$schedule->visit_date}} </td>
               <td> @isset($groups) @foreach($groups as $group) @foreach($group->tasks as $task) {{$task->name}} , @endforeach @endforeach @endisset  </td>
               <td> @if(isset($schedule->change_request)) <a href="{{route('dashboard.patient.reset_schedule' , $schedule->change_request->id)}}" title="Approve" > {{$schedule->change_request->date}} </a> @endif </td>
           </tr>
           @endforeach
       </table>
    
        <div class="col-md-12">

        </div>
    </div>
    @php
        if($patient->patient_schedule->count() > 0 ){
        $current_visit = $patient->patient_schedule->where('visit_date' , '>' , Carbon\Carbon::now()->toDateString() )->first() ;
    }
    @endphp
     {{-- @if(Cookie::get('user_role')==2) --}}
     @if(auth()->user()->role_id == 2)
         <div class="col-lg-4 mt-4 mb-5">
            <h5 class="card-title mb-4">Visit Date</h5>
            <div class="col-md-12">
                <div class="row">
                        {{-- <input type="text" name="enrollment_date" class="datepicker form-control col-md-11" value="{{ $patient->enrollment_date }}"> --}}
                        <input type="text" name="enrollment_date" class="datepicker form-control col-md-11" value="@if(isset($current_visit)) {{ $current_visit->visit_date }} @endif">

                        {{ csrf_field() }}      
                 </div>
            </div>
         </div>

    @endif
    {{-- @if(Cookie::get('user_role')==3) --}}
    @if(auth()->user()->role_id == 3 )
    <div class="col-lg-4 mt-4 mb-5">

        	<!-- This is an investigator view -->
            <h5 class="card-title mb-4">Visit Date</h5>
            <div class="col-md-12">
                <form action="{{ route('dashboard.study.patient.schedule.save' , [$patient->study->id , $patient->id] ) }}" method="post">
                    <div class="row">
                        <input type="hidden" name="visit_id" value="@if(isset($current_visit)) {{ $current_visit->id }} @endif" >
                        <input type="text" name="enrollment_date" class="datepicker form-control col-md-11" value="@if(isset($current_visit)) {{ $current_visit->visit_date }} @endif">
                        
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="patient_id" value="{{ $patient->id }}">
                        <button type="submit" class="btn btn-primary col-md-1 cursor-pointer" title="Save Visit Date"><i
                                    class="fa fa-save"></i></button>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                </form>
            </div>
            
            <!-- Transfer patient -->
            <h5 class="card-title mb-4 mt-4">Transfer Patient</h5>
            <div class="col-md-12">
               <form method="post" action="{{ route('dashboard.study.patient.site.change' , [$patient->study->id , $patient->id ])}}">
 		{{ csrf_field() }}
		{{-- <input type="hidden" name="patient_id" value="{{ $patient->id }}"> --}}
		{{--<input type="hidden" name="table_id" value="{{$activity->tip_study_patient_id[0]->id }}">--}}
		{{-- <input type="hidden" name="created_by" value="{{Cookie::get('user_name') }}"> --}}
		{{--<input type="hidden" name="old_site_id" class="form-control" value="{{$activity->tip_study_patient_id[0]->site_id }}">--}}
                    <div class="row">
@if(isset($activity))
@foreach($activity->sites as $site)


@if($activity->tip_study_patient_id[0]->site_id===$site->site_id)


@else


@endif
@endforeach
@endif
	        <div class="form-group">
			<label for="exampleFormControlSelect1">New Site</label>
			<select class="form-control" id="exampleFormControlSelect1" name="site_id">
            <option selected disabled></option>
            @foreach($patient->study->sites as $site)
            @if($site->site->site_id != $patient->site_id )
            <option value="{{$site->site->site_id}}">{{$site->site->site_name.'---'.$site->site_nbr}} </option>
            @endif
            @endforeach
            {{-- @if(isset($activity))
			@foreach($activity->sites as $site)
	

	@if($patient->study[0]->id == $site->study_id)

@if($activity->tip_study_patient_id[0]->site_id===$site->site_id)

@else


@if($activity->tip_study_audit_count>0)
<option value="{{$site->site_id}}"    
@if($activity->tip_study_audit[0]->audit_id>0)
@if($site->site_id==$activity->tip_study_audit[0]->field_name)
selected
@else
@endif
@endif
>
{{$site->site_name.'---'.$site->site_nbr}}</option>
@else
 <option value="{{$site->site_id}}">{{$site->site_name.'---'.$site->site_nbr}}</option>
@endif

@endif
		   
@endif
 @endforeach
 @endif --}}
		 </select>
		</div>
		</div>

		<div class="row">
		<div class="form-group">
        <label for="exampleFormControlSelect1">Prior Site Name</label>
        <input type="text"  class="form-control" value="{{$patient->site->site_name ?? '-' }}" readonly>

        {{-- @if(isset($activity))
		@foreach($activity->sites as $site)
		@if($activity->tip_study_audit_count>0)
		@if($site->site_id==$activity->tip_study_audit[0]->old_value)

		
		<input type="text"  class="form-control" value="{{$site->site_name}}" readonly>
		@break
		

		@else		
		@endif
		@elseif($site->site_id==$activity->tip_study_patient_id[0]->site_id)
		<input type="text"  class="form-control" value="{{$site->site_name}}" readonly>
		@break

		@endif
        @endforeach
        @endif --}}
		</div>
		</div>
<div class="row">		
<div class="form-group">
<button type="submit" class="btn btn-success col-md-1 cursor-pointer" title="Transfer Patient"><i class="fa fa-save"></i></button>	      
</div>
</div>
<div class="row">
                        @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
 </div>
                </form>
            </div>
 

 <h5 class="card-title mb-4 mt-4">Transferred List </h5>
<div class="col-md-12">
 <table class="table center-aligned-table">
                    <thead>
                    <tr class="text-primary">
			<th>Patient Id</th>

                        <th>Hospital Name</th>
                        <th>Transferred Date</th>
                       
                    </tr>
                    </thead>
 <tbody>

@if(isset($activity))
@foreach($activity->sites as $site)

@if($patient->study[0]->id == $site->study_id)
@foreach($activity->audit as $audit)
@if($audit->field_name==$site->site_id)
<tr>
<td>{{$audit->patient_id}}</td>
<td>{{$site->site_name}}</td>
<td>{{$audit->date_created}}</td>
</tr>

@endif
@endforeach

@endif
@endforeach
@endif
 </tbody>
</table>
</div>

            
            <h5 class="card-title mb-4 mt-4">Visit History</h5>
            <div class="col-md-12">
                <table class="table center-aligned-table">
                    <thead>
                    <tr class="text-primary">
                        <th>No</th>
                        <th>Created By</th>
                        <th>Created At</th>
                        <th>Visit Date</th>
                    </tr>
                    </thead>
                    @if(isset($patient->visit_history))

                    @foreach($patient->visit_history->take(3) as $history)
                        <tbody>
                        <tr class="">
                            <td>{{ $history->id }}</td>
                            <td>{{ $history->creator->fullname ?? 's' }}</td>
                            <td>{{ $history->created_at}}</td>
                            <td>{{ $history->visit_date }}</td>
                        </tr>
                        </tbody>
                    @endforeach
                    @endif
                </table>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Show Full History
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Full History</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr class="text-primary">
                                        <th>No</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                        <th>Visit Date</th>
                                    </tr>
                                    </thead>
                                    @if(isset($patient->visit_history))
                                    @foreach($patient->visit_history as $history)
                                        <tbody>
                                        <tr class="">
                                            <td>{{ $history->id }}</td>
                                            <td>{{ $history->creator->fullname ?? '' }}</td>
                                            <td>{{ $history->created_at}}</td>
                                            <td>{{ $history->visit_date }}</td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                    @endif
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-lg-12 mt-4">
        <h5 class="card-title mb-4">Patient Tasks</h5>
        <div class="table-responsive">
	        @if (session()->has('success'))
				<script>
				swal("Diary SMS!", "{{ session('success') }}", "success");
				</script>
				
				@endif
                <table class="table table-bordered table-hover">
                    <thead>                  
                      <tr class="text-primary">
                        <th width="5%"> # </th>
                        <th width="15%"> Task Name </th>
                        <th width="35%"> Task Description </th>
                        <th width="10%"> Visit Name </th>
                        <th width="10%"> Completed </th>
                        <th width="10%"> Created </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($patient->tasks as $k => $task)
                        {{-- @dump($task->group->schedules) --}}
                            <tr>
                                <td> {{$k+1}} </td>
                                <td>
                                     {{-- <a href="{{route('dashboard.group.tasks.view' , $task->id  )}}" class="text-primary">  </a> --}}
                                          {{$task->name}}  </td>
                                <td> {{ str_limit(strip_tags($task->description) , 80)}} </td>
                                <td> @foreach($task->group->schedules as $schedule) {{$schedule->visit_name}} @endforeach </td>
                                <td> @if(isset($patient->tasks->find($task->id)->pivot->completed_at)) <span class="badge badge-success">{{ Carbon\Carbon::parse($patient->tasks->find($task->id)->pivot->completed_at)->toDateString() }} </span> @else <span class="badge badge-danger">Not Completed</span> @endif </td>
                                <td> {{($task->created_at)->toDateString()}} </td>
                            </tr>
                        @endforeach
                     
                    </tbody>
                  </table>
        </div>
    </div>
    <div class="col-lg-12 mt-4">
        <h5 class="card-title mb-4">Diaries</h5>
        <div class="table-responsive">
	        @if (session()->has('success'))
				<script>
				swal("Diary SMS!", "{{ session('success') }}", "success");
				</script>
				
				@endif
            <table class="table center-aligned-table table-hover">
                <thead>
                <tr class="text-primary">
                    <th>No</th>
                    <th>Diary Name</th>
                    <th>Diary Date</th>
                    <th>Actions</th>
                    <th>Type</th>
                </tr>
                </thead>
                @php
                    $diaries = $patient->diary_schedule;
                    //dd($diaries);
                @endphp
                <tbody>
                    @foreach($diaries as $pk => $patient_diary)
                    <tr class="">
                        <td>{{ $pk+1 }}</td>
                        <td>{{ $patient_diary->diary->diary_name}}</td>
                        <td>{{ $patient_diary->created_at->toDateString()}}</td>
                        <td> <button class="btn btn-success btn-sm" onclick="showQuestions({{$patient_diary->id}})" data-target="#question_model_{{$patient_diary->id}}" >Show Questions</button> @if($patient_diary->type != 2) <a class="btn btn-primary btn-sm" href="{{route('dashboard.study.patient.sendSMS' , [$study->id , $patient_diary->diary->id , $patient->id] )}} " >Send SMS</a> @endif </td>
                        <td> @if($patient_diary->type == 0) 
                            <span class="badge badge-warning">Current</span>
                            @elseif($patient_diary->type == 1)  
                            <span class="badge badge-danger">Overdue</span>
                            @elseif($patient_diary->type == 2) 
                            <span class="badge badge-success">Complete</span>
                            @else 
                            @endif
                        </td>
                    </tr>
                    
                    @endforeach
                    </tbody>
            </table>
        </div>
    </div>

    {{--  @dd($diaries)  --}}
    @foreach($diaries as $diary)
    {{-- @dump($diary->diary->questions) --}}
    <div class="modal fade" id="question_model_{{$diary->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Diary Questions</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body">
            @if($diary->diary->questions->count() > 0 )

            <strong> Frequency: {{$diary->frequency}} </strong><br>

            @foreach($diary->diary->questions as $k => $question)
            <strong class="text-primary">{{$question->question}}</strong> <br>
            

              @php
                if(count($question->specifications) > 0 )
                {
                    foreach($question->specifications as $spec){
                        $specs[] = $spec->name;

                    }
                }
                @endphp

                @php
                $answer = NULL;
                if(!is_null($question->ans->where('patient_id' , $patient->id)->where('frequency' , $diary->frequency)->first()) ) {
                    $answer = $question->ans->where('patient_id' , $patient->id)->where('frequency' , $diary->frequency)->first();
                }
                @endphp
                
                @if(!is_null($question->answer_type) && $question->answer_type == 1 )
              <div class="form-group">
                  <label class="col-form-label"> Answer </label>
                  <textarea name="answer" class="form-control" placeholder="Detail you Answer here" disabled >@if(isset($answer))  {{$answer->answers}}  @endif  </textarea>
                
              </div>
              
              @elseif(!is_null($question->answer_type) && $question->answer_type == 3)
              <div class="form-group">
                  <label class="col-form-label"> Select below answers</label><br>
                  @php

                  $tagsArray =  json_decode( $question->answers);
                  if(isset($answer))  {
                  $answersArray = json_decode(  $answer->answers);  
                  }   
                  @endphp
  
                  @foreach( $tagsArray as $i=> $ans) 
                  <input type="checkbox" id="check{{$i}}" value="{{ $tagsArray[$i] }}" name="choices[]"  
                  @if(isset($answer))  
                  @if(in_array( $ans , $answersArray )) {{"checked"}}  @endif
                  @endif
                  disabled 
                  />
                  <label class="col-form-label" for="check{{$i}}">{{ $tagsArray[$i] }} </label>
                  <br>
                  <div id="additional_fields{{$i}}">
                  @if(isset($answer->specifications))
                  @if(count($answer->specifications) > 0)
                      @foreach($answer->specifications as $spec)
                      @if($spec->name == $tagsArray[$i] )
                      <textarea class="form-control" name="specifications[]" id="specs{{$i}}" placeholder="Plz elaborate your answer" disabled > {{$spec->specification}} </textarea>
                      @endif
                      @endforeach
                      @endif
                      @endif  
                  </div>
                  @endforeach
              </div>
              @elseif(!is_null($question->answer_type) && $question->answer_type == 2)
              <div class="form-group">
                  <label class="col-form-label"> Select below answers</label><br>
                  @php
                  $tagsArray =  json_decode($question->answers);
                  @endphp


                  @foreach( $tagsArray as $i=> $ans) 
                  <input type="radio" id="radio{{$i}}{{$diary->frequency}}" disabled value="{{$ans}}"
                    @if(isset($answer) && $answer->answers == $ans) {{"checked"}}
                  @endif
                    />
                  <label class="col-form-label" for="radio{{$i}}{{$diary->frequency}}">{{ $tagsArray[$i] }} </label>
            
                  <div id="additional_fields{{$i}}">
                  @if(isset($answer->specifications))
                  @if(count($answer->specifications) > 0)
                      @foreach($answer->specifications as $spec)
                      @if($spec->name == $ans )
                      <textarea class="form-control" name="specifications" id="specs{{$i}}" placeholder="Plz elaborate your answer" disabled> {{$spec->specification}} </textarea>
                      @endif
                      @endforeach
                      @endif
                      @endif
                  </div>
                  <br>
                  @endforeach
              </div>
              @endif
                <hr>
            @endforeach
            @else
            <p class="alert alert-info"> No Questions found </p>
            @endif

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    {{-- <div class="col-lg-12 mt-4">
        <h5 class="card-title mb-4">Schedules</h5>
        <div class="table-responsive">
	        @if (session()->has('success'))
				<script>
				swal("Diary SMS!", "{{ session('success') }}", "success");
				</script>
				
				@endif
            <table class="table center-aligned-table">
                <thead>
                <tr class="text-primary">
                    <th>No</th>
                    <th>Visit Date</th>
                    <th>Type</th>
                    <th></th>
                </tr>
                </thead>
                @foreach($patient->patient_schedule as $pk=>$schedule)
                    <tbody>
                    <tr class="">
                        <td>{{ $pk+1 }}</td>
                        <td>{{ $schedule->created_at}}</td>
                        <td>
                            @switch($schedule->type)
                                @case(0)
                                <label class="badge badge-warning">Current</label>
                                @break
                                @case(1)
                                <label class="badge badge-danger">Overdue</label>
                                @break
                                @case(2)
                                <label class="badge badge-success">Completed</label>
                                @break
                            @endswitch
                        </td>
                        <td><a href="#" class="btn btn-primary btn-sm show-questions">Show Questions</a></td>
                       
                       @if($schedule->type==1 && ((int)Cookie::get('user_role')==3) && !isset($patient->info->exit_date))
                    <td><a href="{{ route('dashboard.patient.sendsms', $schedule->id) }}" class="btn btn-primary btn-sm" style="margin-left:5px">Send Sms</a></td>
					@endif
		       </tr>
                    <tr class="questions" style="display: none">
                        <td colspan="5">
                            <div class="row">
                                <div class="col-md-12 mb-3c"><b>Questions</b></div>
                                @if(isset($schedule->questions) && count($schedule->questions))
                                    @foreach($schedule->questions as $k=>$question)
										                                        <div class="col-md-4 mb-3">
                                            <b>{{ $question->question }}</b>
                                            @foreach($question->answers as $answer)
                                                <div class="col-md-12">
                                                    <input type="radio" name="radio{{ $pk.'_'.$k }}"
                                                           @if($answer->selected)checked @endif disabled="disabled"/>
                                                    {{ $answer->answer }}
                                                    @if($answer->selected && $answer->text != 'false')
                                                        <br><input type="text"
                                                                   value="{{ ($answer->text == 'true' || $answer->text == 'false') ? '' : $answer->text }}"
                                                                   disabled="disabled">
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
        </div>
    </div> --}}

    <div class="col-lg-12 mt-4">
        <a data-target="#patient_activity" data-toggle="modal" href="javascript:;" > <h5 class="card-title mb-4" >Patient Activity</h5></a>
        <div class="table-responsive">
            <table class="table center-aligned-table">
                <thead>
                <tr class="text-primary">
                    <th>No</th>
                    <th>Action</th>
                    <th>Created</th>
                </tr>
                </thead>

                @if(isset($patient->user->activityRecord) && $patient->user->activityRecord->count() > 0 )
                @php
                    $i = 0;
                @endphp
                    @foreach($patient->user->activityRecord->take(5)->sortByDesc('created_at') as $k => $activity)
                        <tbody>
                        <tr class="">
                            <td>{{ $i+=1 }}</td>
                            <td> {{$activity->description}} </td>
                            <td> {{$activity->created_at}} </td>
                            </tr>
                        </tbody>
                    @endforeach
                @endif
            </table>
        </div>
    </div>

    <div class="modal fade" id="patient_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">All Patient Activities</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <table class="table center-aligned-table">
                <thead>
                <tr class="text-primary">
                    <th>No</th>
                    <th>Action</th>
                    <th>Created</th>
                </tr>
                </thead>

                @if(isset($patient->user->activityRecord) && $patient->user->activityRecord->count() > 0 )
                    @php
                    $i = 0;
                    @endphp
                @foreach($patient->user->activityRecord->sortByDesc('created_at') as $k => $activity)
                        <tbody>
                        <tr class="">
                            <td>{{ $i+=1 }}</td>
                            <td> {{$activity->description}} </td>
                            <td> {{$activity->created_at}} </td>
                            </tr>
                        </tbody>
                    @endforeach
                @endif
            </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

    @if(auth()->user()->role_id == 3 )
    <div class="col-lg-12 mt-4">
        <h5 class="card-title mb-4">Pre Screenings</h5>

        <div class="table-responsive" >
        
        @if($patient->study->pre_screens->count() > 0 )
            
            <form action="{{route('dashboard.question.answered')}}" method="POST">
                @csrf
                @php
                $questionArray = array();
                @endphp
            @foreach ($patient->study->pre_screens as $s => $screen)
            {{-- @dd($screen) --}}

            <h6 class="text-primary"> {{$screen->name}} </h6>

            @foreach($screen->questions as $question )
                @if($question->status == 1)

                {{--   
                @if(session()->has('success'))
                <p class="alert alert-primary"> {{session('success')}} </p>
                @endif
                @if(session()->has('error'))
                <p class="alert alert-danger"> {{session('error')}} </p>
                @endif --}}
                
                <div>
                    @php
                    if($question->ans){
                        $answer = $question->ans->where('investigator_id' , auth()->user()->sponsor_user->id )->first();                        
                    }
                    @endphp
                    {{-- question id here --}}
                        <input type="hidden" name="question{{$question->id}}" value="{{$question->id}}" />
                        <input type="hidden" name="answer_id{{$question->id}}" value="{{$answer->id ?? 0}}" />
                        {{--  @dump($question->id)  --}}
                            <div class="col-md-8" >
                                <p style="display: inline" > {{$question->question}} </p> : 
                            @if(!is_null($question->answer_type) && $question->answer_type == 1 )
                            <div class="form-group">
                                <textarea name="answer{{$question->id}}" class="form-control" rows="2" placeholder="Detail you Answer here">@if(isset($answer)){{$answer->answer_body}}@endif</textarea>
                               
                            </div>
                            
                            @elseif(!is_null($question->answer_type) && $question->answer_type == 3)
                            <div class="">
                                @php
                                $tagsArray =  explode( ',' , $question->answers);
                                if(isset($answer))  {
                                $answersArray = explode( ',' , $answer->answer_body);  
                                foreach($answersArray as $arr)
                                $array[] = trim(str_replace(array( '[' , ']' , '"' ) , '' , $arr ));
                                }   
                                @endphp
                                {{--  {{dd($answersArray , $array)}}  --}}
                                @foreach( $tagsArray as $i=> $ans) 
                                {{-- @dd($ans , $array , $answer , $question) --}}
                                {{-- {{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} ,  --}}
                                <input style="display:inline" type="checkbox" id="check{{$question->id+$i}}" value="{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}}" name="answer{{$question->id}}[]"  

                                @if(isset($answer))
                                @if(in_array( trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) , $array )) {{"checked"}} @endif
                                @endif
                                @if(isset($specs) && in_array( trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) , $specs ) ) onclick="make_checkbox_description(this.id)" @endif  />

                                <label for="check{{$question->id+$i}}">{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} </label>

                                
                                @endforeach
                            </div>
                            @elseif(!is_null($question->answer_type) && $question->answer_type == 2)
                            <div class="form-group">
                                @php
                                $tagsArray =  explode( ',' , $question->answers);
                                @endphp
                                @foreach( $tagsArray as $i=> $ans) 
                                <input type="radio" id="radio{{$question->id+$i}}" name="answer{{$question->id}}" value="{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}}" 
                                {{-- @if(isset($answer) && $answer->answers == trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) ) --}}
                                  @if(isset($answer) && trim(str_replace(array( '[' , ']' , '"' ) , '' , $answer->answer_body )) == trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) )
                                    {{"checked"}}
                                @endif
                                @if(isset($specs) && in_array( trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) , $specs ) ) onclick="make_radio_description(this.id)" @else 
                                onclick="remove_radio_description(this.id)" @endif />
                
                                <label class="col-form-label" for="radio{{$question->id+$i}}">{{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} </label>
                
                                
                                @endforeach
                            </div>
                            @endif
                        </div>
                    
                    {{-- @if(isset($answer))
                    <button type="submit" class="btn btn-primary" style="margin-bottom:20px">Update</button>
                    @else
                    <button type="submit" class="btn btn-success" style="margin-bottom:20px">Save</button>
                    @endif
                 --}}
                    {{-- form ends here  --}}
               
                
                    <hr>
                
                </div>
                @php
                $questionArray = $question->id;
                @endphp

                <input type="hidden" name="q[]" value="{{$questionArray}}" >
                @endif
            @endforeach
            @endforeach
            <button class="btn btn-success" type="submit">Save</button>
            </form>
        </div>

        @else 
        <p class="alert alert-info">No Pre Screenings Found</p>
        @endif
    </div>
@endif

</div>

    <script>
    $("#patients_Diaries").dataTables({
        "bJQueryUI":true,
        "bSort":false,
        "bPaginate":true,
        "sPaginationType":"full_numbers",
        "iDisplayLength": 10
    });

    function showQuestions(id){

        $("#question_model_"+id).modal("show");
    }

    </script>   

@endsection
