@extends('layouts.site')


@section('content')
    
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				@if ($message = Session::get('ok'))
   				<script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a
                    href="{{ route('dashboard.study.view', $patient->study->id) }}">{{ $patient->study->study_name }}</a>
        </li>
        @if($patient->site != null)
            <li class="breadcrumb-item"><a
                        href="{{ route('dashboard.study.site', [ $patient->study->id , $patient->site->site_id ]) }}">{{ $patient->site->site_name }}</a>
            </li>
        @endif
        <li class="breadcrumb-item active" aria-current="page">{{ $patient->patient_nbr }}</li>
    </ol>
</nav>

<a href="{{ route('dashboard.patient.export-patient-and-schedules', $patient->id) }}"
   class="btn btn-primary pull-right"><i class="fa fa-file-excel-o"></i> EXPORT</a>
<h3 class="text-primary mb-4">Patient
    №{{ $patient->patient_nbr }} {{--@if((int)Cookie::get('user_role')==2)| {{ $patient->name }}@endif--}}</h3>
<div class="row mb-2">
	 @if(empty($patient->password)) 
        <div class="col-md-12">
			<div class="alert alert-secondary" role="alert">
			Patient has not yet confirmed their registration. Please ask patient to review email confirmation from <u>info@delvehealth.com</u>!
			</div>
        </div>
    @endif
    <div class="col-lg-3 mt-4 mb-5">
        <h5 class="card-title mb-4">Patient Information</h5>
       
    
        <div class="col-md-12">
            {{-- @if((int)Cookie::get('user_role')==3) --}}
            @if(auth()->user()->role_id ==3 || auth()->user()->role_id == 2)
                <form action="{{ route('dashboard.patient.update' , $patient->id)}}" method="POST">
                    {{ csrf_field() }}

		            <input type="hidden" name="patient_id" value="{{$patient->id}}">
                    <div class="form-group">
                        <label for=""><b>Patient NBR:</b></label>
                        <input type="text" name="patient_nbr" value="{{ $patient->patient_nbr }}"
                               class="form-control">
                    </div>
					
                    {{-- <div class="form-group">
                        <label for=""><b>Name:</b></label>
                        <input type="text" name="name" value="{{ $patient->user->name }}" class="form-control">
                    </div>
					
					
					
                    <div class="form-group">
                        <label for=""><b>Phone:</b></label>
                        <input type="text" name="phone" value="{{ $patient->phone }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for=""><b>Email:</b></label>
                        <input type="text" name="email" value="{{ $patient->user->email }}" class="form-control">
                    </div> --}}
                    <div class="form-group">
                        <label for=""><b>Exit Date:</b></label>
                        <input type="text" name="exit_date" class="datepicker form-control col-md-11" value="
                        @if(isset($patient->exit_date))
                        {{ Carbon\Carbon::make($patient->exit_date)->format('m/d/Y')}}
                        @endif
                        ">
                    </div>
                    <button type="submit" class="btn brn-primary">Save</button>
                </form>
				@else
				<div class="form-group">
                        <label for=""><b>Patient NBR:</b></label>
                        <input type="text" name="patient_nbr" value="{{ $patient->patient_nbr }}"
                               class="form-control">
                    </div>
					
            @endif
			
        </div>
    </div>
    
     {{-- @if(Cookie::get('user_role')==2) --}}
     @if(auth()->user()->role_id ==3 || auth()->user()->role_id == 2)
    <div class="col-lg-4 offset-4 mt-4 mb-5">
            <h5 class="card-title mb-4">Visit Date</h5>
            <div class="col-md-12">
                <div class="row">
                        <input type="text" name="enrollment_date" class="datepicker form-control col-md-11"
                               value="{{ $patient->enrollment_date }}">
                        {{ csrf_field() }}      
                 </div>
            </div>
            
            
    @endif
    {{-- @if(Cookie::get('user_role')==3) --}}
    @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 2)
        	<!-- This is an investigator view -->
            <h5 class="card-title mb-4">Visit Date</h5>
            <div class="col-md-12">
                <form action="{{ route('dashboard.patient.save-visit-date') }}" method="post">
                    <div class="row">
                        <input type="text" name="enrollment_date" class="datepicker form-control col-md-11"
                               value="{{ $patient->enrollment_date }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="patient_id" value="{{ $patient->id }}">
                        <button type="submit" class="btn btn-primary col-md-1 cursor-pointer" title="Save Visit Date"><i
                                    class="fa fa-save"></i></button>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                </form>
            </div>
            
            <!-- Transfer patient -->
            <h5 class="card-title mb-4 mt-4">Transfer Patient</h5>
            <div class="col-md-12">
               <form method="post" action="{{ route('dashboard.patient.SavePatientIntoSite')}}">
 		{{ csrf_field() }}
		<input type="hidden" name="patient_id" value="{{ $patient->id }}">
		{{--<input type="hidden" name="table_id" value="{{$activity->tip_study_patient_id[0]->id }}">--}}
		<input type="hidden" name="created_by" value="{{Cookie::get('user_name') }}">
		{{--<input type="hidden" name="old_site_id" class="form-control" value="{{$activity->tip_study_patient_id[0]->site_id }}">--}}
                    <div class="row">
@if(isset($activity))
@foreach($activity->sites as $site)


@if($activity->tip_study_patient_id[0]->site_id===$site->site_id)


@else


@endif
@endforeach
@endif
	                    <div class="form-group">
			 <label for="exampleFormControlSelect1">New Site</label>
			 <select class="form-control" id="exampleFormControlSelect1" name="site_id">
                        <option selected disabled></option>
            @if(isset($activity))
			@foreach($activity->sites as $site)
	

	@if($patient->study[0]->id == $site->study_id)

@if($activity->tip_study_patient_id[0]->site_id===$site->site_id)

@else


@if($activity->tip_study_audit_count>0)
<option value="{{$site->site_id}}"    
@if($activity->tip_study_audit[0]->audit_id>0)
@if($site->site_id==$activity->tip_study_audit[0]->field_name)
selected
@else
@endif
@endif
>
{{$site->site_name.'---'.$site->site_nbr}}</option>
@else
 <option value="{{$site->site_id}}">{{$site->site_name.'---'.$site->site_nbr}}</option>
@endif

@endif
		   
@endif
 @endforeach
 @endif
		 </select>
		</div>
		</div>

		<div class="row">
		<div class="form-group">
        <label for="exampleFormControlSelect1">Prior Site Name</label>
        @if(isset($activity))
		@foreach($activity->sites as $site)
		@if($activity->tip_study_audit_count>0)
		@if($site->site_id==$activity->tip_study_audit[0]->old_value)

		
		<input type="text"  class="form-control" value="{{$site->site_name}}" readonly>
		@break
		

		@else		
		@endif
		@elseif($site->site_id==$activity->tip_study_patient_id[0]->site_id)
		<input type="text"  class="form-control" value="{{$site->site_name}}" readonly>
		@break

		@endif
        @endforeach
        @endif
		</div>
		</div>
<div class="row">		
<div class="form-group">
<button type="submit" class="btn btn-success col-md-1 cursor-pointer" title="Transfer Patient"><i class="fa fa-save"></i></button>	      
</div>
</div>
<div class="row">
                        @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
 </div>
                </form>
            </div>
 

 <h5 class="card-title mb-4 mt-4">Transferred List </h5>
<div class="col-md-12">
 <table class="table center-aligned-table">
                    <thead>
                    <tr class="text-primary">
			<th>Patient Id</th>

                        <th>Hospital Name</th>
                        <th>Transferred Date</th>
                       
                    </tr>
                    </thead>
 <tbody>

@if(isset($activity))
@foreach($activity->sites as $site)

@if($patient->study[0]->id == $site->study_id)
@foreach($activity->audit as $audit)
@if($audit->field_name==$site->site_id)
<tr>
<td>{{$audit->patient_id}}</td>
<td>{{$site->site_name}}</td>
<td>{{$audit->date_created}}</td>
</tr>

@endif
@endforeach

@endif
@endforeach
@endif
 </tbody>
</table>
</div>


            
            <h5 class="card-title mb-4 mt-4">Visit History</h5>
            <div class="col-md-12">
                <table class="table center-aligned-table">
                    <thead>
                    <tr class="text-primary">
                        <th>No</th>
                        <th>Created By</th>
                        <th>Created At</th>
                        <th>Visit Date</th>
                    </tr>
                    </thead>
                    @if(isset($patient->visit_history))
                    @foreach($patient->visit_history->take(3) as $history)
                        <tbody>
                        <tr class="">
                            <td>{{ $history->id }}</td>
                            <td>{{ $history->creator->fullname }}</td>
                            <td>{{ $history->created_at}}</td>
                            <td>{{ $history->visit_date }}</td>
                        </tr>
                        </tbody>
                    @endforeach
                    @endif
                </table>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Show Full History
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Full History</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr class="text-primary">
                                        <th>No</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                        <th>Visit Date</th>
                                    </tr>
                                    </thead>
                                    @if(isset($patient->visit_history))
                                    @foreach($patient->visit_history as $history)
                                        <tbody>
                                        <tr class="">
                                            <td>{{ $history->id }}</td>
                                            <td>{{ $history->creator->fullname }}</td>
                                            <td>{{ $history->created_at}}</td>
                                            <td>{{ $history->visit_date }}</td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                    @endif
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="col-lg-12 mt-4">
        <h5 class="card-title mb-4">Schedules</h5>
        <div class="table-responsive">
	        @if (session()->has('success'))
				<script>
				swal("Diary SMS!", "{{ session('success') }}", "success");
				</script>
				
				@endif
            <table class="table center-aligned-table">
                <thead>
                <tr class="text-primary">
                    <th>No</th>
                    <th>Visit Date</th>
                    <th>Type</th>
                    <th></th>
                </tr>
                </thead>
                @foreach($schedules as $pk=>$schedule)
                    <tbody>
                    <tr class="">
                        <td>{{ $schedule->id }}</td>
                        <td>{{ $schedule->created_at}}</td>
                        <td>
                            @switch($schedule->type)
                                @case(0)
                                <label class="badge badge-warning">Current</label>
                                @break
                                @case(1)
                                <label class="badge badge-danger">Overdue</label>
                                @break
                                @case(2)
                                <label class="badge badge-success">Completed</label>
                                @break
                            @endswitch
                        </td>
                        <td><a href="#" class="btn btn-primary btn-sm show-questions">Show Questions</a></td>
                       
                       @if($schedule->type==1 && ((int)Cookie::get('user_role')==3) && !isset($patient->info->exit_date))
		<td><a href="{{ route('dashboard.patient.sendsms', $schedule->id) }}" class="btn btn-primary btn-sm" style="margin-left:5px">Send Sms</a></td>
					@endif
		       </tr>
                    <tr class="questions" style="display: none">
                        <td colspan="5">
                            <div class="row">
                                <div class="col-md-12 mb-3c"><b>Questions</b></div>
                                @if(isset($schedule->questions) && count($schedule->questions))
                                    @foreach($schedule->questions as $k=>$question)
										                                        <div class="col-md-4 mb-3">
                                            <b>{{ $question->question }}</b>
                                            @foreach($question->answers as $answer)
                                                <div class="col-md-12">
                                                    <input type="radio" name="radio{{ $pk.'_'.$k }}"
                                                           @if($answer->selected)checked @endif disabled="disabled"/>
                                                    {{ $answer->answer }}
                                                    @if($answer->selected && $answer->text != 'false')
                                                        <br><input type="text"
                                                                   value="{{ ($answer->text == 'true' || $answer->text == 'false') ? '' : $answer->text }}"
                                                                   disabled="disabled">
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
        </div>
    </div>

    <div class="col-lg-12 mt-4">
        <h5 class="card-title mb-4">Patient Activity</h5>
        <div class="table-responsive">
            <table class="table center-aligned-table">
                <thead>
                <tr class="text-primary">
                    <th>No</th>
                    <th>Action</th>
                    <th>Diary Confirmation</th>
                    <th>Date</th>
                    <th></th>
                </tr>
                </thead>
                
                @if(isset($activity) && count($activity->activity))
                    @foreach($activity->activity as $lnb=>$item)
                        <tbody>
                        <tr class="">
                            <td>{{ $item->id }}</td>
                            <td>
                                @if($item->action=='login')
                                    Authorized
                                @elseif($item->action=='schedule_confirm')
                                    @if(count($item->questions) == 0)
                                        Authorized
                                    @else
                                        <a href="#" class="show-questions">Open Diary</a>
                                    @endif
                                @else
                                    {{ $item->action }}
                                @endif
                            </td>
                            <td>
                                @if($item->action=='schedule_confirm' && isset($item->schedule))
                                    {{ optional($item->schedule)->created_at }}
                                @endif
                            </td>
                            <td>{{ explode('.', $item->created_at->date)[0] }}</td>
                        </tr>
                        @if($item->action=='schedule_confirm')
                            <tr class="questions" style="display: none">
                                <td colspan="5">
                                    <div class="row">
                                        @foreach($item->questions as $k=>$question)

                                            <div class="col-md-4 mb-3">
                                                <b>{{ $question->question }}</b>
                                                @foreach($question->answers as $l=>$answer)
                                                    <div class="col-md-12">
                                                        <input type="radio" name="radiobutton{{ $pk.'_'.$lnb}}"
                                                               @if($answer->selected)checked @endif disabled="disabled"/>
                                                        {{ $answer->answer }}
                                                        @if($answer->selected && $answer->text != false)
                                                            <br><input type="text"
                                                                       value="{{ ($answer->text == 'true' || $answer->text == 'false') ? '' : $answer->text }}"
                                                                       disabled="disabled">
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    @endforeach
                @endif
            </table>
        </div>
    </div>

    
    <div class="col-lg-12 mt-4">
        <h5 class="card-title mb-4">Pre Screenings</h5>
        <div class="table-responsive" >
            
          
            @foreach ($patient->study->pre_screens as $s => $screen)

            {{--  //Pre screening accordian starts here  --}}
            <div class="accordion" id="Pre_screening">
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#Pre_screening{{$s}}" aria-expanded="false" aria-controls="Pre_screening{{$s}}"> {{$screen->name}} </button>
                    </h2>
                  </div>

                  <div style="padding:10px ; background-color: #f2f7f8; " id="Pre_screening{{$s}}" class="collapse" aria-labelledby="headingTwo" data-parent="#Pre_screening">
                    <div class="card-body" style="background-color: #f2f7f8" >
                   
                   


            @if(auth()->user()->role_id == 3)
            <a href="{{route('dashboard.study.pre_screening.viewQuestions' , [$patient->study->id , $patient->id , $screen->id] )}} " class="btn btn-success float-right"> Answer The Question </a> 
            @endif
            <div class="text-secondry" style="margin-left:20px" > <p><strong> {{$screen->name}}</strong> </p> </div> <br>
           
            @foreach ($screen->questions as $q => $question)

            {{--  <div class="accordion" id="Pre_screening_questions">
                
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#question{{$k}}" aria-expanded="false" aria-controls="question{{$k}}">
                        {{$question->question}}
                      </button>
                    </h2>
                  </div>
                  <div id="question{{$k}}" class="collapse" aria-labelledby="headingTwo" data-parent="#Pre_screening_questions">
                    <div class="card-body">
                   sdfsdf
                    </div>
                  </div>
                </div>
                
              </div>  --}}

            {{--  // Question accordian starts here  --}}

              <div class="accordion" id="Pre_screening_questions">
                
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#question{{$s}}{{$q}}" aria-expanded="false" aria-controls="question{{$s}}{{$q}}">
                        {{$question->question}} <span class="text-primary">({{$question->ans->count()}})</span> 
                      
                    </button>
                    <span> <a class=" btn btn-primary btn-sm float-right"   href="{{route('dashboard.study.pre_screening.viewQuestion' , [$patient->study->id , $patient->id ,$screen->id , $question->id] )}} "> <i class="fa fa-reply"></i> Answer </a> </span>
                    </h2>
                  </div>
                  <div style="padding:10px" id="question{{$s}}{{$q}}" class="collapse" aria-labelledby="headingTwo" data-parent="#Pre_screening_questions">
                    <div class="card-body">
                        @if($question->ans->count() == 0 )
                        <p  class="text-danger">Answers Not found </p>
                        
                        @else
                        @php
                        if($question->ans ){
                            $answer = $question->ans->where('investigator_id' , auth()->user()->sponsor_user->id )->first();   
                        }
                        @endphp
                        {{-- dump($answer) --}}
                        @if(isset($answer->answer_body))
                        <p > {{ trim(str_replace(array( '[' , ']' , '"' ) , '' , $answer->answer_body )) ?? "" }} </p> 
                        @endif
                        @endif

                        @if($question->ans->count() > 0 && $answer == NULL )
                        <p  class="text-danger">Answers Not found </p>
                        @endif
                        <hr>

                    </div>
                  </div>
                </div>
                
              </div>
            {{--  // Question accordian ends here  --}}


            {{--  <p class="text-primary"> {{$question->question}} </p>   --}}
            {{--  @if($question->ans->count() == 0 )
            <p  class="text-danger">Answers Not found </p>
            <hr>
            @endif  --}}




            {{--  @foreach ($question->ans as $k => $ans)
             <p class=""> {{ trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans->answer_body ))}} </p> 
             <span class="badge badge-info"> by {{$ans->investigator->user->name}} </span>  
             <hr>
            @endforeach    --}}
            @php
                $answer = NULL;
            @endphp
            @endforeach

            {{-- <table class="table table-hover bg-white">
                <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Answered By</th>
                </tr>
                @foreach ($screen->questions as $question)
                    <tr>
                        <td></td>
                        <td>{{$question->question}} </td>
                    </tr>

                    @foreach ($question->ans as $ans)
                    <tr>
                        <td></td>
                        <td> - </td>
                        <td>{{$ans->answer_body}} </td>
                        <td><span class="badge badge-info">{{$ans->investigator->user->name}}</span></td>
                    </tr>

                    @endforeach
                    @endforeach

                </table> --}}

            {{--  <hr>  --}}
        </div>
    </div>
  </div>
  
</div>
            {{--  //Pre screening accordian ends here  --}}
            @endforeach
        </div>
    </div>


</div>

<script>
$("#patients_Diaries").dataTables({
     "bJQueryUI":true,
      "bSort":false,
      "bPaginate":true,
      "sPaginationType":"full_numbers",
       "iDisplayLength": 10
});
</script>

@endsection
