<table>
    <thead>
    <tr>
        <th>Patient Number</th>
        <th>Date of Diary</th>
        <th>Status</th>
        <th>Site Name</th>
        @foreach($data['questions'] as $k=>$question)
            <th>{{ $k }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
        @foreach($data['patients'] as $patient)
            <tr>
                <td>{{ $patient['id'] }}</td>
            </tr>
            @foreach($patient['schedules'] as $schedule)
            <tr>
                <td></td>
                <td>
                    {{ $schedule->created_at }}<br />
                </td>



                <td>
                    @if($schedule->type==2)
                        Completed
                    @elseif($schedule->type==0)
                        Current
                    @else
                        Overdue
                    @endif
                    <br />
                </td>
@foreach( $patient['site'] as $site)
 
 @if( $site->site_id ==$patient['info']['site_id'] )
<td>
{{$site->site_name}}
</td>
@endif
@endforeach


                @foreach($schedule->questions as $question)
                    <td>
                        @if(collect($question->answers)->where('selected', 1)->first())
                            {{ collect($question->answers)->where('selected', 1)->first()->answer }}
                        @endif
                    </td>
                @endforeach
            </tr>
                 @endforeach
        @endforeach
    </tbody>
</table>