<div class="row mb-2">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-block">
                <a class="btn btn-primary pull-right" href="#"><i class="fa fa-print"></i> Print</a>
                <div class="row">
                    <div class="col-md-5">
                        <h5 class="card-title mb-4">Patients Notes</h5>
                    </div>
                    {{--<div class="col-md-4">--}}
                        {{--<input type="text" class="form-control live-search-box" placeholder="search patient here">--}}
                    {{--</div>--}}
                </div>
                <div class="table-responsive" id="patients-notes">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            <th>No</th>
                            <th>Patient Id</th>
                            <th>Patient Image</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($notes as $note)
                            <tr class="">
                                <td>{{ $note->id }}</td>
                                <td>{{ $note->patient_id }}</td>
                                <td><img class="patient_note_image" src="{{ $note->patient_image }}" alt=""></td>
                                <td>{{ $note->note }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @empty($notes)
                    Notes Not Found!
                @endempty
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-block add_note">
                <div class="row">
                    <div class="col-md-5">
                        <h5 class="card-title mb-4">Add Notes</h5>
                    </div>
                </div>
                <div class="table-responsive" id="patients-notes">
                    <form action="{{ route('dashboard.patient.add-notes') }}" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Select Patient</label>
                            <div class="col-sm-10">
                                <select name="patient_id" id="select_patients" class="form-control">
                                    @foreach($patients as $patient))
                                        <option value="{{$patient->id}}">{{$patient->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Note</label>
                            <div class="col-sm-10">
                                <textarea type="file" name="note" class="form-control" placeholder="Note"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Avatar</label>
                            <div class="col-sm-10">
                                <input type="file" name="patient_image" class="form-control" placeholder="Patient Image">
                            </div>
                        </div>

                        @if($errors->any())
                            <div class="alert alert-danger">
                                {{ $errors->all()[0] }}
                            </div>
                        @endif

                        @if(session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-sm-10">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary">Add Notes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>