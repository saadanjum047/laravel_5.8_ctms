@extends('layouts.site')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item" aria-current="page">
  Registered Patients
  </li>
  
     </ol>
  </nav>
  
<div class="row mb-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <a class="btn btn-primary pull-right" href="{{ route('dashboard.patient.export-patients-list') }}"><i class="fa fa-file-excel-o"></i> Export</a>
                <div class="row">
                    <div class="col-md-2">
                        <h5 class="card-title mb-4">Patients List</h5>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control live-search-box" placeholder="search patient here">
                    </div>
                </div>
                <div class="table-responsive" id="patients-list">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            <th>No</th>
                            <th>Patient Name</th>
                            <th>Patient NBR</th>
                            <th>Visit Date</th>
 			                <th>Action</th>
				
							 
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($patients as $patient)
                        {{--  {{dump($patient->user->patient_schedule)}}  --}}
                            <tr class="">
                                <td> {{$patient->user->name}} </td>
                                <td>{{ $patient->id }}</td>
                                <td><a href="{{route('dashboard.patient.show' , [$patient->study->id , $patient->id] )}} ">{{ $patient->patient_nbr }}</a></td>
                                <td>
                                    @php
                                        $dates = $patient->patient_schedule->pluck('visit_date')->toArray();

                                        if(isset($dates)){
                                        foreach($dates as $date){
                                            if(Carbon\Carbon::parse($date) <= \Carbon\Carbon::now()->format("d/m/y") ){

                                                $curr_date = $date;
                                                break;
                                            }
                                        }
                                        }
                                        @endphp
                                        @if(isset($curr_date)) {{ $curr_date }} @else <span class="badge badge-success"> No Schedule </span> @endif
                                     </td>

                                     <td>
								{{-- <a href="#" class="btn btn-primary btn-sm">Manage</a> --}}
								 {{-- @if(Cookie::get('user_role') == 3) --}}
								 @if(auth()->user()->role_id == 3)
								<a href="{{ route('dashboard.study.diary',  [ $patient->study->id , $patient->id]) }}" class="btn btn-warning btn-sm" style="margin-left:5px">View Diaries</a>
								@endif
								</td>
								
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @empty($patients)
                    Patients Not Found!
                @endempty
            </div>
        </div>
    </div>
</div>

@endsection
