@extends('layouts.site')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
@endsection

@section('content')
    {!! $content !!}
@endsection

@section('scripts')
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function() {
            $( ".datepicker" ).datepicker();
        } );


        $('.show-questions').click(function(e){
            e.preventDefault();
            console.log($(this).parent().parent().parent().find('.questions').toggle());
        });

        jQuery(document).ready(function($){

            $('#patients-list tbody tr').each(function(){
                $(this).attr('data-search-term', $(this).text().toLowerCase());
            });

            $('.live-search-box').on('keyup', function(){
                var searchTerm = $(this).val().toLowerCase();

                $('#patients-list tbody tr').each(function(){

                    if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });

            });

        });
    </script>
@endsection