<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if($schedules != null) 
<div class="row mb-2">

    <div class="col-lg-12">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
		        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
				@foreach($schedules->patient_nbr as $sc)
			  	<li class="breadcrumb-item"><a href="{{ route('dashboard.patient.Diaries', $sc->user_pat_id) }}">
			  	@endforeach
				@foreach($schedules->study_name as $studyname)
				    {{$studyname}}	
				@endforeach   
				</a>
				</li>
                <li class="breadcrumb-item active" aria-current="page">
				@foreach($schedules->patient_nbr as $sc)
					{{$sc->patient_id}}	
				@endforeach
				
				</li>
    		</ol>
		</nav>

        <div class="card">
            <div class="card-block">
                <!--<a class="btn btn-primary pull-right" href="{{ route('dashboard.patient.export-patients-list') }}"><i class="fa fa-file-excel-o"></i> Export</a>-->
                <div class="row">
                    <div class="col-md-4">
                      <h5 class="card-title mb-4 ml-2">Patient Diaries</h5>  	
                    </div>
				@if (session()->has('success'))
				   				<script>
				swal("Good job!", "{{ session('success') }}", "success");
				</script>
				
				@endif
				
                </div>
                <div class="table-responsive" id="patients-Diaries">
                    <table class="table center-aligned-table" id="patients_Diaries">
                        <thead>
                        <tr class="text-primary">
                            <th>No</th>
                           <!-- <th>Patient NBR</th>-->
				<th>type</th>

                            <th>Visit Date</th>
                            <th>Date Confirmed</th>
                            <th>Send SMS</th>
							 
                        </tr>
                        </thead>
                       
                    <tbody>
					 @foreach($schedules->activity as $schedule)
					 
                    <tr class="">
                        <td>{{ $schedule->id }}</td>
                           <td>
                            @switch($schedule->type)
                                @case(0)
                                <label class="badge badge-warning">Current</label>
                                @break
                                @case(1)
                                <label class="badge badge-danger">Overdue</label>
                                @break
                                @case(2)
                                <label class="badge badge-success">Completed</label>
                                @break
                            @endswitch
                        </td>
 			<td>{{ $schedule->created_at}}</td>
			@if($schedule->type==1)
		<td><a href="{{ route('dashboard.patient.sendsms', $schedule->id, $schedules->study_name) }}" class="btn btn-primary btn-sm" style="margin-left:5px">Send Sms</a></td>@endif
			</tr>
			
                        @endforeach
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
</div>
@else

<div class="row mb-2">
	<div class="table-responsive" id="patients-Diaries">
                    <table class="table center-aligned-table" id="patients_Diaries">
                        <thead>
                        <tr class="text-primary">
                            <th>No</th>
                           <!-- <th>Patient NBR</th>-->
				<th>type</th>

                            <th>Visit Date</th>
                            <th>Date Confirmed</th>
                            <th>Send SMS</th>
							 
                        </tr>
                        </thead>
                       
                   

</div>
@endif
<script>
$("#patients_Diaries").dataTables({
     "bJQueryUI":true,
      "bSort":false,
      "bPaginate":true,
      "sPaginationType":"full_numbers",
       "iDisplayLength": 10
});
</script>
