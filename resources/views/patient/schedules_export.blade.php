<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Visit Date</th>
        <th>Type</th>
        @foreach($schedules as $schedule)
            @foreach($schedule['questions'] as $question)
                <th></th>
            @endforeach
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($schedules as $schedule)
        <tr>
            <td><h2>{{ $schedule['id'] }}</h2></td>
            <td>{{ $schedule['created_at'] }}</td>
            <td>{{ $schedule['type'] }}</td>
        </tr>
        {{--<tr>--}}
            {{--<td colspan="{{ count($schedule['questions']) }}">--}}
                {{--Questions--}}
            {{--</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--@foreach($schedule['questions'] as $question)--}}
            {{--<td colspan="3">--}}
                    {{--{{ $question['question'] }}<br />--}}
                    {{--@foreach($question['answers'] as $answer)--}}
                        {{---{{ $answer['answer'] }}<br />--}}
                    {{--@endforeach--}}
                    {{--<br /><br />--}}
            {{--</td>--}}
            {{--@endforeach--}}
        {{--</tr>--}}
    @endforeach
    </tbody>
</table>