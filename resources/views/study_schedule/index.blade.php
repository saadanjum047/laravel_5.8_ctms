@extends('layouts.site')

@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item" aria-current="page">
Study Schedule
</li>

   </ol>
</nav>


<div class="bg-white">

    <div class="card">
        <div class="card-header bg-white">
            
          <h3 class="card-title">Scheduled Studies
            @if(auth()->user()->role_id == 2)
          <button data-toggle="modal" data-target="#create_model" style="float:right" class="btn btn-success"    ><i class="fa fa-plus"></i> Schedule Study</button>
            @endif
        </h3>
        </div>

        @if($errors->any())
        @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{$error}} </p>    
        @endforeach
        @endif
        
    <table class="table table-hover">
        <tr>
            <th>#</th>
            <th>Study Name</th>
            <th>Visit Name</th>
            <th>Target</th>
            <th>Window Low </th>
            <th>Window high </th>
            @if(auth()->user()->role_id == 2)
            <th>Actions </th>
            @endif
        </tr>
        {{-- {{dd($schedules)}} --}}
        @if(isset($schedules))
        @foreach($schedules as $k => $schedule)
        @php
        $array =  trim(str_replace(array( '[' , ']' , '"' ) , '' , $schedule->visit_name ));
        $names = explode(',' , $array);
        @endphp
        <tr>
            <td>{{$k+1}} </td>
            <td>{{$schedule->study->study_name}} </td>
            <td>
              @foreach($names as $name)
              {{$name}} <br>
              @endforeach 
            </td>
            <td>
              @php
              $array =  trim(str_replace(array( '[' , ']' , '"' ) , '' , $schedule->target ));
              $target = explode(',' , $array);
              @endphp
              @foreach($target as $tar)
              {{ $tar  }} <br>
              @endforeach 
             </td>
            <td>{{$schedule->window_low}} </td>
            <td>{{$schedule->window_high}} </td>
            @if(auth()->user()->role_id == 2)
            <td> 
                {{--  <a href="{{route('dashboard.study_schedule.delete' , $schedule->id)}}" class="text-danger"><i class="fa fa-trash fa-lg"></i> </a>   --}}
                
                | <a onclick="update_model_date( {{$schedule}} , {{$schedule->study}} , {{json_encode($target)}} , {{json_encode($names)}} )" class="text-primary"><i class="fa fa-edit fa-lg"></i> </a>
            </td>
            @endif
        </tr>
        @endforeach
        @endif
    </table>
</div>
</div>





<div class="modal fade" id="create_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Schedule Study</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('dashboard.study_schedule')}}" method="POST">
            {{ csrf_field() }} 
        <div class="modal-body">
            
             <div class="form-group">
              <label for="patient_id" class="col-form-label">Studies:</label>
              <select class="form-control" name="study" id="patient_id" required>
                  <option value="" >Select Study</option>
                  @foreach ($studies as $study)
                      <option value="{{$study->id}}">{{$study->study_name}} </option>
                  @endforeach
              </select>
            </div>


          <div id="extra_feilds">
            <div class="form-group">
              <label for="visit_name" class="col-form-label">Visir Name:</label>
              <input type="text" class="form-control" name="name[]" id="visit_name" required>
              </div>
            <div class="form-group">
                <label for="target_data" class="col-form-label">Target:</label>
                <input type="number" class="form-control" name="target[]" id="target_data" required>
            </div>
            
          </div>
          
          <button type="button" class="btn btn-primary" style="margin-bottom:20px" onclick="add_extra_fields()" ><i class="fa fa-plus"></i> </button>
          
            <div class="form-group">
                <label for="window_low_Data" class="col-form-label">Window Low:</label>
                <input type="number" class="form-control" name="window_low" id="window_low_Data" required>
            </div>
            <div class="form-group">
                <label for="window_high_data" class="col-form-label">Window High:</label>
                <input type="number" class="form-control" name="window_high" id="window_high_data" required>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Schedule</button>
        </div>
        </form>

      </div>
    </div>
  </div>



  <div class="modal fade" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Schedule</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('dashboard.study_schedule.update')}}" method="POST">
            {{ csrf_field() }} 
        <div class="modal-body">

          <input type="hidden"  id="schedule" name="schedule" />
            
             <div class="form-group">
              <label for="study" class="col-form-label">Studies:</label>
              <select class="form-control" name="study" id="study" required>
                  <option value="" >Select Study</option>
                  @foreach ($studies as $study)
                      <option value="{{$study->id}}">{{$study->study_name}} </option>
                  @endforeach
              </select>
            </div>

            
            <div id="extra_feilds_update">
              
           </div>
           <button type="button" class="btn btn-primary" style="margin-bottom:20px" onclick="add_extra_fields_update()" ><i class="fa fa-plus"></i> </button>


            <div class="form-group">
                <label for="low" class="col-form-label">Window Low:</label>
                <input type="number" class="form-control" name="window_low" id="low" required>
            </div>
            <div class="form-group">
                <label for="high" class="col-form-label">Window High:</label>
                <input type="number" class="form-control" name="window_high" id="high" required>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update Schedule</button>
        </div>
        </form>

      </div>
    </div>
  </div>


<script>

  var  i = 1;
    function update_model_date(schedule , study , target , visit_names )
    {
        $('.flds').remove();
        $("#update_model").modal('show');
        console.log(target , visit_names);
        $('#visit').val(schedule.visit_name);
        $('#target').val(schedule.target);
        $('#low').val(schedule.window_low);
        $('#high').val(schedule.window_high);
        $('#schedule').val(schedule.id);

        for (j = 0 ; j < target.length ; j++){
          console.log(target[j] , visit_names[j]);
          $('#extra_feilds_update').append( '<div id="fields'+i+'" class="flds" ><div class="form-group"><label for="visit_name'+i+'" class="col-form-label">Visir Name '+i+' :</label><input type="text" required class="form-control" name="name[]" id="visit_name'+i+'" value="'+visit_names[j]+'" /></div><div class="row"><div class="form-group col-md-9"><label for="target_data'+i+'" class="col-form-label">Target '+i+':</label><input type="number" class="form-control" required name="target[]" id="target_data'+i+'" value="'+target[j]+'" /></div><div class="col-md-3"><button type=button id="'+i+'" class="btn btn-danger" style="margin-top:30px" onclick="delete_fields(this.id)"><i class="fa fa-minus"></i> </button></div></div></div>');
          i = i + 1;
        }

        //var patients = document.getElementById('patients');
        /*var options = patients.options;
        for(  i = 1 ; i <= options.length; i++ ){ 
            var option = options[i];
            if(option.value == patient.id ){
              console.log('found');
                var selected = option.index;
                document.getElementById("patients").selectedIndex = selected;
                break;
            }
        } */
        var studies = document.getElementById('study');
        var options = studies.options;
        for( j = 1 ; j <= options.length; j++ ){ 
            var option = options[j];            
            if(option.value == study.id ){

                var select = option.index;
                document.getElementById("study").selectedIndex = select;
                break;

            }
        }
    } 

    function add_extra_fields_update(){
      $('#extra_feilds_update').append( '<div id="fields'+i+'" class="flds" ><div class="form-group"><label for="visit_name'+i+'" class="col-form-label">Visir Name '+i+' :</label><input type="text" class="form-control" required name="name[]" id="visit_name'+i+'" /></div><div class="row"><div class="form-group col-md-9"><label for="target_data'+i+'" class="col-form-label">Target '+i+':</label><input type="number" required class="form-control" name="target[]" id="target_data'+i+'" value="" /></div><div class="col-md-3"><button type=button id="'+i+'" class="btn btn-danger" style="margin-top:30px" onclick="delete_fields(this.id)"><i class="fa fa-minus"></i> </button></div></div></div>');
      i = i + 1;
    }

    function add_extra_fields(){
      //console.log('fields');
      $('#extra_feilds').append( '<div id="fields'+i+'" class="flds"><div class="form-group"><label for="visit_name'+i+'" class="col-form-label">Visir Name '+i+' :</label><input type="text" required class="form-control" name="name[]" id="visit_name'+i+'" /></div><div class="row"><div class="form-group col-md-9"><label for="target_data'+i+'" class="col-form-label">Target '+i+':</label><input type="number" class="form-control" required name="target[]" id="target_data'+i+'" value="" /></div><div class="col-md-3"><button type=button id="'+i+'" class="btn btn-danger" style="margin-top:30px" onclick="delete_fields(this.id)"><i class="fa fa-minus"></i> </button></div></div></div>');
      i = i + 1;
    } 

    function delete_fields(id){
      $('#fields'+id).remove();
      if($('#fields'+id).length == 0 ){
        console.log('fields'+id);
      }
      if ($("#extra_feilds_update .flds").length == 0){ 
        $('#extra_feilds_update').append( ' <div class="flds"><div class="form-group"><label for="visit_name" class="col-form-label">Visir Name:</label><input type="text" class="form-control" required name="name[]" id="visit_name"></div><div class="form-group"><label for="target_data" class="col-form-label">Target:</label><input type="number" required class="form-control" name="target[]" id="target_data"></div></div>' );

      
      }
    }

  </script>

@endsection
{{-- <div id="fields'+i+'">
   <div class="form-group">
     <label for="visit_name'+i+'" class="col-form-label">Visir Name '+i+' :</label>
     <input type="text" class="form-control" name="name[]" id="visit_name'+i+'" />
    </div>
    <div class="row"> 
      <div class="form-group col-md-9">
        <label for="target_data'+i+'" class="col-form-label">Target '+i+':</label>
        <input type="number" class="form-control" name="target[]" id="target_data'+i+'" value="" />
      </div>  
      <div class="col-md-3">
  <button class="btn btn-danger" style="margin-top:30px"><i class="fa fa-minus"></i> </button>
</div>
</div> 
</div> --}}