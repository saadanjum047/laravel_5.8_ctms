

@extends('layouts.site')



@section('content')
    
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>

     <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.investigator.groups', $study->id) }}" >Investigators Tasks</a></li>

     <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('dashboard.study.investigator.groups', $study->id) }}" >Groups</a></li>
         
      
     <li class="breadcrumb-item" aria-current="page">Create Group</li>
     </ol>
 </nav>


<div class="bg-white">

    <div class="card">

        @if($errors->any())
        @foreach ($errors->all() as $error)
            <p class="alert alert-danger"> {{$error}} </p>
        @endforeach
        @endif
        
                    <div class="card-header" style="margin-bottom:20px">
                        <h3 class="card-title"> Add A New Task Group </h3>
                    </div>
        
                    <form action="{{route('dashboard.study.investigator.group.store' , $study->id )}} " method="POST">
        
                        {{ csrf_field() }}
        
                <div class="form-group row text-center">
                    <label class="col-md-2 col-form-label"> Group Name: </label>  
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Enter the name of the Group" name="name" style="border-color:#aaa" required>  
                    </div>
                </div>
                <div class="form-group row text-center">
                    <label class="col-md-2 col-form-label"> Group Description: </label>  
                    <div class="col-md-8">
                        <textarea  id="summernote" style="border-color:#aaa" class="form-control"  name="description"  > </textarea>  
                    </div>
                </div>
                
                    <div class="col-md-10">
                    <button type="submit" style="margin-bottom:20px" class="btn btn-success float-right">Add Task Group</button>
                    </div>
            </form>
        
            </div>


</div>

@endsection