

@extends('layouts.site')


@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if ($message = Session::get('success'))
   <script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
      <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.investigator.groups', $study->id) }}" >investigators Tasks</a></li>
     
   
      <li class="breadcrumb-item" aria-current="page">Groups</li>
     </ol>
 </nav>



<div class="bg-white" style="padding:10px">


    <div class="">
        <div class="card-title">
            <h2>Groups 
               

                <a class="btn btn-success float-right" style="color:white" href="{{route('dashboard.study.investigator.group.create' , $study->id )}}" > <i class="fa fa-plus"></i> Add New Group</a>
            </h2>
        </div>

        <table class="table table-hover">
            <tr> 
                <th> # </th>
                <th> Group Name </th>
                <th> Group Description </th>
                <th> Created </th>
                <th> Actions </th>
            </tr>
    
            @foreach ($groups as $k=> $group)
                
            <tr>
                <td> {{$k+1}} </td>
               

                <td> <a href="{{route('dashboard.study.investigator.group.tasks' , [$study->id , $group->id ])}} "> {{$group->name}} </a> </td>
                <td> {{ str_limit(strip_tags($group->description) , 80)}} </td>
                <td> {{($group->created_at)->toDateString()}} </td>
                <td> 
                    {{--  <a href="javascript:;" class="text-danger" onclick="modal_open({{$group}} , {{$group->schedule ?? NULL }})" > <i class="fa fa-paperclip fa-lg"></i> </a>  --}}

                        
                    <a href="{{route('dashboard.study.investigator.group.edit' , [$study->id ,  $group->id ])}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> |
                    
                    <a href="{{route('dashboard.study.investigator.group.delete' , [$study->id , $group->id ])}} " class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a>

                </td>
            </tr>
            @endforeach
        </table>
    
    </div>
    
</div>

<!-- Modal -->
  {{--  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="{{route('dashboard.group.attach')}} " method="POST">
            @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Assign Schedule</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="group_id" id="group" value="" >
          <div class="form-group">
            <lable class="col-form-label">Schedules</lable>
            <select class="form-control" name="schedule_id" id="schedule" required>
                <option value="" index="-1" >Select Any Schedule</option>
                @foreach ($schedules as $schedule)
                <option value="{{$schedule->id}}">{{$schedule->visit_name}} </option>    
                @endforeach
                <option value="0" class="text-danger">Remove the schedule</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>

      </div>
    </div>
  </div>  --}}


  <script>

    function modal_open(group , schedule){
        
    document.getElementById("schedule").selectedIndex = 0;
    $("#group").val();

    $('#exampleModal').modal('show');

    $("#group").val(group.id);
    var options = document.getElementById('schedule').options; 
    console.log(options);
    for (var i = 0; i <= options.length; i++) {
        var option = options[i]; 
        if( option.value == schedule.id ){
          var selected = option.index;
          document.getElementById("schedule").selectedIndex = selected;
        }
    }
    }
  </script>

@endsection