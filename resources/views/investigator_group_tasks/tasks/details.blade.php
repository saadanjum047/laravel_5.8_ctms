
@extends('layouts.site')


@section('content')



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>

        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.investigator.groups', $study->id) }}" >Investigator Tasks</a></li>
   
        <li class="breadcrumb-item active" aria-current="page">
           <a href="{{ route('dashboard.study.investigator.groups', $study->id) }}" >{{$group->name}}</a></li>
         
        <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('dashboard.study.investigator.group.tasks', [$study->id , $group->id , $task->id ]) }}"> Tasks </a></li>
      
      <li class="breadcrumb-item" aria-current="page">Tasks Details</li>
     </ol>
 </nav>

    

 <div class="bg-white" style="padding:10px">
    <div class="">
    
        
        <div class="card-title">
            <h2>"{{$task->name}}" Details </h2>
        </div>

        <table class="table table-hover">
            <tr> 
                <th> # </th>
                <th> Investigator Name </th>
                <th> Task Status </th>
                <th> Completed </th>
            </tr>
            {{-- {{dd($task->investigators->pluck('id'))}} --}}
            @foreach ($task->investigators as $k=> $investigator)
                
            <tr>
                <td> {{$k+1}} </td>
                <td> {{$investigator->user->name ?? '-' }} </td>
                <td> @if($investigator->pivot->status == 0 )  Incomplete @else Completed @endif </td>
                <td> {{$investigator->pivot->complete_at ?? '-' }} </td>
              
            </tr>
            @endforeach
        </table>
        
    
    </div>
    </div>

 @endsection