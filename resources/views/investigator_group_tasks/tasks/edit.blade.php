

@extends('layouts.site')

@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
   
     <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('dashboard.study.groups', $study->id) }}" >Investigators Tasks</a></li>

     
     <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('dashboard.study.groups', $study->id) }}" >{{$group->name}} </a></li>
        
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.group', [$study->id,$group->id]) }}" >{{$group->name}} Tasks</a></li>
   
     <li class="breadcrumb-item" aria-current="page">Create Tasks</li>
     </ol>
 </nav>


    <div class="bg-white">
        <div class="card">

@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif

            <div class="card-header" style="margin-bottom:20px">
                <h3 class="card-title"> Add A New Task </h3>
            </div>

            <form action="{{route('dashboard.study.investigator.group.task.update' , [ $study->id , $group->id , $task->id ])}} " method="POST">

                {{ csrf_field() }}

        <div class="form-group row text-center">
            <label class="col-md-2 col-form-label"> Task Name: </label>  
            <div class="col-md-8">
                <input type="text" class="form-control" placeholder="enter the name of the task" name="name" style="border-color:#aaa" required value="{{$task->name}}">  
            </div>
        </div>
        <div class="form-group row text-center">
            <label class="col-md-2 col-form-label"> Task Description: </label>  
            <div class="col-md-8">
                <textarea rows="10" id="summernote" style="border-color:#aaa" class="form-control" placeholder="Enter some description" name="description"> {{$task->description}} </textarea>  
            </div>
        </div>
        <div class="form-group row text-center">
            <label class="col-md-2 col-form-label"> Repeated: </label>  
            <div class="col-md-8">
                <input type="number" class="form-control" placeholder="Repeated" name="repeated" style="border-color:#aaa" value="{{$task->repeated}}" required>  
            </div>
        </div>
        

            <div class="col-md-10">
            <button type="submit" style="margin-bottom:20px" class="btn btn-success float-right">Update Task</button>
            </div>
    </form>

    </div>
    </div>

@endsection
