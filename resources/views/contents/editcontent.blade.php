

@extends('layouts.site')

@section('content')
{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!---->
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
	<!--<link rel="stylesheet" href="{{ asset('css/demo.css') }}">-->
	<!--end-->


				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif



     <div class="wrapper">
		<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{route('dashboard.study.view', [$study,'English'])}}" >
{{$study->study_name}}</a>
</li>

<li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Protocols</a></li>


<li class="breadcrumb-item " aria-current="page"> <a href="{{ route('dashboard.study.contents',$study) }}" > Content </a> </li>


<li class="breadcrumb-item " aria-current="page"> Update Content </a> </li>


</ol>
	</nav>

         
			<div class="card">
            <div class="card-header">
             <h3 class="card-subtitle mb-4 text-muted">Update Content</h3>                         	
			</div>
@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif
                               
                 <div class="card-body" style="margin: 20px;padding: 20px;">
        <form class="form-horizontal" method="POST" action="{{route('dashboard.study.Postupdatecontent' , [$study->id , $content->content_id ] )}}" 
        enctype="multipart/form-data">  

                 {{ csrf_field() }}
                    
                 <span id="error"></span>
		
	<input type="hidden" name="content_id" class="form-control" value="{{$content->content_id}}" style="display:none">

		@isset($Study)	
 		{{--  <input type="text" name="study_id" class="form-control" value="{{$Study}}" style="display:none">  --}}
		@endif


               



             
                       
                 <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Content tab</label>


                  <div class="col-sm-10">

                      <select class="form-control " name="content_tab_id"  required>

                  @foreach($contentstab as $data)
                  <option value="{{$data->tab_id}}" @if($content->content_tab->tab_id == $data->tab_id ) {{"selected"}} @endif >{{$data->tab_title}}</option>
                  @endforeach
                  </select>

                  </div>

                </div>

		<div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">  Content Header</label>

                  <div class="col-sm-10">
  			{{--  <input type="hidden" class="form-control " name="study_id"  value="{{$study}}">  --}}

                      <input type="text" class="form-control " name="content_header" value="{{$content->content_header}}" required>
			
                  </div>

                </div>

		<div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">  Content Description</label>



                  <div class="col-sm-10">

                      <textarea type="text" class="form-control " id="summernote" name="content_body"  required>{{$content->content_body}}</textarea>
			
                  </div>

                </div>

               <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;"> Content Language</label>

                  <div class="col-sm-10">

                    <select name="content_lang" class="form-control">

 		                	<option value="{{$content->content_lang}}" selected>{{$content->content_lang}}</option> 

                                        <option value="Afrikaans">Afrikaans</option> 
                                        <option value="Albanian">Albanian</option> 
                                        <option value="Amharic">Amharic</option> 
                                        <option value="Arabic">Arabic</option> 
                                        <option value="Armenian">Armenian</option> 
                                        <option value="Azerbaijani">Azerbaijani</option>
                                        <option value="Bengali">Bengali</option> 
                                        <option value="Bosnian">Bosnian</option>
                                        <option value="Bulgarian">Bulgarian</option> 
                                        <option value="Catalan">Catalan</option>
                                        <option value="Chinese">Chinese</option>
                                        <option value="Czech">Czech</option>
                                        <option value="Danish">Danish</option>
                                        <option value="Dutch">Dutch</option>
                                        <option value="English" selected>English</option>
                                        <option value="Esperanto">Esperanto</option>
                                        <option value="Estonian">Estonian</option>
                                        <option value="Finnish">Finnish</option>
                                        <option value="French">French</option>
                                        <option value="Georgian">Georgian</option>
                                        <option value="German">German</option>
                                        <option value="Scottish Gaelic">Scottish Gaelic</option>
                                        <option value="Greek, Modern">Greek</option>
                                        <option value="Hebrew">Hebrew</option>
                                        <option value="Hindi">Hindi</option>
                                        <option value="Croatian">Croatian</option>
                                        <option value="Hungarian">Hungarian</option>
                                        <option value="Icelandic">Icelandic</option>
                                        <option value="Indonesian">Indonesian</option>
                                        <option value="Italian">Italian</option>
                                        <option value="Japanese">Japanese</option>
                                        <option value="Kinyarwanda">Kinyarwanda</option>
                                        <option value="Korean">Korean</option>
                                        <option value="Latin">Latin</option>
                                        <option value="Latvian">Latvian</option>
                                        <option value="Lithuanian">Lithuanian</option>
                                        <option value="Macedonian">Macedonian</option>
                                        <option value="Malayalam">Malayalam</option>
                                        <option value="Maori">Maori</option>
                                        <option value="Malay">Malay</option>
                                        <option value="Norwegian">Norwegian</option>
                                        <option value="Iranian">Persian, Iranian</option>
                                        <option value="Polish">Polish</option>
                                        <option value="Portuguese">Portuguese</option>
                                        <option value="Pushto">Pushto</option>
                                        <option value="Romanian">Romanian, Rumanian, Moldovan</option>
                                        <option value="Russian">Russian</option>
                                        <option value="Sanskrit">Sanskrit</option>
                                        <option value="Slovak">Slovak</option>
                                        <option value="Slovenian">Slovenian</option>
                                        <option value="Spanish">Spanish</option>
                                        <option value="Serbian">Serbian</option>
                                        <option value="Swedish">Swedish</option>
                                        <option value="Thai">Thai</option>
                                        <option value="Turkish">Turkish</option>
                                        <option value="Ukrainian">Ukrainian</option>
                                        <option value="Urdu">Urdu</option>
                                        <option value="Vietnamese">Vietnamese</option>
                                        <option value="Welsh">Welsh</option>

                                    </select>
			
                  </div>

                </div>
 	            	<div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;"> Content Type</label>

                  <div class="col-sm-10">

                     <select name="content_type" class="form-control">
                                        <option value="c">Regular</option> 
                                        <option value="consent">Consent</option> 
                                    </select>
			
                  </div>

                </div>


     			<div class="form-group row">
                               
          <label for="name" class="col-sm-2 pull-left"> Active</label>

          <div class="col-sm-10">
          <input type="checkbox" name="active" value="1"  @if($content->active === "1") {{"checked"}} @endif /> 
          </div> 
        </div>
            <br>

		   <div class="form-group row">
                               
                <label for="name" class="col-sm-2 pull-left"> Public</label>

              <div class="col-sm-10">
              <input type="checkbox" name="public" value="1" @if($content->public === "1") {{"checked"}} @endif> 
 				</div>
 				</div>
                         

                         
                 			

              </div>  <!-- /.card-body -->

              <div class="card-footer">   
								 
                <button type="submit" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px">Update</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

            </div>

          </div>


  <script type="text/javascript">
   function val() {
   sponsor_id = document.getElementById("sponsor_id").value;
	var _token = $("input[name='_token']").val();
           $.ajax({
          {{--  //url: "{{ route('dashboard.study.getstudies')}}",  --}}
          method: 'post', 
          data: {_token:_token,sponsor_id:sponsor_id},
	     success: function(data) {
		if(data.html!=''){
	    	$('#study').html(data.html);
		}else{
		$('#study').empty();
		$('#study').append("<option value=''>Sorry No Study Found Select Some Other Organization</option>");

		}
            }

      });
}

</script>

@endif
@endsection
