@extends('layouts.site')

@section('content')

{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}
@if(auth() && auth()->user()->role_id == 2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif





 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">
<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
{{$study->study_name}}

</a>
</li>

<li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Protocols</a></li>

<li class="breadcrumb-item active" aria-current="page">
<a href="{{ route('dashboard.study.contents', [$study,'English']) }}" > Content </a>
</li>
<li class="breadcrumb-item active" aria-current="page">
<a href="{{ route('dashboard.study.viewrevision', [$study, $revision->content->content_id ]) }}" > Revisions </a>
</li>
<li class="breadcrumb-item" aria-current="page">
Revisions
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
        
 
  <div class="card-header"><h3 class="pull-left"> Translations </h3> 

    <a href="{{route('dashboard.study.content.version.translation.create' , [$study->id , $revision->content->content_id , $revision->revision_id ])}} " class="btn btn-success float-right"> Create New </a>
  </div>

  <div class="box-header">
  </div>
            <div class="card-block">

                                                    
				<div class="progress">
            <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
        </div>



          <div class="box">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
            <table id="myTable" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th> Version </th>
                  <th> Revision Header</th>
                  <th> Header</th>
                  <th> Description</th>
                  <th> Language</th>
                  <th> Video Link</th>
                  <th> Actions</th>                                             
                </tr>
                </thead>
                <tbody>

@foreach($translations as $data)
<tr>
  <td>{{ $data->revision->version}}</td>
  <td>{{ $data->revision->revision_header}}</td>
  <td>{{ $data->header}}</td>
    <td>{{ str_limit(strip_tags($data->description) , 200  ) }}</td>
    <td>{{ $data->language}}</td>
    <td>{{ $data->video_url}}</td>

    <td>
    <a href="{{route('dashboard.study.content.version.translation.edit' , [ $study->id , $revision->content->content_id , $revision->revision_id , $data->id ])}} " title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit fa-lg"></i></a>
    <a href="{{route('dashboard.study.content.version.translation.delete' ,  [ $study->id , $revision->content->content_id , $revision->revision_id , $data->id ] )}}" class="text-danger" title="Delete Organization"><i class="fa fa-trash fa-lg"></i></a>
  
    </td>
</tr>
@endforeach   
                             
                </tbody>
               </table>
               
            </div>
	     </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>


    



  <script>
    function openDetails(revision , site , content){

      document.getElementById("all_sites").selectedIndex = "0";

      
      $("#assign_site").modal('show');
      $('#revision_id').val(revision.revision_id);
      $('#current_site_id').val(site.site_id);

      var content_id = content.content_id;
      var revision_id = revision.revision_id;
      console.log(revision_id , content_id);
      $('#remove_btn').remove();
      if(site != null){
        $("#all_sites option").each(function()
        { 
          if(this.value == site.site_id){
            var val = this.index;

            document.getElementById("all_sites").selectedIndex = this.index;
            $('#btn_div').append(' <a class="btn btn-danger" href="/dashboard/contents/'+content_id+'/revision/'+revision_id+'/remove" style="color:white;" id="remove_btn"> <i class="fa fa-trash"></i> Remove Site</a>');
            
          }

        });
      }
      }

      function upenModel(revision){
        $("#assign_site").modal('show');
        $('#revision_id').val(revision.revision_id);
      }
    
  </script>



@endif



@endsection