@extends('layouts.site')

@section('content')

{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif





 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">
<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
{{$study->study_name}}

</a>
</li>

<li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Protocols</a></li>

<li class="breadcrumb-item active" aria-current="page">
<a href="{{ route('dashboard.study.contents', [$study,'English']) }}" > Content </a>
</li>
<li class="breadcrumb-item" aria-current="page">
Revisions
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left"> Revisions</h3>   </div>
            <div class="card-block">

                                                    
				<div class="progress">
            <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
        </div>



          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
            <table id="myTable" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th> Version </th>
                  <th> Content Header</th>
                  <th> Header</th>
                  <th> Description</th>
                  <th> Language</th>
                  <th> Video Link</th>
                  <th> Site</th>
                  <th> Translated </th>
                  <th> Active </th>
                  <th> Actions</th>                                             
                </tr>
                </thead>
                <tbody>


@foreach($revisions as $data)
<tr>
  <td>{{ $data->version}}</td>
  <td>{{ $data->content->content_header}}</td>
  <td>{{ $data->revision_header}}</td>
    <td>{{ str_limit(strip_tags($data->revision_des) , 200  ) }}</td>
    <td>{{ $data->revision_language}}</td>
    <td>{{ $data->video_url}}</td>
    <td>{{ $data->site->site->site_name ?? " " }}</td>

    <td><h6> <a href="{{route('dashboard.study.content.version.translation' , [$study->id , $data->content_id , $data->revision_id] )}}" >View ({{$data->languages->count()}}) </a></h6> </td>

    <td>  @if($data->active == 1 ) <span class="badge badge-success">Active</span>  @else <span class="badge badge-warning">Inactive</span> @endif  </td>


    <td>
    <a href="{{route('dashboard.study.content.editRevision' , [ $study->id , $content->content_id , $data->revision_id ])}} " title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit fa-lg"></i></a>
    <a href="{{route('dashboard.study.content.deleteRevision' , [$study->id , $content->content_id , $data->revision_id])}}" class="text-danger" title="Delete Organization"><i class="fa fa-trash fa-lg"></i></a>
    <a href="javascript:;" @if($data->site != NULL )  onclick="openDetails( {{$data}} , {{$data->site->site ?? NULL }} , {{$data->content ?? NULL}})" @else onclick="upenModel({{$data}})" @endif class="text-success" title="Delete Organization"><i class="fa fa-save fa-lg"></i></a>
    </td>
{{-- href="{{route('dashboard.study.content.assignSite' , [$study->id , $content->content_id , $data->revision_id])}}"             --}}
</tr>
@endforeach   
                             
                </tbody>
               </table>
               
            </div>
	     </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>


    <div class="modal fade" id="assign_site" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

          <form action="{{route('dashboard.study.content.assignSite' , [$study->id , $content->content_id])}}" method="POST">

            {{ csrf_field() }}
            <input type="hidden" name="revision_id" id="revision_id" value="" >
            <input type="hidden" name="current_site_id" id="current_site_id" value="" >
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Assing Revision to the Site</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           
            <div class="form-group">
              <label>Select the Site</label>
              <select class="form-control" name="site_id" id="all_sites">
              <option value="" index="-1">Choose any Site</option>
                @foreach ($sites as $site)
                <option value="{{$site->site->site_id}}"> {{$site->site->site_name}} </option>   
                @endforeach

              </select>
            </div>
            <div id="btn_div">
            <a class="btn btn-danger" style="color:white ; display:none" id="remove_btn"> <i class="fa fa-trash"></i> Remove Site</a>
          </div>
          </div>
      
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         
            <button type="submit" class="btn btn-primary">Assing</button>

          </div>
        </form>
        </div>
      </div>
    </div>



  <script>
    function openDetails(revision , site , content){

      document.getElementById("all_sites").selectedIndex = "0";

      
      $("#assign_site").modal('show');
      $('#revision_id').val(revision.revision_id);
      $('#current_site_id').val(site.site_id);

      var content_id = content.content_id;
      var revision_id = revision.revision_id;
      console.log(revision_id , content_id);
      $('#remove_btn').remove();
      if(site != null){
        $("#all_sites option").each(function()
        { 
          if(this.value == site.site_id){
            var val = this.index;

            document.getElementById("all_sites").selectedIndex = this.index;
            $('#btn_div').append(' <a class="btn btn-danger" href="/dashboard/contents/'+content_id+'/revision/'+revision_id+'/remove" style="color:white;" id="remove_btn"> <i class="fa fa-trash"></i> Remove Site</a>');
            
          }

        });
      }
      }

      function upenModel(revision){
        $("#assign_site").modal('show');
        $('#revision_id').val(revision.revision_id);
      }
    
  </script>



@endif



@endsection