@extends('layouts.site')

@section('content')


<style>
  td:{
    overflow-wrap: break-word;
  }
</style>

{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

      @if ($message = Session::get('success'))
        <script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<div class="wrapper">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('dashboard.study.view', [$study,'English']) }}" >{{$study->study_name}}</a></li>

    <li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Protocols</a></li>

    <li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Content</a></li>
    <li class="breadcrumb-item " aria-current="page">View Content</a></li>
    

  </ol>
</nav>
<!-- Main content -->

  <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
    <div class="card">
      <div class="card-header"><h3 class="pull-left"> Contents</h3>   </div>
      <div class="card-block">
        <div class="progress">
          <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
          </div>
                  </div>
          <div class="box">
          <div class="box-header">
            
          </div>
          <!-- /.box-header -->
          <div class="box-body">
  <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body "  style="overflow-x:auto;">



<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
  <a class="nav-link" id="pills-Calculator-tab" data-toggle="pill" href="#pills-Calculator" role="tab" aria-controls="pills-Calculator" aria-selected="false">Calculator</a>
</li>
<li class="nav-item">
  <a class="nav-link" id="pills-Inclusion-tab" data-toggle="pill" href="#pills-Inclusion" role="tab" aria-controls="pills-Inclusion" aria-selected="false">Inclusion</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillscontact-tab" data-toggle="tab" href="#pills-Exclusion" role="tab" aria-controls="Exclusion" aria-selected="false">Exclusion</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsImplantvideo-tab" data-toggle="tab" href="#pills-Implantvideo" role="tab" aria-controls="Implantvideo" aria-selected="false">Implant video</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsTrainingMaterial-tab" data-toggle="tab" href="#pills-TrainingMaterial" role="tab" aria-controls="TrainingMaterial" aria-selected="false">Training Material</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsStudyDesign-tab" data-toggle="tab" href="#pills-StudyDesign" role="tab" aria-controls="StudyDesign" aria-selected="false">Study Design</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsAdverseEvents-tab" data-toggle="tab" href="#pills-AdverseEvents" role="tab" aria-controls="AdverseEvents" aria-selected="false">Adverse Events</a>
</li>
<li class="nav-item">
<a class="nav-link" id="Procedure-tab" data-toggle="tab" href="#pills-Procedure" role="tab" aria-controls="Procedure" aria-selected="false">Procedure</a>
</li>
<li class="nav-item">
<a class="nav-link" id="Notification-tab" data-toggle="tab" href="#pills-Notification" role="tab" aria-controls="Notification" aria-selected="false">Notification</a>
</li>

<li class="nav-item">
<a class="nav-link" id="Other-tab" data-toggle="tab" href="#pills-Other" role="tab" aria-controls="Other" aria-selected="false">Other</a>
</li>



</ul>
<div class="tab-content" id="pills-tabContent">
<div class="tab-pane fade show active" id="pills-Calculator" role="tabpanel" aria-labelledby="pills-Calculator-tab"> 
  <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
<tbody> @foreach($contents as $data)
@php
  $revision = $data->revisions->where('active' , 1)->first();
@endphp
{{--  @dump($data , )  --}}
@if($data->content_tab_id==0)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td width="40%">{!! str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) !!}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision </a>  ({{$data->revisions->count()}}) 
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>
<div class="tab-pane fade" id="pills-Inclusion" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

  <thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>
  
  <th>Operations</th><th>Actions</th>
  
  </tr></thead>
                  <tbody> @foreach($contents as $data)
  @php
      $revision = $data->revisions->where('active' , 1)->first();
  @endphp
  
  @if($data->content_tab_id==1)
  <tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>

  
  <td>
  
  <a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>
  
  <a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
  </td>
    <td>
  <a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
  <a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
  </td> </tr> 
  @endif
  @endforeach 
  </tbody>
  </table>
</div>



<div class="tab-pane fade" id="pills-Exclusion" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==2)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>



<div class="tab-pane fade" id="pills-Implantvideo" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==3)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>



<div class="tab-pane fade" id="pills-TrainingMaterial" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==4)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>


<div class="tab-pane fade" id="pills-StudyDesign" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==5)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>

<div class="tab-pane fade" id="pills-AdverseEvents" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==6)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>


<div class="tab-pane fade" id="pills-Procedure" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==7)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>


<div class="tab-pane fade" id="pills-Notification" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==8)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>

<div class="tab-pane fade" id="pills-Other" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">

<thead>  <tr><th> Version </th> <th>Content Header</th> <th> Content Body</th> <th>Content Lang</th>

<th>Operations</th><th>Actions</th>

</tr></thead>
                <tbody> @foreach($contents as $data)
@php
    $revision = $data->revisions->where('active' , 1)->first();
@endphp

@if($data->content_tab_id==9)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>


<td>

<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
  <td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="{{ route('dashboard.study.contents.delete', [$study->id,$data->content_id])}}" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>



</div>

          
              
          </div>
      </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>


@else

<?php $patient_id = Session::get('user_id');
$get_study_id=DB::table('tip_study_patient')->where('patient_id', $patient_id)->get();?>
@if($get_study_id->count()>0)


<div class="wrapper">
  <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
      <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">
<a href="{{ route('dashboard.study', [$study,'English']) }}" >
{{$study->study_name}}

</a>
</li>
<li class="breadcrumb-item" aria-current="page">
Content
</li>

  </ol>
</nav>

  <!-- Main content -->

    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
    <div class="card">
<div class="card-header"><h3 class="pull-left"> Contents</h3>   </div>
          <div class="card-block">

                                                  
      <div class="progress">
                          <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>



        <div class="box">
          <div class="box-header">
            
          </div>
          <!-- /.box-header -->
          <div class="box-body">
  <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body "  style="overflow-x:auto;">



<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
  <a class="nav-link" id="pills-Calculator-tab" data-toggle="pill" href="#pills-Calculator" role="tab" aria-controls="pills-Calculator" aria-selected="false">Calculator</a>
</li>
<li class="nav-item">
  <a class="nav-link" id="pills-Inclusion-tab" data-toggle="pill" href="#pills-Inclusion" role="tab" aria-controls="pills-Inclusion" aria-selected="false">Inclusion</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillscontact-tab" data-toggle="tab" href="#pills-Exclusion" role="tab" aria-controls="Exclusion" aria-selected="false">Exclusion</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsImplantvideo-tab" data-toggle="tab" href="#pills-Implantvideo" role="tab" aria-controls="Implantvideo" aria-selected="false">Implant video</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsTrainingMaterial-tab" data-toggle="tab" href="#pills-TrainingMaterial" role="tab" aria-controls="TrainingMaterial" aria-selected="false">Training Material</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsStudyDesign-tab" data-toggle="tab" href="#pills-StudyDesign" role="tab" aria-controls="StudyDesign" aria-selected="false">Study Design</a>
</li>
<li class="nav-item">
<a class="nav-link" id="pillsAdverseEvents-tab" data-toggle="tab" href="#pills-AdverseEvents" role="tab" aria-controls="AdverseEvents" aria-selected="false">Adverse Events</a>
</li>
<li class="nav-item">
<a class="nav-link" id="Procedure-tab" data-toggle="tab" href="#pills-Procedure" role="tab" aria-controls="Procedure" aria-selected="false">Procedure</a>
</li>
<li class="nav-item">
<a class="nav-link" id="Notification-tab" data-toggle="tab" href="#pills-Notification" role="tab" aria-controls="Notification" aria-selected="false">Notification</a>
</li>

<li class="nav-item">
<a class="nav-link" id="Other-tab" data-toggle="tab" href="#pills-Other" role="tab" aria-controls="Other" aria-selected="false">Other</a>
</li>



</ul>
<div class="tab-content" id="pills-tabContent">
<div class="tab-pane fade show active" id="pills-Calculator" role="tabpanel" aria-labelledby="pills-Calculator-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===0)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr> 
@endif
@endforeach 
</tbody>
</table>
</div>
<div class="tab-pane fade" id="pills-Inclusion" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===1)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>

<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>



<div class="tab-pane fade" id="pills-Exclusion" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===2)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>



<div class="tab-pane fade" id="pills-Implantvideo" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===3)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>



<div class="tab-pane fade" id="pills-TrainingMaterial" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===4)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>


<div class="tab-pane fade" id="pills-StudyDesign" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===5)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>

<div class="tab-pane fade" id="pills-AdverseEvents" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===6)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>


<div class="tab-pane fade" id="pills-Procedure" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===7)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>


<div class="tab-pane fade" id="pills-Notification" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===8)
@dump($data)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  
<td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>

<div class="tab-pane fade" id="pills-Other" role="tabpanel" aria-labelledby="pills-Inclusion-tab"> <table id="myTable" class="table table-bordered table-striped" width="100%">
              <thead>  <tr><th>Content Header</th> <th> Content Body</th> <th>Content Lang</th><th>Operations</th><th>Actions</th></tr></thead>
              <tbody> @foreach($contents as $data)
@if($data->content_tab_id===9)
<tr><td>{{$revision->version ?? '' }} </td> <td>{{$revision->revision_header ?? $data->content_header}}</td><td>{{ str_limit(strip_tags($revision->revision_des ?? $data->content_body ) , 200) }}</td><td>{{$revision->revision_language ?? $data->content_lang}}</td>
  <td>
<a href="{{route('dashboard.study.addrevision',[$study,$data->content_id])}}" title="Add Revision" class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Revision</a>

<a href="{{route('dashboard.study.viewrevision',[$study,$data->content_id])}}" title="View Revision" class="btn btn-danger btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Revision</a>
</td>
<td>
<a href="{{ route('dashboard.study.Editcontent', [$study,$data->content_id])}}" title="Update Organization" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
<a href="" title="Delete Organization"><i class="fa fa-trash"></i></a>
</td> </tr>
@endif
@endforeach 
</tbody>
</table>
</div>



</div>












          
              
          </div>
      </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>


@endif


@endif
@endsection