@extends('layouts.site')

@section('content')
{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}

@if(auth() && auth()->user()->role_id ==2)

 <meta name="csrf-token" content="{{ csrf_token() }}" />
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!---->
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
	<!--<link rel="stylesheet" href="{{ asset('css/demo.css') }}">-->
	<!--end-->


				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif



     <div class="wrapper">
		<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
{{$study->study_name}}

</a>
</li>

<li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Protocols</a></li>


<li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Content</a></li>

<li class="breadcrumb-item " aria-current="page">

Add Content Calculator
</a>
</li>


</ol>
	</nav>

         
			<div class="card">
            <div class="card-header">
             <h3 class="card-subtitle mb-4 text-muted">Add Content Calculator</h3>                         	
			</div>
                
      @if($errors->any())
      @foreach ($errors->all() as $error)
          <p class="alert alert-danger"> {{$error}} </p>
      @endforeach
      @endif
                               
                 <div class="card-body" style="margin: 20px;padding: 20px;">
				<form class="form-horizontal" method="POST" action="{{route('dashboard.study.contents.calculator' , $study->id)}}" enctype="multipart/form-data">  

                 {{ csrf_field() }}
                    
                 <span id="error"></span>
		
		

                {{-- <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Content Organization</label>



                  <div class="col-sm-10">

                      <select class="form-control " name="sponsor_id"  onchange="val()" id="sponsor_id" required>

                @foreach($organizations as $data)   
                      <option value="{{$data->sponsor_id}}">{{$data->sponsor_name}}</option>
                @endforeach
                </select>

                  </div>

                </div>



                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Content Study</label>



                  <div class="col-sm-10">

                     <select class="form-control " name="study_id" id="study" required>
		    </select>
			
                  </div>

                </div> --}}
                       
                 <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Content tab</label>



                  <div class="col-sm-10">

                      <select class="form-control " name="content_tab_id"  required>

			@foreach($contentstab as $data)
			<option value="{{$data->tab_id}}" >{{$data->tab_title}}</option>
			@endforeach
			</select>

                  </div>

                </div>

		<div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">  Content Header</label>

                  <div class="col-sm-10">
      <input type="text" class="form-control " name="content_header"  required>
			
                  </div>

                </div>

		<div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">  Field Name</label>

                  <div class="col-sm-10">
  			
                      <input type="text" class="form-control " name="content_body"  required>
			
                  </div>

                </div>

               <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;"> Content Language</label>

                  <div class="col-sm-10">

                    <select name="content_lang" class="form-control">
                                        <option value="Afrikaans">Afrikaans</option> 
                                        <option value="Albanian">Albanian</option> 
                                        <option value="Amharic">Amharic</option> 
                                        <option value="Arabic">Arabic</option> 
                                        <option value="Armenian">Armenian</option> 
                                        <option value="Azerbaijani">Azerbaijani</option>
                                        <option value="Bengali">Bengali</option> 
                                        <option value="Bosnian">Bosnian</option>
                                        <option value="Bulgarian">Bulgarian</option> 
                                        <option value="Catalan">Catalan</option>
                                        <option value="Chinese">Chinese</option>
                                        <option value="Czech">Czech</option>
                                        <option value="Danish">Danish</option>
                                        <option value="Dutch">Dutch</option>
                                        <option value="English" selected>English</option>
                                        <option value="Esperanto">Esperanto</option>
                                        <option value="Estonian">Estonian</option>
                                        <option value="Finnish">Finnish</option>
                                        <option value="French">French</option>
                                        <option value="Georgian">Georgian</option>
                                        <option value="German">German</option>
                                        <option value="Scottish Gaelic">Scottish Gaelic</option>
                                        <option value="Greek, Modern">Greek</option>
                                        <option value="Hebrew">Hebrew</option>
                                        <option value="Hindi">Hindi</option>
                                        <option value="Croatian">Croatian</option>
                                        <option value="Hungarian">Hungarian</option>
                                        <option value="Icelandic">Icelandic</option>
                                        <option value="Indonesian">Indonesian</option>
                                        <option value="Italian">Italian</option>
                                        <option value="Japanese">Japanese</option>
                                        <option value="Kinyarwanda">Kinyarwanda</option>
                                        <option value="Korean">Korean</option>
                                        <option value="Latin">Latin</option>
                                        <option value="Latvian">Latvian</option>
                                        <option value="Lithuanian">Lithuanian</option>
                                        <option value="Macedonian">Macedonian</option>
                                        <option value="Malayalam">Malayalam</option>
                                        <option value="Maori">Maori</option>
                                        <option value="Malay">Malay</option>
                                        <option value="Norwegian">Norwegian</option>
                                        <option value="Iranian">Persian, Iranian</option>
                                        <option value="Polish">Polish</option>
                                        <option value="Portuguese">Portuguese</option>
                                        <option value="Pushto">Pushto</option>
                                        <option value="Romanian">Romanian, Rumanian, Moldovan</option>
                                        <option value="Russian">Russian</option>"
                                        <option value="Sanskrit">Sanskrit</option>
                                        <option value="Slovak">Slovak</option>
                                        <option value="Slovenian">Slovenian</option>
                                        <option value="Spanish">Spanish</option>
                                        <option value="Serbian">Serbian</option>
                                        <option value="Swedish">Swedish</option>
                                        <option value="Thai">Thai</option>
                                        <option value="Turkish">Turkish</option>
                                        <option value="Ukrainian">Ukrainian</option>
                                        <option value="Urdu">Urdu</option>
                                        <option value="Vietnamese">Vietnamese</option>
                                        <option value="Welsh">Welsh</option>

                                    </select>
			
                  </div>

                </div>

 			<div class="form-group row">
			<div class="col-sm-12">

				<div class="alert alert-warning">
                                    <strong>Info!</strong> This scheduler will use the information from the Events. Make sure to fill out all the events.
                                </div>
 				</div>
 				</div>

     			<div class="form-group row">
                               
                              <label for="name" class="col-sm-2 pull-left"> Active</label>
 
                                     <div class="col-sm-10">
                                      <input type="checkbox" name="active" value="1"> 
			</div> 
		 </div>
				
		                           

                         
                 			

              </div>  <!-- /.card-body -->

              <div class="card-footer">   
								 
                <button type="submit" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px" onclick="return passvalidate()">Create</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

            </div>

          </div>


  <script type="text/javascript">
   function val() {
   sponsor_id = document.getElementById("sponsor_id").value;
	var _token = $("input[name='_token']").val();
           $.ajax({
            url: "/dashboard/getstudies/"+sponsor_id,
	     success: function(data) {
        if(data != 0){
          //$('#study').html(data.html);	not found 
          $('#study').empty();
          for(i = 0 ; i <= data.length ; i++ ){
            var name = data[i].study_name;
            var id = data[i].id;
            console.log(name, id);
            $('#study').append("<option value="+id+">"+name+"</option>");
          }
          
            }else{
            $('#study').empty();
            $('#study').append("<option value=''>Sorry No Study Found Select Some Other Organization</option>");
  
      }
            }

      });
}

</script>

@endif
@endsection
