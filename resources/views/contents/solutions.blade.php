

@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}

@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!---->
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
	<!--<link rel="stylesheet" href="{{ asset('css/demo.css') }}">-->
	<!--end-->


				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif



     <div class="wrapper">
		<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
{{$study->study_name}}

</a>
</li>

<li class="breadcrumb-item " aria-current="page"><a href="{{ route('dashboard.study.contents',$study) }}" >Protocols</a></li>


<li class="breadcrumb-item " aria-current="page">

<a href="{{ route('dashboard.study.contents',$study) }}" >
Content
</a>
</li>
<li class="breadcrumb-item " aria-current="page">Study Solution</a></li>


</ol>
	</nav>

         
			<div class="card">
            <div class="card-header">
             <h3 class="card-subtitle mb-4 text-muted">Add Solution</h3>                         	
			</div>
                
      @if($errors->any())
      @foreach ($errors->all() as $error)
          <p class="alert alert-danger"> {{$error}} </p>
      @endforeach
      @endif    
                               
                 <div class="card-body" style="margin: 20px;padding: 20px;">
				<form class="form-horizontal" method="POST" action="{{route('dashboard.study.contents.solutions' , $study->id)}}" enctype="multipart/form-data">
 

                 {{ csrf_field() }}
                    
                		
		

                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">System Name</label>



                  <div class="col-sm-10">

                       <input type="text" class="form-control " name="name"  required>


                  </div>

                </div>
				        <div class="form-group row">
                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Type</label>
                  <div class="col-sm-10">
                     <select class="form-control">
					          <option value="" selected> Select Any Option </option>
					          {{-- <option value="volvo"> </option> --}}
                    <option value="volvo">EDC</option>
                    <option value="saab">CTMS</option>
                    <option value="mercedes">eTMF</option>
                  </select>
                  </div>
                </div>


                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">API Link</label>



                  <div class="col-sm-10">

                     <input type="text" class="form-control " name="api_link"  required>
			
                  </div>

                </div>
                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Username</label>



                  <div class="col-sm-10">

                     <input type="text" class="form-control " name="username"  required>
			
                  </div>

                </div>
                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Password</label>



                  <div class="col-sm-10">

                     <input type="text" class="form-control " name="password"  required>
			
                  </div>

                </div>
                       
                 <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Select Study</label>



                  <div class="col-sm-10">

                      <select class="form-control " name="study_id"  required>
				
			@foreach($studies as $data)
			<option value="{{$data->id}}">{{$data->study_name}}</option>
			@endforeach
			</select>

                  </div>

                </div>

		
		
                              			

              </div>  <!-- /.card-body -->

              <div class="card-footer">   
								 
                <button type="submit" name="add_new" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px" onclick="return passvalidate()">Create</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

            </div>

          </div>


   


@endif
@endsection
