@extends('layouts.site')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id ) }}">{{$study->study_name}}</a></li>

        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.messages.investigators' , $study->id ) }}"> Patients </a></li>

        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.patient.messages' , [$study->id , $patient->id]) }}"> {{$patient->user->name}} Messages </a></li>

        <li class="breadcrumb-item active"> Message Details </li>        
    </ol>
</nav>
    

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($msg = Session::get('error'))
<script>
swal("Sorry!", "{{$msg }}", "danger");
</script>
@endif
@if ($msg = Session::get('success'))
<script>
swal("Done!", "{{$msg }}", "success");
</script>
@endif


<div class="bg-white" style="padding:20px">

    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">{{$message->title}}
        </div>
    <p style="padding:10px">
        {{ strip_tags($message->body) }}
    </p>
    <br>
    @if(isset($message->attachment))
    @php
        list($name , $extension) = explode( '.' , $message->attachment);
    @endphp

    @if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif' || $extension == 'jfif' )
    <img src="/messages/{{$message->attachment}}" style="max-width:500px" >
    @else
    <p class="alert alert-warning">The Attachment is a file,
    <a href="{{route('dashboard.download.file' ,[ 'messages' , $message->attachment] )}}">Click Here</a>
    to download</p>
    @endif
    
    @else
    <p class="alert alert-warning">No Attachment found</p>
    @endif
    
    
  


</div>
<hr>
@if($message->replies->where('investigator_id', auth()->user()->sponsor_user->id )->count() > 0 )
<div class="row" style="padding:30px; ">

@foreach($message->replies->where('investigator_id', auth()->user()->sponsor_user->id ) as $reply)
<div class="col-md-10 offset-2 " style="border-radius:5px; padding:20px; margin-top:30px; background-color: #d9edf7; overflow-wrap: break-word; ">

    <div class="arrow-right" ></div>

<h4> {{$reply->title}} </h4><br>

{!! $reply->body !!}
<span class="small float-right"> Date: <strong> {{$reply->created_at->toDateString()}} </strong> </span>
</div>
@endforeach
</div>
@endif    

<button class="btn btn-success mt-2" onclick="openReplyForm()" ><i class="fa fa-reply"></i> Reply Now</button>

</div>



<div style=" padding:20px; margin-top:30px; background-color: lightgreen; display:none;" id="replay_area" >
    <form action="{{route('dashboard.study.patient.message.reply' , [$study->id , $patient->id , $message->id ])}}" method="post">
        @csrf
    <div class="form-group">
        <label class="col-form-label">Title</label>
        <input class="form-control" name="title" placeholder="title" />
    </div>
    <div class="form-group">
        <label class="col-form-label">Body</label>
        <textarea id="summernote" name="body" ></textarea>
    </div>
    <button class="btn btn-primary" type="submit"><i class="fa fa-envelope"></i> Send</button>
</form>
</div>

    <script>
        function openReplyForm(){
        $('#replay_area').toggle();}
    </script>



@endsection