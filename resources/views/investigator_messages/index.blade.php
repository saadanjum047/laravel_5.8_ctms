@extends('layouts.site')

@section('content')



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id ) }}">{{$study->study_name}}</a></li>

        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.patients.messages' , $study->id ) }}">Patient Messages</a></li>


        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.patients.messages' , $study->id ) }}"> Patients </a></li>

        <li class="breadcrumb-item active"> {{$patient->user->name}} Messages </li>
    </ol>
</nav>
    

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<div class="bg-white" style="padding:20px">

    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Patient Messages
        </div>

    <table class="table table-hover">
    <tr>
        <th>#</th>
        <th>Title</th>
        <th width="50%" >Attachment</th>
        <th>Created</th>
    </tr>
@php
    $i = 0;
@endphp
   
    @foreach ($patient->messages as $msg)
    <tr>
        <td> {{$i+=1}} </td>
        <td>
            <a href="{{route('dashboard.study.patient.message.show' , [ $study->id , $patient->id , $msg->id] )}} "> {{$msg->title}} </a>
             </td>
        <td> {{$msg->attachment}} </td>
        <td> {{($msg->created_at)->toDateString()}} </td>
    </tr>
    @endforeach

</table>

</div>
</div>
    
@endsection