@extends('layouts.site')

@section('content')



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id ) }}">{{$study->study_name}}</a></li>

        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.patients.messages' , $study->id ) }}">Patient Messages</a></li>

        <li class="breadcrumb-item active"> Patients </li>
    </ol>
</nav>
    

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<div class="bg-white" style="padding:20px">

    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Patients Messages
        </div>

    <table class="table table-hover">
    <tr>
        <th>#</th>
        <th>Patient Name</th>
        <th>Total Messages</th>
    </tr>

    
    @foreach ($patients as $k => $patient)
    <tr>
        <td> {{$k+1}} </td>
        <td>
        <a href="{{route('dashboard.study.patient.messages' , [ $study->id , $patient->id] )}}">{{$patient->user->name}}</a>
        </td>
        <td> {{$patient->messages->where('patient_id' , $patient->id )->count() }} </td>
    </tr>
    @endforeach
    

</table>

</div>
</div>
    
@endsection