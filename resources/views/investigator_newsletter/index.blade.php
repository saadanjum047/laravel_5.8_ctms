@extends('layouts.site')
@section('styles')

  <style>
  table {border-collapse:collapse; table-layout:fixed; width:310px;}
      </style>
      @endsection
@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item" aria-current="page">
  Details
  </li>
  
     </ol>
  </nav>

<div class="bg-white" style="padding:20px">
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Newsletters
        </div>

<table class="table">
    <tr>
        <th width="10%" >#</th>
        <th width="30%"> Title </th>
        <th width="50%"> Description </th>
        <th width="10%"> Created </th>
    </tr>

    
    @foreach ($newsletters as $k => $news)
    @if($news->publish_date <= Carbon\Carbon::now()->toDateString())
    @php
    $curr_news = auth()->user()->newsletters->find($news->id);
    @endphp
    @if(!is_null($curr_news))
        @if($curr_news->pivot->status == 0)
    <tr style="background-color: #f1f4f8">
        @elseif($curr_news->pivot->status == 1)
    <tr>
        @endif
        @endif
        <td>{{$k+1}} </td>
        <td width="20%">  <a href="{{route('dashboard.study.newsletters.view' , [$study->id , $news->id ] )}}">  {{$news->title}}  
            
            @if(!is_null($curr_news))
            @if($curr_news->pivot->status == 0)
            <span class="badge badge-pill badge-danger">New</span>
            @endif
            @endif
             </a> </td>
        <td style="word-wrap:break-word;">  {{ str_limit( strip_tags($news->body) , 200) }}  </td>
        <td>{{($news->created_at)->toDateString()}} </td>
        <td>
    </tr>
    @endif
    @endforeach

</table>

</div>
</div>


@endsection