
@extends('layouts.site')

@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
     
   
      <li class="breadcrumb-item" aria-current="page">Patient Images</li>
     </ol>
 </nav>
 
<div class="row mb-2">

    <div class="col-lg-6">
        <div class="card" style="margin-top: 50px;">
            <div class="card-block add_note">
                <div class="row">
                    <div class="col-md-5">
                        <h5 class="card-title mb-4">Add Patients Images</h5>
                    </div>
                </div>
                <div class="table-responsive" id="patients-notes">
                    <form action="{{ route('dashboard.patient.images.store') }}" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Select Patient</label>
                            <div class="col-sm-10">
                                <select name="patient_id" id="select_patients" class="form-control" required>
                                    <option value="" selected>Select Any patient</option>
                                    @foreach($patients as $patient))
                                    <option value="{{$patient->id}}">{{$patient->user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                           <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Avatar</label>
                            <div class="col-sm-10">
                                <input type="file" name="patient_image" class="form-control" placeholder="Patient Image" required>
                            </div>
                        </div>

                        @if($errors->any())
                            <div class="alert alert-danger">
                                {{ $errors->all()[0] }}
                            </div>
                        @endif

                        @if(session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-sm-10">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary">Add image</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection