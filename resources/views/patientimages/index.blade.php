
@extends('layouts.site')

@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
     
   
      <li class="breadcrumb-item" aria-current="page">Patient Images</li>
     </ol>
 </nav>
 
<div class="row mb-2">
 {{-- @if((int)Cookie::get('user_role')===2) --}}

 @if(auth()->user()->role_id == 2)
    <div class="col-lg-6">
        <div class="card" style="margin-top: 50px;">
            <div class="card-block">
               
                <div class="row">
                    <div class="col-md-5">
                        <h5 class="card-title mb-4">Patients Images</h5>
                    </div>
                  </div>
                <div class="table-responsive" id="patients-notes">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            <th>Patient Id</th>
                            <th>Patient Image</th>
 				<th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            {{--  @dd($patients)  --}}
                        @foreach($patients as $patient)   
                        @if($patient->images != null ) 
                        @foreach($patient->images as $image)
                            <tr class="">
 				                <td>{{ $image->patient_id}}</td>
                                <td><img class="patient_note_image" src="{{url('/')}}/pat_images/{{$image->patient_image}}" alt=""></td>
                                <td></td>
                            </tr>
                    @endforeach
                    @endif
                    @endforeach
                           </tbody>
                    </table>
                </div>
                </div>
        </div>
    </div>
@else
    <div class="col-lg-6">
        <div class="card" style="margin-top: 50px;">
            <div class="card-block add_note">
                <div class="row">
                    <div class="col-md-5">
                        <h5 class="card-title mb-4">Add Patients Images</h5>
                    </div>
                </div>
                <div class="table-responsive" id="patients-notes">
                    <form action="{{ route('dashboard.patient.images.store') }}" method="post" enctype="multipart/form-data">
                        {{-- <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Select Patient</label>
                            <div class="col-sm-10">
                                <select name="patient_id" id="select_patients" class="form-control" required>
                                    <option value="" selected>Select Any patient</option>
                                    @foreach($patients as $patient))
                                    <option value="{{$patient->id}}">{{$patient->user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}
                           <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Avatar</label>
                            <div class="col-sm-10">
                                <input type="file" name="patient_image" class="form-control" placeholder="Patient Image" required>
                            </div>
                        </div>

                        @if($errors->any())
                            <div class="alert alert-danger">
                                {{ $errors->all()[0] }}
                            </div>
                        @endif

                        @if(session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-sm-10">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary">Add image</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
</div>
@endsection