@extends('layouts.site')

@section('content')
{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}

@if(auth() && auth()->user()->role_id ==2)


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('success'))
<script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

		<a href="{{ route('dashboard.study.view', [$study->id ,'English']) }}" >
		{{$study->study_name}}
		
		</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">

            <a href="{{ route('dashboard.study.sites', [$study->id ,'English']) }}" >Sites
        </a>
        </li>

     		<li class="breadcrumb-item active" aria-current="page">

		
		{{$site->site_name}}
		
		
		</li>

 <li class="breadcrumb-item " aria-current="page">
	Users
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left"> View Users</h3> <a href="{{ route('dashboard.study.sites.user.create', [$study ,$site->site_id] ) }}" class="btn btn-primary pull-right">Create User</a> 
  <a href="{{ route('dashboard.study.site.tasks', [$study->id ,$site->site_id] ) }}" class="btn btn-success pull-right mr-2">View Tasks</a> 
 </div>
            <div class="card-block">

                                                    
				<div class="progress">
                <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>  


          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
            <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th> Name</th>
                  <th> Email</th>
                  <th> Role</th>
                  <th> Address</th>
                  <th> Phone</th>
                  <th> Country</th>
                  <th> Zip</th>
		              <th>Status</th>

                  <th>Actions</th>
                                  
                </tr>
                </thead>
                <tbody>
                @if(isset($users))
              @foreach($users as $user)
              <tr>
                {{-- {{dump($user->user)}} --}}
                <td>{{$user->user->name ?? $user->fullname}}</td>
                <td>{{$user->user->email ?? $user->email}}</td>
                <td>{{$user->user->study_user->where('study_id' , $study->id)->first()->role_detail->name ?? $user->email}}</td>
                <td>{{$user->address1}}</td>
                <td>{{$user->phone}}</td>
                <td>{{$user->country}}</td>
                <td>{{$user->zip}}</td>
                @if($user->is_activate==1)
                <td><a href="{{route('dashboard.change_user_status', $user->id)}}"   style="margin:1px;color:green" title="change status">Active</a></td>
                @else
                <td><a href="{{route('dashboard.change_user_status' , $user->id )}}"   style="margin:1px;color:red" title="change status">Inactive</a></td>

                @endif
                  <td>
      <a href="{{route('dashboard.study.sites.user.edit',[ $study->id , $site->site_id , $user ])}}"   style="margin:1px;" title="Update user"><i class="fa fa-edit fa-lg"></i></a>
      
      <a href="{{route('dashboard.study.user.delete',[ $study->id , $user->id ])}}"   style="margin:1px;" class="text-danger" title="Delete User"><i class="fa fa-trash fa-lg"></i></a>

		</td>
		                
              </tr>

                 @endforeach       
                 @endif            
                </tbody>
               </table>
               
            </div>
	     </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

@endif
@endsection