

@extends('layouts.site')

@section('content')
{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}
@if(auth() && auth()->user()->role_id ==2)


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success").then(function() {
    window.location = "{{ route('dashboard.site.ViewSites', $study) }}";
});

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
       {{$study->study_name}}
      </a>
    </li>
    <li class="breadcrumb-item"><a href="{{ route('dashboard.study.sites' , $study->id)}}">Sites</a></li>

<li class="breadcrumb-item " aria-current="page">
Edit Site
</li>
</ol>
</nav>



     <div class="wrapper">
        <div class="card">
            <div class="card-header">

                     
                    <div class="col-md-5">
                        <h5 class="card-title mb-4"> Update Site</h5>
                    </div>
               
                </div>
               
				
              <div class="card-body" style="margin: 20px;padding: 20px;">

                @if($errors->any())
                @foreach ($errors->all() as $error)
                    <p class="alert alert-danger"> {{$error}} </p>
                @endforeach
                @endif
                


			  <form class="form-horizontal" action="{{route('dashboard.study.sites.update' , [ $study->id , $site->site_id  ])}}"  method="POST">
		       {{ csrf_field() }}
                  <!--<div class="form-group row" style="display:none">
                  <label for="name" class="col-sm-2 control-label">Organization</label>

                  <div class="col-sm-4 ">
                      <select class="form-control" name="user_id" required="required">   
                          <option selected>Select a Organization</option>
 				<option value="193">Organization1</option>
 				

                                   
                      </select>  
                          
                          
                  </div>
                </div><!-- /.end-row -->
				
	
		 {{--  <input type="text" name="sponser_id" class="form-control" value="{{$response->data[0]->uuid}}" style="display:none">  --}}

                <div class="form-group row">  
                    
                  <label for="name" class="col-sm-2 control-label" style="font-weight: bold;">Name </label>

                 <div class="col-sm-10">
                    <input type="site_name" name="site_name" class="form-control" value="{{$site->site_name}}" placeholder="Name" required>
                </div>
               </div><!-- /.end-row -->
                 <div class="form-group row">
                    
                  <label for="name" class="col-sm-2 control-label" style="font-weight: bold;">Site Number </label>

                 <div class="col-sm-10">
                    <input type="site_nbr" name="site_nbr" class="form-control" value="{{$site->site_nbr}}" placeholder="Site Number" required>
                </div>
               </div><!-- /.end-row -->
                    <div class="form-group row">
                  <label for="address" class="col-sm-2 control-label"  style="font-weight: bold;">Address</label>

                 <div class="col-sm-10">
                    <input type="address1" name= "address1" class="form-control" value="{{$site->address1}}" placeholder="Address" required>
                  </div>
                    </div><!-- /.end-row -->
                          <div class="form-group row" >
                  <label for="address2" class="col-sm-2 control-label" style="font-weight: bold;">Address2</label>

                 <div class="col-sm-10">
                    <input type="address2" name= "address2" class="form-control" value="{{$site->address2}}" placeholder="Address2">
                  </div>
                   </div><!-- /.end-row -->
             
                  
                         <div class="form-group row">
                  <label for="city" class="col-sm-2 control-label" style="font-weight: bold;">City</label>

                  <div class="col-sm-10">
                    <input type="city" name="city" class="form-control" value="{{$site->city}}" placeholder="City" required>
                  </div>
                 </div><!-- /.end-row -->
						 
					<div class="form-group row">
                  <label for="state" class="col-sm-2 control-label" style="font-weight: bold;">State</label>

                 <div class="col-sm-10">
                    <input type="text" name="state" class="form-control" value="{{$site->state}}" placeholder="State" required>
                  </div>
                 </div><!-- /.end-row -->
                  
                   <div class="form-group row">
                  <label for="country" class="col-sm-2 control-label" style="font-weight: bold;">Country</label>

                 <div class="col-sm-10">
                    <input type="country" name="country" class="form-control" value="{{$site->country}}" placeholder="Country" required>
                  </div>
                  </div><!-- /.end-row -->
				  
				
				        <div class="form-group row">
                  <label for="country" class="col-sm-2 control-label" style="font-weight: bold;">Planned Patients</label>

                 <div class="col-sm-10">
                    <input type="number" name="planned_patients" class="form-control" value="{{$site->site->planned_patients}}" placeholder="planned patients" required>
		                 <input type="number" name="study_site_id" class="form-control" value="{{$site->site->study_site_id}}" style="display:none">

                  </div>
                  </div><!-- /.end-row -->
                  
                   <div class="form-group row">
                  <label for="name" class="col-sm-2 control-label" style="font-weight: bold;">Patient Range </label>

                  <div class="col-sm-4">
                    <input type="pat_lo" name="pat_lo" class="form-control" value="{{$site->pat_lo}}" placeholder="Low Range" required>
                </div>
                <div class="col-sm-4">
                    <input type="pat_hi" name="pat_hi" class="form-control" value="{{$site->pat_hi}}" placeholder="Hi Range" required>
                </div>
                </div><!-- /.end-row -->
            
                
				 
                </div>        <!-- /.card-body -->
				   
              <div class="card-footer">
                
                <button type="submit" class="btn btn-info  pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px">Update</button>
                <input type="password" name="site_id" class="form-control" value="{{$site->site_id}}" style="display:none">
              </div>
              </form> 
              <!-- /.card-footer -->
             
          </div>
      </div>
	  
	  
	  
	  

@endif
@endsection









