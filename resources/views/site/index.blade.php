@extends('layouts.site')

@section('content')
{{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)  --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('success'))
<script>
swal("Good job!", "{{$message }}", "success").then(function() {
});

</script>
@endif
@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif




 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
<?php 
//	echo "testing...";
//dd($response);
//die();
?>	
{{$study->study_name}}

</a>
</li>

 <li class="breadcrumb-item " aria-current="page">
	Sites
</li>
		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left"> View Sites</h3> <a href="{{ route('dashboard.study.sites.create', $study->id ) }}" class="btn btn-primary pull-right">Create Site</a>  </div>
            <div class="card-block">

                                                    
				<div class="progress">
        <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
        </div>



          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
            <table id="tablesite" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th> Name</th>
                            <th> Address</th>
                            <th> Country</th>
                            <th> Actions</th>
                            </tr>
                    </thead>
                    <tbody>
                  @foreach($study->sites as $site)
                  <tr>
                    <td width="50%" > {{$site->site->site_name}}</td>
                    <td> {{$site->site->address1}}</td>
                    <td> {{$site->site->country}}</td>
                    <td>  
                      <a href="{{ route('dashboard.study.sites.user.create',[$study->id,$site->site->site_id]) }}"   class="btn btn-sm btn-xs btn-primary" style="margin:1px;" title="Add New User">
                      <i class="fa fa-plus" style="margin:3px;"></i><i class="fa fa-user" style="margin:3px;"></i></a> 
                      <a href="{{ route('dashboard.study.sites.users',[$study->id ,$site->site->site_id]) }}"   class="btn btn-sm btn-xs btn-secondary" style="margin:1px;" title="View Site Users">
                      <i class="fa fa-users" style="margin:3px;"></i></a> 
                      <a href="{{ route('dashboard.study.sites.edit',[ $study->id , $site->site->site_id] ) }}"  style="margin:1px;" title="Update Site"><i class="fa fa-edit"></i></a>

                    </td>
                    </tr>

					@endforeach
                                        
                                    </tbody>
                                </table>
               
            		</div>
		</div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

@endif
@endsection