@extends('layouts.site')


@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{ $message }}", "success");
</script>
@endif

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item" aria-current="page">
Diaries
</li>

   </ol>
</nav>

<div class="bg-white">
    <div class="card">
    <div class="card-header bg-white">
            
        <h3 class="card-title">Diaries
            <button style="float:right" class="btn btn-success" data-toggle="modal" data-target="#create_model"><i class="fa fa-plus"></i> Add Diary</button></h3>
            @if($errors->any())
            @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @endif
         
      </div>
      <table class="table table-hover">
    <tr>
        <th>#</th>
        <th>Diary Name</th>
        <th>Study</th>
        <th>frequency</th>
        <th>End Duration</th>
        <th>Actions</th>
    </tr>
    @foreach ($diaries as $i => $diary)
    {{--  {{dump($diary->study)}}  --}}
    <tr>
        <td>{{$i+1}} </td>
        <td><a href="{{route('dashboard.diary_questions' , $diary->id )}}"> {{$diary->diary_name}} </a> </td>
        <td> {{ $diary->study->study_name }} </td>
        <td> {{$diary->end_duration}} </td>
        <td> {{$diary->frequency}} </td>
        <td> 
            {{--  href="{{ route('diaries.remove' , $diary) }}"  --}}
            <a class="text-danger fa fa-trash fa-lg" href="{{ route('dashboard.diaries.remove' , $diary)}}" ></a> 
            {{-- |<a class="text-danger fa fa-trash fa-lg" href="{{URL('dashboard/diaries/remove', $diary)}} " ></a> | --}}
            <a class="text-primary fa fa-edit fa-lg"  onclick="update_model_data({{$diary}} ,{{$diary->study}})"> </a> </td>
    </tr>

    @endforeach

</table>
</div>

</div>

<div class="modal fade" id="create_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Diary</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.diaries')}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <label for="diary-name" class="col-form-label">Diary Name:</label>
              <input type="text" class="form-control" id="diary-name" name="diary_name">
            </div>
            <div class="form-group">
                <label for="diary-frequency" class="col-form-label">Frequency:</label>
                <input type="number" class="form-control" id="diary-frequency" name="diary_frequency">
              </div>
              <div class="form-group">
                <label for="end_duration" class="col-form-label">End Duration:</label>
                <input type="number" class="form-control" id="end_duration" name="end_duration">
              </div>
              <div class="form-group">
              <label for="study_name" class="col-form-label">Studies:</label>
            <select class="form-control" id="study_name" name="study_name" >
                <option value="" index="-1">Select Study</option>
                @foreach($studies as $study)
                <option value="{{$study->id}} "> {{$study->study_name}} </option>
                @endforeach
            </select>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Create</button>
        </div>
        </form>

      </div>
    </div>
  </div>



  <div class="modal fade" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Diary</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.diaries.update' )}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <input type="hidden" value="" id="diary_id" name="diary_id" />
              <label for="diary-name" class="col-form-label">Diary Name:</label>
              <input type="text" class="form-control" id="diary-name1" name="diary_name">
            </div>
            <div class="form-group">
                <label for="diary-frequency" class="col-form-label">Frequency:</label>
                <input type="number" class="form-control" id="diary-frequency1" name="diary_frequency">
              </div>
              <div class="form-group">
                <label for="end_duration" class="col-form-label">End Duration:</label>
                <input type="number" class="form-control" id="end_duration1" name="end_duration">
              </div>
              <div class="form-group">
              <label for="study_name" class="col-form-label">Studies:</label>
            <select class="form-control" id="study_name1" name="study_name" >
                @foreach($studies as $study)
                <option value="{{$study->id}}"> {{$study->study_name}} </option>
                @endforeach
            </select>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>

      </div>
    </div>
  </div>

@endsection

@section('scripts')
<script>
    function update_model_data(diary , study){
        console.log(diary , study);
        $('#update_model').modal('show');
        $("#diary-name1").val(diary.diary_name);
        $("#diary_id").val(diary.id);
        $("#diary-frequency1").val(diary.frequency);
        $("#end_duration1").val(diary.end_duration);
        var options = study_name1.options; 
        for (var i = 0; i <= options.length; i++) {
            var option = options[i]; 
            if( option.value == study.id ){
              var selected = option.index;
              document.getElementById("study_name1").selectedIndex = selected;
            }
        }
    }
</script>
@endsection