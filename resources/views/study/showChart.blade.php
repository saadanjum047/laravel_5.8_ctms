

@extends('layouts.site')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item">{{ $study->study_name }}</li>
        <li class="breadcrumb-item active" aria-current="page">Study Flow</li>
    </ol>
</nav>

<div class="row mt-5">

    <iframe src="http://admin.studypal.mobi/study/getChart.php?study_id={{ $study->id }}" frameborder="0" width="100%" height="600" scrolling="no"></iframe>

</div>
<style>
    iframe{
        overflow:hidden;
    }
</style>

@endsection