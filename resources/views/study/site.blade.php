@extends('layouts.site')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Home</a></li>
        <li class="breadcrumb-item"><a
                    href="{{ route('dashboard.study.view', ['id'=>$study->id, 'language'=>'English']) }}">{{ $study->study_name }}
                </a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">{{ $site->site_name }}</li>
    </ol>
</nav>

<div class="row mt-5">
    <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 mb-4">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title font-weight-normal text-warning">{{ $patient_count->count() }}</h4>
                <p class="card-text">Patients Enrolled</p>
                {{--  <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-warning" role="progressbar"
                         style="width: {{ @$patient_count }}%" aria-valuenow="{{ @$patient_count }}" aria-valuemin="0"
                         aria-valuemax="100">{{ $patient_count }} %
                    </div>
                </div>  --}}
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 mb-4">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title font-weight-normal text-danger">
                    <a href="{{ route('dashboard.incompleteDiary', [$study->id, $site->site_id]) }}"
                       class="text-danger">{{@ number_format($information->patients_incomplete_diary, 0) }}</a>
                </h4>
                <p class="card-text">Compliance</p>
                {{--  <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-danger" role="progressbar"
                         style="width: 0%"
                         aria-valuenow="{{ 100*$incomplete_diary/($incomplete_diary != 0 ? $incomplete_diary : 1) }}"
                         aria-valuemin="0"
                         aria-valuemax="100">0%
                    </div>
                </div>  --}}
            </div>
        </div>
    </div>
</div>

<div class="row mb-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-4">
                        <h5 class="card-title">Site: {{ $site->site_name }}({{ $site->site_nbr }})</h5>
                    </div>
                    <div class="col-md-4 offset-md-8">
                        {{--  <a href="{{ route('dashboard.patient.export-patients-list', ['site_id'=>$site->site_id,'study_id'=>$study->id]) }}"
                           class="btn btn-primary pull-right"><i class="fa fa-file-excel-o"></i> EXPORT</a>
                             --}}
                           <a href="{{ route('dashboard.export.sitepatients' , $site->site_id ) }}"
                           class="btn btn-primary pull-right"><i class="fa fa-file-excel-o"></i> EXPORT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="card-title mb-4">Site Patients</h5>
                    </div>
                </div>

                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>Patient ID</th>
                        <th>Visit Date</th>
			<th>Exit Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($patients as $patient)
                        <tr>
                            <td>
                                {{-- <a href="{{ route('dashboard.study.patient.show', [ 'id'=>$patient->id,'site'=>$site->site_id]) }}"> --}}
                                <a href="{{ route('dashboard.study.patient.show', [ $study->id , $patient->id, $site->site_id]) }}">
                                    @if(isset($patient->for_current_investiagator) && $patient->for_current_investiagator==1)
                                        <strong>{{ $patient->info->patient_nbr }} </strong>
                                    @else
                                        {{ $patient->patient_nbr }}
                                    @endif
                                </a>
                            </td>
                            <td>
                                @if((int)$patient->type===2)
                                    <span class="badge badge-success">
                                @elseif((int)$patient->type===1)
                                     <span class="badge badge-danger">
                                @elseif((int)$patient->type===0)
                                     <span class="badge badge-primary">
                                @endif
                                     {{ substr($patient->updated_at, 0, -3) }}
                                </span>
                            </td>
			<td><span class="badge {{ $patient->exit_date !=NULL? 'badge-danger':'badge-success' }}">{{ $patient->exit_date}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if(count($patients) == 0)
                    Patients Not Found!
                @endif
            </div>
        </div>
    </div>
</div>

@endsection