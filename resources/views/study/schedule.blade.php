@extends('layouts.site')

@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , [ $study->id , 'Endlish' ])}}">{{$study->study_name}}</a></li>
   <li class="breadcrumb-item" aria-current="page"> Schedules </li>

   </ol>
</nav>


<div class="bg-white" style="padding:10px">
@if($errors->any())
@foreach ($errors->all() as $error)
 <p class="alert alert-danger"> {{$error}} </p>   
@endforeach
@endif

<form action="{{route('dashboard.study_schedule' , $study->id)}}" method="POST">
 {{csrf_field()}}
    <div class="card">
        <div class="card-header bg-white">
            <h3 class="card-title">Study Schedules</h3>
          </div>

    <table class="table table-hover table-bordered" id="dynamic_field">
        <tr>
            <th> Visit Name </th>
            <th> Window Low </th>
            <th> Target </th>
            <th> Window High</th>
            <th></th>
        </tr>
        <tfoot id="fields" >
            {{--  @if($study->schedules->count() == 0)
            <tr id="row1" class="dynamic-added">
                <td ><input type="text" name="visit_name[]" placeholder="Enter Visit Name" required class="form-control name_list" /></td>  
                <td><input type="text" name="low_target[]" placeholder="Enter Low Target" class="form-control name_list" required /></td>  
                <td><input type="text" name="target[]" placeholder="Enter Visit Target" class="form-control name_list"  required/></td>  
                <td><input type="text" name="high_target[]" placeholder="Enter High Target" class="form-control name_list" required /></td>  
                <td><button type="button" name="remove" id="1" class="btn btn-danger btn_remove" onclick="delete_fields(this.id)"  >X</button></td>  --}}
                    
                    {{--  <button type="button" name="add" id="add" onclick="add_extra_fields()" class="btn btn-success">Add More</button>  --}}
             
             
            {{--  </tr>
            @endif  --}}
            @if($study->schedules->count() > 0)
            @foreach($study->schedules as $k => $schedule)
            @php
                $count = $k;
            @endphp
            <tr id="row{{$k}}" class="dynamic-added">
                <input type="hidden" value="{{$schedule->id}}" name="schedule_id[]" > 
                <td><input type="text"  name="visit_name[]" placeholder="Enter Visit Name" class="form-control name_list" value="{{$schedule->visit_name}}" required /></td>
                <td><input type="text" name="low_target[]"  placeholder="Enter Low Target" class="form-control name_list" value="{{$schedule->window_low}}" required /></td>
                <td><input type="text" name="target[]"  placeholder="Enter Visit Target" class="form-control name_list" value="{{$schedule->target}}" required/></td>
                <td><input type="text"  name="high_target[]" placeholder="Enter High Target" class="form-control name_list" value="{{$schedule->window_high}}" required /></td>
                {{--  @if($k == 0)
                <td><button type="button" name="add" id="add" onclick="add_extra_fields()" class="btn btn-success">Add More</button></td>
                @else  --}}
                <td><button type="button" name="remove" id="{{$k}}" class="btn btn-danger btn_remove" onclick="delete_fields(this.id)" >X</button></td>
                {{--  @endif  --}}
            </tr>

                @endforeach
            @endif
        </tfoot>
            {{--  @endforeach  --}}
    </table>
</div>


@if(auth()->user()->role_id == 3)    
    @php
        
    if( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->count() > 0 ){
        $array = auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'create')->where('module_id' , 12 )->first();
        if(!is_null($array)){
            @endphp
            <button type="button" name="add" id="add" onclick="add_extra_fields()" class=" btn btn-success" style="margin-top:20px">Add Extra Row</button>
            <button class="btn btn-primary" style="margin-top:20px" >Save</button>
            @php

                }
    }
    @endphp    
   
@elseif(auth()->user()->role_id == 2)

<button type="button" name="add" id="add" onclick="add_extra_fields()" class=" btn btn-success" style="margin-top:20px">Add Extra Row</button>
<button class="btn btn-primary" style="margin-top:20px" >Save</button>

@endif

</form>

</div>




<script>
  /*  $(document).ready(function(){      
    var = 1;
    $('#add').click(function(){  
        console.log('call');
        i++;  
        $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="visit_name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input type="text" name="low_target[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input type="text" name="target[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input type="text" name="high_target[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
   });  
} */

    var j = @if(isset($count)) {{ $count+1 }} @else 1 @endif ;
    console.log(j);

    function add_extra_fields(){
        console.log(j);
        $('#fields').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="visit_name[]" placeholder="Enter Visit Name" required class="form-control name_list" /></td><td><input type="text" name="low_target[]" required placeholder="Enter Low Target " class="form-control name_list" /></td><td><input type="text" name="target[]" required placeholder="Enter Visit Target" class="form-control name_list" /></td><td><input type="text" required name="high_target[]" placeholder="Enter High Target" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove" onclick="delete_fields(this.id)" >X</button></td></tr>'); 
        j++; 
    }

    function delete_fields(id){

        console.log(id);
        $('#row'+id).remove();
        /*
        if($('#fields .dynamic-added').length == 0 ){
            $('#fields').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="visit_name[]" placeholder="Enter Visit Name" class="form-control name_list" /></td><td><input type="text" name="low_target[]" placeholder="Enter Low Target" class="form-control name_list" /></td><td><input type="text" name="target[]" placeholder="Enter Visit Target" class="form-control name_list" /></td><td><input type="text" name="high_target[]" placeholder="Enter High Target" class="form-control name_list" /></td><td><button type="button" name="add" id="add" onclick="add_extra_fields()" class="btn btn-success">Add More</button></td></tr>'); 
            i++; 
        
        }
        */
    }

  </script>

@endsection

