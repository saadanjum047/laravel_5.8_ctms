@isset($workflow->workflow_id)
    @php
        $number = 12;
    @endphp
    <li>
        <a href=""
           class="chart-element"

           style="background-color:{{$workflow->color}}">
                            <span style="color: {{ color_inverse($workflow->color) }}">
                                {{ $workflow->process_name }}
                            </span>
        </a>
        @if(count($workflow->child) > 0)
            <ul>
                @foreach($workflow->child as $workflow)
                    @include('study.chart_each', ['workflow' => $workflow])
                @endforeach
            </ul>
        @endif
    </li>
@endisset
