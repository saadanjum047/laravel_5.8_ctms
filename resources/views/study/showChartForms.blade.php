<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ $study->study->study_name }}</li>
        <li class="breadcrumb-item active" aria-current="page">Forms</li>
    </ol>
</nav>

<div class="row mt-5">

    <div class="col-md-6">
        <div class="table-responsive" id="patients-list">
            <table class="table center-aligned-table">
                <thead>
                <tr class="text-primary">
                    <th>No</th>
                    <th>Form Name</th>
                    <th>Form Details</th>
                    <th>Form Comment</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($workflows->forms as $form)
                    <tr class="">
                        <td>{{ $form->form_id }}</td>
                        <td>{{ $form->form_name }}</td>
                        <td>{{ $form->form_details }}</td>
                        <td>{{ $form->form_comment }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<style>
    .chart-element {
        position: relative;
        display: inline-block;
        width: 340px;
        padding: 10px;
        text-align: center;
        font-weight: bold;
        border: 1px solid;
        margin: 20px 12px;
    }

    .chart-element.one:after {
        position: absolute;
        content: ">";
        width: 20px;
        height: 5px;
        right: -31px;
        color: #000000;
    }

    .chart-element.one:before {
        position: absolute;
        content: "";
        background-color: #000000;
        width: 20px;
        height: 2px;
        right: -20px;
        bottom: 18px;
    }
</style>