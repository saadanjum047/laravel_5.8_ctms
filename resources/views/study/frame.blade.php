@extends('layouts.site')


@if(isset($study->study_name))
    @section('navbar')
        <!--Navbar-->
        <nav class="navbar bg-primary-gradient col-lg-12 col-12 p-0 fixed-top navbar-inverse d-flex flex-row">
            <div class="bg-white text-center navbar-brand-wrapper">
                <a class="navbar-brand brand-logo" href="{{ route('home') }}"><img src="{{ asset('images/sp_logo_back.png') }}" /></a>
                <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('images/logo-sp1.jpg') }}" alt=""></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center">
                <button class="navbar-toggler navbar-toggler hidden-md-down align-self-center mr-3" type="button" data-toggle="minimize">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <h5 class="font-weight-bold mt-2 mr-3 text-white">{{ $study->study_name }}</h5>

                <form class="form-inline mt-2 mt-md-0 hidden-md-down">
                    <input class="form-control mr-sm-2 search" type="text" placeholder="Search">
                </form>
                <ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
                    <li class="nav-item">
                        <a class="nav-link profile-pic" href="#"><img class="rounded-circle" src="images/face.jpg" alt=""></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-th"></i></a>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right hidden-lg-up align-self-center" type="button" data-toggle="offcanvas">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <!--End navbar-->
    @endsection
@endif

@section('content')
    {!! $content !!}
@endsection

@section('styles')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('scripts')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select-study').select2();
        });
    </script>
@endsection