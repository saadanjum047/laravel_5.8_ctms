@extends('layouts.site')

@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('success'))
    <script>
swal("Good job!", "{{$message }}", "success");
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $study->study_name }}</li>
        </ol>
    </nav>
		

    <div class="row mt-5">
        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 mb-4">
            <div class="card">
                <div class="card-block">
                    {{-- <h4 class="card-title font-weight-normal text-warning">{{ count($patients_type_counts) }}</h4> --}}
                    <h4 class="card-title font-weight-normal text-warning">
                        @if(isset($all_patients)) {{ $all_patients->where('study_id' , $study->id)->count() }} @else 0 @endif 
                        </h4>
                    <p class="card-text">Patients Enrolled</p>
                    {{-- <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-warning" role="progressbar"
                             style="width: 100%" aria-valuenow="{{ @$procent_patients }}"
                             aria-valuemin="0"
                             aria-valuemax="100">100%
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-6 mb-4">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title font-weight-normal text-danger">
                        <a href="{{ route('dashboard.study.diary.incomplete', $study->id) }}"
                           class="text-danger">{{ @number_format($patients_incomplete_diary, 0) }}</a>
                    </h4>
                    <p class="card-text">Compliance</p>
                    {{-- <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-danger" role="progressbar"
                             style="width: 0%"
                             aria-valuenow="{{100*@$incomplete_diary/(@$incomplete_diary != 0 ? @$incomplete_diary : 1) }}"
                             aria-valuemin="0"
                             aria-valuemax="100">0%
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-4">
                            <h5 class="card-title mb-4">Guide</h5>
                            <a href="{{ $study->guide_pdf }}" target="_blank">{{ $study->guide_pdf }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                       aria-controls="home"
                       aria-selected="true">
                        @if((int)Cookie::get('user_role')===3)
                            Patients
                        @else
                            Sites
                        @endif
                    </a>
                </li>
                @if(auth()->user()->role_id == 2)
                <li class="nav-item">
                    <a class="nav-link" id="patients_tab" data-toggle="tab" href="#all_pats" role="tab"
                        aria-controls="patients_tab" aria-selected="false">Patients</a>
                </li>
                @endif
                @if(auth()->user()->role_id == 4)
                <li class="nav-item">
                    <a class="nav-link" id="diary_tab" data-toggle="tab" href="#diary" role="tab"
                        aria-controls="diary" aria-selected="false">Diaires</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="schedules_tab" data-toggle="tab" href="#schedules" role="tab"
                        aria-controls="diary" aria-selected="false">Schedules</a>
                </li>
              </li>
                <li class="nav-item">
                    <a class="nav-link" id="medicines_tab" data-toggle="tab" href="#medicines" role="tab"
                        aria-controls="diary" aria-selected="false">Medicines</a>
                </li>
                @endif

                {{-- @if((int)Cookie::get('user_role')===3) --}}
                @if(auth()->user()->role_id ==3)
                    <li class="nav-item">
                        <a class="nav-link" id="content-tab" data-toggle="tab" href="#content" role="tab"
                           aria-controls="content" aria-selected="false">Content</a>
                    </li>
                @endif
            </ul>

            {{-- @if((int)Cookie::get('user_role')===3) --}}
            @if(auth()->user()->role_id == 3)
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title mb-4">Study Patients</h5>
                                    </div>
                                </div>

                                <table class="table">
                                    <thead class="text-primary">
                                    <tr>
                                        <th>Patient ID</th>
                                        <th>Visit Date</th>
                                        <th>Exit Date</th>
                                        <th># Overdue Diary</th>
                                        <th>Password Created</th>
                                        {{--<th>Transfer</th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>


                                    @foreach($tip_users as $createdby)


                                    @if($createdby->fullname===Cookie::get('user_name'))
                                    @php $owner= $createdby->id;  @endphp
                                    @endif
                                    @endforeach

                                <?php //dd($information->tip_users); ?>

                                <?php  //echo json_encode($study->patients);   //?>
                                                                

                            @php
                            $patients = collect();
                            $all_patients = collect();
                            if($study->sites->count() > 0){
                            $all_sites = $study->sites;

                            foreach($all_sites as $site){
                                if($site->site->patients->count() > 0 ){

                                $patients = $site->site->patients->where('study_id' , $study->id);
                                
                                $all_patients = $all_patients->merge($patients);
                            }}}
                            @endphp

                            @foreach($all_patients as $patient)


 <tr>

@php //dump($patient); @endphp
    <td>
        <a href="{{ route('dashboard.study.patient.show',[ $study->id , $patient->id]) }}" style="
 {{--  @foreach($study_audit as $audit)
 @if($audit->patient_id==$patient->info->patient_id &&  $patient->info->study_id==request()->route()->id && $patient->info->site_id==$audit->field_name )

@elseif($audit->patient_id==$patient->info->patient_id &&  $patient->info->study_id !=request()->route()->id && $patient->info->site_id==$audit->field_name )
 
@else
pointer-events:none
@endif
@endforeach  --}}
">
{{--  {{dd($patient)}}  --}}
@if(isset($patient->for_current_investiagator) && $patient->for_current_investiagator==1)
<strong>{{ $patient->patient_nbr }}</strong>


 @foreach($tip_study_audit as $audit)

@if($audit->patient_id==$patient->info->patient_id &&  $patient->info->study_id==request()->route()->id)
<strong class="text-danger pull-right">Transferred</strong>
@elseif($patient->info->study_id !=request()->route()->id)
@if($audit->patient_id==$patient->info->patient_id)
<strong class="text-danger pull-right">Transferred</strong>
@endif

@endif
@endforeach
@else 
{{ $patient->patient_nbr }}
{{--  
 @foreach($study_audit as $audit)
@if($audit->patient_id==$patient->info->patient_id &&  $patient->info->study_id==request()->route()->id)
<strong class="text-danger pull-right">Transferred</strong>
@elseif($patient->info->study_id !=request()->route()->id)
@if($audit->patient_id==$patient->info->patient_id)
<strong class="text-danger pull-right">Transferred</strong>
@endif
@endif
@endforeach  --}}

								

						
                        @endif
                    </a>
                </td>
                <td>
                    @if((int)$patient->type===2)
                        <span class="badge badge-success">
            @elseif((int)$patient->type===1)
                                <span class="badge badge-danger">
            @elseif((int)$patient->type===0)
                                        <span class="badge badge-primary">
            @endif
            @if(DateTime::createFromFormat('m/d/Y', $patient->enrollment_date) !==false)
                {{ $patient->enrollment_date }}
            @else
                Missing date
            @endif
            </span>
            </td>
			<td><span class="badge  {{ $patient->exit_date!==NULL? 'badge-danger':'badge-success' }} " >
			{{ $patient->exit_date!==NULL? $patient->exit_date :'Active' }}
			</span></td>
                <td>
                        <center>
                    @if((int)$patient->overdue_diary>0)
                        <span class="badge badge-danger">
                        {{ $patient->overdue_diary }}
                        </span>
            @else
                <span class="badge badge-success">
                        {{ $patient->overdue_diary }}
                        </span>

            @endif
                    </center>
                </td>
                <td>
                    <center>
                    @if(($patient->confirmed)==1)
                    <span class="badge badge-success">Yes</span>
                @else<span class="badge badge-danger"> No</span>
                @endif
                </center></td>
					{{--<td><button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm" id="{{$patient->info->patient_id}}" onclick="Transferpat(id)">Transfer</button></td>--}}
                                        </tr>

   @endforeach
	
                                    </tbody>
                                </table>

                                @empty($study->patients)
                                    Patients Not Found!
                                @endempty
                            </div>
                        </div>
                    </div>
                    
                    
                    {{-- @if((int)Cookie::get('user_role')===3) --}}
                    @if(auth()->user()->role_id == 3)
                    {{-- contents tab --}}
                        <div class="tab-pane fade" id="content" role="tabpanel" aria-labelledby="content-tab">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="card-title mb-4">Study Content</h5>
                                        </div>
                                    </div>
                                    <div class="table-responsive" id="patients-list">
                                        {{-- @if(isset($study->content->content_id)) --}}
                                            <table class="table center-aligned-table">
                                                <thead>
                                                <tr class="text-primary">
                                                    <th>No</th>
                                                    <th>Header</th>
                                                    <th>Body</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @if(isset($study->contents))
                                                    @foreach( $study->contents as $content)
                                                <tr class="">
                                                    <td>{{ $content->content_id }}</td>
                                                    <td>{!! $content->content_header !!}</td>
                                                    <td>{!! $content->content_body !!}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        {{-- @endif --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    
                </div>

            @else
            

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title mb-4">Study Sites</h5>
                                    </div>
                                </div>

                                <table class="table">
                                    <thead class="text-primary">
                                    <tr>
                                        <th>Site ID</th>
                                        <th>Site Name</th>
                                        <th>Patients Count</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($study->sites))
                                    @foreach($study->sites as $k => $site)
                                    @php
                                    if($k <= 5)
                                    if(isset($site->site))
                                    $sit[] = $site->site;  
                                    if(isset($site->site->patients))  
                                    $sit_pats[] = count($site->site->patients->where('study_id' , $study->id));    
                                    @endphp
                                        <tr>
                                            <td>
                                                {{ $site->site->site_nbr ?? ""  }}
                                            </td>
                                            <td>
                                                <a href="{{ route('dashboard.study.site' , [$study->id , $site->site_id]  )}} ">{{@ $site->site->site_name }}
                                            </a>
                                            </td>
                                            <td>
                                                {{-- {{ dd($site->site->patients->count()) }} --}}
                                                @if(isset($site->site->patients))
                                                {{$site->site->patients->where('study_id' , $study->id)->count()}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                    
                                    </tbody>
                                </table>
                                @if(count($study->sites) == 0 )
                                <p class="alert alert-primary"> No Sites found  </p>
                                @endif

                                @empty($study->patients)
                                    Patients Not Found!
                                @endempty
                            </div>
                        </div>
                    </div>

                    @if(auth()->user()->role_id == 4)
                    <div class="tab-pane fade" id="diary" role="tabpane1" aria-labelledby="diary_tab">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title mb-4">Diaries</h5>
                                    </div>
                                </div>
                                <div class="table-responsive" id="patients-list">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>#</th>
                                            <th>Diary Name</th>
                                            <th>Frequency</th>
                                            <th>Duration</th>
                                            <th>Created</th>
                                        </tr>
                                        {{--  {{dd(auth()->user()->patient->study->diary)}}  --}}
                                        @foreach ($study->diary as $k => $diary)
                                        @if($diary->status == 1)
                                            <tr>
                                                <td>{{$k+1}} </td>
                                                <td>
                                                     {{-- <a href="{{route('dashboard.study.diary.question' , $diary->id)}}"> {{$diary->diary_name}} </a> 
                                                     --}}
                                                    
                                                     <a href="{{route('dashboard.study.diaries' , [ $study->id , $diary->id])}}"> {{$diary->diary_name}} </a> 
                                                    
                                                    </td>
                                                <td> {{$diary->frequency}} </td>
                                                <td>{{$diary->end_duration}} </td>
                                                <td>{{$diary->created_at}} </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="schedules" role="tabpane1" aria-labelledby="schedules_tab">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title mb-4">Schedules</h5>
                                    </div>
                                </div>
                                <div class="table-responsive" id="patients-list">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>#</th>
                                            <th>Study Name</th>
                                            <th>Schedule Name</th>
                                            <th>Visit Date</th>
                                        </tr>
                                        @if(isset(auth()->user()->patient->patient_schedule))
                                        @foreach (auth()->user()->patient->patient_schedule as $k => $schedule)
                                        <tr>
                                            <td> {{$k+1}} </td>    
                                            <td> {{$schedule->study->study_name ?? ''}} </td>    
                                            <td> {{$schedule->visit_name}} </td>    
                                            <td>  {{$schedule->visit_date}} </td>    
                                        </tr>    
                                        @endforeach
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="medicines" role="tabpane1" aria-labelledby="medicines_tab">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title mb-4">Schedules</h5>
                                    </div>
                                </div>
                                <div class="table-responsive" id="patients-list">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>#</th>
                                            <th>Medicine Name</th>
                                            <th>Medicine Dose</th>
                                            <th>Created Date</th>
                                        </tr>
                                        @foreach ($study->medicines as $k => $medicine)
                                        <tr>
                                            <td> {{$k+1}} </td>    
                                            <td> {{$medicine->name}} </td>    
                                            <td> {{$medicine->dose}} </td>    
                                            <td>  {{$medicine->created_at}} </td>    
                                        </tr>    
                                        @endforeach
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif



                    {{-- @if((int)Cookie::get('user_role')===3) --}}
                    @if(auth()->user()->role_id == 3)


                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="card-title mb-4">Study Content</h5>
                                        </div>
                                    </div>
                                    <div class="table-responsive" id="patients-list">
                                        @if(isset($study->content->content_id))
                                            <table class="table center-aligned-table">
                                                <thead>
                                                <tr class="text-primary">
                                                    <th>No</th>
                                                    <th>Header</th>
                                                    <th>Body</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="">
                                                    <td>{{ $study->content->content_id }}</td>
                                                    <td>{!! $study->content->content_header !!}</td>
                                                    <td>{!! $study->content->content_body !!}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif

                    <div class="tab-pane fade" id="all_pats" role="tabpane1" aria-labelledby="patients_tab">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title mb-4">Patients</h5>
                                    </div>

                                    <table class="table table-hover">
                                        <tr>
                                            <th> # </th>
                                            <th> Name </th>
                                            <th> NBR </th>
                                            <th> Enrolment Date </th>
                                        </tr>
                                        @if(isset($all_patients))
                                        @foreach($all_patients->where('study_id' , $study->id) as $k => $patient)
                                        <tr>
                                            <td> {{$k+1}} </td>
                                            <td> {{$patient->user->name}} </td>
                                            <td> {{$patient->patient_nbr}} </td>
                                            <td> {{Carbon\Carbon::parse($patient->enrollment_date)->toDateString()}} </td>

                                        </tr>
                                        @endforeach
                                        @endif
                                    </table>
                                </div>
        
                                
                              
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    @endif


        </div>
        <div class="col-md-5 mt-4">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title mb-4">Site Progress</h5>
                                    </div>
                                </div>

                                {{--  @if(isset($study->sites))
                                    @foreach($study->sites as $k => $site)
                                    @php
                                    if($k <= 5)
                                    if(isset($site->site))
                                    $sit[] = $site->site;  
                                    if(isset($site->site->patients))  
                                    $sit_pats[] = count($site->site->patients->where('study_id' , $study->id));    
                                @endphp
                                @endforeach
                                @endif  --}}

                                {{--  {{dd($sit_pats)}}  --}}
                                @if(isset($sit_pats) && isset($sit))
                                <canvas id="barChart" style="height:230px"
                                data-counts="{{  json_encode($sit_pats) }}"

                                    data-studies="{{ json_encode(array_map(function($item){
                                        return str_limit($item, 12, '');
                                    }, array_pluck($sit, 'site_name'))) }}"
                            ></canvas>
                                @endif
                                
                            
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

    </div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
 	<h4 class="modal-title">Transfer Patient Into Site</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
<form method="post" action="{{ route('dashboard.patient.SavePatientIntoSite')}}">
{{ csrf_field() }}

<input type="hidden" id="patid" name="patient_id">
<div class="form-group">
<lable>Choose Site:</lable>
<select name="site_id" class="form-control">
<option selected disabled>Choose Site</option>
@if(isset($sites)))
@foreach($sites as $site)
<option value="{{$site->site_id}}"> {{$site->site_name}}---{{$site->site_nbr}}</option>
@endforeach
@endif
</select>
</div>
<div class="form-group">
<button type="submit" class="btn btn-primary">Save</button>
</div>
</form>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
function Transferpat(id){
document.getElementById("patid").value =id;
  }</script>














@endsection
