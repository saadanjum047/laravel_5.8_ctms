

<table>
    <tr>
        <th width="10" >Site Number</th>
        <th width="10">Patinet Number</th>
        <th width="10">Patinet ID</th>
        <th width="20">Date</th>
        <th width="30">Date of Diary</th>
        <th width="20">Status</th>
        <th width="40">Site Name</th>
        @foreach($data[1] as $diary)
        @foreach ($diary->questions as $question)
        <th width="40"> {{$question->question}} </th>    
        @endforeach
        @endforeach
    </tr>
   @foreach($data[0] as $patient)
   @foreach($data[1] as $diary)
    <tr>
        <td> {{$patient->site->site_nbr ?? ''}} </td>
        <td> {{$patient->patient_nbr}} </td>
        <td> {{$patient->id}} </td>
        <td> {{$diary->created_at->format('j F, Y')}} </td>
        <td> {{$diary->created_at->format('l j F, Y')}} </td>

        <td>  @if(isset($diary->diary_schedules->where('patient_id' , $patient->id)->first()->type)) 
            @if($diary->diary_schedules->where('patient_id' , $patient->id)->first()->type == 0 )
            Current
            @elseif($diary->diary_schedules->where('patient_id' , $patient->id)->first()->type == 1)
            Overdue
            @elseif($diary->diary_schedules->where('patient_id' , $patient->id)->first()->type == 2)
            Completed
            
            @endif
            @else -
        @endif
     </td>
        <td> {{$patient->site->site_name ?? ''}} </td>

        @foreach ($diary->questions as $question)
        @php
        $answer = $question->ans->where('patient_id', $patient->id)->first();
        
        if( $question->answer_type == 3 ){
            $answer = json_decode($answer);
        }
        @endphp
        <td> @if(isset($answer))  {{ str_replace( array('"' , '[' , ']' ) , '' , $answer->answers) ?? '' }} @endif  </td>    
        @endforeach

    </tr>
    @endforeach
    @endforeach
</table>