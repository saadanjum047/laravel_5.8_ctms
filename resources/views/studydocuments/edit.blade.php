

@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!---->
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
	<!--<link rel="stylesheet" href="{{ asset('css/demo.css') }}">-->
	<!--end-->


				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success").then(function() {
    window.location = "{{ route('dashboard.study.documents', $study->id) }}";
});

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif



     <div class="wrapper">
		<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >
{{$document->name}}



</a>
</li>
<li class="breadcrumb-item " aria-current="page">

<a href="{{ route('dashboard.study.documents', $study->id) }}" >
Study Documents
</a>
</li>
<li class="breadcrumb-item " aria-current="page">

Update Study Document
</a>
</li>


</ol>
	</nav>

         
			<div class="card">
            <div class="card-header">
             <h3 class="card-subtitle mb-4 text-muted">Update Study Document</h3>                         	
			</div>
                

                            
              <div class="card-body" style="margin: 20px;padding: 20px;">
            <form class="form-horizontal" method="POST" action="{{route('dashboard.study.document.update' , [$study->id , $document->id])}}" enctype="multipart/form-data">  

              {{ csrf_field() }}
                
              <span id="error"></span>


            @isset($Study)	
            <input type="text" name="id" class="form-control" value="{{$document->id}}" style="display:none">

            <input type="text" name="study_id" class="form-control" value="{{$Study}}" style="display:none">
            
            @endif


            <div class="form-group row">

              <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Document Name</label>



              <div class="col-sm-10">

                  <input type="text" class="form-control " name="name"  placeholder="File Name" value="{{$document->name}}">

              </div>

            </div>

                  <div class="form-group row">

              <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Document </label>



              <div class="col-sm-10">

              <input type="file" class="form-control " name="document">
              
              {{$document->document}}

                  </div>

                </div>
                       
                 
                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Document Type </label>



                  <div class="col-sm-10">
                    <select class="form-control" name="document_type">
                      <option value="" index="-1" selected></option></option>) >Select Any of the Document Type</option>
                      <option value="Protcol" @if($document->document_type == 'Protcol') selected @endif>Protcol</option>
                      <option value="Education" @if($document->document_type == 'Education') selected @endif>Education</option>
                      <option value="Training" @if($document->document_type == 'Training') selected @endif>Protocol</option>
                    </select>
                  </div>

                </div>

     
                         
                 			

              </div>  <!-- /.card-body -->

              <div class="card-footer">   
				 
                <button type="submit"  class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px">Update</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

            </div>

          </div>


   


@endif
@endsection
