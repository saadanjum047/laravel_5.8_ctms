

@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!---->
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
	<!--<link rel="stylesheet" href="{{ asset('css/demo.css') }}">-->
	<!--end-->


				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success").then(function() {
    window.location = "{{ route('dashboard.study.StudyDocuments', $Study) }}";
});
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif



     <div class="wrapper">
		<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
{{$study->study_name}}

</a>
</li>
<li class="breadcrumb-item " aria-current="page">

<a href="{{ route('dashboard.study.documents', $study->id) }}" >
Study Documents
</a>
</li>
<li class="breadcrumb-item " aria-current="page">

Create Study Document
</a>
</li>


</ol>
	</nav>

         
			<div class="card">
            <div class="card-header">
             <h3 class="card-subtitle mb-4 text-muted">Add New Study Document</h3>                         	
			</div>
                

                               
                 <div class="card-body" style="margin: 20px;padding: 20px;">
				<form class="form-horizontal" method="POST" action="{{route('dashboard.study.document.store' , $study->id)}}" enctype="multipart/form-data">  

                 {{ csrf_field() }}
                    
                 <span id="error"></span>
		
		@isset($Study)	
 		<input type="text" name="study_id" class="form-control" value="{{$Study}}" style="display:none">
				@endif


                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Document Name</label>



                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="name"  placeholder="File Name" required>

                  </div>

                </div>

                      <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Document </label>



                  <div class="col-sm-10">

                      <input type="file" class="form-control " name="document">

                  </div>

                </div>
                       
                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Document Type </label>



                  <div class="col-sm-10">
                    <select class="form-control" name="document_type">
                      <option value="" index="-1">Select Any of the Document Type</option>
                      <option value="Protcol">Protcol</option>
                      <option value="Education">Education</option>
                      <option value="Training">Training</option>
                    </select>
                  </div>

                </div>
                       
                   

     
                         
                 			

              </div>  <!-- /.card-body -->

              <div class="card-footer">   
								 
                <button type="submit" name="add_new" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px" onclick="return passvalidate()">Create</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

            </div>

          </div>


   


@endif
@endsection
