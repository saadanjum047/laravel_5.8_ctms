@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if(auth() && auth()->user()->role_id == 2)




<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "error");
</script>
@endif








 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >
{{$study->study_name}}

</a>
</li>
<li class="breadcrumb-item " aria-current="page">
Study Documents
</li>


</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left">View Study Documents</h3> <a href="{{ route('dashboard.study.document.create', $study->id) }}" class="btn btn-primary pull-right">Create Study Document</a>  </div>
            <div class="card-block">

                                                    
				<div class="progress">
          <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
        </div>



          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
                            <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th width="30%"> Name</th>
                  <th width="30%"> Document Type</th>
                  <th>Actions</th>
                                  
                </tr>
                </thead>
                <tbody>
          
              @foreach($documents as $data)
                      
              <tr>
                <td>
                <a href="{{route('dashboard.study.documents.show' , [$study->id , $data->id] )}}">  {{$data->name}}</a>
                </td>
                <td>{{$data->document_type ?? ''}}</td>

                <td>
                <a href="{{ route('dashboard.study.document.edit', [$study,$data->id]) }}" title="Update Study Document"><i class="fa fa-edit fa-lg"></i></a>
                </td>
				                
              </tr>

                 @endforeach                   
                </tbody>
               </table>
               
          	  </div>
		</div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

@endif
@endsection