@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if( auth() && auth()->user()->role_id ==2)


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif

 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">
Studies
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left"> Studies</h3> <a href="{{ route('dashboard.study.create')}}" class="btn btn-primary pull-right">Create Study</a>  </div>
            <div class="card-block">

                                                    
				<div class="progress">
            <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
        </div>

          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                <th> study</th>
                <th> Title</th>
		            <th> Description</th>
                <th> Start date</th>
                <th> End date</th>
                <th> Actions</th>                              
                </tr>
                </thead>
                <tbody>
               @foreach($studies as $study)                     
              <tr>
                <td><a href="{{ route('dashboard.study.view', [$study->id,'English']) }}">{{$study->study_name}}</a></td>
                <td>{{$study->study_title}}</td>
                <td>{!! $study->study_description !!}</td>
                <td>{{$study->startdate}}</td>
                <td>{{$study->enddate}}</td>
                <td>
                 {{-- <a href="{{ route('dashboard.study.alter', $study->id )}}" title="Update Study" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit fa-lg"></i></a> --}}
                  <a href="{{route('dashboard.studies.edit' , $study->id )}}" class="text-primary" ><i class="fa fa-edit fa-lg"></i> </a>
                   <!--<a href="" title="Delete Study"><i class="fa fa-trash"></i></a>-->
                </td>
              </tr>
                @endforeach                
                </tbody>
               </table>
               
            </div>
	     </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

@endif
@endsection