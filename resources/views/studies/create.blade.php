@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if(auth() && auth()->user()->role_id ==2)




<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('success'))
 <script>
swal("Good job!", "{{$message }}", "success").then(function(){
 window.location = "{{ route('dashboard.studies') }}";
});
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif

 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.studies') }}" >
Studies

</a>

</li>

        <li class="breadcrumb-item" aria-current="page">
Add Study
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left">Add Study</h3>  </div>
            <div class="card-block">

@if($errors->any())
@foreach ($errors->all() as $error)

<p class="alert alert-danger"> {{$error}} </p>    
@endforeach
@endif

	<form class="form-horizontal" method="POST" action="{{route('dashboard.studies')}}" enctype="multipart/form-data">  
  

                 {{ csrf_field() }}
                    
                 <span id="error"></span>
		

                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Organization</label>



                  <div class="col-sm-10">

                    <Select class="form-control" name="organization"  required>
		              	<option selected value="">Select Organization</option>
                    @foreach($organizations as $org)   
                        <option value="{{$org->sponsor_id}}">{{$org->sponsor_name}}</option>
                    @endforeach
                  </Select>

                  </div>

                </div>

                <div class="form-group row">

                  <label for="lastname_input" class="col-sm-2 control-label" style="font-weight: bold;">Protocol ID</label>



                   <div class="col-sm-10">

                      <input type="text" class="form-control " name="protocol_id"  placeholder="Study Name" required>

                  </div>

                </div>

                <div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Study Name</label>



                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="study_name" placeholder="Study Name" required>

                  </div>

                </div>

                                 
                    <div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Study Title</label>



                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="study_title" placeholder="Study Title" required>

                  </div>

                </div>



                               
                    <div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Study Description</label>



                  <div class="col-sm-10">

                      <textarea type="text" class="form-control" id="summernote" name="study_description" ></textarea>

                  </div>

                </div>
		<div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Start Date</label>



                  <div class="col-sm-10">

                      <input type="date" class="form-control " name="startdate" >

                  </div>

                </div>
		<div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">End Date</label>



                  <div class="col-sm-10">

                      <input type="date" class="form-control " name="enddate" >

                  </div>

                </div>


		<div class="form-group row">
		 <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Access Code</label>

		<div class="col-sm-10">
		<?php
	   	$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                $pass = array(); //remember to declare $pass as an array
                $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                for ($i = 0; $i < 7; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];
                }
                $newpass = implode($pass);
				   ?>
		<input type="text" class="form-control " name="access_code" value="<?php echo $newpass; ?>" readonly>
				
		</div>
                </div>


 		<div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Background Image</label>



                  <div class="col-sm-10">

                      <input type="file" class="form-control" name="background_img"  required>

                  </div>

                </div>

                                 
                 			

              </div>  <!-- /.card-body -->

              <div class="card-footer">   
	          	 <input type="hidden" name="sponsor_id" value="">
				 
                <button type="submit" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px">Create</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

          	    
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
@endif
@endsection
