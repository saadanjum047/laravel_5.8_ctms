@extends('layouts.site')


@section('content')

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>

   <li class="breadcrumb-item"><a href="{{route('dashboard.study.patient_engagements' , $study->id )}}">Patient Engagement Questions</a></li>

   
   <li class="breadcrumb-item" aria-current="page"> 
    @if(isset($question))
    Update Question
    @else
    Add Question
    @endif
</li>

   </ol>
</nav>

@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif

<div class="bg-white" style="padding:20px">
      
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif

    <h2> @if(isset($question )) Update Question  @else
         Create Question @endif </h2>
    <form action="@if(isset($question )) {{route('dashboard.study.patient_engagements.update' , [ $study->id , $question->id] )}}  @else
    {{route('dashboard.study.patient_engagements.store' , $study->id )}} @endif" method="post" enctype="multipart/form-data" >
        {{ csrf_field() }}

        {{-- <input type="hidden" name="diary_id" value="{{$question->diary->id}}" >  --}}
      <div class="modal-body">
          <div class="form-group">
            <label for="question" class="col-form-label">Question:</label>
            <input type="text" name="question" class="form-control" id="question" @if(isset($question )) value="{{$question->question}}"  @endif placeholder="Question" required />
          </div>
          
          @php
          if(isset($question)){
              $choices = json_decode($question->answers);
            }
              @endphp
          <div class="row">
          <div class="col-md-8">
              <div class="form-group">
            <label class="col-form-label">Choice 1:</label>
            <input type="text" class="form-control" name="choices[]" @if(isset($choices)) value="{{$choices[0]}}" @endif >
            </div>
          </div>
            <div class="col-md-4">
                <div class="form-group" style="margin-top:30px">
                <input type="radio" name="correct_answer" id="radio1" value="0" @if(isset($question->correct_answer) && $question->correct_answer == 0 ) checked @endif >
                <label class="col-form-label" for="radio1"  >Correct Answer</label>
            </div>
          </div>
          </div>  <div class="row">
          <div class="col-md-8">
              <div class="form-group">
            <label class="col-form-label">Choice 2:</label>
            <input type="text" class="form-control" name="choices[]" @if(isset($choices)) value="{{$choices[1]}}" @endif>
            </div>
          </div>
            <div class="col-md-4" >
                <div class="form-group" style="margin-top:30px">
                <input type="radio" name="correct_answer" id="radio2" value="1" @if(isset($question->correct_answer) && $question->correct_answer == 1 ) checked @endif>
                <label class="col-form-label" for="radio2">Correct Answer</label>
            </div>
          </div>
          </div>  
          <div class="row">
          <div class="col-md-8">
              <div class="form-group">
            <label class="col-form-label">Choice 3:</label>
            <input type="text" class="form-control" name="choices[]" @if(isset($choices)) value="{{$choices[2]}}" @endif>
            </div>
          </div>
            <div class="col-md-4">
                <div class="form-group">
                <input type="radio" name="correct_answer" style="margin-top:30px" id="radio3" value="2" @if(isset($question->correct_answer) && $question->correct_answer == 2 ) checked @endif>
                <label class="col-form-label" for="radio3"  >Correct Answer</label>
            </div>
          </div>
          </div> 
          
          <div class="row">
          <div class="col-md-8">
              <div class="form-group">
            <label class="col-form-label">Choice 4:</label>
            <input type="text" class="form-control" name="choices[]" @if(isset($choices)) value="{{$choices[3]}}" @endif>
            </div>
          </div>
            <div class="col-md-4">
                <div class="form-group">
                <input type="radio" name="correct_answer" style="margin-top:30px" id="radio4" value="3" @if(isset($question->correct_answer) && $question->correct_answer == 3 ) checked @endif required >
                <label class="col-form-label" for="radio4">Correct Answer</label>
            </div>
          </div>
          </div>

        <div class="form-group">
            <lable class="col-form-label">Attachment</lable> 
            <input type="file" class="form-control" name="attach" required>
            {{ $question->attachment ?? NULL }}
        </div>
          
    </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-success">@if(isset($question )) Update Question @else 
            Add Question @endif</button>
      </div>
    </form>

</div>

@endsection

