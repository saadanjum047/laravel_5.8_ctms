@extends('layouts.site')


@section('content')


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id)}}"> {{$study->study_name}} </a></li>
   <li class="breadcrumb-item" aria-current="page">
    Patient Engagement Questions</li>
   </ol>
</nav>


<div class="bg-white">
  
@if(session()->has('message'))
<p class="alert alert-primary"> {{session('message')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif


    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Mystery Questions 
          
          <a href="{{ route('dashboard.study.patient_engagements.create' , $study->id )}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Question </a>

        </h3>
        </div>

<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Question</th>
    <th >Answers</th>
    <th >Correct Answer</th>
    <th>Created</th>
    <th>Action</th>
</tr>

@foreach ($questions as $k => $question)
    
<tr>
    <td> {{$k+1}} </td>
    <td> {{$question->question }} </td>
    <td> {{ trim(str_replace(array( '[' , ']' , '"' ) , '' , $question->answers  )) }} </td>
    <td> 
        @php
            $choices = json_decode($question->answers);
        @endphp
        {{ $choices[$question->correct_answer] ?? '' }} </td>
    <td> {{($question->created_at)->toDateString() }} </td>
    <td>
        
        <a href="{{route('dashboard.study.patient_engagements.details' , [$study->id , $question->id ])}}" class="text-success" > <i class="fa fa-info-circle fa-lg"></i> </a>
        
        <a href="{{route('dashboard.study.patient_engagements.edit' , [$study->id , $question->id ])}}" class="text-primary" > <i class="fa fa-edit fa-lg"></i> </a>

        <a href="{{route('dashboard.study.patient_engagements.remove' , [$study->id , $question->id ])}}" class="text-danger" > <i class="fa fa-trash fa-lg"></i> </a>
    </td>
</tr>

@endforeach

</table>

</div>
</div>





@endsection

