@extends('layouts.site')


@section('content')


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id)}}"> {{$study->study_name}} </a></li>

<li class="breadcrumb-item"><a href="{{ route('dashboard.study.patient_engagements' , $study->id)}}"> Questions </a></li>
   <li class="breadcrumb-item" aria-current="page">
    Patient Engagement Questions</li>
   </ol>
</nav>


<div class="bg-white">
  
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif


    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Mystery Question Answers</h3></div>

<table class="table table-hover">
<tr>
    <th>#</th>
    <th> Patient Name</th>
    <th> Correct Answer </th>
    <th> Answer </th>
    <th> Status </th>
    <th> Answered Date</th>
    {{--  <th>Action</th>  --}}
</tr>
@php
    $choices = json_decode($question->answers);
@endphp
@foreach ($patients as $k => $patient)
{{-- @dump($patient) --}}
@php
    if($question->ans->count() > 0 ){
    $answer = $question->ans->where('patient_id' , $patient->id)->first();
        $choices = json_decode($question->answers);
    
}
@endphp
    
<tr>
    <td> {{$k+1}} </td>
    <td> {{$patient->user->name ?? '' }} </td>

    <td> {{$choices[$question->correct_answer] ?? '' }} </td>

    <td> @if(isset($answer))  {{$answer->answer_body ?? '' }} @endif </td>
    <td> @if(isset($answer->status)) @if($answer->status == 1) <span class="badge badge-success">Correct</span>  @elseif($answer->status == 0) <span class="badge badge-danger">Wrong</span> @endif @endif </td>
    <td> @if(isset($answer)) {{$answer->created_at->toDateString() ?? '' }} @endif </td>
    
</tr>
@php
  
@endphp
@endforeach

</table>

</div>
</div>





@endsection

