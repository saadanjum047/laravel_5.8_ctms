@extends('layouts.site')

@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item" aria-current="page">
    Scheduled Patients
</li>

   </ol>
</nav>



<div class="bg-white">

    <div class="card">
        <div class="card-header bg-white">
            
          <h3 class="card-title">Scheduled Patients
        </div>

        @if($errors->any())
        @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{$error}} </p>    
        @endforeach
        @endif
        
    <table class="table table-hover">
        <tr>
            <th>#</th>
            <th>Visit Name</th>
            <th>Study Name</th>
            <th>Patient Name</th>
            <th>Vsisit Date</th>
        </tr>
        {{-- {{dd($schedules)}} --}}
        @if(isset($schedules))
        @foreach($schedules as $k => $schedule)
        <tr>
          {{--  {{dd($schedule->user)}}  --}}
            <td>{{$k+1}} </td>
            <td>{{$schedule->visit_name}} </td>
            <td>{{$schedule->study->study_name}} </td>
            <td>{{$schedule->user->name}} </td>
            <td>{{$schedule->visit_date}} </td>
            {{-- <td>  --}}
                {{-- <a href="{{route('dashboard.patient_schedule.delete' , $schedule->id)}}" class="text-danger"><i class="fa fa-trash fa-lg"></i> </a>  --}}
                
                {{--| <a onclick="update_model_date({{$schedule}} , {{$schedule->study}} , {{$schedule->patient}}  ) " class="text-primary"><i class="fa fa-edit fa-lg"></i> </a> --}}
            {{-- </td> --}}
        </tr>
        @endforeach
        @endif
    </table>
</div>
</div>




{{-- 
<div class="modal fade" id="create_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Schedule Study</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('dashboard.patient_schedule')}}" method="POST">
            {{ csrf_field() }} 
        <div class="modal-body">
            
              <div class="form-group">
                <label for="visit_name" class="col-form-label">Visir Name:</label>
                <input type="text" class="form-control" name="name" id="visit_name">
                </div>
           
             <div class="form-group">
              <label for="patient_id" class="col-form-label">Studies:</label>
              <select class="form-control" name="study" id="patient_id">
                  <option value="" >Select Study</option>
                  @foreach ($studies as $study)
                      <option value="{{$study->id}}">{{$study->study_name}} </option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="target_data" class="col-form-label">Target:</label>
                <input type="number" class="form-control" name="target" id="target_data">
            </div>
            <div class="form-group">
                <label for="window_low_Data" class="col-form-label">Window Low:</label>
                <input type="number" class="form-control" name="window_low" id="window_low_Data">
            </div>
            <div class="form-group">
                <label for="window_high_data" class="col-form-label">Window High:</label>
                <input type="number" class="form-control" name="window_high" id="window_high_data">
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Schedule</button>
        </div>
        </form>

      </div>
    </div>
  </div>



  <div class="modal fade" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Schedule</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('dashboard.patient_schedule.update')}}" method="POST">
            {{ csrf_field() }} 
        <div class="modal-body">
            
              <div class="form-group">
                <label for="visit" class="col-form-label">Visir Name:</label>
                <input type="text" class="form-control" name="name" id="visit">
                </div>
           
             <div class="form-group">
              <label for="study" class="col-form-label">Studies:</label>
              <select class="form-control" name="study" id="study">
                  <option value="" >Select Study</option>
                  @foreach ($studies as $study)
                      <option value="{{$study->id}}">{{$study->study_name}} </option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="target" class="col-form-label">Target:</label>
                <input type="number" class="form-control" name="target" id="target">
            </div>
            <div class="form-group">
                <label for="low" class="col-form-label">Window Low:</label>
                <input type="number" class="form-control" name="window_low" id="low">
            </div>
            <div class="form-group">
                <label for="high" class="col-form-label">Window High:</label>
                <input type="number" class="form-control" name="window_high" id="high">
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update Schedule</button>
        </div>
        </form>

      </div>
    </div>
  </div> --}}


<script>
    function update_model_date(schedule , study , patient){
        $("#update_model").modal('show');
        $('#visit').val(schedule.visit_name);
        $('#target').val(schedule.target);
        $('#low').val(schedule.window_low);
        $('#high').val(schedule.window_high);
        $('#schedule').val(schedule.id);
        console.log(schedule , study , patient) ;

        var patients = document.getElementById('patients');
        /*var options = patients.options;
        for(  i = 1 ; i <= options.length; i++ ){ 
            var option = options[i];
            if(option.value == patient.id ){
              console.log('found');
                var selected = option.index;
                document.getElementById("patients").selectedIndex = selected;
                break;
            }
        } */
        var studies = document.getElementById('study');
        var options = studies.options;
        for( j = 1 ; j <= options.length; j++ ){ 
            var option = options[j];            
            if(option.value == study.id ){
              console.log('found');

                var select = option.index;
                document.getElementById("study").selectedIndex = select;
                break;

            }
        }
    }
</script>

@endsection