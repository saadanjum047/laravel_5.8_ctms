@extends('layouts.site')

@section('content')
    

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.newsletter' , [ $study->id , $newsletter->id] )}}">Newsletters</a></li>
     <li class="breadcrumb-item" aria-current="page">
{{$newsletter->title}}  Details
  </li>
  
     </ol>
  </nav>


<div class="bg-white" style="padding:20px">
    <h3> {{$newsletter->title}} </h3> 

    <table class="table table-hover">
        <tr>
            <th>#</th>
            <th>User Name </th>
            <th>Status </th>
            <th>Read Time  </th>
        </tr>
        @foreach ($newsletter->users as $k => $user)
        <tr>
            <td> {{$k+1}} </td>
            <td> {{$user->name}} </td>
            <td>
                @if($user->pivot->status == 0)
             Not Read 
            @else
            Read
            @endif
             </td>
            <td> {{ $user->pivot->read_at ?? '-' }} </td>

        </tr>
        @endforeach

    </table>
      
</div>

@endsection