
@extends('layouts.site')


@section('content')



<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.newsletter' , $study->id )}}">Newsletters</a></li>
     <li class="breadcrumb-item" aria-current="page">
  Update Newsletter
  </li>
  
     </ol>
  </nav>

  
<div class="bg-white" style="padding:20px">
    @if($errors->any())
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif
    <div class="col-md-8">
        <h3>Update Newsletter</h3>
        <form action="{{route('dashboard.study.newsletter.update' , [$study->id , $newsletter->id ])}}" method="POST" >
             {{ csrf_field() }}
    <div class="form-group">
        <label class="col-form-label"> Title </label>
        <input type="text" class="form-control" placeholder="title" name="title" value="{{$newsletter->title}}" required />
    </div>

    <div class="form-group">
        <label class="col-form-label"> Description: </label>
        <textarea class="form-control" rows="20" id="summernote" placeholder="Description here" name="description" > {{$newsletter->body}} </textarea>
    </div>

    <div class="form-group">
        <label class="col-form-label"> Tags: </label>  
            <select  class="js-example-basic-multiple form-control" name="tags[]" multiple >
                @php

                $ids = $newsletter->tags->pluck('id')->toArray();

                @endphp
            @foreach ($tags as $tag)
                <option value="{{$tag->id}}" @if(in_array($tag->id , $ids)) selected @endif > {{$tag->name ?? '-'}} </option>
            @endforeach    
            </select>  
        </div>
    <div class="form-group">
        <button class="btn btn-primary" >Update</button>
    </div>
        </form>
</div>
</div>


@endsection



@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script> 
        $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            placeholder : 'Select Tags',
        }
        );
        });
    </script>
@endsection