
@extends('layouts.site')


@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.newsletter' , $study->id )}}">Newsletters</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.tags' , $study->id )}}">Tags</a></li>
     <li class="breadcrumb-item" aria-current="page">
  Edit Newsletter
  </li>
  
     </ol>
  </nav>



<div class="bg-white" style="padding:20px">
@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif
    <div class="col-md-8">
        <h3>Edit Tag</h3>
        <form action="{{route('dashboard.study.tags.update' , [$study->id , $tag ])}}" method="POST" >
             {{ csrf_field() }}
    <div class="form-group">
        <label class="col-form-label"> Name: </label>
        <input type="text" class="form-control" placeholder="name" name="name" value="{{$tag->name}}" required />
    </div>

    <div class="form-group">
        <label class="col-form-label"> Description: </label>
        <textarea class="form-control"  id="summernote" rows="20" placeholder="Description here"  name="description">{{$tag->description}} </textarea>
    </div>
    
    <div class="form-group">
        <label class="col-form-label"> Users: </label>  
            <select  class="js-example-basic-multiple form-control" name="users[]" multiple >
                @php
                    $ids = $tag->users->pluck('id')->toArray();
                @endphp
            @foreach ($users as $user)
                <option value="{{$user->id}}" @if(isset($ids) && in_array($user->id , $ids ) ) selected @endif  > {{$user->name ?? '-'}} </option>
            @endforeach    
            </select>  
    </div>
    <div class="form-group">
        <button class="btn btn-primary"> Update</button>
    </div>
        </form>
</div>
</div>


@endsection



@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script> 
        $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            placeholder : 'Select Users',
        }
        );
        });
    </script>
@endsection