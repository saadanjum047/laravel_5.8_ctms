@extends('layouts.site')

@section('styles')

  <style>
  table {border-collapse:collapse; table-layout:fixed; width:310px;}
      </style>
      @endsection
@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.newsletter' , $study->id )}}">Newsletter</a></li>
     <li class="breadcrumb-item" aria-current="page">
  Tags
  </li>
  
     </ol>
  </nav>



<div class="col-md-12">
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">  Newsletters
            <a class="btn btn-success float-right" href="{{route('dashboard.study.tags.create' , $study->id)}}" > <i class="fa fa-plus"></i> Add New Tag </a>
        </h3>
        </div>
      <!-- /.card-header -->
    <table class="table table-hover">
        <tr>
            <th width="5%">#</th>
            <th width="10%">Name</th>
            <th width="20%"> Number of Users </th>
            <th width="40%"> Description </th>
            <th width="10%"> created </th>
            <th width="10%"> Action </th>
        </tr>
        {{-- {{ str_limit(strip_tags($tag->description), 40) }} --}}
      
        @foreach ($tags as $k => $tag )
            <tr>
                <td> {{$k+1}} </td>
                <td> {{$tag->name}} </td>
                <td> {{$tag->users->count()}} </td>
                <td style="word-wrap:break-word;" >{{ str_limit(strip_tags($tag->description), 500) }} </td>
                <td> {{$tag->created_at}} </td>
                <td>
                    <a class="text-primary" href="{{route('dashboard.study.tags.edit' , [$study->id , $tag->id ])}} "> <i class="fa fa-edit fa-lg"></i> </a>
                    
                    <a class="text-danger" href="{{route('dashboard.study.tags.delete' , [$study->id , $tag->id ])}} "> <i class="fa fa-trash fa-lg"></i> </a> 
                </td>
            </tr>
        @endforeach

    </table>
      
    </div>
    <!-- /.card -->
  </div>

@endsection