@extends('layouts.site')

@section('content')
    

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.newsletter' , $study->id )}}">Newsletters</a></li>
     <li class="breadcrumb-item" aria-current="page">
  {{$newsletter->title}}
  </li>
  
     </ol>
  </nav>


<div class="bg-white" style="padding:20px">

    <div class="text-center">
    <h3> {{$newsletter->title}} </h3> 
</div>
    <br>

    {!! $newsletter->body !!}



</div>


@endsection