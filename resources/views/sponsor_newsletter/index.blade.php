@extends('layouts.site')

@section('styles')

  <style>
  table {border-collapse:collapse; table-layout:fixed; width:310px;}
      </style>
      @endsection
@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item" aria-current="page">
  Newsletters
  </li>
  
     </ol>
  </nav>



<div class="col-md-12">
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">  Newsletters
            <a style="margin-left:10px" class=" btn btn-primary float-right" href="{{route('dashboard.study.tags' , [$study->id])}} "> <i class="fa fa-tag"></i>
              User Tags </a>
              
            @if(auth()->user()->role_id == 3)
            @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'create')->where('module_id' , 14 )->toArray() ) )
           
            <a href="{{ route('dashboard.study.newsletter.create' , $study->id)}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Newsletters</a></h3>

            @endif

            @elseif(auth()->user()->role_id == 2)
          <a href="{{ route('dashboard.study.newsletter.create' , $study->id)}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Newsletters</a>
            @endif
        </h3>
        </div>
      <!-- /.card-header -->
    <table class="table table-hover">
        <tr>
            <th width="5%">#</th>
            <th width="30%">Title</th>
            <th width="35%"> Description </th>
            <th width="10%"> Count </th>
            <th width="10%"> created </th>
            <th width="10%"> Action </th>
        </tr>
        {{-- {{ str_limit(strip_tags($post->description), 40) }} --}}
        @foreach ($newsletters as $k => $news)
        <tr>
          @php
          $count = NULL;
          $count = DB::table('newsletter_user')->where('newsletter_id' , $news->id)->where('status' , 1 )->get();
          @endphp

          <td> {{$k+1}} </td>
            <td> <a href="{{route('dashboard.study.newsletter.show' , [$study->id , $news->id ])}}">  {{$news->title}}  </a> </td>
            <td style="word-wrap:break-word;"> {{ str_limit( strip_tags($news->body) , 200) }} </td>
            <td> {{$count->count() ?? 0 }} </td>
            <td> {{($news->created_at)->toDateString()}} </td>
            <td>
                <a href="{{route('dashboard.study.newsletter.details' , [ $study->id , $news->id ] )}} " class="text-primary"> <i class="fa fa-info-circle fa-lg"></i> </a>
              
                @if(auth()->user()->role_id == 3)
              @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'update')->where('module_id' , 14 )->toArray() ) )
             |<a href="{{route('dashboard.study.newsletter.edit' , [ $study->id , $news->id ] )}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> 

             @endif
             @elseif(auth()->user()->role_id == 2)
                <a href="{{route('dashboard.study.newsletter.edit' , [ $study->id , $news->id ] )}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> 
             @endif
                

                
            @if(auth()->user()->role_id == 3)
            @if( ! empty( auth()->user()->study_user->where('study_id' , $study->id)->first()->role_detail->permissions->where('name' , 'delete')->where('module_id' , 14 )->toArray() ) )
              |
                <a href="{{route('dashboard.study.newsletter.delete' ,[ $study->id , $news->id ])}}" class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a>

                @endif
                @elseif(auth()->user()->role_id == 2)
                <a href="{{route('dashboard.study.newsletter.delete' ,[ $study->id , $news->id ])}}" class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a>


                @endif

            </td>
        </tr>
        @endforeach
    </table>
      
    </div>
    <!-- /.card -->
  </div>

@endsection