@extends('layouts.site')



@section('content')
    {!! $content !!}
@endsection

@section('styles')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('scripts')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select-study').select2();
        });
    </script>
@endsection