@extends('layouts.site')



@section('content')


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
        <li class="breadcrumb-item"><a href="{{ route('dashboard.tasks')}}">Tasks</a></li>    
        <li class="breadcrumb-item" aria-current="page">{{$task->name}}</li>
    </ol>
 </nav>


 
<div class="bg-white" style="padding:10px">

    <h2 > {{ $task->name }} </h2> <br>

    <p> {{ strip_tags($task->description) }} </p>
    <hr>
    
    <img src="{{url('/')}}/site_tasks/documents/{{$task->document}}" style="max-width:800px; max-height:800px" >
</div>


@endsection