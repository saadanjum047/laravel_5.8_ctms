@extends('layouts.site')

@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
        <li class="breadcrumb-item" aria-current="page">Tasks</li>
    </ol>
 </nav>

    
<div class="bg-white" style="padding:10px">
<div class="card">

    
    <div class="card-title">
        <h2>Investigator Tasks
    </div>
    <table class="table">
        <tr> 
            <th> # </th>
            <th> Task Name </th>
            <th> Task Description </th>
            <th> Created </th>
            <th> Actions </th>
        </tr>

        {{-- {{dd(auth()->user()->sponsor_user->tasks)}} --}}
        @if(auth()->user()->sponsor_user->tasks->count() > 0 )
        @foreach (auth()->user()->sponsor_user->tasks as $k=> $task)
            
        <tr>
            <td> {{$k+1}} </td>
            <td> <a href="{{route('dashboard.task.view' , $task->id)}}"> {{$task->name}} </a> </td>
            <td> {{ str_limit(strip_tags($task->description) , 80)}} </td>
            <td> {{($task->created_at)->toDateString()}} </td>
            <td> 
                @if($task->pivot->status == 0)
              <a href="{{route('dashboard.task.completed' , $task->id)}} " class="text-success"> <i class="fa fa-thumbs-up fa-lg"></i> </a>
              @else
                completed
              @endif
            </td>
        </tr>
        @endforeach
        @endif
    </table>

</div>
</div>
@endsection