<table>
    <tr>
    <th>#</th>
    <th>Name</th>
    <th>Email</th>
    <th>NBR</th>
    </tr>

    @foreach ($data as $k => $patient)
    <tr>

        <td> {{$k+1}} </td>
        <td> {{$patient->user->name}}  </td>
        <td> {{$patient->user->email}}  </td>
        <td> {{$patient->patient_nbr}}  </td>
    </tr>
    @endforeach
 
</table>