

@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if( auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success").then(function() {
    window.location = "{{ route('dashboard.user.ViewUsers', $Study) }}";
});

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif



     <div class="wrapper">
		<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        	<li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
			@isset($Study)
			<li class="breadcrumb-item active" aria-current="page">
				 <a href="{{ route('dashboard.study', [$study,'English']) }}" >{{$study->study_name}}</a>
			
			</li>
			@endif
			@isset($site_id)
			<li class="breadcrumb-item active" aria-current="page">
				 <a href="{{ route('dashboard.study', [$study,'English']) }}" >{{$study->study_name}}</a>
			
			</li>
			@endif
        <li class="breadcrumb-item active" aria-current="page">

		<a href="" >Update User</a>
		</li>
		</ol>
		</nav>

         
			<div class="card">
            <div class="card-header">
             <h3 class="card-subtitle mb-4 text-muted">Update User</h3>                         	
			</div>
                
@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif
                               
                 <div class="card-body" style="margin: 20px;padding: 20px;">
				<form class="form-horizontal" method="POST" action="{{route('dashboard.study.user.update' , $user )}}">  

                 {{ csrf_field() }}
                    
                 <span id="error"></span>
		@isset($site_id)	
 		<input type="text" name="site_id" class="form-control" value="{{$site_id}}" style="display:none"> 
		<input type="text" name="study_id" class="form-control" value="" style="display:none">

		@endif

		@isset($study)	
 		<input type="text" name="study_id" class="form-control" value="{{$study}}" style="display:none"> 
		<input type="text" name="site_id" class="form-control" value="" style="display:none">
		@endif

    @php
     list($first , $last ) = explode(' ' , $user->fullname);   
    @endphp
                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">First Name</label>



                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="firstname_input" value="{{$first}}" placeholder="First Name" required>

                  </div>

                </div>

                <div class="form-group row">

                  <label for="lastname_input" class="col-sm-2 control-label" style="font-weight: bold;">Last Name</label>



                   <div class="col-sm-10">

                      <input type="text" class="form-control " name="lastname_input" value="{{$last}}" placeholder="Last Name" >

                  </div>

                </div>

                <div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Email</label>



                  <div class="col-sm-10">

                      <input type="email" class="form-control " name="email_input" value="{{$user->email}}" placeholder="Email" required>

                  </div>

                </div>

                <div class="form-group row" style="display:none">


                  <label for="pass_input" class="col-sm-2 control-label" style="font-weight: bold;">Password</label>



                   <div class="col-sm-10">

                      <input type="password" class="form-control " name="pass_input"  value="{{$user->password}}" placeholder="Password" required>

                  </div>

                </div>

                   <div class="form-group row" style="display:none">

                  <label for="con_pass_input" class="col-sm-2 control-label" style="font-weight: bold;">Confirm Password</label>



                  <div class="col-sm-10">

                      <input type="password" class="form-control " name="con_pass_input"  value="{{$user->password}}" placeholder="Confirm Password" required>

                  </div>

                </div>

                  <div class="form-group row">

                  <label for="phone_input" class="col-sm-2 control-label" style="font-weight: bold;">Phone</label>
                   <div class="col-sm-10">
                  <input type="text" class="form-control" name="phone_input" value="{{$user->phone}}" placeholder="Phone" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="" required>

                   <!--<input type="number" class="form-control " name="phone_input" value="{{$user->phone}}" placeholder="Phone" required>-->
                  </div>  
                </div>

                <div class="form-group row">

                  <label for="addr_input" class="col-sm-2 control-label" style="font-weight: bold;">Address</label>

                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="addr_input" value="{{$user->address1}}" placeholder="Address" >

                  </div>
                </div>

                   <div class="form-group row">
                  <label for="addr2_input" class="col-sm-2 control-label" style="font-weight: bold;">Address2</label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control " name="addr2_input" value="{{$user->address2}}" placeholder="Address2" >
                  </div>
                </div>

                  <div class="form-group row">
                  <label for="city_input" class="col-sm-2 control-label" style="font-weight: bold;">City</label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control " name="city_input" value="{{$user->city}}" placeholder="City" >
                  </div>
                </div>

                  <div class="form-group row">
                  <label for="state_input" class="col-sm-2 control-label" style="font-weight: bold;">State</label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control " name="state_input" value="{{$user->state}}" placeholder="State" >

                  </div>

                </div>

                <div class="form-group row">
                  <label for="country_input" class="col-sm-2 control-label" style="font-weight: bold;">Country</label>
                    <div class="col-sm-10">

                  <select  class="form-control " name="country_input">
 <option value="{{$user->country}}" selected>{{$user->country}}</option>

    <option value="Afghanistan">Afghanistan</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antartica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic</option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Congo">Congo, the Democratic Republic of the</option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
    <option value="Croatia">Croatia (Hrvatska)</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="East Timor">East Timor</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia">Ethiopia</option>
    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="France Metropolitan">France, Metropolitan</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern Territories</option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-Bissau">Guinea-Bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
    <option value="Holy See">Holy See (Vatican City State)</option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran">Iran (Islamic Republic of)</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
    <option value="Korea">Korea, Republic of</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Lao">Lao People's Democratic Republic</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon">Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macau">Macau</option>
    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Micronesia">Micronesia, Federated States of</option>
    <option value="Moldova">Moldova, Republic of</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Reunion">Reunion</option>
    <option value="Romania">Romania</option>
    <option value="Russia">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
    <option value="Saint LUCIA">Saint LUCIA</option>
    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia (Slovak Republic)</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa">South Africa</option>
    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
    <option value="Span">Spain</option>
    <option value="SriLanka">Sri Lanka</option>
    <option value="St. Helena">St. Helena</option>
    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syria">Syrian Arab Republic</option>
    <option value="Taiwan">Taiwan, Province of China</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania">Tanzania, United Republic of</option>
    <option value="Thailand">Thailand</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos">Turks and Caicos Islands</option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Vietnam">Viet Nam</option>
    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Yugoslavia">Yugoslavia</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
</select>

                  </div>

                </div>

                  <div class="form-group row">

                  <label for="inputEmail3" class="col-sm-2 control-label" style="font-weight: bold;">Zip Code -  </label>



                  <div class="col-sm-10">

                    <input type="text" class="form-control " name="zip_input" value="{{$user->zip}}" placeholder="Zip Code" >

                  </div>

                </div>

                <div class="form-group row">

                  <label for="role" class="col-sm-2 control-label" style="font-weight: bold;">Role </label>
                  <div class="col-sm-10">
                 <select class="form-control" name="role_id" id="role" required >

                  @foreach($sponsor_roles as $role)
                   <option value="{{$role->id}}" @if($user->user_role == $role->id) selected @endif >{{$role->name}} </option>
                   @endforeach
                 </select>
                  </div>

                </div>


              </div>  <!-- /.card-body -->

              <div class="card-footer">   
				  <input type="hidden" name="user_id" value="{{$user->id}}">
				 
                <button type="submit" name="add_new" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px" onclick="return passvalidate()">Update</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

            </div>

          </div>


   


@endif
@endsection
