@extends('layouts.site')

@section('content')
    
<div class="col-md-12">

    
    <h1>User Edit</h1>
    <hr>
    @if($errors->any())
    @foreach($errors->all() as $error )
    <p class="alert alert-danger"></p>
    @endforeach
    @endif
    <form action="{{ route('dashboard.user.edit') }}" method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{ auth()->user()->email }}"autocomplete="off">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password" autocomplete="off" min="8">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Password Confirmation</label>
            <div class="col-sm-10">
                <input type="password" name="password_confirmation" class="form-control" id="inputPassword3" placeholder="Password Confirmation" autocomplete="off" min="8">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Avatar</label>
            <div class="col-sm-10">
                <input type="file" name="image" class="form-control" placeholder="Patient Image">
            </div>
        </div>


        @if($errors->any())
            <div class="alert alert-danger">
                {{ $errors->all()[0] }}
            </div>
        @endif

        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="form-group row">
            <div class="col-sm-10">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Update account</button>
            </div>
        </div>
    </form>
</div>

@endsection
