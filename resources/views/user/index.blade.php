@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if(auth() && auth()->user()->role_id ==  2)



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif








 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
        <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
{{$study->study_name}}

</a>
</li>
{{-- 
 <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.study.view', [$study,'English']) }}" >
{{$study->site_name}}

</a>
</li> --}}

 <li class="breadcrumb-item " aria-current="page">
	Users
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left"> View Users</h3> <a href="{{ route('dashboard.study.user.create', $study) }}" class="btn btn-primary pull-right">Create User</a>  </div>
            <div class="card-block">

                                                    
				<div class="progress">
        <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
        </div>

          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th> Name</th>
                  <th> Email</th>
                  <th> Role</th>
                  <th> Address</th>
                  <th> Phone</th>
                  <th> Country</th>
                  <th> Zip</th>
		              <th>Status</th>
                  <th>Actions</th> 
                </tr>
                </thead>
                <tbody>
              @if(isset($users))
              @foreach($users as $user)
              
              @if(!is_null($user->user))
              @if($user->user->created_by == auth()->user()->id )
              <tr>
                {{-- {{dump($user->user->study_user->role_detail->name ?? 'no')}} --}}
                <td>{{$user->fullname}}</td>
                <td>{{$user->email}}</td>
                <td>{{ isset($user->user->study_user->where('study_id' , $study->id)->first()->role_detail->name) ? $user->user->study_user->where('study_id' , $study->id)->first()->role_detail->name : '' }}  </td>
                <td>{{$user->address1}}</td>
                <td>{{$user->phone}}</td>
 		            <td>{{$user->country}}</td>
                <td>{{$user->zip}}</td>

                @if($user->is_activate == 1)
                <td><a href="{{route('dashboard.change_user_status', $user->id)}}"   style="margin:1px;color:green" title="change status">Active</a></td>
              @else
              <td><a href="{{route('dashboard.change_user_status', $user->id)}}"   style="margin:1px;color:red" title="change status">Deactivate</a></td>

              @endif
              <td>
                <a href="{{route('dashboard.study.user.edit',[ $study->id , $user->id ])}}"   style="margin:1px;" title="Update user"><i class="fa fa-edit fa-lg"></i></a>

                <a href="{{route('dashboard.study.user.delete',[ $study->id , $user->id ])}}"   style="margin:1px;" class="text-danger" title="Delete User"><i class="fa fa-trash fa-lg"></i></a>
              </td>
		                
              </tr>
              @endif
              @endif
                 @endforeach          
                 @endif         
                </tbody>
               </table>
               
            </div>
	     </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

@endif
@endsection