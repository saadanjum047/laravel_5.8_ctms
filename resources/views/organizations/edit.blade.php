@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

{{-- @if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success").then(function(){
 window.location = "{{ route('dashboard.Organizations') }}";

});


</script>

@endif --}}

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif








 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.organizations') }}" >
Organizations

</a>

</li>

<li class="breadcrumb-item" aria-current="page">
Edit Organization
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left">Edit Organization</h3>  </div>
            <div class="card-block">
    <form class="form-horizontal" method="POST" action="{{route('dashboard.organizations.update' , $org->sponsor_id)}}" enctype="multipart/form-data">  

                 {{ csrf_field() }}
                    
                 <span id="error"></span>

		  <input type="hidden"  name="id"  value="{{$org->sponsor_id}}">

 		    <input type="hidden"  name="user_id" value="{{$org->user_id}}">                    
  
                <div class="form-group row">

                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Name</label>



                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="name"  placeholder="Name" value="{{$org->sponsor_name}}" required>

                  </div>

                </div>

                <div class="form-group row">

                  <label for="lastname_input" class="col-sm-2 control-label" style="font-weight: bold;">Address</label>



                   <div class="col-sm-10">

                      <input type="text" class="form-control " name="address"  value="{{$org->sponsor_address}}" required>

                  </div>

                </div>

                <div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">City</label>



                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="city" value="{{$org->sponsor_city}}" required>

                  </div>

                </div>

                                 
                    <div class="form-group row">

                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">State</label>



                  <div class="col-sm-10">

                      <input type="text" class="form-control " name="state" placeholder="State" value="{{$org->sponsor_state}}" required>
                  </div>

                </div>
                    <div class="form-group row">
                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Phone</label>

                  <div class="col-sm-10">
                      <input class="form-control" type="text" value="{{$org->sponsor_phone}}" placeholder="phone" name="phone"  />
                  </div>

                </div>

           		{{-- <div class="form-group row">  
                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Logo</label>
                  <div class="col-sm-10">
                      <input type="file" class="form-control " name="logo"  vlaue="{{$org->sponsor_logo}}" >

                  </div>

                </div> --}}

                                 
              </div>  <!-- /.card-body -->

              <div class="card-footer">   
				  <input type="hidden" name="sponsor_id" value="">
				 
                <button type="submit" name="add_new" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px" onclick="return passvalidate()">Update</button>  

                   </div>

              <!-- /.card-footer -->

            </form>
          

                  </div>
      </div>
    </div>
</div>
    </div>
</div>
@endif
@endsection