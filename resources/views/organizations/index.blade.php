@extends('layouts.site')

@section('content')
{{-- @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2) --}}
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('success'))
  <script>
swal("Good job!", "{{$message }}", "success");

</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">
Organizations
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left"> Organizations</h3> <a href="{{ route('dashboard.organizations.create')}}" class="btn btn-primary pull-right">Create Organization</a>  </div>
            <div class="card-block">                            
				<div class="progress">
          <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
        </div>

          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
            <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th> Name</th>
                  <th> Address</th>
		  <th> City</th>
                  <th> Phone</th>
		  <th>Organization Related Operations</th>
                  <th>Actions</th>
                                  
                </tr>
                </thead>
                <tbody>
  		 @foreach($organizations as $org)                     
        <tr>
      <td>{{$org->sponsor_name}}</td>
      <td>{{$org->sponsor_address}}</td>
      <td>{{$org->sponsor_city}}</td>
      <td>{{$org->sponsor_phone}}</td>
      {{-- <td><img src="{{ asset('storage/organizations/'.$org->sponsor_logo) }}" style="width:30px"></td> --}}
 <td>
<button onclick="site_add({{$org->sponsor_id}})" class="btn btn-warning btn-sm btn-xs" style="margin: 3px;padding: 2px;" 
data-toggle="modal" data-target="#myModal"  title="Relate Site">Relate Site</button>

<a href="{{Route('dashboard.organizations.edit' , $org->sponsor_id )}}" title="Add Site " class="btn btn-primary btn-sm btn-xs" style="margin: 3px;padding: 2px;">Add Site</a>
<a href="{{Route('dashboard.viewsite',[$org->sponsor_id])}}" title="View Site " class="btn btn-success btn-sm btn-xs" style="margin: 3px;padding: 2px;">View Site</a>
</td>

	

 <td>
<a href="{{ route('dashboard.organizations.edit', $org->sponsor_id)}}" title="Update Organization" data-toggle="tooltip" data-placement="top">
<i class="fa fa-edit fa-lg"></i></a>

<a href="{{route('dashboard.organizations.remove', $org->sponsor_id )}}" title="Delete Organization"><i class="fa fa-trash fa-lg" style="color:red"></i></a>
</td>
</tr>
@endforeach               
  </tbody>
  </table>
               
            </div>
	     </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

<style>
    a.link.revisionl {
        padding-right: 5px;
        border-right: 1px solid lightgrey;
    }
</style>


<script>

 // function site_add(sid)
   // {
//alert('hey man!') ;       
   // }
</script>






<!-- Modal -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">

  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
<h4 class="modal-title">Relate Site</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      {{-- <div class="modal-body">
<form method="post" action="{{route('dashboard.relatestudy')}}">
 {{csrf_field()}}
<input type="hidden" name="sponsor_id"  id="sId">
@foreach($response->Site as $data)

      
<div class="form-group">

        <div class="checkbox">
            <label>
                <input type="checkbox" name="checkbox[]" value="{{$data->site_id}}"> {{$data->site_name}}
            </label>
        </div>
    </div>
@endforeach
      </div> --}}
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 	<button type="submit"  class="btn btn-primary">Submit</button>

      </div>
</form>
    </div>

  </div>
</div>

<script>
       function site_add(sid)
    {
	jQuery("#sId").val(sid)
   	//alert(sid);
     //jQuery.ajax({
           // url: "",
           // method: "POST",
           // type: "POST",
            //data: 'sid=' + sid,
           // success: function (response) {
            //    jQuery("#sId").val(sid);
             //   jQuery("#new_model_org").empty();
             //  jQuery("#new_model_org").html(response);
             //  jQuery("#basicModal").modal();
          //  }
       // });
    }




</script>







@endif
@endsection