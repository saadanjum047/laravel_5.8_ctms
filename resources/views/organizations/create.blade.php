@extends('layouts.site')

@section('content')
@if(auth() && auth()->user()->role_id ==2)

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif

@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">

<a href="{{ route('dashboard.organizations') }}" >
Organizations
</a>

</li>
<li class="breadcrumb-item" aria-current="page">
Create Organization
</li>

		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">

        
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
 <div class="card-header"><h3 class="pull-left">Create Organization</h3>  </div>
  <div class="card-block">
@if($errors->any())
@foreach ($errors as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif

	<form class="form-horizontal" method="POST" action="{{route('dashboard.organizations')}}" enctype="multipart/form-data">  
                 {{ csrf_field() }}
                 <span id="error"></span>
                <div class="form-group row">
                  <label for="firstname_input" class="col-sm-2 control-label" style="font-weight: bold;">Name</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control " name="name"  placeholder="Name" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="lastname_input" class="col-sm-2 control-label" style="font-weight: bold;">Address</label>
                   <div class="col-sm-10">
                      <input type="text" class="form-control " name="address"  placeholder="Address" >
                  </div>
                </div>
                <div class="form-group row">
                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">City</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control " name="city" placeholder="City" required>
                  </div>
                </div>         
                    <div class="form-group row">
                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">State</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control " name="state" placeholder="State" required>
                  </div>
                </div>       
                    <div class="form-group row">
                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Phone</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control " name="phone" placeholder="Phone" required>
                  </div>
                </div>
 		          {{-- <div class="form-group row">
                  <label for="email_input" class="col-sm-2 control-label" style="font-weight: bold;">Logo</label>
                  <div class="col-sm-10">
                      <input type="file" class="form-control " name="logo"  required>
                  </div>
                </div> --}}
              </div>  <!-- /.card-body -->
              <div class="card-footer">   
				     <input type="hidden" name="sponsor_id" value="">
				 
                <button type="submit" name="add_new" class="btn btn-info pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px" >Create</button>  

                   </div>

              <!-- /.card-footer -->

            </form>

          	    
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

@endif
@endsection