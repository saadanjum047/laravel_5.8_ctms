@extends('layouts.site')

@section('content')
@if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)

 <div class="wrapper">
    <!-- Content Header (Page header) -->
<nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
    
       
 <li class="breadcrumb-item " aria-current="page">
	Sites
</li>
		</ol>
	</nav>

    <!-- Main content -->

      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-4">
      <div class="card">
            <div class="card-block">

                                                    
				<div class="progress">
                            <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>



          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		<div class="box">
            
            <!-- /.box-header -->
            <div class="box-body "  style="overflow-x:auto;">
                            <table id="tablesite" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th> Name</th>
                                            <th> Address</th>
                                            <th> Country</th>
                                           {{-- <th> Actions</th>--}}
                                            </tr>
                                    </thead>
                                    <tbody>
					@foreach($response->Object as $data)
 					<tr>
                                            <td> {{$data->site_name}}</td>
                                            <td> {{$data->address1}}</td>
                                            <td> {{$data->country}}</td>
                                            {{--<td>  
                                              <a href="{{ route('dashboard.Registeruserbysite',[$Study,$data->site_id]) }}"   class="btn btn-sm btn-xs btn-primary" style="margin:1px;" title="Add New User">
					<i class="fa fa-plus" style="margin:3px;"></i><i class="fa fa-user" style="margin:3px;"></i></a> 
						<a href="{{ route('dashboard.editsite',[$Study,$data->site_id])}}"  style="margin:1px;" title="Update Site"><i class="fa fa-edit"></i></a>

					   </td>--}}
                                        </tr>

					@endforeach
                                        
                                    </tbody>
                                </table>
               
            		</div>
		</div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>

@endif
@endsection