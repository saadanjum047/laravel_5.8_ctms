

@extends('layouts.site')

@section('content')
@if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')==2)
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

				@if ($message = Session::get('success'))
   				<script>
swal("Good job!", "{{$message }}", "success").then(function(){
window.location="{{route('dashboard.Organizations')}}";
});
</script>

@endif

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif






     <div class="wrapper">
        <div class="card">
            <div class="card-header">

                     
                    <div class="col-md-5">
                        <h5 class="card-title mb-4"> Add New Site</h5>
                    </div>
               
                </div>
               
				
              <div class="card-body" style="margin: 20px;padding: 20px;">
			  <form class="form-horizontal" action="{{route('dashboard.postsite')}}"  method="POST">
		       {{ csrf_field() }}
                  <!--<div class="form-group row" style="display:none">
                  <label for="name" class="col-sm-2 control-label">Organization</label>

                  <div class="col-sm-4 ">
                      <select class="form-control" name="user_id" required="required">   
                          <option selected>Select a Organization</option>
 				<option value="193">Organization1</option>
 				

                                   
                      </select>  
                          
                          
                  </div>
                </div><!-- /.end-row -->
				
 	
		 <input type="hidden" name="created_by" class="form-control" value="{{$response->data[0]->uuid}}" style="display:none">
 		<input type="hidden" name="sponser_id" class="form-control" value="{{$sponsor_id}}" style="display:none">
		<input type="hidden" name="user_id" class="form-control" value="{{$sponsor_id}}" style="display:none">


                <div class="form-group row">
                    
                  <label for="name" class="col-sm-2 control-label" style="font-weight: bold;">Name </label>

                 <div class="col-sm-10">
                    <input type="site_name" name="site_name" class="form-control" id="name" placeholder="Name" required>
                </div>
               </div><!-- /.end-row -->
                 <div class="form-group row">
                    
                  <label for="name" class="col-sm-2 control-label" style="font-weight: bold;">Site Number </label>

                 <div class="col-sm-10">
                    <input type="site_nbr" name="site_nbr" class="form-control" id="site_nbr" placeholder="Site Number" required>
                </div>
               </div><!-- /.end-row -->
                    <div class="form-group row">
                  <label for="address" class="col-sm-2 control-label" style="font-weight: bold;">Address</label>

                 <div class="col-sm-10">
                    <input type="address1" name= "address1" class="form-control" id="address" placeholder="Address" required>
                  </div>
                    </div><!-- /.end-row -->
                          <div class="form-group row" >
                  <label for="address2" class="col-sm-2 control-label" style="font-weight: bold;">Address2</label>

                 <div class="col-sm-10">
                    <input type="address2" name= "address2" class="form-control" id="address2" placeholder="Address2">
                  </div>
                   </div><!-- /.end-row -->
             
                  
                         <div class="form-group row">
                  <label for="city" class="col-sm-2 control-label" style="font-weight: bold;">City</label>

                  <div class="col-sm-10">
                    <input type="city" name="city" class="form-control" id="city" placeholder="City" required>
                  </div>
                 </div><!-- /.end-row -->
						 
					<div class="form-group row">
                  <label for="state" class="col-sm-2 control-label" style="font-weight: bold;">State</label>

                 <div class="col-sm-10">
                    <input type="text" name="state" class="form-control" id="state" placeholder="State" required>
                  </div>
                 </div><!-- /.end-row -->
                  
                   <div class="form-group row">
                  <label for="country" class="col-sm-2 control-label" style="font-weight: bold;">Country</label>

                 <div class="col-sm-10">
                    <input type="country" name="country" class="form-control" id="country" placeholder="Country" required>
                  </div>
                  </div><!-- /.end-row -->
				  
				
				   <div class="form-group row">
                  <label for="country" class="col-sm-2 control-label" style="font-weight: bold;">Planned Patients</label>

                 <div class="col-sm-10">
                    <input type="number" name="planned_patients" class="form-control" id="planned_patients" placeholder="planned patients" required>
                  </div>
                  </div><!-- /.end-row -->
                   <div class="form-group row">
                  <label for="name" class="col-sm-2 control-label" style="font-weight: bold;">Patient Range </label>

                  <div class="col-sm-4">
                    <input type="pat_lo" name="pat_lo" class="form-control" id="pat_lo" placeholder="Low Range" required>
                </div>
                <div class="col-sm-4">
                    <input type="pat_hi" name="pat_hi" class="form-control" id="pat_hi" placeholder="Hi Range" required>
                </div>
                </div><!-- /.end-row -->
            
                
				 
                </div>        <!-- /.card-body -->
				   
              <div class="card-footer">
                
                <button type="submit" class="btn btn-info  pull-left" style="margin-left:15px;border-radius:0px;width:100px;padding:10px">Submit</button>
                
              </div>
              </form> 
              <!-- /.card-footer -->
             
          </div>
      </div>
	  
	  
	  
	  

@endif
@endsection









