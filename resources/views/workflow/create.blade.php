@extends('layouts.site')

@section('styles')

  <style>


  table {border-collapse:collapse; table-layout:fixed; width:310px;}
      </style>
      @endsection
@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.workflow' , $study->id )}}">Workflow</a></li>


     <li class="breadcrumb-item" aria-current="page">
  Add Workflow
  </li>
  
     </ol>
  </nav>



<div class="col-md-8 bg-white" style="padding:20px">
    
    <div>
        <h2> Create Workflow </h2>
    <form action="{{route('dashboard.study.workflow.store' , $study->id )}}" method="POST">
        @csrf
        <div class="form-group">
            <label class="col-form-label">Color</label>
            <input type="color"  id="head" name="color" value="#e66465">
        </div>

        
        <div class="form-group">
            <label class="col-form-label">Process Name</label>
            <input type="text" class="form-control" name="name" >
        </div>

        <div class="form-group">
            <label class="col-form-label">Level</label>
            <input type="number" class="form-control" name="level" >
        </div>



        <div class="form-group">
            <label class="col-form-label">Parent Process</label>
            <select class="form-control" name="parent">
                <option value="" selected> Select Parent Process </option>
                @foreach($workflows as $workflow)
                <option value="{{$workflow->workflow_id}}">{{$workflow->process_name}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
    </div>    

</div>

@endsection


@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script> 
        $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            placeholder : 'Select Parent Process',
        }
        );
        });
    </script>
@endsection