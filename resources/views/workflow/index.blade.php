@extends('layouts.site')

@section('styles')

  <style>
  table {border-collapse:collapse; table-layout:fixed; width:310px;}
      </style>
      @endsection
@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
     <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id )}}">{{$study->study_name}}</a></li>
     <li class="breadcrumb-item" aria-current="page">
  Workflow
  </li>
  
     </ol>
  </nav>



<div class="col-md-12">
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">  Workflow
         
          <a href="{{ route('dashboard.study.workflow.create' , $study->id)}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Workflow</a>

        </h3>
        </div>
      <!-- /.card-header -->
    <table class="table table-hover">
        <tr>
            <th width="5%">#</th>
            <th width="10%">Color</th>
            <th width="25%"> Process Name </th>
            <th width="25%"> Parent </th>
            <th width="10%"> created </th>
            <th width="10%"> Action </th>
        </tr>
        {{-- {{ str_limit(strip_tags($post->description), 40) }} --}}
        @foreach ($workflows as $k => $workflow)
        <tr>
          <td> {{$k+1}} </td>
            <td> {{$workflow->color}} </td>
            <td style="word-wrap:break-word;"> {{ $workflow->process_name }} </td>
            <td >@if($workflow->parent ) {{ $workflow->parent->process_name }} @endif </td>
            <td> {{($workflow->created_at)->toDateString()}} </td>
            <td> 
            <a class="text-primary" href="{{route('dashboard.study.workflow.edit' , [$study->id , $workflow->workflow_id] )}}"> <i class="fa fa-edit fa-lg"></i> </a> 
            
            <a class="text-danger" href="{{route('dashboard.study.workflow.delete' , [$study->id , $workflow->workflow_id] )}}"> <i class="fa fa-trash fa-lg"></i> </a> 
            </td>
        </tr>
        @endforeach
    </table>
      
    </div>
    <!-- /.card -->
  </div>

@endsection