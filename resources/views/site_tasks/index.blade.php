


@extends('layouts.site')


@section('content')



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
     
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.sites.users', [$study->id, $site->site_id]) }}" >{{$site->site_name}}</a></li>
     
     <li class="breadcrumb-item" aria-current="page">Group Tasks</li>
     </ol>
 </nav>

    
<div class="bg-white" style="padding:10px">
<div class="card">

    
    <div class="card-title">
        <h2>Site Tasks
        <a class="btn btn-success float-right" style="color:white" href="{{route('dashboard.study.site.task.create' , [$study->id , $site->site_id] )}}" > <i class="fa fa-plus"></i> Add New Task</a></h2>
    </div>
    <table class="table">
        <tr> 
            <th> # </th>
            <th> Task Name </th>
            <th> Task Description </th>
            <th> No. of Users </th>
            <th> Created </th>
            <th> Actions </th>
        </tr>

        @foreach ($tasks as $k=> $task)
            
        <tr>
            <td> {{$k+1}} </td>
            <td> {{$task->name}} </td>
            <td> {{ str_limit(strip_tags($task->description) , 80)}} </td>
            <td> {{$task->investigators->count()}} </td>
            <td> {{$task->created_at}} </td>
            <td> 
                <a href="{{route('dashboard.study.site.task.edit' , [$study->id , $site->site_id ,  $task->id ])}} " class="text-primary"> <i class="fa fa-edit fa-lg"></i> </a> |
                <a href="{{route('dashboard.study.site.task.delete' , [$study->id  , $site->site_id , $task->id ])}} " class="text-danger"> <i class="fa fa-trash fa-lg"></i> </a> |
                <a href="{{route('dashboard.study.site.task.details' , [$study->id , $site->site_id , $task->id ])}} " class="text-success"> <i class="fa fa-info-circle fa-lg"></i> </a>
            </td>
        </tr>
        @endforeach
    </table>

</div>
</div>
@endsection