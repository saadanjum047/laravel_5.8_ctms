

@extends('layouts.site')

@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.view', [$study->id,'English']) }}" >{{$study->study_name}}</a></li>
     <li class="breadcrumb-item active" aria-current="page">
     <a href="{{ route('dashboard.study.site.tasks', [$study->id , $site->site_id ]) }}" >Tasks</a></li>

     <li class="breadcrumb-item" aria-current="page">Update Tasks</li>
     </ol>
 </nav>


    <div class="bg-white">
        <div class="card">

@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif

            <div class="card-header" style="margin-bottom:20px">
                <h3 class="card-title"> Update Task </h3>
            </div>

            <form action="{{route('dashboard.study.site.task.update' ,[ $study->id , $site->site_id , $task->id])}} " method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}

        <div class="form-group row text-center">
            <label class="col-md-2 col-form-label"> Task Name: </label>  
            <div class="col-md-8">
                <input type="text" style="border-color:#aaa" class="form-control" placeholder="enter the name of the task" name="name" value="{{$task->name}}" >  
            </div>
        </div>
        <div class="form-group row text-center">
            <label class="col-md-2 col-form-label"> Task Description: </label>  
            <div class="col-md-8">
                <textarea rclass="form-control"  id="summernote" style="border-color:#aaa"  placeholder="Enter some description" name="description"> {{$task->description}} </textarea>  
            </div>
        </div>


        @php
        $ids = $task->investigators->pluck('id')->toArray();
        //dd( $ids , $task->patients);
        @endphp

        <div class="form-group row text-center">
            <label class="col-md-2 col-form-label"> Investigators: </label>  
            <div class="col-md-8">
                <select  class="js-example-basic-multiple form-control" name="investigators[]" multiple="multiple">

                    @foreach ($investigators as $investigator)
                    <option value="{{$investigator->id}}" @if(in_array(  $investigator->id , $ids ) ) selected @endif > {{$investigator->user->name}} </option>
                @endforeach    
                </select>  
            </div>
            
        </div>
        <div class="form-group row text-center">
            <label class="col-md-2 col-form-label"> Documnet: </label>  
            <div class="col-md-8">
              <input type="file" name="document" class="form-control" />
              {{$task->document ?? ''}}
            </div>
        </div>

            <div class="col-md-10">
            <button type="submit" style="margin-bottom:20px" class="btn btn-success float-right">Update Task</button>
            </div>
    </form>

    </div>
    </div>

@endsection



@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script> 
        $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            placeholder : 'Select Users',
        });
        });
    </script>
@endsection