@extends('layouts.site')

@section('content')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $question->diary->study->id )}}"> {{$question->diary->study->study_name}} </a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary.questions' , [$question->diary->study->id , $question->diary->id] )}}"> Diary Questions </a></li>
   <li class="breadcrumb-item" aria-current="page">
Diary Questions
</li>

   </ol>
</nav>

<div class="bg-white" style="padding:20px">
    <h3 style="color:#1991eb"> {{$question->question}} </h3> <br>
    @if($question->answer)
    <h6> {{$question->answer->answers}} </h6>
    @endif


    

    @if($question->answer && auth()->user()->sponsor_user->is_coordinator == 1)

    @php
        $vote = $question->answer->votes->where('user_id' , auth()->user()->id )->where('answer_id' , $question->answer->id )->first();
    @endphp

    {{--  {{dd($question->answer->comment->where('user_id' , auth()->user()->id)->toArray())}}      --}}
@php
if(!empty($question->answer->comment)){
$user_comment = $question->answer->comment->where('user_id' , auth()->user()->id)->first();
}

@endphp

    <hr>
    <div class="row ">
    <div class="col-md-1 text-center" >
        <div class="text-center"><h3 id="total_votes"> {{$question->answer->votes->count() ?? 0 }}  </h3> </div>
        <div style="margin-left:35px ;">
        <h1>    
        {{-- <div class="row"> <a href="{{route('dashboard.diary.question.vote' , [$question->diary->id , $question->id , $question->answer->id ] )}}" @if(isset($vote)) class="text-success" @endif > <i class="fa fa-caret-up"></i> </a>  </div> --}}
        
        <div class="row"> <a href="javascript:;" id="up_btn" onclick="voteUp()"  @if(isset($vote)) class="text-success" @endif > <i class="fa fa-caret-up"></i> </a>  </div>

        {{-- <div class="row">  <a href="{{route('dashboard.diary.question.unvote' , [$question->diary->id , $question->id , $question->answer->id ] )}}" style="color:#343a40" ><i class="fa fa-caret-down"></i> </a> </div> --}}
        
        <div class="row">  <a href="javascript:;" onclick="voteDown()" style="color:#343a40" ><i class="fa fa-caret-down"></i> </a> </div>

        </h1>
        </div>
    </div>
    
    <div class="col-md-10">
    <form action="@if(isset($user_comment)) {{route('dashboard.diary.question.comment.update' , [$question->diary->id , $question->id , $user_comment->id ])}}
        @else {{route('dashboard.diary.question.comment' , [$question->diary->id , $question->id])}} @endif" method="POST">
        {{ csrf_field() }}
    {{--  <input type="checkbox" id="flag" name="flag" value="1" @if(isset($question->answer->flag)) {{"checked"}} @endif >
    <label for="flag">Flag</label>
      --}}
        
        {{--  {{dd($question->answer->flag)}}  --}}
      <h2  class="float-right"> <a href="javascript:;" style="@if( isset($question->answer->flag )) color:red; @else color:rgb(0, 0, 0)@endif" id="flags"  class="fa fa-flag fa-lg" onclick="flag()" ></a> </h2>

      {{-- <h2  class="float-right"> <i style="@if( isset($question->answer->flag) && $question->answer->flag->user_id == auth()->user()->id ) color:red; @else color:rgb(0, 0, 0)@endif" id="flags"  class="fa fa-flag fa-lg" onclick="change_color()" ></i> </h2>
       --}}
      <input type="hidden" id="flag_val" name="flag" @if(isset($question->answer->flag)) value="1" @else value="0" @endif />


      @php
      @endphp


    <div class="form-group">
        <label class=""> Comment </label>
        <textarea class="form-control" name="comment" placeholder="Your Comment here">@if(isset($user_comment) && $user_comment->user_id == auth()->user()->id ) {{$user_comment->comment}}@endif</textarea>
    </div>
    <div>
        @if(isset($user_comment) && $user_comment->user_id == auth()->user()->id ) 
        <button class="btn btn-primary">Update</button>
        <a href="{{route('dashboard.diary.question.comment.delete' , [$question->diary->id , $question->id , $user_comment->id ])}}" class="btn btn-danger">Delete</a>
        @else
        <button class="btn btn-success">Save</button>
        @endif
    </div>
    </form>
</div>
</div>
    @endif
</div>

@if(!is_null($question->answer))

    <script>
        function change_color(){
            var color = $("#flags").css('color');
            console.log(color);
            if( $("#flags").css('color') == 'rgb(0, 0, 0)' ){
                $('#flags').css("color", "red");
                $('#flag_val').val(1);

            }else{
                $('#flags').css("color", "black");
                $('#flag_val').val(0);

            }
        }


        function voteUp(){

            $.ajax({
                url : "{{route('dashboard.diary.question.vote' , [$question->diary->id , $question->id , $question->answer->id ] )}}",
                success:function(response){
                    //console.log(response);
                    if(response === "Already Voted"){
                        swal("Sorry!", "You have voted already", "error");
                    }else{
                    $('#total_votes').html(response);
                    $('#up_btn').css('color' , '#2a8837' );
                    swal("Done!", "You have voted successfully", "success");
                    }
                }
            });
        }

        function voteDown(){

            $.ajax({
                url : "{{route('dashboard.diary.question.unvote' , [$question->diary->id , $question->id , $question->answer->id ] )}}",
                success: function(response){
                    //console.log(response);
                    if(response === "You have not voted"){
                        swal("Sorry!", "You have not voted", "error");
                    }else{
                    $('#total_votes').html(response);
                    $('#up_btn').css('color' , 'black' );
                    swal("Done!", "Unvoted successfully", "success");
                    $('#up_btn').css('color' , '#1991eb' );
                    //console.log('color should change');
                }
            }
            });
        }

        
        function flag(){

            $.ajax({
                url : "{{route('dashboard.flaging' , $question->answer->id )}}",
                success: function(response){
                    //console.log(response);
                    if(response === "Answer is flagged"){
                        swal("Done!", "Answer is flagged", "success");
                        $('#flags').css("color", "red");

                    }if(response === "Answer is Not flagged"){
                        swal("Done!", "Answer is Not flagged", "success");
                        $('#flags').css("color", "black");
                  
                    
                }
            }
            }); 
        }

        
    </script>
@endif

@endsection
