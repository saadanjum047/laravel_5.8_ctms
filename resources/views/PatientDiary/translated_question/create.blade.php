@extends('layouts.site')


@section('content')

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $real_question->diary->study->id )}}">{{$real_question->diary->study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary' , $real_question->diary->study->id )}}">Diaries</a></li>

   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary.questions' , [$real_question->diary->study->id , $real_question->diary->id] )}}">{{$real_question->diary->name}} Questions</a></li>
   
   @if(isset($question))
   <li class="breadcrumb-item"><a href="{{route('dashboard.study.diary.questions' ,[ $real_question->diary->study->id , $real_question->diary->id] )}}">Diary Questions</a></li>
   @endif

   <li class="breadcrumb-item"><a href="{{route('dashboard.study.diary.question.versions' ,[ $real_question->diary->study->id , $real_question->diary->id , $real_question->id ] )}}">Translated Questions</a></li>
   
   <li class="breadcrumb-item" aria-current="page"> 
    @if(isset($question))
    Update Question
    @else
    Question Version
    @endif
</li>

   </ol>
</nav>

@if($errors->any())
@foreach ($errors->all() as $error)
    <p class="alert alert-danger"> {{$error}} </p>
@endforeach
@endif

<div class="bg-white" style="padding:20px">
      
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif

    <h2> @if(isset($question )) Update Question  @else
         Create Question @endif </h2>
    <form action="@if(isset($question )) {{route('dashboard.study.diary.question.version.update' , [ $study->id , $diary->id , $real_question , $question->id] )}}  @else
    {{route('dashboard.study.diary.question.version.store' , [ $real_question->diary->study->id , $real_question->diary->id , $real_question->id] )}} @endif" method="post">
        {{ csrf_field() }}

        <input type="hidden" name="diary_id" value="{{$real_question->diary->id}}" > 
      <div class="modal-body">
          <div class="form-group">
            <label for="question" class="col-form-label">Question:</label>
            <input type="text" name="question" class="form-control" id="question" @if(isset($question )) value="{{$question->question}}"  @endif placeholder="Question" required />
          </div>
          {{--  <div class="form-group">
            <label for="question" class="col-form-label">Notify me Answer (optional):</label>
            <input type="text" name="notify" class="form-control" id="question" @if(isset($question )) value="{{$question->notification_response}}"  @endif placeholder="Notify me Answer (optional)" >
          </div>  --}}
          <div class="form-group">
            <label for="answer_type" class="col-form-label">Answer Type:</label>
            <select name="answer_type"  class="form-control" id="answer_type">
            <option @if(isset($question->answer_type) && $question->answer_type == 1)) {{"selected"}} @endif value="1">Text</option>  
            <option @if(isset($question->answer_type) && $question->answer_type == 2)) {{"selected"}} @endif value="2">Radio Buttons</option>  
            <option @if(isset($question->answer_type) && $question->answer_type == 3)) {{"selected"}} @endif value="3">Check Boxes</option>  
            </select> 
          </div>
         

          <div class="row">
          <button class="btn btn-primary offset-11"  @if(!isset($question ))style="display:none" @elseif(isset($question) && $question->answer_type == 1)) style="display:none" @else {{"selected"}} @endif type="button" id="add_div" ><i class="fa fa-plus"></i> </button></div>
          
            {{--  {{dd($question->specifications)}}  --}}
        <div id="answers">
          @php
              if(isset($question->integer)){
                $integers = trim(str_replace(array( '[' , ']' , '"' ) , '' , $question->integer ));
                $integerArray = explode(',' , $integers );
                
              }
          @endphp

          @if(isset($question ))  
          @if(isset($question->answers))
        @php
        $i = 0;
        $tagsArray =  explode( ',' , $question->answers);
        
        @endphp
        @if($question->specifications->count() > 0 )
        @php
            
        $specs = $question->specifications->pluck('name')->toArray(); 

              
        @endphp
        @endif

        @foreach( $tagsArray as $k=> $ans) 
        @php
        $i++;
        @endphp
            <div class="row" id="ans{{$k}}">
            <div class="form-group col-md-4" style="display:inline-block">
            <lable class="col-form-label">Choice{{$k+1}}</lable> 
              <input type="text"  class="form-control" name="answers[]" value={{trim(str_replace(array( '[' , ']' , '"' , '{' , '}' ) , ''  , $ans ))}} onchange="update_radio(this.id)" id="choice{{$k}}" /> </div>  
              

              <div class="form-group col-md-2" style="display:inline-block;"><lable class="col-form-label">Score </lable> <input type="number" class="form-control" name="integer[]" value="{{$integerArray[$k] ?? NULL}}">  </div>

            <div class="col-md-2" style="margin-top:20px" >  
              <input type="checkbox" id="super{{$k}}" name="specifications[]" onchange="check_value(this.id)" @if(isset($specs)) @if(in_array( trim(str_replace(array( '[' , ']' , '"' , '{' , '}' ) , '' , $ans )) , $specs ) ) checked value="{{trim(str_replace(array( '[' , ']' , '"' , '{' , '}' ) , '' , $ans ))}}" @endif @endif   /> 
              <label class="col-form-label" for="super{{$k}}">Specification</label></div> 
            
            <div class="col-md-2" style="margin-top:20px" >  
              <input type="radio" name="notify" id="{{$k}}" onchange="radio_value(this.id)" @if(isset($question->notification_response) && ($question->notification_response  == trim(str_replace(array( '[' , ']' , '"' , '{' , '}' ) , '' , $ans ))) ) checked value={{trim(str_replace(array( '[' , ']' , '"'  , '{' , '}') , '' , $ans ))}} @endif  /> 
              <label class="col-form-label" for="{{$k}}">Notification</label></div> 
            
            <div class="col-md-1 float-right"> 
              <button type="button" style="margin-left:10px; margin-top:20px" class="btn btn-danger float-right" id="ans{{$k}}" onclick="div_delete(this.id)" ><i class="fa fa-minus" ></i> </button></div> </div>
            
            @endforeach

          @endif 
          @endif
        </div>
          

      <div class="form-group">
        <select class="form-control" name="lang" required >
            <option value="" selected> Select Language </option>
            <option value="Afrikaans">Afrikaans</option> 
            <option value="Albanian">Albanian</option> 
            <option value="Amharic">Amharic</option> 
            <option value="Arabic">Arabic</option> 
            <option value="Armenian">Armenian</option> 
            <option value="Azerbaijani">Azerbaijani</option>
            <option value="Bengali">Bengali</option> 
            <option value="Bosnian">Bosnian</option>
            <option value="Bulgarian">Bulgarian</option> 
            <option value="Catalan">Catalan</option>
            <option value="Chinese">Chinese</option>
            <option value="Czech">Czech</option>
            <option value="Danish">Danish</option>
            <option value="Dutch">Dutch</option>
            <option value="English" selected>English</option>
            <option value="Esperanto">Esperanto</option>
            <option value="Estonian">Estonian</option>
            <option value="Finnish">Finnish</option>
            <option value="French">French</option>
            <option value="Georgian">Georgian</option>
            <option value="German">German</option>
            <option value="Scottish Gaelic">Scottish Gaelic</option>
            <option value="Greek, Modern">Greek</option>
            <option value="Hebrew">Hebrew</option>
            <option value="Hindi">Hindi</option>
            <option value="Croatian">Croatian</option>
            <option value="Hungarian">Hungarian</option>
            <option value="Icelandic">Icelandic</option>
            <option value="Indonesian">Indonesian</option>
            <option value="Italian">Italian</option>
            <option value="Japanese">Japanese</option>
            <option value="Kinyarwanda">Kinyarwanda</option>
            <option value="Korean">Korean</option>
            <option value="Latin">Latin</option>
            <option value="Latvian">Latvian</option>
            <option value="Lithuanian">Lithuanian</option>
            <option value="Macedonian">Macedonian</option>
            <option value="Malayalam">Malayalam</option>
            <option value="Maori">Maori</option>
            <option value="Malay">Malay</option>
            <option value="Norwegian">Norwegian</option>
            <option value="Iranian">Persian, Iranian</option>
            <option value="Polish">Polish</option>
            <option value="Portuguese">Portuguese</option>
            <option value="Pushto">Pushto</option>
            <option value="Romanian">Romanian, Rumanian, Moldovan</option>
            <option value="Russian">Russian</option>
            <option value="Sanskrit">Sanskrit</option>
            <option value="Slovak">Slovak</option>
            <option value="Slovenian">Slovenian</option>
            <option value="Spanish">Spanish</option>
            <option value="Serbian">Serbian</option>
            <option value="Swedish">Swedish</option>
            <option value="Thai">Thai</option>
            <option value="Turkish">Turkish</option>
            <option value="Ukrainian">Ukrainian</option>
            <option value="Urdu">Urdu</option>
            <option value="Vietnamese">Vietnamese</option>
            <option value="Welsh">Welsh</option>
        </select>
      </div>
    </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-success">@if(isset($question )) Update Question @else 
            Add Question @endif</button>
      </div>
    </form>

</div>

@endsection


@section('scripts')
    

<script>

  $('#answer_type').change( function (){
    var select = $('#answer_type').val();
    if(select == 2 || select == 3){
     $('#add_div').show();
    }
    if(select == 1){
      $('#add_div').hide();
      $('#answers div').remove();
    }
    console.log(select);

  } );
 
  var i = @if(isset($question) && isset($question->answers) )  {{$i+1}} @else 1  @endif;
  $("#add_div").click( function (){
    console.log('cehck');
    $('#answers').append( '  <div class="row"  id="ans'+i+'"> <div class="form-group col-md-4" style="display:inline-block;"  > <lable class="col-form-label">Choice '+i+' </lable> <input type="text"  class="form-control" name="answers[]" id="choice'+i+'" onchange="update_radio(this.id)"  /> </div> <div class="form-group col-md-2" style="display:inline-block;"><lable class="col-form-label">Score </lable> <input type="number" class="form-control" name="integer[]">  </div> <div class="col-md-2" style="margin-top:20px" >  <input type="checkbox" id="super'+i+'" name="specifications[]" onchange="check_value(this.id)" /> <label class="col-form-label" for="super'+i+'">Specification</label></div> <div class="col-md-2" style="margin-top:20px" >  <input type="radio" name="notify" id="'+i+'" onchange="radio_value(this.id)"  /> <label class="col-form-label" for="'+i+'">Notification</label></div> <div class="col-md-1 float-right">  <button type="button" style="margin-left:10px; margin-top:20px" class="btn btn-danger " id="ans'+i+'"  onclick="div_delete(this.id)" ><i class="fa fa-minus" ></i> </button> </div></div>');
    i = i + 1;
  } );

  function div_delete(id){
    $('#'+id).remove();
  }
  function radio_value(id){
    console.log(id);
    var value ;
     value = $('#choice'+id).val();
     console.log(value);
     $('#'+id).val(value);
  }

  function check_value(id){
    console.log('check');
    var value;
    id = id.replace(/\D/g, "");
    value = $('#choice'+id).val();
    $('#super'+id).val(value);
    
  }

  function update_radio(id){
    console.log('radio');

    id = id.replace(/\D/g, "");
    $('#'+id).val($('#choice'+id).val());
    if($('#super'+id).prop('checked') == true){
      $('#super'+id).val($('#choice'+id).val())
      console.log('cehcked');
    }
    console.log(id);
  }
  


  </script>



@endsection