@extends('layouts.site')


@section('content')


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $study->id)}}"> {{$study->study_name}} </a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary' , $diary->study->id)}}"> {{$diary->diary_name}} </a></li>

   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary.questions' , [$study->id , $diary->id  ])}}"> Questions </a></li>
   <li class="breadcrumb-item" aria-current="page">
Translated Questions
</li>

   </ol>
</nav>


<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}


    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Translated Questions of the {{$question->question}}
          
          <a href="{{ route('dashboard.study.diary.question.version.create' , [$study->id , $diary->id  , $question->id ] )}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Another Version </a>

        </h3>
        </div>

<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Question</th>
    <th >Language</th>
    <th >Answers</th>
    <th >Answer Type</th>
    <th>Notification Response</th>
    <th>Created</th>
    <th>Action</th>
</tr>

@if(isset($question->versions))
@foreach ($question->versions as $k => $question)
<tr>
  {{--  {{ dd($question->diary) }}  --}}
    <td>{{$k+1}} </td>

  
    <td >{{$question->question}} </td>
    <td >{{$question->language}} </td>

    {{--  <td > <a href="{{route('dashboard.diary.questions' , [$question->diary->id , $question->id]  )}}"> {{$question->question}} </a> </td>  --}}
       <td> @if(isset($question->answers))
      @php
      $tagsArray =  explode( ',' , $question->answers);
      @endphp
      @foreach( $tagsArray as $ans) 
      {{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} , 
      @endforeach
     @endif </td>
  
    <td> @if($question->answer_type == 1) {{ 'Text' }} @elseif ( $question->answer_type == 3 ) {{'Checkboxes'}} @elseif($question->answer_type == 2) {{'RadioButtons'}} @else {{''}} @endif </td>
    <td> {{$question->notification_response}} </td>
    <td> {{(($question->created_at)->toDateString())}} </td>
  
    <td> 
      
      <a href="{{route('dashboard.study.diary.question.version.remove' , $question->id)}}"> <i class="text-danger fa fa-trash fa-lg"></i></a> |
      
      <a class="text-primary" href="{{route('dashboard.study.diary.question.version.edit' , [ $question->base_question->diary->study->id , $question->base_question->diary->id ,  $question->base_question->id , $question->id ]) }}" > <i class="fa fa-edit fa-lg" ></i> </a> 
     </td>
</tr>

@endforeach
@endif
</table>

</div>
</div>





@endsection

