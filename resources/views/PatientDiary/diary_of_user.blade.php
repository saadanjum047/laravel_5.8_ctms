@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view', [$diary->study->id , 'English'] )}}">{{$diary->study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary', $diary->study->id )}}">{{$diary->diary_name}} </a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary.patients', [$diary->study->id , $diary->id ] )}}">Patients</a></li>
   <li class="breadcrumb-item" aria-current="page">
Diary Questions
</li>

   </ol>
</nav>

  
  
<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Patient Diary
        </div>
        
<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Name</th>
    <th >Frequency</th>
   
</tr>
@php
$i = 1;
@endphp
@for ($i = 1; $i <= $diary->frequency; $i ++)
<tr>
    <td> {{$i}} </td>
    <td>
        <a href="{{route('dashboard.study.diary.patient.frequency.questions' , [$diary->study->id , $diary->id , $patient->id , $i])}}" >
        {{$diary->diary_name}} </a> </td>
    <td>  
        @php
        //$numberFormatter = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
        @endphp 
        {{-- {{$numberFormatter->format($i)}} --}}
        {{$i}}
    </td>
</tr>

@endfor
</table>

</div>
</div>





@endsection

