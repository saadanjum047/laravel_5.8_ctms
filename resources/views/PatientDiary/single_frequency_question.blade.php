@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view', [$diary->study->id , 'English'] )}}">{{$diary->study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary', $diary->study->id )}}">{{$diary->diary_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary.patients', [$diary->study->id , $diary->id ] )}}">Patients</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary.patients.diaries', [$diary->study->id , $diary->id , $patient->id ] )}}">Frequency</a></li>
   <li class="breadcrumb-item" aria-current="page">
Diary Questions
</li>

   </ol>
</nav>

{{--  
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view', [$diary->study->id , 'English'] )}}">{{$diary->study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary', $diary->study->id )}}">Diaries</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary.patients', [$diary->study->id , $diary->id ] )}}">Patients</a></li>
   <li class="breadcrumb-item" aria-current="page">
Diary Questions
</li>

   </ol>
</nav>  --}}


<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}


    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Questions of the {{$diary->diary_name}} Diary
{{-- 
            @if(auth()->user()->role_id == 3)
            @if( ! empty( auth()->user()->study_user->role_detail->permissions->where('name' , 'create')->where('module_id' , 16 )->toArray() ) )

          <a href="{{ route('dashboard.study.diary.create' , $study->id)}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Question</a>
          
          @endif
          @elseif(auth()->user()->role_id == 2)
          <a href="{{ route('dashboard.study.diary.create' , $study->id)}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Question</a>


          @endif --}}

        </h3>
        </div>
        
<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Question</th>
    <th >Answers</th>
    {{-- <th >Patitent Answers</th> --}}
    <th >Answer Type</th>
    <th>Notification Response</th>
    <th>Created</th>
    {{-- <th>Action</th> --}}
</tr>
@php
$i = 1;
@endphp
@foreach ($diary->questions as $question)
<tr>
  {{--  {{ dd($question->diary) }}  --}}
    <td>{{$i}} </td>

     <td >
     
       {{-- <a href="{{route('dashboard.diary.questions' , [$question->diary->id , $question->id]  )}}"> {{$question->question}} </a>  --}}
       <a href="{{route('dashboard.study.diary.patient.frequency.question.answer' , [ $question->diary->study->id , $question->diary->id , $patient->id , $freq , $question->id]  )}}"> {{$question->question}} </a> 
      </td>


    {{--  <td > <a href="{{route('dashboard.diary.questions' , [$question->diary->id , $question->id]  )}}"> {{$question->question}} </a> </td>  --}}
       <td> @if(isset($question->answers))
      @php
      $tagsArray =  explode( ',' , $question->answers);
      @endphp
      @foreach( $tagsArray as $ans) 
      {{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} , 
      @endforeach
     @endif </td>
    {{-- <td> 
    @if(isset($question->answer->answers))
    @php
    $arr = explode( ',' , trim(str_replace(array( '[' , ']' , '"' ) , '' , $question->answer->answers )));
    foreach($question->answer->specifications as $spec){
        $specs[] = $spec->specification;
    }
    @endphp
    @foreach ($arr as $k=> $a)
        {{$a}}
        <br>
        @foreach($question->answer->specifications as $spec)
        @if($a == $spec->name)
        <p class="alert"> {{$spec->specification}} </p>
        @endif
        @endforeach
    @endforeach   
    
     </h6>
    @endif
    </td> --}}
    <td> @if($question->answer_type == 1) {{ 'Text' }} @elseif ( $question->answer_type == 3 ) {{'Checkboxes'}} @elseif($question->answer_type == 2) {{'RadioButtons'}} @else {{''}} @endif </td>
    <td> {{$question->notification_response}} </td>
    <td> {{($question->date_created)}} </td>
  
    {{-- <td> 
      
      @if(auth()->user()->role_id == 3)
      @if( ! empty( auth()->user()->study_user->role_detail->permissions->where('name' , 'delete')->where('module_id' , 16 )->toArray() ) )
      
      <a href="{{route('dashboard.patientdiary.remove' , $question->id )}}"> <i class="text-danger fa fa-trash fa-lg"></i></a> 

      @endif
      @elseif(auth()->user()->role_id == 2)
      <a href="{{route('dashboard.patientdiary.remove' , $question->id )}}"> <i class="text-danger fa fa-trash fa-lg"></i></a> 

      @endif




      @if(auth()->user()->role_id == 3)
      @if( ! empty( auth()->user()->study_user->role_detail->permissions->where('name' , 'update')->where('module_id' , 16 )->toArray() ) )

      | <a href="{{route('dashboard.study.diary.edit',[ $question->diary->study->id , $question->diary->id ,  $question->id] )}} " class="text-primary" > <i class="fa fa-edit fa-lg" ></i> </a> 

      @endif
      @elseif(auth()->user()->role_id == 2)

      | <a href="{{route('dashboard.study.diary.edit',[ $question->diary->study->id , $question->diary->id ,  $question->id] )}} " class="text-primary" > <i class="fa fa-edit fa-lg" ></i> </a> 
      @endif --}}

{{--  
      <a href="{{route('dashboard.patientdiary.remove' , $question->id )}}"> <i class="text-danger fa fa-trash fa-lg"></i></a>   --}}
      {{--  | <a href="{{route('dashboard.study.diary.edit',[ $question->diary->study->id , $question->diary->id ,  $question->id] )}} " class="text-primary" > <i class="fa fa-edit fa-lg" ></i> </a>  --}}
     {{-- </td> --}}
</tr>
@php
$i += 1;
@endphp
@endforeach
</table>

</div>
</div>





@endsection

