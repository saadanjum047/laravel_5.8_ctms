@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view', [$diary->study->id , 'English'] )}}">{{$diary->study->study_name}}</a></li>
   <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diary', $diary->study->id )}}">{{$diary->diary_name}}</a></li>
   <li class="breadcrumb-item" aria-current="page">
Diary Questions
</li>

   </ol>
</nav>


<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Patient Diary
        </div>
        
<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Name</th>
    <th >Email</th>
    <th >Study</th>
   
</tr>
@php
$i = 1;
@endphp
@foreach ($patients as $k => $patient)
<tr>
    <td> {{$k+1}} </td>
    <td> 
      <a href="{{route('dashboard.study.diary.patients.diaries' ,  [$diary->study->id , $diary->id , $patient->id] )}}" >
      {{$patient->user->name}} </a> </td>
    <td> {{$patient->user->email}} </td>
    <td> {{$patient->study->study_name}} </td>
</tr>
@php
$i += 1;
@endphp
@endforeach
</table>

</div>
</div>





@endsection

