@extends('layouts.site')


@section('content')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item" aria-current="page">
Diary Questions
</li>

   </ol>
</nav>


<div class="bg-white">
{{--   
@if(session()->has('success'))
<p class="alert alert-primary"> {{session('success')}} </p>
@endif
@if(session()->has('error'))
<p class="alert alert-danger"> {{session('error')}} </p>
@endif --}}
    <div class="card">
        <div class="card-header bg-white">         
          <h3 class="card-title">Patient Diary
          <a href="{{ route('dashboard.patientdiary.create')}} " style="float:right" class="btn btn-success"  ><i class="fa fa-plus"></i> Add Question</a></h3>
        </div>
        
<table class="table table-hover">
<tr>
    <th>#</th>
    <th >Question</th>
    <th >Answers</th>
    <th >Diary</th>
    <th>Notification Response</th>
    <th>Created</th>
    <th>Action</th>
</tr>
@php
$i = 1;
@endphp
@foreach ($questions as $question)
<tr>
  {{--  {{ dd($question->diary) }}  --}}
    <td>{{$i}} </td>
    <td >{{$question->question}} </td>
       <td> @if(isset($question->answers))
      @php
      $tagsArray =  explode( ',' , $question->answers);
      @endphp
      @foreach( $tagsArray as $ans) 
      {{trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans ))}} , 
      @endforeach
     @endif </td>
    <td> @if(isset($question->diary)) {{$question->diary->diary_name}} @endif </td>
    <td> {{$question->notification_response}} </td>
    <td> {{($question->date_created)}} </td>
  
    <td> <a href="{{route('dashboard.patientdiary.remove' , $question->id )}}"> <i class="text-danger fa fa-trash fa-lg"></i></a> | <a href="{{route('dashboard.patientDiary.edit', $question->id )}} " class="text-primary" > <i class="fa fa-edit fa-lg" ></i> </a> </td>
</tr>
@php
$i += 1;
@endphp
@endforeach
</table>

</div>
</div>





@endsection

