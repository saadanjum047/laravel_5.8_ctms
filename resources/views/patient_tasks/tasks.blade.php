


@extends('layouts.site')


@section('content')



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif



<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>    
     
      <li class="breadcrumb-item" aria-current="page">Tasks</li>
     </ol>
 </nav>

    
<div class="bg-white" style="padding:10px">
<div class="card">

    
    <div class="card-title">
        <h2>Tasks</h2>
    </div>
    <table class="table">
        <tr> 
            <th> # </th>
            <th> Task Name </th>
            <th> Task Description </th>
            <th> Completed </th>
            <th> Created </th>
            <th> Action </th>
        </tr>

        @foreach ($tasks as $k=> $task)
        <tr>    
            <td> {{$k+1}} </td>
            <td> <a href="{{route('dashboard.group.tasks.view' , $task->id  )}} "> {{$task->name}} </a> </td>
            <td> {{ str_limit(strip_tags($task->description) , 80)}} </td>

            <td> @if(auth()->user()->patient->tasks->find($task->id)->pivot->status == 1) {{auth()->user()->patient->tasks->find($task->id)->pivot->completed_at }} @endif </td>

            <td> {{($task->created_at)->toDateString()}} </td>

            <td> @if(auth()->user()->patient->tasks->find($task->id)->pivot->status == 1) <a onclick="show_marked()">&#9989; </a> @else <a href="{{route('dashboard.mark.task' , $task->id)}}" class="text-primary"> &#9989; </a> @endif </td>
        </tr>
        @endforeach
    </table>

</div>
</div>


<script>
    function show_marked(){
        swal("Sorry!", "You have marked that task completed", "error");

    }
</script>

@endsection