@extends('layouts.site')

@section('content')
    
<div class="row mb-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-2">
                        <h5 class="card-title mb-4">Patients List</h5>
                    </div>
                </div>
                <div class="table-responsive" id="patients-list">
                    <table class="table center-aligned-table">
                        <thead>
                        <tr class="text-primary">
                            @if(Cookie::get('user_role') == 2)
                                <th>Patient Nbr</th>
                            @else
                                <th>No</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Created</th>
                                <th>Date Confirmed</th>
                                <th>Manage</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>


 
 
 
                        @foreach($patients as $patient)
                       
                            <tr class="">
                                {{--  @if(Cookie::get('user_role') == 2)  --}}
                                <td>{{ $patient->id }}</td>
                                <td><a href="{{ route('dashboard.patient.show', [ $study->id , $patient->id] ) }}">{{ $patient->user->name}}</a></td>
                                <td>{{ $patient->phone }}</td>
                                <td>{{ $patient->user->email }}</td>
                                <td>{{ $patient->created_at->toDateString() }}</td>
                                <td> @if(isset($patient->confirmation_date)) {{ $patient->confirmation_date->toDateString() }} @endif </td>
                                <td><a href="{{ route('dashboard.patient.show', [ $study->id , $patient->id]) }}"
                                        class="btn btn-primary btn-sm">Manage</a></td>
                                {{--  @endif  --}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @empty($patients)
                    Patients Not Found!
                @endempty
            </div>
        </div>
    </div>
</div>

@endsection
