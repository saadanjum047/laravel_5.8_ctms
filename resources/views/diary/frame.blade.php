@extends('layouts.site')

@section('content')
    {!! $content !!}
@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {
            var csrf_token = $('meta[name="csrf-token"]').attr('content');
            $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                if (options.type.toLowerCase() === "post") {
                    // initialize `data` to empty string if it does not exist
                    options.data = options.data || "";

                    // add leading ampersand if `data` is non-empty
                    options.data += options.data ? "&" : "";

                    // add _token entry
                    options.data += "_token=" + encodeURIComponent(csrf_token);
                }
            });

            $('.save-text').click(function(){
                $(this).parent().parent().find('.answer').trigger('click');
                alert('Text successfully saved!');
            });

            $('.answer').click(function () {
                let question_id = $(this).data('questionId');
                let text = $(this).parent().parent().find(".text input").val();

                let radioButtons = $(this).parent().parent().parent().find("input:radio");
                let selectedIndex = radioButtons.index(radioButtons.filter(':checked'));
                let answers_length = radioButtons.length;

                $(this).parent().parent().parent().find('.text').hide();
                if (text != 'false') {
                    $(this).parent().parent().find('.text').val('');
                    $(this).parent().parent().find('.text').show();
                }

                let data = {
                    question_id,
                    selectedIndex,
                    answers_length,
                    text,
                };


                $.ajax({
                    type: "post",
                    url: '/dashboard/diary/save/answers',
                    data: {data: data},
                    success: function (result) {
                        if(result.unanswered_count == 0) {
                            $('.confirm-diary').show();
                        }
                    }
                });
            })
        })
    </script>
@endsection