<div class="page-header mb-5">
        <h1>{{ $diary->created_at }}</h1>
</div>
<div class="row">
    @foreach($diary_questions as $k=>$question)
        <div class="col-md-4">
                <h5>{{ $question->question }}</h5>
                @foreach($question->answers as $answer)
                        <div class="radio">
                                <label>
                                        <input type="radio" class="answer"
                                               data-text="{{$answer->text}}"
                                               data-question-id="{{$question->id}}" name="answer{{$k}}"
                                               @if($answer->selected)checked="checked"@endif @if($diary->type==2)disabled="disabled"@endif>
                                        {{$answer->answer}}
                                </label>
{{--                                {{ (int)$answer->text != 1 }}--}}
                                <div class="text" @if($answer->selected && ($answer->text != 'false')) style="display: block" @else style="display: none" @endif>
                                        <input type="text" value="{{ $answer->text != 'true' ? $answer->text : '' }}" @if($diary->type==2)disabled="disabled" @endif>
                                        <button class="btn btn-sm save-text" @if($diary->type==2)disabled="disabled" @endif>Save Text</button>
                                </div>
                        </div>
                @endforeach
        </div>
    @endforeach
    @if($diary->type!==2)
            <div class="col-md-12 mt-3 confirm-diary" @if($unanswered_count) style="display:none" @endif>
                 <a class="btn btn-success btn-block" href="{{ route("dashboard.confirm-diary", $diary->id) }}">Confirm</a>
            </div>
    @else
            <div class="alert alert-success">
                    Confirmed
            </div>
    @endif
</div>