<!DOCTYPE html>
<html lang="en">
<head>
<title>
Food Diary
</title>
 <meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<style> 
input[type=button], input[type=submit], input[type=reset] {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 16px 32px;
  text-decoration: none;
  margin: 4px 2px;
  cursor: pointer;
}
</style>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h1><center>Food Diary</center></h1>  
  <div class="form-group row">
  <label for="example-datetime-local-input" class="col-2 col-form-label">Date and time</label>
  <div class="col-10">
    <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
  </div>
</div>  
 <div class="row">
	 <div class="col col-md-8">
		 <label for="exampleFormControlSelect1">Breakfast</label>
		 <div class="form-group">
			 <form name="add_breakfast" id="add_breakfast">
				<div class="add_breakfast">
				<div>
					<input type="text" name="bfast_field[]" id="bfast-box" value="" size=40/><input type="number" name="bfast_servfield[]" value="" placeholder="# serving"/>
					<a href="javascript:void(0);" class="add_bfast_button" title="Add field"><span class="glyphicon glyphicon-plus"></a>
					
					</div>
					 <div class="row" id="data"></div> 
				</div>				 
			 </form>
		 </div>
		 
	 </div>
	
	 <div class="col col-md-8">
		 <label for="exampleFormControlSelect1">Lunch</label>
		 <div class="form-group">
			 <form name="add_lunch" id="add_lunch">
				<div class="add_lunch">
				<div>
					<input type="text" name="lunch_field[]" value="" size=40/><input type="number" name="lunch_servfield[]" value="" placeholder="# serving"/>
					<a href="javascript:void(0);" class="add_lunch_button" title="Add field"><span class="glyphicon glyphicon-plus"></a>
					</div>
				</div>				 
			 </form>
		 </div>
	 </div>
	 <div class="col col-md-8">
		 <label for="exampleFormControlSelect1">Dinner</label>
		 <div class="form-group">
			 <form name="add_dinner" id="add_dinner">
				<div class="add_dinner">
				<div>
					<input type="text" name="dinner_field[]" value="" size=40/><input type="number" name="dinner_servfield[]" value="" placeholder="# serving"/>
					<a href="javascript:void(0);" class="add_dinner_button" title="Add field"><span class="glyphicon glyphicon-plus"></a>
					</div>
				</div>				 
			 </form>
		 </div>
	 </div>  
<div class="col col-md-12">
<input type="text" class="form-controls"  style="width:80%" id="userdata"><button class="btn btn-success btn-sm" onclick="Searchdatalive();">Filter</button>
</div>
</div>
   <div class="row" id="data">
  </div>     
<div class="row" id="sdata">
  </div> 
<!--Breakfast
https://www.mynetdiary.com/diabetes.html
	<text>
	
	//API
	https://api.edamam.com/api/food-database/parser?nutrition-type=logging&ingr=<text>&app_id=6ca19b8d&app_key=b7018616a1a4f389449ceb541acf9860
	
	Lunch
	<text>
	
	Dinner-->
</div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script>
		$(document).ready(function(){
			var max_fields = 10;
			var add_bfast_button = $('.add_bfast_button');
			var add_lunch_button = $('.add_lunch_button');
			var add_dinner_button = $('.add_dinner_button');
			var field_wrapper = $('.add_breakfast');
			var new_bfast_html = '<div><input type="text" name="bfast_field[]" value="" size=40/><input type="number" name="bfast_servfield[]" value="" placeholder="# serving"/><a href="javascript:void(0);" class="remove_bfast_button" title="Remove field"><span class="glyphicon glyphicon-minus"></a></div>';
			var new_lunch_html = '<div><input type="text" name="lunch_field[]" value="" size=40/><input type="number" name="lunch_servfield[]" value="" placeholder="# serving"/><a href="javascript:void(0);" class="remove_lunch_button" title="Remove field"><span class="glyphicon glyphicon-minus"></a></div>';
			var new_dinner_html = '<div><input type="text" name="dinner_field[]" value="" size=40/><input type="number" name="dinner_servfield[]" value="" placeholder="# serving"/><a href="javascript:void(0);" class="remove_dinner_button" title="Remove field"><span class="glyphicon glyphicon-minus"></a></div>';
			var input_count = 1;
			// Add bfast dynamically
			$(add_bfast_button).click(function(){
				if(input_count < max_fields){
				input_count++;
				$(add_breakfast).append(new_bfast_html);
				}
			});
			$(add_lunch_button).click(function(){
				if(input_count < max_fields){
				input_count++;
				$(add_lunch).append(new_lunch_html);
				}
			});
			$(add_dinner_button).click(function(){
				if(input_count < max_fields){
				input_count++;
				$(add_dinner).append(new_dinner_html);
				}
			});
			// Remove dynamically added button
			$(add_breakfast).on('click', '.remove_bfast_button', function(e){
				e.preventDefault();
				$(this).parent('div').remove();
				input_count--;
			});
			$(add_lunch).on('click', '.remove_lunch_button', function(e){
				e.preventDefault();
				$(this).parent('div').remove();
				input_count--;
			});
			$(add_dinner).on('click', '.remove_dinner_button', function(e){
				e.preventDefault();
				$(this).parent('div').remove();
				input_count--;
			});
		});
</script>

<script>
    //When the page has loaded.
$( document ).ready(function(){
    $("#bfast-box").keyup(function(){
    $.ajax({
        url: 'https://api.edamam.com/api/food-database/parser?nutrition-type=logging&ingr='+$(this).val()+'&app_id=6ca19b8d&app_key=b7018616a1a4f389449ceb541acf9860',
        type: 'get',
        beforeSend: function(){
			$("#bfast-box").css("background","#FFF no-repeat 165px");
		},
		success: function(data){
        //console.log(data.hints.food);
      
          // $('#content').html(data);
		$.each(data.hints, function( key, value) {
		//console.log( key + ": " + value.food.nutrients);
			//console.log(value.food);
			
		 var datas ='<div class="col col-md-12" onclick='+selectbreakfast(value.food.label)+'><p>Brand:'+value.food.brand+'</p><label>'+value.food.label+'</label><br>';
		if(value.food.image){
		 var datas1='<img src="'+value.food.image+'" width="20" height="20">';
		}
		else{
		var datas1='';
		}
		var datas2='<br><label>Nutrients:</label><br><table class="table"><thead><tr><th scope="col">CHOCDF</th><th scope="col">ENERC_KCAL</th><th scope="col">Fat</th><th scope="col">FIBATG</th><th scope="col">PROCNT</th></tr></thead><tr><td>'+ value.food.nutrients.CHOCDF + '</td><td>'+value.food.nutrients.ENERC_KCAL+'</td><td>'+value.food.nutrients.FAT+'</td><td>'+value.food.nutrients.FIBTG+'</td><td>'+value.food.nutrients.FIBTG+'</td></tr></table>';
		if(value.food.foodContentsLabel){
		 var  datas3= '<p>'+ value.food.foodContentsLabel+'</p>';
		}else{
		var datas4='<p></p>'; 
		}
		var datas5='</div>'; 
		
		$("#data").show();
			$("#data").html(datas+datas1+datas2+datas5);
			$("#bfast-box").css("background","#FFF");
		//$('#data').append(datas+datas1+datas2+datas5);			            
		 })
		                
		 },
	
            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                $('#content').html(errorMsg);
              }
        });
    });
 });
 
 function selectbreakfast(val) {
$("#search-box").val(val);
$("#suggesstion-box").hide();
}

function returndata(id){
	alert(id);
	//here we can get single record by click event by passing foodId
	//https://api.edamam.com/api/food-database/parser?nutrition-type=logging&ingr=food_blfrcjza6a7sb5b05tf47ajdve1u&app_id=6ca19b8d&app_key=b7018616a1a4f389449ceb541acf9860
}

</script>


<script>
       
    function Searchdatalive() {
       document.getElementById("data").style.display = "none";
	   document.getElementById("data").style.display = "block";
       var lol = document.getElementById('userdata').value;
	   var uri="https://api.edamam.com/api/food-database/parser?nutrition-type=logging&ingr="+lol+"&from=0&to=10&app_id=6ca19b8d&app_key=b7018616a1a4f389449ceb541acf9860";

  //  alert(uri);


  		$.ajax({
            type : 'get',
            url  : uri,
            success: function(res){
			console.log(res);

			$.each(data.hints, function( key, value) {
			//console.log( key + ": " + value.food.nutrients);
 			//console.log(value.food);

			// var datas ='<li id="'+value.food.foodId+'" onclick="returndata(id)">Category:'+ value.food.category+ '</h2><p>Brand:'+value.food.brand+'</p><p>'+value.food.categoryLabel+'</p><label>'+value.food.label+'</label><br>';
			var datas ='<li id="'+value.food.foodId+'" onclick="returndata(id)"><p>Brand:'+value.food.brand+'</p><label>'+value.food.label+'</label><br>';

			 if(value.food.image){
			 var datas1='<img src="'+value.food.image+'" width="20" height="20">';
			}
			else{
			var datas1='';
			}
			var datas2='<br><label>Nutrients:</label><br>CHOCDF:'+ value.food.nutrients.CHOCDF + '<br>ENERC_KCAL:'+value.food.nutrients.ENERC_KCAL+'<br>FAT:'+value.food.nutrients.FAT+'<br>FIBTG:'+value.food.nutrients.FIBTG+'<br>PROCNT:'+value.food.nutrients.FIBTG+'<br>';
			if(value.food.foodContentsLabel){
			 var  datas3= '<p>'+ value.food.foodContentsLabel+'</p>';
			}else{
			var datas4='<p></p>'; 
			}
			var datas5='</li>'; 
			$('#sdata').append(datas+datas2+datas3);			            
			 })
			                
			 },

            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                $('#content').html(errorMsg);
              }
        });
    }
    </script>


</body>	
</html>