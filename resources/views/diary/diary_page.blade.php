<div class="row">
    <div class="col-md-4">
        <a href="{{ route('dashboard.index') }}" class="btn btn-warning btn-lg btn-block">
            Current Diary Questions
            <span class="pull-right btn btn-secondary btn-sm">{{ $diary_types[0]->count }}</span>
        </a>
    </div>
    <div class="col-md-4">
        <a href="{{ route('dashboard.diaryPage', 1) }}" class="btn btn-danger btn-lg btn-block">
            Overdue Diary Questions
            <span class="pull-right btn btn-secondary btn-sm">{{ $diary_types[1]->count }}</span>
        </a>
    </div>
    <div class="col-md-4">
        <a href="{{ route('dashboard.diaryPage', 2) }}" class="btn btn-success btn-lg btn-block">
            Complete Diary Questions
            <span class="pull-right btn btn-secondary btn-sm">{{ $diary_types[2]->count }}</span>
        </a>
    </div>
    <div class="col-md-12 mt-5">
        @if($type==1)
            <h3>Overdue Diary Questions</h3>
        @endif
        @if($type==2)
           <h3>Complete Diary Questions</h3>
        @endif
        @foreach($current_diary as $diary)
            <a @if($diary->type !=4)href="{{ route('dashboard.diary_questions', $diary->id) }}"@else disabled="disabled" @endif class="btn btn-block
                {{ $diary->type==2 ? 'btn-success' : '' }}
            {{ $diary->type==1 ? 'btn-danger' : '' }}
            ">{{ $diary->created_at }}</a>
        @endforeach
    </div>
</div>