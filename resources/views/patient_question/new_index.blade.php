@extends('layouts.site')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        
        <li class="breadcrumb-item"><a href="#">{{$diary->study->study_name}}</a></li>
        <li class="breadcrumb-item"><a href="{{route('dashboard.study.diaries.status' , [$diary->study->id , $diary->id , $status] )}}">{{$diary->diary_name}} Diaries </a></li>
        <li class="breadcrumb-item active" aria-current="page"> {{$diary->diary_name}} Questions </li>
    </ol>
</nav>
    

<div class="container bg-white" style="padding:20px; " >
    <div>
        <h2 style="margin-bottom:20px;"> Question of the "{{$diary->diary_name}}" diary </h2>
    </div>
    <div>
        @if(count($diary->questions) > 0)
        <form action="{{route('dashboard.diary.question.storeDiary' , [$diary->id , $freq ])}}" method="POST" >
        @csrf
        @foreach ($diary->questions as $question)
        
        <h4> {{$question->question}} </h4>

        @php
        if(!is_null( $question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first() ) ) {
            $answer = $question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first();

        }
        @endphp
        @php
        if(count($question->specifications) > 0 )
        {
            foreach($question->specifications as $spec){
                $specs[] = $spec->name;

            }
        }
        @endphp
        <input type="hidden" value="{{$question->id}}" name="questions[]" />
       
        {{-- @dump($question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first()) --}}
        @if(!is_null($question->answer_type) && $question->answer_type == 1 )
        

        <div class="form-group">
            <label class="col-form-label"> Answer </label>
            <textarea name="answer{{$question->id}}" class="form-control" rows="8" placeholder="Detail you Answer here" >@if(isset($answer)){{$answer->answers}}@endif</textarea>
          
        </div>
        
        @elseif(!is_null($question->answer_type) && $question->answer_type == 3)

        <div class="form-group">
            <label class="col-form-label"> Select below answers</label><br>
            @php
            $tagsArray =  json_decode( $question->answers);
            if(isset($answer))  {
            $answersArray = json_decode(  $answer->answers);  
            }   
            @endphp
            @foreach( $tagsArray as $i=> $ans) 
            <input type="checkbox" id="check{{$question->id}}{{$i}}" value="{{ $tagsArray[$i] }}" name="answer{{$question->id}}[]"  
            @if(isset($answer))  
            @if(in_array( $ans , $answersArray )) {{"checked"}}  @endif
            @endif
            @if(isset($specs) && in_array( $ans  , $specs ) ) onclick="make_checkbox_description(this.id , {{$question->id}})" @endif             
            />
            {{-- @if(in_array( $ans , $answersArray )) @dump($ans) @endif --}}
            <label class="col-form-label" for="check{{$question->id}}{{$i}}">{{ $tagsArray[$i] }} </label>
            <br>
            <div id="additional_fields{{$question->id}}{{$i}}">
            @if(isset($answer->specifications))
            @if(count($answer->specifications) > 0)
                @foreach($answer->specifications as $spec)
                @if($spec->name == $tagsArray[$i] )
                <textarea class="form-control" name="specifications{{$question->id}}[]" id="specs{{$question->id}}{{$i}}" placeholder="Plz elaborate your answer">{{$spec->specification}}</textarea>
                @endif
                @endforeach
                @endif
                @endif  
            </div>
            @endforeach
        </div>
        @elseif(!is_null($question->answer_type) && $question->answer_type == 2)
        {{-- @dd($answer->answers) --}}
        <div class="form-group">
            <label class="col-form-label"> Select below answers</label><br>
            @php
            $tagsArray =  json_decode($question->answers);
            @endphp
            @foreach( $tagsArray as $i=> $ans) 
            <input type="radio" id="radio{{$question->id}}{{$i}}" name="answer{{$question->id}}" value="{{$tagsArray[$i]}}" 
              @if(isset($answer) && $answer->answers == $tagsArray[$i] )) )
                {{"checked"}}
            @endif
            @if(isset($specs) && in_array( $ans , $specs ) ) onclick="make_radio_description(this.id , {{$question->id}})" @else 
            onclick="remove_radio_description(this.id)" @endif 
            {{-- onchange="remove_radio_description({{$question->id}})"  --}}
            />

            <label class="col-form-label" for="radio{{$question->id}}{{$i}}">{{ $tagsArray[$i] }} </label>

            <div id="additional_fields{{$question->id}}{{$i}}">
            @if(isset($answer->specifications))
            @if(count($answer->specifications) > 0)
                @foreach($answer->specifications as $spec)
                @if($spec->name == trim(str_replace(array( '[' , ']' , '"' ) , '' , $ans )) )
                <textarea class="form-control" name="specifications{{$question->id}}" id="specs{{$question->id}}{{$i}}" placeholder="Plz elaborate your answer">{{$spec->specification}}</textarea>
                @endif
                @endforeach
                @endif
                @endif
            </div>
            <br>
            @endforeach
        </div>
        @endif
        <hr>
        @php
            $answer = NULL;
        @endphp
        @endforeach

        <button type="submit" class="btn btn-success">Save</button>
        </form>
        @else
        <h5> Sorry! No Questions has been added so far </h5>
        <a href="{{URL('dashboard')}}">Go back</a>
        @endif
    </div>
</div>



<script>
    @php
    if(!isset($i)) 
    $i = 0 ; 
    @endphp
    
    var n = {{$i}};
    var i = 1;
    function make_radio_description( id , question_id){
        var radio_id = id.replace( /^\D+/g, ''); 


        if($('#radio'+radio_id).prop("checked") == true){
        if (!$('#specs'+radio_id).length){
            console.log('checked but not exists');
            var area_name = "specifications"+question_id;
            $('textarea[name ="'+area_name+'"]').remove();

            $('#additional_fields'+radio_id).append('<textarea class="form-control" name="specifications'+question_id+'" id="specs'+radio_id+'" placeholder="Plz elaborate your answer"></textarea>');
            i = i + 1;
        }
        }
    }
    
    
    function remove_radio_description(id){
        //var radio_id = id.replace( /^\D+/g, ''); 
        console.log(id , 'remove called');
        $('textarea[name ="specifications"+id]').remove();
    }
    


    function make_checkbox_description(id , question_id ){

        var check_id = id.replace( /^\D+/g, ''); 
        console.log( 'make_checkbox_description' , id , check_id , $('#specs'+check_id).length);
        if($('#check'+check_id).prop("checked") === false){
            if ($('#specs'+check_id).length){
                console.log('unchecked but exists');
            $('#specs'+check_id).remove();
        }
        }

        if($('#check'+check_id).prop("checked") === true){
        if (!$('#specs'+check_id).length){
            console.log('checked but not exists');

            $('#additional_fields'+check_id).append('<textarea class="form-control" name="specifications'+question_id+'[]" id="specs'+check_id+'" placeholder="Plz elaborate your answer"></textarea>');
            i = i + 1;
        }
        }
      
    }


    function remove_checkbox_description(id){
        var check_id = id.replace( /^\D+/g, ''); 
        console.log( 'remove_checkbox_description' , id ,check_id , ) ;
        if($('#check'+check_id).prop("checked") == true){
            //console.log('checked');
        }
        if($('#check'+check_id).prop("checked") == false){
            console.log('unchecked');
            
        }
    }

</script>


@endsection
