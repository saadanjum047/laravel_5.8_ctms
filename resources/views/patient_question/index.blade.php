@extends('layouts.site')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        
        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.view' , $diary->study->id) }}">{{$diary->study->study_name}}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.study.diaries' , [$diary->study->id , $diary->id]) }}">{{$diary->diary_name}} </a></li>
        <li class="breadcrumb-item active" aria-current="page"> {{$diary->diary_name}} Questions </li>
    </ol>
</nav>
    

<div class="container bg-white" style="padding:20px; " >
    <div>
        <h2 style="margin-bottom:20px;"> Question of the "{{$diary->diary_name}}" diary </h2>
    </div>
    <div>
        @if(count($diary->questions) > 0)
        @foreach ($diary->questions as $question)
        <h6> <a href="{{route('dashboard.study.diary.question.view' , [$question->id , $freq])}}"> {{ $question->question }} </a> </h6> <br>
        
        @php
        if(!is_null($question->ans->where('patient_id' , auth()->user()->patient->id)->first()) ) {
            $answer = $question->ans->where('patient_id' , auth()->user()->patient->id)->where('frequency' , $freq)->first();
        }
        @endphp

        @if(isset($answer))
        <h6> 
        @php
        $arr = explode( ',' , trim(str_replace(array( '[' , ']' , '"' ) , '' , $answer->answers )) );
        foreach($answer->specifications as $spec){
            $specs[] = $spec->specification;
        }
        @endphp
        @foreach ($arr as $k=> $a)
            {{$a}}
            <br>
            @foreach($answer->specifications as $spec)
            @if($a == $spec->name)
            <p class="alert"> {{$spec->specification}} </p>
            @endif
            @endforeach
        @endforeach   
        
         </h6>
        
        @if(isset($answer->specifications))
        
        @endif
        @if(is_array($answer->answers))
        {{1}}
        @endif
        @endif
        <hr>
        @php
            $answer = NULL;
        @endphp
        @endforeach
        @else
        <h5> Sorry! No Questions has been added so far </h5>
        <a href="{{URL('dashboard')}}">Go back</a>
        @endif
    </div>
</div>

@endsection
