@extends('layouts.site')


@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">{{$diary->study->study_name}}</a></li>

        <li class="breadcrumb-item"><a >{{$diary->diary_name}}</a></li>

    </ol>
</nav>


<div class="bg-white" style="padding:20px">

<div class="card">

    <div class="card-title">
         @if($status == 0)<h2 class="text-warning"> Current Diaries </h2>
         @elseif($status == 1) <h2 class="text-danger"> Overdue Diaries </h2>
          @elseif($status == 2) <h2 class="text-success"> Completed Diaries </h2>
            @endif 
    </div>
    <table class="table table-hover">
        <tr>
            <th>#</th>
            <th>Diary Name</th>
            <th>Diary Frequency</th>
        </tr>
        @php
            $i = 0;
        @endphp
        @foreach($diary->diary_schedules->where('patient_id' , auth()->user()->patient->id ) as $k => $scheduled_diary)
        @if($scheduled_diary->type == $status)
        <tr>
            <td> {{$i+=1}} </td>
            <td>
                <a href="{{route('dashboard.diary.all.questions.status' , [$diary->id , $scheduled_diary->frequency , $status ])}}">
                {{$scheduled_diary->diary->diary_name}} </a> </td> 
            <td> {{$scheduled_diary->frequency}} </td>
        </tr>
        @endif
        @endforeach
    </table>
</div>

</div>

@endsection