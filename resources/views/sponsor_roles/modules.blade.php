@extends('layouts.site')

@section('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@section('content')

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "error");
</script>
@endif
@if ($message = Session::get('errors'))
<script>
swal("Sorry!", "{{print_r($message) }}", "error");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

<div class="bg-white">
    <div class="card">
    <div class="card-header bg-white">
            
        <h3 class="card-title">Modules
            <button style="float:right" class="btn btn-success" data-toggle="modal" data-target="#create_model"><i class="fa fa-plus"></i> Add Module</button></h3>
            @if($errors->any())
            @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @endif

      </div>
      <table class="table table-hover">
    <tr>
        <th>#</th>
        <th>Module Name</th>
        <th>Actions</th>
    </tr>

    {{-- {{ dd($roles)}} --}}

    @foreach ($modules as $i => $mod)
   
    <tr>
        <td>{{$i+1}} </td>
        <td>{{$mod->name}} </td>
       
        <td> 
            <a class="text-danger fa fa-trash fa-lg" href="{{route('dashboard.modules.remove', $mod)}} " ></a> 
            {{-- <a class="text-primary fa fa-edit fa-lg"  onclick="update_model_data({{$role}} ,{{$role->orgs}})"> </a>  --}}
        </td>
    </tr>

    @endforeach

</table>
</div>

</div>

<div class="modal fade" id="create_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Module</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.modules')}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <label for="diary-name" class="col-form-label">Role Name:</label>
              <input type="text" class="form-control" id="diary-name" name="module">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Create</button>
        </div>
        </form>

      </div>
    </div>
  </div>



  {{-- <div class="modal fade" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Role</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.sponsor_roles.update' )}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <input type="hidden" value="" id="role_id" name="role_id" />
              <label for="diary-name" class="col-form-label">Role Name:</label>
              <input type="text" class="form-control" id="role-name1" name="role_name">
            </div>

            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>

      </div>
    </div>
  </div> --}}

@endsection

@section('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
  <script>
      $(document).ready(function() {
          $('.js-example-basic-multiple').select2();
      });
       $(document).ready(function() {
          $('js-example-basic-single').select();
      });
  </script>
<script>
    function update_model_data(role , org){
        console.log(role , org);
        $('#update_model').modal('show');
        $("#role-name1").val(role.name);
        $("#role_id").val(role.id);
        var options = org_name1.options; 
        for (var i = 0; i <= options.length; i++) {
            var option = options[i]; 
            if( option.value == org.sponsor_id ){
              var selected = option.index;
              document.getElementById("org_name1").selectedIndex = selected;
            }
        }
    }
</script>
@endsection
