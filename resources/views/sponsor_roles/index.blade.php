@extends('layouts.site')

@section('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@section('content')

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "error");
</script>
@endif
@if ($message = Session::get('errors'))
<script>
swal("Sorry!", "{{print_r($message) }}", "error");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
   <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
   <li class="breadcrumb-item" aria-current="page">
Sponsor Roles
</li>

   </ol>
</nav>


<div class="bg-white">
    <div class="card">
    <div class="card-header bg-white">
            
        <h3 class="card-title">Sponsor Roles
            <a style="float:right" class="btn btn-success" href="{{route('dashboard.sponsor_roles.create')}} " ><i class="fa fa-plus"></i> Add Role</a></h3>
            @if($errors->any())
            @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @endif

      </div>
      <table class="table table-hover">
    <tr>
        <th>#</th>
        <th>Role Name</th>
        <th>Actions</th>
    </tr>

    {{-- {{ dd($roles)}} --}}

    @foreach ($roles as $i => $role)
        
    <tr>
        <td>{{$i+1}} </td>
        <td> <a href="{{route('dashboard.sponsor_roles.edit' , $role->id )}} "> {{$role->name}} </a></td>
       
        <td> 
            <a class="text-danger fa fa-trash fa-lg" href="{{route('dashboard.sponsor_roles.remove', $role)}} " ></a> |
            <a class="text-primary fa fa-edit fa-lg" href="{{route('dashboard.sponsor_roles.edit' , $role->id )}} " </a> </td>
    </tr>

    @endforeach

</table>
</div>

</div>

{{-- <div class="modal fade" id="create_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Role</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.sponsor_roles')}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <label for="diary-name" class="col-form-label">Role Name:</label>
              <input type="text" class="form-control" id="diary-name" name="role_name">
            </div>
              <div class="form-group">
              <label for="study_name" class="col-form-label">Organizations:</label>
            <select multiple class=" js-example-basic-multiple form-control" style="width:100%"  id="study_name" name="org_id[]" >
                @foreach($organizations as $org)
                <option value="{{$org->sponsor_id}} "> {{$org->sponsor_name}} </option>
                @endforeach
            </select>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Create</button>
        </div>
        </form>

      </div>
    </div>
  </div> --}}



  {{-- <div class="modal fade" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Role</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.sponsor_roles.update' )}}" method="POST">
         {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group">
              <input type="hidden" value="" id="role_id" name="role_id" />
              <label for="diary-name" class="col-form-label">Role Name:</label>
              <input type="text" class="form-control" id="role-name1" name="role_name">
            </div>

              <div class="form-group">
              <label for="study_name" class="col-form-label">Organizations:</label>
            <select class="js-example-basic-single form-control" style="width:100%"  id="org_name1" name="org_id" >
                @foreach($organizations as $org)
                <option value="{{$org->sponsor_id}}"> {{$org->sponsor_name}} </option>
                @endforeach
            </select>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>

      </div>
    </div>
  </div> --}}

@endsection


