@extends('layouts.site')


@section('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@section('content')

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "error");
</script>
@endif
@if ($message = Session::get('errors'))
<script>
swal("Sorry!", "{{print_r($message) }}", "error");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif


<div class="bg-white" style="padding:20px;">
<form @if(isset($role)) action="{{route('dashboard.sponsor_roles.update' , $role)}}" @else action="{{route('dashboard.sponsor_roles')}}" @endif  method="post">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-2 ">
            <label class="col-form-label" ><h4> Role Name </h4></label>
        </div>
        <div class="col-md-6 ">
          <input type="text" class="form-control" placeholder="Role Name here . . " name="role" @if(isset($role)) value="{{$role->name}}" @endif />
          @if(isset($role))
          <input type="hidden" value="{{$role->id}}" name="role_id" />
          @endif
        </div>
      
    </div>
  


<div style="margin-top:20px; margin-bottom:30px">
@foreach($modules as $k => $mod)
<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse{{$k}}" aria-expanded="false" aria-controls="collapseThree">
          {{$mod->name}}
        </button>
      </h5>
    </div>



    <div id="collapse{{$k}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body" style="margin:10px">
        @foreach($mod->permissions as $per)
        @if(isset($role))
        @php
            $ids = $role->permissions->pluck('id')->toArray();
        @endphp
        @endif
        <div class="col-md-2">
          <input type="checkbox" value="{{$per->id}}" id="{{$per->name}}{{$k}}" name="per[]"  @if(isset($ids) && in_array($per->id , $ids)  ) {{'checked'}} @endif  />
          <label for="{{$per->name}}{{$k}}"> {{$per->name}} </label>
         </div>
         @endforeach
      </div>
    </div>


  </div>
</div>
@endforeach

@if(isset($role))
<button style="margin-top:10px" type="submit" class="btn btn-success float-right"> Update </button>
@else
<button style="margin-top:10px" type="submit" class="btn btn-primary float-right"> Create </button>
@endif
</div>
</form>

</div>
@endsection
