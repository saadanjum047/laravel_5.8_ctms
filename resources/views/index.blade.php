@extends('layouts.site')

@section('styles')
<style>

    <style>
        .study-box{
        background-color: #FFFFFF;
            padding: 10px;
            font-size: 10px;
             margin: 10px;
            margin-top: 10px;
            text-align: center;
           
        }
        
        .row-striped:nth-of-type(odd){
          background-color: #9d9999;
          border-left: 2px #000000 solid;
        }
        
        .row-striped:nth-of-type(even){
          background-color: #c4c1c1;
          border-left: 2px #efefef solid;
        }
        
        .row-striped {
            padding: 2px 0;
        }
        
        .row-upcoming:nth-of-type(odd){
          background-color: #d8954d;
          border-left: 2px #112a6b solid;
        }
        
        .row-upcoming:nth-of-type(even){
          background-color: #d8954d;
          border-left: 2px #efefef solid;
        }
        
        .row-upcoming {
            padding: 2px 0;
        }
        
        path {  stroke: #fff; }
        path:hover {  opacity:0.9; }
        rect:hover {  fill:blue; }
        .axis {  font: 10px sans-serif; }
        .legend tr{    border-bottom:1px solid grey; }
        .legend tr:first-child{    border-top:1px solid grey; }
        
        .axis path,
        .axis line {
          fill: none;
          stroke: #000;
          shape-rendering: crispEdges;
        }
        
        .x.axis path {  display: none; }
        .legend{
            margin-bottom:46px;
            display:inline-block;
            border-collapse: collapse;
            border-spacing: 0px;
        }
        .legend td{
            padding:4px 5px;
            vertical-align:bottom;
        }
        .legendFreq, .legendPerc{
            align:right;
            width:30px;
        }
        .col-center{
            margin:0 auto;
          }
          

        {{-- old styles --}}


     
        </style>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


        @endsection

@section('content')

{{--  
@foreach (auth()->user()->study_user->role_detail->permissions as $item)
   {{ dump($item->module_id)}}
@endforeach
--}}

@if ($message = Session::get('error'))
<script>
swal("Sorry!", "{{$message }}", "danger");
</script>
@endif
@if ($message = Session::get('success'))
<script>
swal("Done!", "{{$message }}", "success");
</script>
@endif

    {{--  @if(!empty(Cookie::get('user_role')) && (int)Cookie::get('user_role')!==1)  --}}
    @if(auth() && auth()->user()->role_id !=1 && auth() && auth()->user()->role_id !=4 )
    {{--  @if(auth() && auth()->user()->role_id < 3)  --}}
    <br>

    
    @if(auth() && auth()->user()->role_id == 2 )
    <a href="{{ route('dashboard.study.create')}}" class="btn btn-primary pull-right">Create Study</a> 
    @endif

        <h3 class="text-primary mb-4">Study Visit Overview</h3>
        @if(empty($studies))
        <div class="row" style="width: 100%;height: 300px;overflow: auto;border-bottom: 1px solid #0f67a9">
            No studies
        </div>
        @elseif(count($studies) )
        <div class="row" style="width: 100%;height: 300px;overflow: auto;border-bottom: 1px solid #0f67a9">
        

        @foreach($studies as $k=>$studey)
        {{--  {{dd($studey, $studey->organization)}}  --}}
                <!-- Button trigger modal -->
                    <!-- Modal -->
        <div class="modal fade" id="modal{{$k}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ $studey->study_name }}  </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{--  @if((int)Cookie::get('user_role')===3)  --}}
                        @if(auth()->user()->role_id == 3)
                            @foreach(collect($studey->content)->unique('content_lang') as $content)
                                <a href="{{ route('dashboard.study.view', [$studey->id,$content->content_lang]) }}" class="btn btn-primary">{{ $content->content_lang }}</a>
                            @endforeach
                        @elseif(auth()->user()->role_id ==2)
                            @foreach($studey->sites as $site)
                                <a href="{{ route('dashboard.study.view', [$studey->id,$site->site_id]) }}" class="btn btn-primary mb-1">{{ $site->site_name }} </a>
                            @endforeach
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
    
    

    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-6 col-6 mb-4">
        <div class="card bg-white"  >
            <div class="card-block">
                
                @if(auth()->user()->role_id == 2 )
                <a  href="{{ route('dashboard.studies.edit' , $studey )}}" class="float-right text-primary fa fa-edit" style="float:right; margin:-8px" ></a>
                @endif
                <h6 class="card-title font-weight-normal text-muted">
        
                </h6>
                <h6 class="card-subtitle mb-4 text-muted">
                    
                    {{-- Study Name or title for sponsors  --}}
                    
                    <a href="{{ route('dashboard.study.view', [$studey->id,'English']) }}" >{{ $studey->study_name ?? $studey->study_title }} {{ $studey->patients_count }}</a>

                   
                </h6>
                        
                <div class="progress">
                    <div class="progress-bar bg-info progress-slim" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
    
                <span class="pull-right" style="font-weight: 600;">
                {{--  no of patients in a study  --}}
                @php

                $total_pats = array();
                $pats = array();

                
                if($studey->sites->count() > 0){
                foreach($studey->sites as $site){
                    if($site->site->patients->count() > 0 ){
                $pats = $site->site->patients->where('study_id' , $studey->id)->toArray();
                $total_pats = array_merge($total_pats , $pats);
                
                }
                }}
              
                @endphp
                
                {{count($total_pats) ?? 0 }}
                </span>

            <div class="card-body">
              <div class="row">
    
                    {{--<table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                    <th> Study</th>
                    <th> Title</th>
                    <th> Description</th>
                    <th> Start date</th>
                    <th> End date</th>
                    <th> Actions</th>
                                                       
                    </tr>
                    </thead>
                    <tbody>
                   @foreach($response->AllStudies as $data)   
                      
                  <tr>
                    <td><a href="{{ route('dashboard.study', [$data->id,'English']) }}">{{$data->study_name}}</a></td>
    
                    <td>{{$data->study_title}}</td>
                    <td>{{$data->study_description}}</td>
                    <td>{{$data->startdate}}</td>
                    <td>{{$data->enddate}}</td>
    
             
        
                    <td>
                    <a href="{{ route('dashboard.EditStudy', [$data->id])}}" title="Update Study" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="" title="Delete Study"><i class="fa fa-trash"></i></a>
                    </td>
                                                
                    </tr>
    
                    @endforeach                 
                    </tbody>
                   </table>--}}


    
                    </div>
                    </div>
                    </div>
                    
                </div>
            </div>
            
            @endforeach


        @else
            <h5>You do not have Studies!</h5>
        @endif
    </div>

    <div class="row mt-5">
        <div class="col-lg-12  mb-4">
            <div class="card">
                <div class="card-block pl-0">
                    <h5 class="card-title mb-4 ml-2">Studies</h5>
                    <div class="col-sm-12">
                        @if(empty($studies))
                            No studies
                        @elseif(count($studies) )

                        {{-- @dd(array_pluck($studies, 'patients_count')) --}}

                        @php
                        foreach ($studies as $stud){
                            $total_pats = array();
                            $pats = array();
                            if($stud->sites->count() > 0){
                            foreach($stud->sites as $site){
                            $pats = $site->site->patients->where('study_id' , $stud->id)->toArray();
                            $total_pats = array_merge($total_pats , $pats);
                            }}
                            $total_pats_count[] = count($total_pats);
                            //$patients[] = $stud->patients->count();
                        }
                       
                        @endphp
                                                
                        <canvas id="lineChart" data-counts='{{ json_encode($total_pats_count) }}'
                            data-studies='{{ json_encode(array_map(function($item){
                        return str_limit($item, 6, '');
                    }, array_pluck($studies, 'study_name'))) }}'></canvas>
                            @endif
                    </div>
                </div>
            </div>

            {{--@if((int)Cookie::get('user_role')===3)--}}
            {{--@if(auth()->user()->role_id ==3)--}}
                {{--<div class="mt-2">--}}
                    {{--<h3>Select Study</h3>--}}
                    {{--<form action="{{ route('dashboard.save-study') }}" method="post">--}}
                        {{--<div class="form-group">--}}
                            {{--<select class="select-study form-control" name="study_id">--}}
                                {{--@foreach($studies as $study)--}}
                                    {{--<option value="{{ $study->id }}">{{ $study->study_name ? $study->study_name : $study->study_title }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
    
                            {{--@if(session('status'))--}}
                                {{--<div class="alert alert-success">--}}
                                    {{--{{ session('status') }}--}}
                                {{--</div>--}}
                            {{--@elseif($errors->any())--}}
                                {{--<div class="alert alert-danger">--}}
                                    {{--{{ $errors->all()[0] }}--}}
                                {{--</div>--}}
                            {{--@endif--}}
    
    
                            {{--{{ csrf_field() }}--}}
                            {{--<button type="submit" class="btn btn-primary">Save Study</button>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--@endif--}}
        {{-- </div> --}}
        {{--<div class="col-lg-6  mb-4">--}}
            {{--<div class="card">--}}
                {{--<div class="card-block">--}}
                    {{--<h5 class="card-title mb-4">Patient Engagement</h5>--}}
                    {{--<canvas id="doughnutChart" data-info='{{ json_encode($information->patients_type_counts) }}' style="height:250px"></canvas>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    
    
    @else
        
    {{-- // study for patient  --}}
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            @if(auth()->user()->role_id == 4)
                            <div class="col-md-5">
                                <h3> @if(isset($patient_study ))  
                                    {{ $patient_study->study_name }}
                                    {{--  <a href="{{route('dashboard.study.view' , $patient_study->id)}} "> {{ $patient_study->study_name }}</a>  --}}
                                    @endif</h3>
                            </div>
                            <div class="col-md-4">
                                <h5 class="card-title mb-4">Guide</h5>
                                <a href=" @if(isset($patient_study))  {{ $patient_study->guide_pdf }} @endif" target="_blank">  @if(isset($patient_study )) {{ $patient_study->guide_pdf }} @else {{'Blank'}} @endif </a>
                            </div>
                            @endif

                            {{--  for side roles  --}}
                            @if(auth()->user()->role_id > 4 )
                            <div class="col-md-5">
                                <h3> @if(isset(auth()->user()->sponsor_user->study ))  
                                    <a href="{{route('dashboard.study.view' , auth()->user()->sponsor_user->study->id)}} "> {{ auth()->user()->sponsor_user->study->study_name }}</a>
                                    @endif</h3>
                            </div>
                            <div class="col-md-4">
                                <h5 class="card-title mb-4">Guide</h5>
                                <a href=" @if(isset(auth()->user()->sponsor_user->study))  {{ auth()->user()->sponsor_user->study->guide_pdf }} @endif" target="_blank">  @if(isset(auth()->user()->sponsor_user->study )) {{ auth()->user()->sponsor_user->study->guide_pdf }} @else {{'Blank'}} @endif </a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            @php

            /*dd(auth()->user()->patient->study->engagement_questions);
            $all_question = collect();
            $answer = collect();
            $ans = 0;
            $unans = 0;
            foreach (auth()->user()->patient->study->diary->where('status' , 1) as $diary){
                $all_question = $all_question->merge($diary->questions);
                
                 foreach($diary->questions as $question ){
                    if($question->ans->where('patient_id' , auth()->user()->patient->id )->first() )
                    $ans += 1; 
                    else
                    $unans += 1; 
                }

                
            $all_question = auth()->user()->patient->study->engagement_questions;
            $ans = 0;
            $unans = 0;
            foreach ($all_question as $question){

                if($question->ans->where('patient_id' , auth()->user()->patient->id)->first())
                    $ans += 1; 
                    else
                    $unans += 1;
            }*/

            //dd(auth()->user()->patient->diary_schedule );

            
            @endphp
            
            <div class="col-md-6 " style="margin-bottom:20px">
                
                <div class="card ">
                    <div class="card-header bg-white">
                      <h3 class="card-title"><i class="fa fa-question-circle"></i> Patient Engagement Question</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table class="table table-bordered table-hover">
                        <tr>
                        <div class="col-md-12">
                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">{{auth()->user()->patient->diary_schedule->count()}} : Total Diaries </div>
                                </div>
                        </div>
                        </tr> 
                        <tr>
                        <div class="col-md-12">
                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar bg-success" role="progressbar" style="width: @if(auth()->user()->patient->diary_schedule->count() > 0 ) {{auth()->user()->patient->diary_schedule->where('type' , 2)->count()/auth()->user()->patient->diary_schedule->count()*100}}% @else 0 @endif" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">{{auth()->user()->patient->diary_schedule->where('type' , 2)->count()}} : Completed Diaries </div>
                                </div>
                        </div>
                        </tr>
                        <tr>
                        <div class="col-md-12">
                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: @if(auth()->user()->patient->diary_schedule->count() > 0 ) {{auth()->user()->patient->diary_schedule->where('type' , '!=' , 2)->count()/auth()->user()->patient->diary_schedule->count()*100}}% @else 0 @endif" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">{{auth()->user()->patient->diary_schedule->where('type' , '!=' , 2)->count()}} :Remaining Diaries </div>
                                </div>
                        </div>
                        </tr>

                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
            <div class="col-md-6" style="margin-bottom:20px">
                
                <div class="card ">
                    <div class="card-header bg-white">
                      <h3 class="card-title"><i class="fa fa-calendar"></i> Schedules </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table class="table table-bordered table-hover">
                        <thead>                  
                          <tr>
                            <th>#</th>
                            <th>Visit Name</th>
                            <th>Visit Date</th>     
                            <th>Action</th>     
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            @foreach (auth()->user()->patient->patient_schedule as $k => $visit)
                            {{-- @dump($visit) --}}
                            <tr style="background-color: @if($visit->visit_date > Carbon\Carbon::now()) yellow;  @elseif($visit->visit_date < Carbon\Carbon::now()) #58d8a3; color:white; @endif " >
                                    <td> {{$k+1}} </td>
                                    <td> {{$visit->visit_name}} </td>
                                    <td> {{$visit->visit_date}} </td>
                                    <td> @if($visit->visit_date > Carbon\Carbon::now()) @if(isset($visit->change_request)) {{$visit->change_request->date}} @else  <button class="btn btn-success btn-sm" onclick="open_visit_date_change({{$visit->id}})" >Change Date</button>   @endif @endif </td>
                                </tr>
                            @endforeach
                          </tr>
                         
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

            <div class="modal fade" id="visit_date_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">

                    <form action="{{route('dashboard.patient.request_reset_schedule')}}" method="POST">
                        @csrf
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Change Visit Date</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" value="" name="visit_id" id="current_visit_id" />
                            <label class="col-form-label">Next Date</label>
                            <input id="visit_date_change" type="date" name="date" class="form-control" required />
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>



            <div class="col-md-6" style="margin-bottom:20px">
                
                <div class="card ">
                    <div class="card-header bg-white">
                      <h3 class="card-title"><i class="fa fa-medkit"></i> Study Medicines </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table class="table table-bordered table-hover">
                        <thead>                  
                            <tr>
                                <th>#</th>
                                <th>Medicine Name</th>
                                <th>Medicine Dose</th>
                                <th>Created Date</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            @foreach (auth()->user()->patient->study->medicines as $k => $medicine)
                        
                            <tr>
                                <td> {{$k+1}} </td>    
                                <td> {{$medicine->name}} </td>    
                                <td> {{$medicine->dose}} </td>    
                                <td>  {{$medicine->created_at->toDateString()}} </td>    
                            </tr>    
                            @endforeach
                          </tr>
                         
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
                
            
               
                
                <div class="col-md-12" style="margin-bottom:20px">
                
                    <div class="card ">
                        <div class="card-header bg-white">
                          <h3 class="card-title"><i class="fa fa-book"></i> Diaries</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <table class="table table-bordered table-hover">
                            <thead>                  
                              <tr>
                                <th>#</th>
                                <th>Diary Name</th>
                                <th>Current</th>
                                <th>Overdue</th>
                                <th>Completed</th>
                                <th>End Date</th>
                                <th>Created</th>  

                            </tr>
                            </thead>
                            <tbody>

                                {{--  @dd(auth()->user()->patient->study->diary)  --}}
                                @foreach (auth()->user()->patient->study->diary->where('status' , 1) as $k => $diary)
                                
                                    <tr>
                                        <td>{{$k+1}} </td>
                                        <td>                                            
                                        {{--  <a href="{{route('dashboard.study.diaries' , [ auth()->user()->patient->study->id , $diary->id])}}" class="text-primary"> {{$diary->diary_name}} </a>   --}}
                                        {{$diary->diary_name}} 
                                        </td>
                                        <td> <a href="{{route('dashboard.study.diaries.status' , [ auth()->user()->patient->study->id , $diary->id , 0 ])}}" class="text-primary" title="View Current Diaries" >
                                        <span class="badge badge-warning"> {{$diary->diary_schedules->where('type' , 0)->where('patient_id' ,auth()->user()->patient->id  )->count() ?? 0}} </span></a>
                                        </td>
                                       
                                        <td> <a href="{{route('dashboard.study.diaries.status' , [ auth()->user()->patient->study->id , $diary->id , 1 ])}}" class="text-primary" title="View Overdue Diaries"> <span class="badge badge-danger">{{$diary->diary_schedules->where('type' , 1)->where('patient_id' ,auth()->user()->patient->id  )->count()}}</span></a></td> 

                                        <td> <a href="{{route('dashboard.study.diaries.status' , [ auth()->user()->patient->study->id , $diary->id , 2 ])}}" class="text-primary" title="View Completed Diaries"> <span class="badge badge-success">{{$diary->diary_schedules->where('type' , 2)->where('patient_id' ,auth()->user()->patient->id  )->count()}}</span> </a> </td> 
                                        
                                        </td>
                                        <td>{{ $diary->diary_schedules->where('patient_id' , auth()->user()->patient->id)->first()->created_at->addDays($diary->end_duration)->toDateString() }} </td>
                                        <td>{{$diary->diary_schedules->where('patient_id' , auth()->user()->patient->id)->first()->created_at->toDateString()}} </td>
                                        <td>  </td>
                                    </tr>
                                @endforeach
                             
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
               <div class="col-md-12">
                   
                <div class="card ">
                    <div class="card-header bg-white">
                      <h3 class="card-title"><i class="fa fa-list-ol"></i> Patient Tasks</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table class="table table-bordered table-hover">
                        <thead>                  
                          <tr>
                            <th width="5%"> # </th>
                            <th width="15%"> Task Name </th>
                            <th width="45%"> Task Description </th>
                            <th width="10%"> Completed </th>
                            <th width="10%"> Created </th>
                            <th width="10%"> Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach (auth()->user()->patient->tasks as $k => $task)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> <a href="{{route('dashboard.group.tasks.view' , $task->id  )}}" class="text-primary"> {{$task->name}} </a> </td>
                                    <td> {{ str_limit(strip_tags($task->description) , 80)}} </td>
                                    <td> {{ auth()->user()->patient->tasks->find($task->id)->pivot->completed_at ?? '-' }} </td>
                                    <td> {{($task->created_at)->toDateString()}} </td>
                                    <td> @if(auth()->user()->patient->tasks->find($task->id)->pivot->status == 1) <a href="javascript:;"  title="mark as complete" onclick="show_marked()">&#9989; 
                                    </a> @else <a href="{{route('dashboard.mark.task' , $task->id)}}" class="text-primary"  title="mark as complete"> &#9989; </a> @endif </td>
                                </tr>
                            @endforeach
                         
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
            
                </div>               

       
    <!--
            <div class="col-md-4">
                <a href="{{ route('dashboard.index') }}" class="btn btn-warning btn-lg btn-block">
                    Current Questions
                    <span class="pull-right btn btn-secondary btn-sm">@if(isset($diary_types ))   {{ $diary_types[0]->count }} @else {{'10'}} @endif </span>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ url('dashboard.diaryPage', 1) }}" class="btn btn-danger btn-lg btn-block">
                    Remaining Questions
                    <span class="pull-right btn btn-secondary btn-sm">@if(isset($diary_types ))  {{ $diary_types[1]->count }} @else {{'20'}} @endif</span>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ url('dashboard.diaryPage', 2) }}" class="btn btn-success btn-lg btn-block">
                    Answered Questions
                    <span class="pull-right btn btn-secondary btn-sm">@if(isset($diary_types ))  {{ $diary_types[2]->count }} @else {{'30'}} @endif</span>
                </a>
            </div>
            -->
            <div class="col-md-4 mt-5">
                        </div>
           


@endsection

@section('scripts')

        <script src="http://d3js.org/d3.v3.min.js"></script>
    <script>
    function dashboard(id, fData){
        var barColor = 'steelblue';
        function segColor(c){ return {Current:"#807dba", Overdue:"#e08214",Complete:"#41ab5d"}[c]; }
        
        // compute total for each state.
        fData.forEach(function(d){d.total=d.freq.Current+d.freq.Overdue+d.freq.Complete;});
        
        // function to handle histogram.
        function histoGram(fD){
            var hG={},    hGDim = {t: 60, r: 0, b: 30, l: 0};
            hGDim.w = 0 - hGDim.l - hGDim.r, 
            hGDim.h = 0 - hGDim.t - hGDim.b;
                
            //create svg for histogram.
            var hGsvg = d3.select(id).append("svg")
                .attr("width", hGDim.w + hGDim.l + hGDim.r)
                .attr("height", hGDim.h + hGDim.t + hGDim.b).append("g")
                .attr("transform", "translate(" + hGDim.l + "," + hGDim.t + ")");
    
            // create function for x-axis mapping.
            var x = d3.scale.ordinal().rangeRoundBands([0, hGDim.w], 0.1)
                    .domain(fD.map(function(d) { return d[0]; }));
    
            // Add x-axis to the histogram svg.
            hGsvg.append("g").attr("class", "x axis")
                .attr("transform", "translate(0," + hGDim.h + ")")
                .call(d3.svg.axis().scale(x).orient("bottom"));
    
            // Create function for y-axis map.
            var y = d3.scale.linear().range([hGDim.h, 0])
                    .domain([0, d3.max(fD, function(d) { return d[1]; })]);
    
            // Create bars for histogram to contain rectangles and freq labels.
            var bars = hGsvg.selectAll(".bar").data(fD).enter()
                    .append("g").attr("class", "bar");
            
            //create the rectangles.
            bars.append("rect")
                .attr("x", function(d) { return x(d[0]); })
                .attr("y", function(d) { return y(d[1]); })
                .attr("width", x.rangeBand())
                .attr("height", function(d) { return hGDim.h - y(d[1]); })
                .attr('fill',barColor)
                .on("mouseover",mouseover)// mouseover is defined below.
                .on("mouseout",mouseout);// mouseout is defined below.
                
            //Create the frequency labels above the rectangles.
            bars.append("text").text(function(d){ return d3.format(",")(d[1])})
                .attr("x", function(d) { return x(d[0])+x.rangeBand()/2; })
                .attr("y", function(d) { return y(d[1])-5; })
                .attr("text-anchor", "middle");
            
            function mouseover(d){  // utility function to be called on mouseover.
                // filter for selected state.
                var st = fData.filter(function(s){ return s.State == d[0];})[0],
                    nD = d3.keys(st.freq).map(function(s){ return {type:s, freq:st.freq[s]};});
                   
                // call update functions of pie-chart and legend.    
                pC.update(nD);
                leg.update(nD);
            }
            
            function mouseout(d){    // utility function to be called on mouseout.
                // reset the pie-chart and legend.    
                pC.update(tF);
                leg.update(tF);
            }
            
            // create function to update the bars. This will be used by pie-chart.
            hG.update = function(nD, color){
                // update the domain of the y-axis map to reflect change in frequencies.
                y.domain([0, d3.max(nD, function(d) { return d[1]; })]);
                
                // Attach the new data to the bars.
                var bars = hGsvg.selectAll(".bar").data(nD);
                
                // transition the height and color of rectangles.
                bars.select("rect").transition().duration(500)
                    .attr("y", function(d) {return y(d[1]); })
                    .attr("height", function(d) { return hGDim.h - y(d[1]); })
                    .attr("fill", color);
    
                // transition the frequency labels location and change value.
                bars.select("text").transition().duration(500)
                    .text(function(d){ return d3.format(",")(d[1])})
                    .attr("y", function(d) {return y(d[1])-5; });            
            }        
            return hG;
        }
        
        // function to handle pieChart.
        function pieChart(pD){
            var pC ={},    pieDim ={w:150, h: 150};
            pieDim.r = Math.min(pieDim.w, pieDim.h) / 2;
                    
            // create svg for pie chart.
            var piesvg = d3.select(id).append("svg")
                .attr("width", pieDim.w).attr("height", pieDim.h).append("g")
                .attr("transform", "translate("+pieDim.w/2+","+pieDim.h/2+")");
            
            // create function to draw the arcs of the pie slices.
            var arc = d3.svg.arc().outerRadius(pieDim.r - 10).innerRadius(0);
    
            // create a function to compute the pie slice angles.
            var pie = d3.layout.pie().sort(null).value(function(d) { return d.freq; });
    
            // Draw the pie slices.
            piesvg.selectAll("path").data(pie(pD)).enter().append("path").attr("d", arc)
                .each(function(d) { this._current = d; })
                .style("fill", function(d) { return segColor(d.data.type); })
                .on("mouseover",mouseover).on("mouseout",mouseout);
    
            // create function to update pie-chart. This will be used by histogram.
            pC.update = function(nD){
                piesvg.selectAll("path").data(pie(nD)).transition().duration(500)
                    .attrTween("d", arcTween);
            }        
            // Utility function to be called on mouseover a pie slice.
            function mouseover(d){
                // call the update function of histogram with new data.
                hG.update(fData.map(function(v){ 
                    return [v.State,v.freq[d.data.type]];}),segColor(d.data.type));
            }
            //Utility function to be called on mouseout a pie slice.
            function mouseout(d){
                // call the update function of histogram with all data.
                hG.update(fData.map(function(v){
                    return [v.State,v.total];}), barColor);
            }
            // Animating the pie-slice requiring a custom function which specifies
            // how the intermediate paths should be drawn.
            function arcTween(a) {
                var i = d3.interpolate(this._current, a);
                this._current = i(0);
                return function(t) { return arc(i(t));    };
            }    
            return pC;
        }
        
        // function to handle legend.
        function legend(lD){
            var leg = {};
                
            // create table for legend.
            var legend = d3.select(id).append("table").attr('class','legend');
            
            // create one row per segment.
            var tr = legend.append("tbody").selectAll("tr").data(lD).enter().append("tr");
                
            // create the first column for each segment.
            tr.append("td").append("svg").attr("width", '16').attr("height", '16').append("rect")
                .attr("width", '16').attr("height", '16')
                .attr("fill",function(d){ return segColor(d.type); });
                
            // create the second column for each segment.
            tr.append("td").text(function(d){ return d.type;});
    
            // create the third column for each segment.
            tr.append("td").attr("class",'legendFreq')
                .text(function(d){ return d3.format(",")(d.freq);});
    
            // create the fourth column for each segment.
            tr.append("td").attr("class",'legendPerc')
                .text(function(d){ return getLegend(d,lD);});
    
            // Utility function to be used to update the legend.
            leg.update = function(nD){
                // update the data attached to the row elements.
                var l = legend.select("tbody").selectAll("tr").data(nD);
    
                // update the frequencies.
                l.select(".legendFreq").text(function(d){ return d3.format(",")(d.freq);});
    
                // update the percentage column.
                l.select(".legendPerc").text(function(d){ return getLegend(d,nD);});        
            }
            
            function getLegend(d,aD){ // Utility function to compute percentage.
                return d3.format("%")(d.freq/d3.sum(aD.map(function(v){ return v.freq; })));
            }
    
            return leg;
        }
        
        // calculate total frequency by segment for all state.
        var tF = ['Current','Overdue','Complete'].map(function(d){ 
            return {type:d, freq: d3.sum(fData.map(function(t){ return t.freq[d];}))}; 
        });    
        
        // calculate total frequency by state for all segment.
        var sF = fData.map(function(d){return [d.State,d.total];});
    
        var hG = histoGram(sF), // create the histogram.
            pC = pieChart(tF), // create the pie-chart.
            leg= legend(tF);  // create the legend.
    }
    </script>
    
    <script>
        
    var n1 =1
    var n2 = 4
    var n3 =7
    
    var freqData=[
    {State:'Questions ',freq:{Current:n1, Overdue:n2, Complete:n3}}
    ];
    
    dashboard('#dashboard',freqData);
    </script>
    @endif


   
    <script>
        function show_marked(){
            swal("Sorry!", "You have marked that task completed", "error");
        }

        function open_visit_date_change(id){

            $('#visit_date_change').val(null);
            $('#current_visit_id').val(id);
            $('#visit_date_modal').modal('show');
        }
    </script>

@endsection