$(function () {
    /* ChartJS
     * -------
     * Data and config for chartjs
     */
    var data = {
        labels: $("#lineChart").data('studies'),
        datasets: [{
            label: '# of Enrolled Patients',
            data: $("#lineChart").data('counts'),
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    };
    var options = {
        scales: {
            yAxes: [{
                ticks: {
                    responsive: true,
                    min: 0,
                    stepSize: 1,
                    beginAtZero: true
                }
            }]
        }
    };

    if ($("#lineChart").length) {
        var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
        var lineChart = new Chart(lineChartCanvas, {
            type: 'line',
            data: data,
            options: options
        });
    }

    var doughnutData = {
        datasets: [{
            data: $('#doughnutChart').data('info'),
            backgroundColor: ['#d9534f', '#36a2eb', '#5cb85c']
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Have not Accessed Study',
            'Accessed Study',
            'Responded to Questionnaire'
        ]
    };
    var doughnutOptions = {
        responsive: true,
        animation: {
            animateScale: true,
            animateRotate: true
        }
    };

    if ($("#doughnutChart").length) {
        var doughnutChartCanvas = $("#doughnutChart").get(0).getContext("2d");
        var doughnutChart = new Chart(doughnutChartCanvas, {
            type: 'doughnut',
            data: doughnutData,
            options: doughnutOptions
        });
    }

    var areaData = {
        labels: ["CT20132", "MP23002", "CTM102", "Impala CT", "VM CardioM", "IP-90002"],
        datasets: [{
            label: '# of Clicks',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: 'origin',      // 0: fill to 'origin'
            fill: '+2',          // 1: fill to dataset 3
            fill: 1,             // 2: fill to dataset 1
            fill: false,         // 3: no fill
            fill: '-2'          // 4: fill to dataset 2
        }]
    };

    var areaOptions = {
        plugins: {
            filler: {
                propagate: true
            }
        }
    }

    var barChartData = {

        labels: $('#barChart').data('studies'),
        datasets: [{
            label: '# of Enrolled Patients',
            data: $('#barChart').data('counts'),
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            datalabels: {
                listeners: {
                    // called only for labels of the 1st dataset
                    click: function (context) {
                        alert(context);
                    }
                }
            }
        }]
    };

    var barChartOptions = {
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    stepSize: 1,
                    beginAtZero: true
                }
            }]
        },
        events: ['click'],
    };

    if ($("#barChart").length) {
        var cv = document.getElementById('barChart');
        var ctx = cv.getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: barChartOptions
        });
        cv.onclick = function (evt) {
            var activePoint = chart.getElementAtEvent(evt);
            console.log('activePoint', activePoint)
        };
    }


    if ($("#areaChart").length) {
        var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
        var areaChart = new Chart(areaChartCanvas, {
            type: 'line',
            data: areaData,
            options: areaOptions
        });
    }

});
