<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreScreeningAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_screening_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('screening_id');
            $table->unsignedInteger('question_id');
            $table->unsignedInteger('investigator_id');
            $table->string('answer_body')->nullable();
            $table->string('answer_other_sp')->nullable();
            $table->string('sub_nbr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_screening_answers');
    }
}
