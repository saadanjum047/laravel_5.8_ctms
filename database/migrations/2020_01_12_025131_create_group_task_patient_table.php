<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTaskPatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_task_investigator', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('investigator_id');
            $table->unsignedInteger('task_id');
            $table->boolean('status')->default(0);
            $table->dateTime('complete_at')->nullable();
            $table->unsignedInteger('repeated')->nullable();
            $table->boolean('overdue')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_task_investigator');
    }
}
