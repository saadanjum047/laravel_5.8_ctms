<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTipStudyPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tip_study_patient', function (Blueprint $table) {
            $table->dropColumn('site_id');
            $table->dropColumn('enrollment_date');

        });

        
        Schema::table('tip_study_patient', function (Blueprint $table) {
            $table->string('site_id')->after('study_id')->nullable();
            $table->dateTime('enrollment_date')->default(now());

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tip_study_patient', function (Blueprint $table) {
            $table->dropColumn('site_id');
            $table->dropColumn('enrollment_date');

        });

        Schema::table('tip_study_patient', function (Blueprint $table) {
            $table->string('site_id')->after('study_id')->nullable();
            $table->dateTime('enrollment_date');

        });

    }
}
