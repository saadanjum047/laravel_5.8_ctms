<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranlatedRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tranlated_revisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('revision_id');
            $table->string('header');
            $table->text('description')->nullable();
            $table->string('language')->nullable();
            $table->string('video_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tranlated_revisions');
    }
}
