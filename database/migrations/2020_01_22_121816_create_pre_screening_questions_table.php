<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreScreeningQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_screening_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('study_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('screening_id');
            $table->text('question');
            $table->unsignedInteger('answer_type')->default(1);
            $table->text('answers')->nullable();
            $table->string('notification_response')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_screening_questions');
    }
}
