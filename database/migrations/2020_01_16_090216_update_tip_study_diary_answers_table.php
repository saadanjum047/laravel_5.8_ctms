<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTipStudyDiaryAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tip_study_diary_answers', function (Blueprint $table) {
            $table->unsignedInteger('frequency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tip_study_diary_answers', function (Blueprint $table) {
            $table->unsignedInteger('frequency');
            
        });
    }
}
