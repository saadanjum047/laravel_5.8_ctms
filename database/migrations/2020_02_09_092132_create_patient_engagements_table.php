<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientEngagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_engagements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('study_id');
            $table->unsignedInteger('created_by');
            $table->string('question');
            $table->string('answers')->nullable();
            $table->string('attachment')->nullable();
            $table->string('correct_answer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_engagements');
    }
}
