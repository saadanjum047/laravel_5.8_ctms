<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('investigator_id');
            $table->unsignedInteger('study_id');
            $table->unsignedInteger('patient_id');
            $table->string('title');
            $table->text('body')->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_messages');
    }
}
