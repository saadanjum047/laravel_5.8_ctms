<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientConsentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_consents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('study_id');
            $table->unsignedInteger('patient_id');
            $table->string('signature');
            $table->boolean('status')->default(0);
            $table->dateTime('acceptence_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_consents');
    }
}
