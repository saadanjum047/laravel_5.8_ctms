<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStudyVisitScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::table('study_visit_schedule', function (Blueprint $table) {
                $table->dropColumn('target');
            });
            Schema::table('study_visit_schedule', function (Blueprint $table) {
                $table->string('target')->after('study_id');
            });      
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('study_visit_schedule', function (Blueprint $table) {
            $table->dropColumn('target');
        });
        Schema::table('study_visit_schedule', function (Blueprint $table) {
            $table->string('target')->after('study_id');
        });      
    }
}
